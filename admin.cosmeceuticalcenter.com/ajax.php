<?php

require('core/core.php');

if (isset($_GET['m'])) {
    require('core/handler/access-ajax-handler.php');

    if ($_GET['m'] == "login") {  // Inicio de sesión
        require('core/bin/ajax/ajx-session.php');
    } elseif ($_GET['m'] == "remove") {  // Eliminar
        require('core/bin/ajax/ajx-remove.php');
    } elseif ($_GET['m'] == "user") {  // Usuarios
        require('core/bin/ajax/ajx-user.php');
    } elseif ($_GET['m'] == "products-category") {  // Categoría de productos
        require('core/bin/ajax/ajx-products-category.php');
    } elseif ($_GET['m'] == "products") {  // productos
        require('core/bin/ajax/ajx-products.php');
    } elseif ($_GET['m'] == "inventory") {  // Inventario
        require('core/bin/ajax/ajx-inventory.php');
    } elseif ($_GET['m'] == "carousel") {  // Carrusel
        require('core/bin/ajax/ajx-carousel.php');
    } elseif ($_GET['m'] == "brands") {  // Marcas
        require('core/bin/ajax/ajx-brands.php');
    } elseif ($_GET['m'] == "orders") {  // Ordenes
        require('core/bin/ajax/ajx-orders.php');
    } elseif ($_GET['m'] == "zones") {  // Zonas
        require('core/bin/ajax/ajx-zones.php');
    } elseif ($_GET['m'] == "seo") {  // Seo
        require('core/bin/ajax/ajx-seo.php');
    } elseif ($_GET['m'] == "sub-category") {  // Sub categorías
        require('core/bin/ajax/ajx-products-sub-category.php');
    } elseif ($_GET['m'] == "newsletter") {  // Newsletter
        require('core/bin/ajax/ajx-newsletter.php');
    } elseif ($_GET['m'] == "features") {  // Comentarios de clientes
        require('core/bin/ajax/ajx-features.php');
    } elseif ($_GET['m'] == "coupon") {  // Cupones
        require('core/bin/ajax/ajx-coupon.php');
    } else {
        header('location: index.php');
    }
} else {
    include('location: index.php');
}

?>
