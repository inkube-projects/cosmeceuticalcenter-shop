$(function(e){
    var form_name = $("#key").data('form');
    var act = $("#key").data('act');

    //Date range picker
    $('#range').daterangepicker({
        "locale": {
            "format": "DD-MM-YYYY",
            "separator": " al ",
            "applyLabel": "Aplicar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Desde",
            "toLabel": "hasta",
            "customRangeLabel": "Custom",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Deciembre"
            ],
            "firstDay": 1
        }
    });

    $("#" + form_name).validate({
        rules:{
            name: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            canonical_id:{
                required: true,
            },
            discount: {
                required: true,
            },
            description: {
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            }
        },
        messages:{
            name: {
                required: "Debes ingresar el nombre del cupón",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            canonical_id:{
                required: "Debes ingresar el ID del cupón",
            },
            discount: {
                required: "Debes agregar el desceunto en %",
            },
            description: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist(act, form_name);
            return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} act     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist(act, form_name) {
    var i = $('#' + form_name).serialize();
    var ext_url = 0;

    if (act == 2) {
        ext_url = $('#key').data('id');
    }

    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/adm/coupon/' + act + '/' + ext_url,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", base_url + "admin/coupon/edit/" + r.id + "/OK" + act);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
