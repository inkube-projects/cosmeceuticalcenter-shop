$(function(e){
    // Configuración de la paginación con DataTable
    $('.table').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "No se han encontrado resultados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No se han encontrado resultados",
            "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
            "search": "Buscar: ",
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"
            }
        }
    });

    // Limpiar el formulario de búsqueda
    $('.btn-reset').on('click', function(e){
        $('#frm-search select').val('').trigger('change');
        $('#frm-search input[type=text]').val('');
    });
});

function setOrder(imgID)
{
    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/adm/carousel/3/' + imgID,
        data: {
            order: $('#order_' + imgID).val()
        },
        dataType: 'json',
        success: function(r){

        }
    });

}
