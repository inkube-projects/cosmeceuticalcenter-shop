$(function(e){
    // Select2
    $(".select2").select2();

    // Mascara para fechas
    $("[data-mask]").inputmask();

    // Configuración de la paginación con DataTable
    $('.table').DataTable({
        "order": [[ 0, "desc" ]],
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "No se han encontrado resultados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No se han encontrado resultados",
            "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
            "search": "Buscar: ",
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"
            }
        }
    });

    // Limpiar el formulario de búsqueda
    $('.btn-reset').on('click', function(e){
        $('#frm-search select').val('').trigger('change');
        $('#frm-search input[type=text]').val('');
    });
});
