$(function(e){
    var form_name = $("#key").data('form');
    var act = $("#key").data('act');

    $('.select2').select2();

    $("#" + form_name).validate({
        rules:{
            keywords:{
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            description:{
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            section:{
                required: true,
            },

        },
        messages:{
            keywords:{
                required: 'Debes agregar las palabras clave',
                noSpecialCharacters: 'No se permiten caracteres especiales',
            },
            description:{
                required: 'Debes agregar una descripción',
                noSpecialCharacters: 'No se permiten caracteres especiales',
            },
            section:{
                required: 'Debes seleccionar una sección',
            },

        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist(act, form_name);
            return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} act     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist(act, form_name) {
    var i = $("#" + form_name).serialize();
    var ext_url = 0;

    if (act == 2) {
        ext_url = $('#key').data('id');
    }

    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/adm/seo/' + act + '/' + ext_url,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", base_url + "admin/seo/edit/" + r.id + "/OK" + act);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
