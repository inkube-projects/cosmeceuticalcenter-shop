$(function(e){
    var form_name = $("#key").data('form');
    var act = $("#key").data('act');

    // Select 2
    $(".select2").select2();

    // Mascara para fechas
    $("[data-mask]").inputmask();

    // Data Table
    $('.table').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "No se han encontrado resultados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No se han encontrado resultados",
            "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
            "search": "Buscar: ",
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"
            }
        }
    });

    /**
     * Se verifica que la imagen de portada sea una imagen válida
     * @param  {event} e Evento
     * @return {string}
     */
    $("#cover_image").on("change", function(e){
        var img = $(this).val();

        if (!(/\.(jpg|jpeg|png|gif)$/i).test(img)) {
            $(".btn-submit-cover-image").addClass('btn-warning').html("Debes ingresar una imagen válida (jpg, jpeg, png, gif)").attr('disabled', true);
        } else {
            $(".btn-submit-cover-image").removeClass('btn-warning').addClass('btn-success').html("Guardar imagen de portada").attr('disabled', false);
        }
    });

    $(".btn-submit-cover-image").on("click", function(e){
        $("#mod-img-cover").modal('show');
        $("#ajx-img-cover").html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        $("#ajx-img").html("");
        saveImgAjax(form_name, base_admin + 'ajax.php?m=user&act=3', 'ajx-img-cover');
        $("#cover_image").val('');
        return false;
    });

    if (form_name != "frm-admin") {
        $("#" + form_name).validate({
            rules: {
                name: {
                    required: true,
                    noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
                },
                last_name: {
                    required: true,
                    noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
                },
                country: {
                    required: true,
                },
                city: {
                    required: true,
                    noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
                },
                province: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true
                },
                phone_1: {
                    digits: true,
                    maxlength: 11
                },
                phone_2: {
                    digits: true,
                    maxlength: 11
                },
                username: {
                    required: true,
                    noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
                },
                pass_1: {
                    required: (act == 1) ? true : false,
                },
                pass_2: {
                    required: (act == 1) ? true : false,
                    equalTo: "#pass_1"
                },
                status: {
                    required: true,
                },
                role: {
                    required: true,
                },
                company: {
                    noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
                },
                location_1: {
                    required: true,
                    noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
                },
                location_2: {
                    noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
                },
                postal_code: {
                    required: true,
                    noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
                },
                note: {
                    noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
                },
            },
            messages: {
                name: {
                    required: "Debes ingresar al menos un nombre",
                    noSpecialCharacters: "No se permiten caracteres especiales"
                },
                last_name: {
                    required: "Debes ingresar tu apellido",
                    noSpecialCharacters: "No se permiten caracteres especiales"
                },
                country: {
                    required: "Debes seleccionar un país",
                },
                city: {
                    required: "Debes ingresar el nombre de la ciudad",
                    noSpecialCharacters: "No se permiten caracteres especiales"
                },
                province: {
                    required: "Debes seleccionar una provincia",
                },
                email: {
                    required: "Debes ingresar un e-mail",
                    email: "Debes ingresar un email válido"
                },
                phone_1: {
                    digits: "Debes ingresar un número telefónico válido",
                    maxlength: "Debes ingresar un número telefónico válido"
                },
                phone_2: {
                    digits: "Debes ingresar un número telefónico válido",
                    maxlength: "Debes ingresar un número telefónico válido"
                },
                username: {
                    required: "Debes ingresar un nombre de usuario",
                    noSpecialCharacters: "No se permiten caracteres especiales"
                },
                pass_1: {
                    required: "Debes ingresar una contraseña",
                },
                pass_2: {
                    required: "Debes repetir la contraseña",
                    equalTo: "Las contraseñas deben ser iguales"
                },
                status: {
                    required: "Debes seleccionar un estado de la cuenta",
                },
                role: {
                    required: "Debes seleccionar un Rol de usuario",
                },
                company: {
                    noSpecialCharacters: "No se permiten caracteres especiales"
                },
                location_1: {
                    required: "Debes ingresra la dirección",
                    noSpecialCharacters: "No se permiten caracteres especiales"
                },
                location_2: {
                    noSpecialCharacters: "No se permiten caracteres especiales"
                },
                postal_code: {
                    required: "Debes ingresar un código postal",
                    noSpecialCharacters: "No se permiten caracteres especiales"
                },
                note: {
                    noSpecialCharacters: "No se permiten caracteres especiales"
                },
            },
            errorPlacement: function (error, element) {
                var name = $(element).attr("name");
                error.appendTo($("#" + name + "_validate"));
            },
            submitHandler: function(form) {
                persist(act, form_name);
                return false;
            }
        });
    }

    $("#frm-admin").validate({
        rules: {
            name: {
                required: true,
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            last_name: {
                required: true,
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            country: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            username: {
                required: true,
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            phone_1: {
                digits: true,
                maxlength: 11
            },
            phone_2: {
                digits: true,
                maxlength: 11
            },
            pass_1: {
                required: (act == 1) ? true : false,
            },
            pass_2: {
                required: (act == 1) ? true : false,
                equalTo: "#pass_1"
            },
            status: {
                required: true,
            },
            role: {
                required: true,
            }
        },
        messages: {
            name: {
                required: "Debes ingresar al menos un nombre",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            last_name: {
                required: "Debes ingresar tu apellido",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            country: {
                required: "Debes seleccionar un país",
            },
            email: {
                required: "Debes ingresar un e-mail",
                email: "Debes ingresar un email válido"
            },
            username: {
                required: "Debes ingresar un nombre de usuario",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            phone_1: {
                digits: "Debes ingresar un número telefónico válido",
                maxlength: "Debes ingresar un número telefónico válido"
            },
            phone_2: {
                digits: "Debes ingresar un número telefónico válido",
                maxlength: "Debes ingresar un número telefónico válido"
            },
            pass_1: {
                required: "Debes ingresar una contraseña",
            },
            pass_2: {
                required: "Debes repetir la contraseña",
                equalTo: "Las contraseñas deben ser iguales"
            },
            status: {
                required: "Debes seleccionar un estado de la cuenta",
            },
            role: {
                required: "Debes seleccionar un Rol de usuario",
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function(form) {
            var opt = (act == 1) ? 5 : 6;
            persistAdmin(opt, "frm-admin");
            return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} act     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist(act, form_name) {
    var i = $("#" + form_name).serialize();
    var ext_url = "";

    if (act == 2) {
        ext_url = "&u=" + $('#key').data('id');
    }

    $.ajax({
        type: 'POST',
        url: base_admin + 'ajax.php?m=user&act=' + act + ext_url,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", "admin/user/edit/" + r.id + "/OK" + act);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}

/**
 * Persiste la información al ajax
 * @param  {ineteger} act     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persistAdmin(act, form_name) {
    var i = $("#" + form_name).serialize();
    var ext_url = "";

    if (act == 6) {
        ext_url = "&u=" + $('#key').data('id');
    }

    $.ajax({
        type: 'POST',
        url: base_admin + 'ajax.php?m=user&act=' + act + ext_url,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", "admin/administrator/edit/" + r.id + "/OK" + act);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}

function genPDF(){
    html2canvas(document.querySelector(".wrapper")).then(canvas => {
        document.body.appendChild(canvas);
        var img = canvas.toDataURL("image/png");
        var doc = new jsPDF();
        doc.addImage(img,'JPEG',20,20);
        doc.save('test.pdf');
    });
}
