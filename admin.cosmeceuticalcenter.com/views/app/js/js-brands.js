$(function(e){
    var form_name = $("#key").data('form');
    var act = $("#key").data('act');

    // Cambia el estilo del botón de la imagen
    $("#image").on('change', function(){
        var image = $(this).val();
        if (image != "") {
            $('#img-image').addClass('btn-success');
            $('#img-image').removeClass('btn-default');
        }
    });

    $("#" + form_name).validate({
        rules:{
            name:{
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            description:{
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            image:{
                extension: "jpg|jpeg|png|gif"
            },

        },
        messages:{
            name:{
                required: 'Debes agregar el nombre de la Marca',
                noSpecialCharacters: 'No se permiten caracteres especiales',
            },
            description:{
                noSpecialCharacters: 'No se permiten caracteres especiales',
            },
            image:{
                extension: "Debes ingresar una imagen válida (jpg, jpeg, png, gif)"
            },

        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist(act, form_name);
            return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} act     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist(act, form_name) {
    var i = new FormData(document.getElementById(form_name));
    var ext_url = 0;

    if (act == 2) {
        ext_url = $('#key').data('id');
    }

    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/adm/brands/' + act + '/' + ext_url,
        data: i,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", base_url + "admin/brands/edit/" + r.id + "/OK" + act);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
