$(function(e){
    var form_name = $("#key").data('form');
    var act = $("#key").data('act');
    $('.select2').select2();
    // Configuración de la paginación con DataTable
    $('.table').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "No se han encontrado resultados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No se han encontrado resultados",
            "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
            "search": "Buscar: ",
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"
            }
        }
    });

    $('#country').on('change', function(){
        var country = $(this).val();

        if (country == "73") {
            $("#province").attr('disabled', false);
        } else {
            $("#province").attr('disabled', true);
            $(".js-province-container").html('');

            $.ajax({
                type: 'POST',
                url: base_url + 'ajx/adm/orders/5/0',
                data: {
                    province: $('#province').val(),
                    country: $(this).val()
                },
                dataType: 'json',
                success: function(r){
                    if (r.status == 'OK') {
                        $('#ajx-sub-total').html(r.calculate.sub_total);
                        $('#ajx-shipping').html(r.calculate.shipping);
                        $('#ajx-total').html(r.calculate.total);
                    } else {
                        $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                        $(".btn-submit").attr("disabled", false).html('Guardar');
                        $(document).scrollTop(0);
                    }
                }
            });
        }
    });

    $('#province').on('change', function(){
        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/adm/orders/5/0',
            data: {
                province: $(this).val(),
                country: $('#country').val()
            },
            dataType: 'json',
            success: function(r){
                if (r.status == 'OK') {
                    $('#ajx-sub-total').html(r.calculate.sub_total);
                    $('#ajx-shipping').html(r.calculate.shipping);
                    $('#ajx-total').html(r.calculate.total);
                } else {
                    $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                    $(".btn-submit").attr("disabled", false).html('Guardar');
                    $(document).scrollTop(0);
                }
            }
        });
    });

    $("#" + form_name).validate({
        rules:{
            status:{
                required: (act == 2) ? true : false,
            },
            user:{
                required: (act == 1) ? true : false,
            },
            company: {
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            tracking_number: {
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
        },
        messages:{
            status:{
                required: 'Debes seleccionar un estado',
            },
            user:{
                required: "Debes seleccionar un usuario",
            },
            company: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            tracking_number: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist(act, form_name);
            return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} act     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist(act, form_name) {
    var i = $("#" + form_name).serialize();
    var ext_url = 0;

    if (act == 2) {
        ext_url = $('#key').data('id');
    }

    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/adm/orders/' + act + '/' + ext_url,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", base_url + "admin/orders/edit/" + r.id + "/OK" + act);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}

function addProduct(product_id)
{
    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/adm/orders/3/0',
        data: {
            product: product_id,
            province: $("#province").val(),
            country: $("#country").val()
        },
        dataType: 'json',
        success: function(r){
            if (r.status == 'OK') {
                $('#ajx-order').html(r.data);
                $('#ajx-sub-total').html(r.calculate.sub_total);
                $('#ajx-shipping').html(r.calculate.shipping);
                $('#ajx-total').html(r.calculate.total);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}

function removeProduct(product_id)
{
    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/adm/orders/4/0',
        data: {
            product: product_id,
            province: $("#province").val(),
            country: $("#country").val()
        },
        dataType: 'json',
        success: function(r){
            if (r.status == 'OK') {
                $('#ajx-order').html(r.data);
                $('#ajx-sub-total').html(r.calculate.sub_total);
                $('#ajx-iva').html(r.calculate.iva);
                $('#ajx-total').html(r.calculate.total);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
