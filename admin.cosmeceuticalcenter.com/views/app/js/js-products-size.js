$(function(){
    var form_name = $("#key").data('form');
    var act = $("#key").data('act');

    $("#" + form_name).validate({
        rules:{
            name:{
                required: true,
                noSpecialCharacters: /([*?^${}><\[\]\\])/g
            }
        },
        messages:{
            name:{
                required: "Debes agregar el nombre del color",
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {


            persist(act, form_name);
            return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} act     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist(act, form_name) {
    var i = $("#" + form_name).serialize();
    var ext_url = 0;
    var action = 15;

    if (act == 2) {
        ext_url = $('#key').data('id');
        action = 16;
    }

    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/adm/products/' + action + '/' + ext_url,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", base_url + "admin/products/size/edit/" + r.id + "/OK" + act);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
