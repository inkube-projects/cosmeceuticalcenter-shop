/* Create an array with the values of all the input boxes in a column, parsed as numbers */
$.fn.dataTable.ext.order['dom-text-numeric'] = function(settings, col)
{
    return this.api().column(col, {order:'index'}).nodes().map(function ( td, i) {
        return $('input', td).val() * 1;
    } );
}

$(function(e){
    $(".select2").select2();

    // Limpiar el formulario de búsqueda
    $('.btn-reset').on('click', function(e){
        $('#frm-search select').val('').trigger('change');
        $('#frm-search input[type=text]').val('');
    });

    // Se actualiza el inventario
    $('.inventory').on('focusout', function(e){
        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/adm/inventory/4/' + $(this).data('product'),
            data: {
                quantity: $(this).val()
            },
            dataType: 'json',
            beforeSend: function(){

            },
            success: function(r){

            }
        });
    });
});

function setInventory(productID) {
    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/adm/inventory/4/' + productID,
        data: {
            quantity: $(".inventory-" + productID).val()
        },
        dataType: 'json',
        beforeSend: function(){

        },
        success: function(r){

        }
    });
}

function showModalExtra(productID) {
    $.ajax({
        type: 'GET',
        url: base_url + 'ajx/adm/inventory/5/' + productID,
        dataType: 'json',
        beforeSend: function(){
            $('#mod-stock-extra').modal();
            $('#ext-stock-body').html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            $('#ext-stock-body').html(r.data);

            $('.only-number').on('input', function () {
                this.value = this.value.replace(/[^0-9]/g,'');
            });
        }
    });
}

function persistExtraStock() {
    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/adm/inventory/6/0',
        data: $("#frm-stock-extra").serialize(),
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $('.js-alert-stock').html('<div class="alert alt-alert alert-success" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            } else {
                $('.js-alert-stock').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            }

            $(".btn-submit").attr("disabled", false).html('Guardar stock');
        }
    });
}