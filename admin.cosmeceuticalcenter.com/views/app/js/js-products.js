$(function(e){
    var form_name = $("#key").data('form');
    var act = $("#key").data('act');

    CKEDITOR.replace('description', {
       customConfig : '../../js/js-ckeditor-config.js'
    });

    CKEDITOR.replace('presentation', {
       customConfig : '../../js/js-ckeditor-config.js'
    });

    CKEDITOR.replace('active_principles', {
       customConfig : '../../js/js-ckeditor-config.js'
    });

    CKEDITOR.replace('prescription', {
       customConfig : '../../js/js-ckeditor-config.js'
    });

    CKEDITOR.replace('use_recommendations', {
       customConfig : '../../js/js-ckeditor-config.js'
    });

    CKEDITOR.replace('results_benefits', {
       customConfig : '../../js/js-ckeditor-config.js'
    });

    // Agregar color
    $('.js-add-color').on('click', function(e){
        e.preventDefault();
        var cnt_color = $("#cnt_color").val();
        var element_value = parseInt(cnt_color) + 1;

        var bodyColor = '<div class="row"><div class="col-md-6"><select class="form-control select2" name="color_' + element_value + '" id="color_' + element_value + '"><option value="">Seleccione un color</option></select></div></div><div class="col-md-11 css-noFloat center-block text-center well css-marginT20"><div class="btn btn-default btn-file css-width100 btn-file-gallery" id="btn-color-' + element_value + '"><i class="fa fa-camera"></i> <span>Agregar imágenes</span><input type="file" name="gallery_color_' + element_value + '[]" id="gallery_color_' + element_value + '" class="gallery_color_' + element_value + ' css-gallery-image" onChange="changeClass(\'btn-color-' + element_value + '\')" multiple></div><p>Las imagenes deben tener mínimo 150 x 150px</p></div><div class="row css-marginT40" id="ajx-gallery-color-' + element_value + '"></div><hr>';
        $("#ajx-colors").append(bodyColor);
        // var optionList = document.getElementById('rec_mode').options;
        colorOption.forEach(function(ele) {
            $('#color_' + element_value).append(new Option(ele.color, ele.id));
        });

        $("#cnt_color").val(element_value);
        $(".select2").select2();
    });

    // select2
    $(".select2").select2();

    // Se obtiene el iva de la marca
    $('#brand').on('change', function(e){
        var brand = $(this).val();
        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/adm/products/12/0',
            dataType: 'json',
            data: {
                brand: brand
            },
            success: function(r) {
                if (r.status == "OK") {
                    $("#iva").val(r.iva);
                } else {
                    $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                }
            }
        });
    });

    // Date Rangue
    $('.date_range').daterangepicker({
        "locale": {
            "format": "DD-MM-YYYY",
            "separator": " al ",
            "applyLabel": "Aplicar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Desde",
            "toLabel": "hasta",
            "customRangeLabel": "Custom",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Deciembre"
            ],
            "firstDay": 1
        }
    });

    // Se verifica si está habilitada la opcion de novedad
    if(!$("#novelty").prop('checked')) {
        $('#novelty_date_range').attr('disabled', true).val('');
    } else {
        $('#novelty_date_range').attr('disabled', false);
    }
    $('#novelty').on('change', function(){
        if(!$(this).prop('checked')) {
            $('#novelty_date_range').attr('disabled', true).val('');
        } else {
            $('#novelty_date_range').attr('disabled', false).val('');
        }
    });

    // Se verifica si esta habilitada la opción de promocion/descuento
    if(!$("#discount_promo").prop('checked')) {
        $('#discount_price').attr('disabled', true);
        $('#discount_date_range').attr('disabled', true);
    } else {
        $('#discount_price').attr('disabled', false);
        $('#discount_date_range').attr('disabled', false);
    }
    $('#discount_promo').on('change', function(){
        if(!$(this).prop('checked')) {
            $('#discount_price').attr('disabled', true).val('');
            $('#discount_date_range').attr('disabled', true).val('');
        } else {
            $('#discount_price').attr('disabled', false);
            $('#discount_date_range').attr('disabled', false);
        }
    });

    // Se verifica si esta habilitada la opción de recomendación semanal
    if(!$("#weekly_recommendation").prop('checked')) {
        $('#weekly_price').attr('disabled', true).val('');
        $('#weekly_date_range').attr('disabled', true).val('');
    } else {
        $('#weekly_price').attr('disabled', false);
        $('#weekly_date_range').attr('disabled', false);
    }
    $('#weekly_recommendation').on('change', function(){
        if(!$(this).prop('checked')) {
            $('#weekly_price').attr('disabled', true).val('');
            $('#weekly_date_range').attr('disabled', true).val('');
        } else {
            $('#weekly_price').attr('disabled', false);
            $('#weekly_date_range').attr('disabled', false);
        }
    });

    // Se selecciona la subcategoría
    $('#category').on('change', function(){
        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/adm/products/11/0',
            data: {
                category: $(this).val(),
                product_id: $("#key").data('id')
            },
            dataType: 'json',
            success: function(r) {
                if (r.status == 'OK') {
                    $('#ajx-subcategory').html(r.data);
                    $(".select2").select2();
                } else {
                    $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                    $(".btn-submit").attr("disabled", false).html('Guardar');
                    $(document).scrollTop(0);
                }
            }
        });
    });

    /**
     * Se verifica que la imagen de portada sea una imagen válida
     * @param  {event} e Evento
     * @return {string}
     */
    $("#cover_image").on("change", function(e){
        var img = $(this).val();

        if (!(/\.(jpg|jpeg|png|gif)$/i).test(img)) {
            $(".btn-submit-cover-image").addClass('btn-warning').html("Debes ingresar una imagen válida (jpg, jpeg, png, gif)").attr('disabled', true);
        } else {
            $(".btn-submit-cover-image").removeClass('btn-warning').addClass('btn-success').html("Guardar imagen de portada").attr('disabled', false);
        }
    });

    $(".btn-submit-cover-image").on("click", function(e){
        $("#mod-img-cover").modal('show');
        $("#ajx-img-cover").html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        $("#ajx-img").html("");
        saveImgAjax(form_name, base_url + 'ajx/adm/products/3/0', 'ajx-img-cover');
        $("#cover_image").val('');
        return false;
    });

    // Subir imagenes de galería
    $('#gallery_images').on('change', function(e){
        if ($(this).val() != "") {
            $('.btn-file-gallery').addClass('btn-success').removeClass('btn-default');
            $('.btn-file-gallery span').html('Subiendo imágenes, por favor espere... <i class="fa fa-spinner fa-spin"></i>');
            $('.btn-submit').attr('disabled', true);
            uploadImages(form_name);
        }
    });

    // Mostrar imagenes temporales en modal
    $('#ajx-gallery').on('click', '.btn-show', function(e){
        var image = $(this).data('image');
        $('#mod-image').modal('show');
        $('#js-image').html('<img src="' + image + '" class="img-responsive css-noFloat center-block">');
        e.preventDefault();
    });

    // Mostrar imagenes de galería
    $('#js-gallery').on('click', '.btn-show', function(e){
        var image = $(this).data('image');
        $('#mod-image').modal('show');
        $('#js-image').html('<img src="' + image + '" class="img-responsive css-noFloat center-block">');
        e.preventDefault();
    });

    $("#" + form_name).validate({
        rules:{
            name:{
                required: true,
                noSpecialCharacters: /([*?^${}><\[\]\\])/g
            },
            description:{
                required: true,
            },
            price:{
                required: true,
            },
            content:{
                noSpecialCharacters: /([*?^${}><\[\]\\])/g
            },
            category:{
                required: true,
            },
            brand:{
                required: true,
            },
            seo_title: {
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            seo_keywords: {
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            seo_description: {
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            tags: {
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            }
        },
        messages:{
            name:{
                required: "Debes ingresar el Nombre del producto",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            description:{
                required: "Debes agregar la descripción",
            },
            price:{
                required: "Debes agregar un precio",
            },
            content:{
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            category:{
                required: "Debes seleccionar una categoría",
            },
            brand:{
                required: "Debes seleccionar una marca",
            },
            seo_title: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            seo_keywords: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            seo_description: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            tags: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            $('#description').val(CKEDITOR.instances.description.getData());
            $('#active_principles').val(CKEDITOR.instances.active_principles.getData());
            $('#prescription').val(CKEDITOR.instances.prescription.getData());
            $('#use_recommendations').val(CKEDITOR.instances.use_recommendations.getData());
            $('#results_benefits').val(CKEDITOR.instances.results_benefits.getData());
            $('#presentation').val(CKEDITOR.instances.presentation.getData());

            persist(act, form_name);
            return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} act     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist(act, form_name) {
    // var i = $("#" + form_name).serialize();
    var i = new FormData(document.getElementById(form_name));
    var ext_url = 0;

    if (act == 2) {
        ext_url = $('#key').data('id');
    }

    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/adm/products/' + act + '/' + ext_url,
        data: i,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", base_url + "admin/products/edit/" + r.id + "/OK" + act);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}

/**
 * Se encarga de subir las imágenes de galería
 * @param  {string} form_name Nombre del formulario
 * @return {boolean}
 */
function uploadImages(form_name) {
    var formData = new FormData(document.getElementById(form_name));

    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/adm/products/5/0',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(r){
            if (r.status == "OK") {
                $('#ajx-gallery').html(r.data);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(document).scrollTop(0);
            }

            $('#gallery_images').val('');
            $('.btn-file-gallery').removeClass('btn-success').addClass('btn-default');
            $('.btn-file-gallery span').html('Agregar imágenes');
            $('.btn-submit').attr('disabled', false);
        }
    });

    return false;
}

/**
 * Elimina imágenes temporales de galería
 * @param  {integer} acc   Acción a realizar
 * @param  {string} image Nombre de la imagen
 * @return {void}
 */
function removeImage(image) {
    if (confirm("¿Eliminar imagen?")) {
        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/adm/products/6/0',
            dataType: 'json',
            data: {
                img: image
            },
            success: function(r) {
                if (r.status == "OK") {
                    $('#ajx-gallery').html(r.data);
                    $('.btn-file-gallery').removeClass('btn-success').addClass('btn-default');
                    $('.btn-file-gallery span').html('Agregar imágenes');
                    $('.btn-submit').attr('disabled', false);
                } else {
                    $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                    $(document).scrollTop(0);
                }
            }
        });
    }
}

function removeGalleryImage(id, e){
    if (confirm('¿Seguro de que deseas eliminar la imagen de galería?')) {
        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/adm/products/7/' + $("#key").data('id'),
            dataType: 'json',
            data: {
                image_id: id
            },
            success: function(r) {
                if (r.status == "OK") {
                    $('#js-gallery').html(r.data);
                } else {
                    $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                }
            }
        });
    }

    e.preventDefault();
}

function changeClass(IDelement) {
    $('#' + IDelement).addClass('btn-success').removeClass('btn-default');
}

function showImage(image) {
    $('#mod-image').modal('show');
    $('#js-image').html('<img src="' + image + '" class="img-responsive css-noFloat center-block">');
}

function removeColorImage(id, e){
    if (confirm('¿Seguro de que deseas eliminar la imagen?')) {
        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/adm/products/17/' + $("#key").data('id'),
            dataType: 'json',
            data: {
                image_id: id
            },
            success: function(r) {
                if (r.status == "OK") {
                    location.reload();
                } else {
                    $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                }
            }
        });
    }

    e.preventDefault();
}

function removeColor(id, e){
    if (confirm('¿Seguro de que deseas eliminar la imagen?')) {
        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/adm/products/18/' + $("#key").data('id'),
            dataType: 'json',
            data: {
                color_id: id
            },
            success: function(r) {
                if (r.status == "OK") {
                    location.reload();
                } else {
                    $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                }
            }
        });
    }

    e.preventDefault();
}