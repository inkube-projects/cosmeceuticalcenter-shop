$(function(e){
    var form_name = $("#key").data('form');
    var act = $("#key").data('act');

    $(".select2").select2();
    $('#country').on('change', function(){
        var country = $(this).val(),
            flag = false;

        for (var i in country) {
            if (country[i] == "73") {
                flag = true;
            }
        }

        // Si el pais es españa se habilitan las provincias
        if (flag) {
            $('#province').attr('disabled', false);
        } else {
            $('#province').attr('disabled', true);
            $('#province').val('').trigger('change');
            $(".js-province-container").html('');
        }
    })

    $("#province").on('change', function(){
        var province_id = $(this).val();

        $.ajax({
            type: 'GET',
            url: base_url + 'ajx/adm/zones/3/' + province_id,
            dataType: 'json',
            beforeSend: function(){
                $("#province").attr("disabled", true);
            },
            success: function(r){
                if (r.status == "OK") {
                    $(".js-province-container").append(r.data);

                } else {
                    $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                    $(document).scrollTop(0);
                }

                $("#province").attr("disabled", false);
            }
        });
    });

    $("#" + form_name).validate({
        rules:{
            name:{
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            value:{
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },

        },
        messages:{
            name:{
                required: 'Debes agregar un nombre',
                noSpecialCharacters: 'No se permiten caracteres especiales',
            },
            value:{
                required: 'Debes agregar un valor de envío',
                noSpecialCharacters: 'No se permiten caracteres especiales',
            },

        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist(act, form_name);
            return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} act     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist(act, form_name) {
    var i = $("#" + form_name).serialize();
    var ext_url = 0;

    if (act == 2) {
        ext_url = $('#key').data('id');
    }

    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/adm/zones/' + act + '/' + ext_url,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", base_url + "admin/zones/edit/" + r.id + "/OK" + act);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
