$(function(e){
    var form_name = $("#key").data('form');
    var act = $("#key").data('act');

    $('.table').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "No se han encontrado resultados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No se han encontrado resultados",
            "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
            "search": "Buscar: ",
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"
            }
        }
    });

    $('.select2').select2();

    $('.btn-remove').on('click', function(e){
        var f_id = $(this).data('id');

        if (confirm('¿Seguro de eliminar E-mail?')) {
            $.ajax({
                type: 'POST',
                url: base_url + 'ajx/adm/inventory/remove-email',
                data: {
                    id: f_id
                },
                dataType: 'json',
                beforeSend: function(){
                    $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
                },
                success: function(r){
                    if (r.status == 'OK') {
                        $(location).attr("href", base_url + "admin/inventory/edit/" + $('#key').data('id') + "/OK3");
                    } else {
                        $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                        $(".btn-submit").attr("disabled", false).html('Guardar');
                        $(document).scrollTop(0);
                    }
                }
            });
        }
    });

    $("#" + form_name).validate({
        rules:{
            product:{
                required: (act == 1) ? true : false,
            },
            quantity:{
                required: true,
                number: true
            },
            unity:{
                required: true,
            },

        },
        messages:{
            product:{
                required: 'Debes seleccionar un producto',
            },
            quantity:{
                required: 'Debes agregar la cantidad',
                number: 'Solo se pemriten números'
            },
            unity:{
                required: 'Debe seleccionar la unidad',
            },

        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist(act, form_name);
            return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} act     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist(act, form_name) {
    var i = $("#" + form_name).serialize();
    var ext_url = 0;

    if (act == 2) {
        ext_url = $('#key').data('id');
    }

    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/adm/inventory/' + act + '/' + ext_url,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", base_url + "admin/inventory/edit/" + r.id + "/OK" + act);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
