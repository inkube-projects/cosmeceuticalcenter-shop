/**
 * Custom JS
 */

 // Sidebar

$(function() {
	var hv = $('#h_m').data('hv');
	var hm = $('#h_m').data('hm');

	// Init perfect scrollbar
	$(".sidebar").perfectScrollbar({
		suppressScrollX: true
	});

	// Toggles
	$('[data-toggle="tooltip"]').tooltip({
      placement : 'top'
   });

	// Sidebar: Toggle user info
	$(".sidebar-user__info").click(function() {
		$(".sidebar-user__nav").slideToggle(300, function() {
			$(".sidebar").perfectScrollbar("update");
		});
		return false;
	});

	// Sidebar: Toggle sidebar dropdown
	$(".sidebar-nav__dropdown > a").click(function() {
		// $('li[data-h-opt="' + hv + '"]').removeClass('css-d-block');
		$('.sidebar-nav__submenu').not($(this).parent("li").find(".sidebar-nav__submenu")).css("display","");
		$(this).parent("li").toggleClass("open");
		$(this).parent("li").find(".sidebar-nav__submenu").slideToggle(300, function() {
			$(".sidebar").perfectScrollbar("update");
		});
		return false;
	});

	// Sidebar: Toggle sidebar
	$("#sidebar__toggle, .sidebar__close").click(function() {
		$(".wrapper").toggleClass("alt");
		return false;
	});

	// Smart alerts
	if ($(".smart-alert").length) {
		// Init smart alerts
		var smartAlerts = new SmartAlerts();
		// Generate alerts (ui_alerts.html example)
		$(".smart-alert").each(function() {
			var alertType = $(this).data("alert-type");
			var alertContent = $(this).data("alert-content");

			$(this).click(function() {
				smartAlerts.generate(alertType, alertContent);
				return false;
			});
		});
	}

	$('li[data-h-opt="' + hv + '"]').addClass('active');
	$('li[data-h-opt="' + hv + '"] ul.sidebar-nav__submenu').css('display', 'block');
	$('li[data-h-opt="' + hv + '"] li[data-h-subopt="' + hm + '"]').addClass('active open');
});



// Collapse plugin

$("[data-toggle='collapse']").click(function(e) {
	e.preventDefault();
});
