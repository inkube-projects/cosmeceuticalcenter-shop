<?php
/**
 * Metodos generales
 */
class GeneralMethods
{
    public $db;

    function __construct($db)
    {
        $this->db = $db;
        $this->user_id = @$_SESSION['ADMIN_SESSION_COSME']['id'];
        $this->user_username = @$_SESSION['ADMIN_SESSION_COSME']['username'];
        $this->user_code = @$_SESSION['ADMIN_SESSION_COSME']['code'];
        $this->user_email = @$_SESSION['ADMIN_SESSION_COSME']['email'];
        $this->user_role = @$_SESSION['ADMIN_SESSION_COSME']['role'];
    }

    /**
     * Crea los logs que relacionados con las acciones a los módulos de los usuarios
     * @param string $desc Descripcion del log
     */
    public function addLogs($desc)
    {
        $ip_acces = @$_SERVER['REMOTE_ADDR'];
        $ip_proxy = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $ip_compa = @$_SERVER['HTTP_CLIENT_IP'];

        $arr_values = array(
            'user_id' => $this->user_id,
            'username' => $this->user_username,
            'action' => $desc,
            'ip_access' => $ip_acces,
            'ip_proxy' => $ip_proxy,
            'ip_company' => $ip_compa,
            'create_at' => date('Y-m-d H:i:s'),
        );

        $result = $this->db->insertAction("user_logs", $arr_values);

        return true;
    }

    /**
     * Obtiene el objeto del usuario
     * @return array
     */
    public function getUser($id)
    {
        $s = "SELECT * FROM user WHERE id='".$id."'";
        $arr_user = $this->db->fetchSQL($s);

        return $arr_user[0];
    }

    /**
     * Se encarga de guardar una archivo temporal en su ruta final
     * @param  string $name       Nombre que tendra el archivo
     * @param  string $old        Nombre de un archivo existente que se reemplazara
     * @param  string $temp_path  Ruta temporal del archivo
     * @param  string $final_path Ruta final del archivo
     * @param  string $table      Nombre de la tabla
     * @param  array  $param      Arreglo de los parameytros a modificar
     * @param  string $where      Condición para modificar
     * @return void
     */
    public function saveImage($name, $old = "", $temp_path, $final_path, $table, array $param, $where)
    {
        $filename = $temp_path.$name;

        if (fileExist($filename)) {
            rename($filename, $final_path.$name);
            rename($temp_path."min_".$name, $final_path."min_".$name);

            if ((!is_null($old)) && ($old != "")) {
                @unlink($final_path.$old);
                @unlink($final_path."min_".$old);
            }

            foreach ($param as $key => $val) {
                $update = $this->db->updateAction($table, array($key => $val), $where);
            }

            @unlink($filename);
            @unlink($temp_path."min_".$name);
        }
    }

    /**
    * Crea la imagen de Portada
    * @return string
    */
    public function createIMG($directory_temp, $directory_profile)
    {
        $dir = $directory_temp;
        $handle = opendir($dir);
        $file = readdir($handle);
        while ($file = readdir($handle)){
            if (is_file($dir.$file)) {
                $image = $file;
            }
        }
        $ext = explode(".", $image)[1];

        $x = sanitize($_POST['x']);
        $y = sanitize($_POST['y']);
        $w = sanitize($_POST['w']);
        $h = sanitize($_POST['h']);
        $targ_w = $w;
        $targ_h = $h;
        $jpeg_quality = 90;
        $src = $directory_temp.$image;
        $src_min = $directory_profile."min_".$image;

        if(($ext == "jpeg")||($ext == "jpg")){
            $img_r = imagecreatefromjpeg($src);
        }if($ext == "png"){
            $img_r = imagecreatefrompng($src);
        } if($ext == "gif"){
            $img_r = imagecreatefromgif($src);
        }

        $src = $directory_temp.$this->user_code."_".$image;
        $dst_r = imagecreatetruecolor($targ_w, $targ_h);
        imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);

        if (($ext == "jpeg") || ($ext== "jpg ")){
            imagejpeg($dst_r,$src,90);
        } elseif ($ext == "png") {
            imagepng($dst_r, $src);
        } elseif ($ext=="gif") {
            imagegif($dst_r,$src);
        }

        // se crea la miniatura
        $src_min = $directory_profile."min_".$this->user_code."_".$image;
        list($width, $height) = getimagesize($src);
        $new_width = 200;
        $new_height = floor($height * ($new_width / $width));
        $targ_w = $new_width;
        $targ_h = $new_height;
        $dst_r = imagecreatetruecolor($targ_w, $targ_h);
        imagecopyresampled($dst_r,$img_r,0,0,$x,$y, $targ_w,$targ_h,$w,$h);

        if(($ext=="jpeg")||($ext=="jpg")){
            imagejpeg($dst_r,$src_min,90);
        } if($ext=="png"){
            imagepng($dst_r,$src_min);
        } if($ext=="gif"){
            imagegif($dst_r,$src_min);
        }

        imagedestroy($dst_r);

        // rename($src, $directory_profile.$image);
        return $this->user_code."_".$image;
    }

    /**
     * Realiza un envío de email
     * @param  string $email_template Ruta de la plantilla de email
     * @param  array  $arr_data       Arregko con data a mostrar
     * @param  string $target_address Email destino
     * @param  string $target_name    Nombre del destino
     * @param  string $subject        Asunto
     * @return void
     */
    public function sendEmailTemplate($email_template, $arr_data, $target_address, $target_name, $subject)
    {
        $mail = new PHPMailer(true);

        ob_start();
        include($email_template);
        $template = ob_get_clean();

        //Server settings
        // $mail->SMTPDebug = 2;
        $mail->isSMTP();
        $mail->Host = EMAIL_HOST;
        $mail->SMTPAuth = true;
        $mail->Username = EMAIL_SENDER;
        $mail->Password = EMAIL_PASSWORD;
        $mail->SMTPSecure = 'tls';
        // $mail->Port = EMAIL_PORT;
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        //Recipients
        $mail->setFrom(EMAIL_SENDER, 'Cosmeceutical center');
        $mail->addAddress($target_address, $target_name);
        // $mail->addAddress("ali.jose118@gmail.com", "Cosmeceutical");

        //Content
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body    = $template;
        $mail->AltBody = $template;

        $mail->send();
    }
}
?>
