<?php
include('core/Model/GeneralMethods.php');
include('core/bin/Services/ProductsService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $productsService = new ProductsService($db);

    switch ($act) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();
            $arr_required = array('name','description','price');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name'], "Nombre: no se permiten caracteres especiales", "#$^*\|");
                isValidString($_POST['content'], "Contenido: no se permiten caracteres especiales", "#$%^*\|");
                isValidDecimal($_POST['price'], "El precio no es válido");
                isValidString($_POST['seo_title'], "SEO Título: no se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['seo_keywords'], "SEO Palabras clave: no se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['seo_description'], "SEO Descripción: no se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['tags'], "Tags: no se permiten caracteres especiales", "#$%^*\|");
                isValidNumber($_POST['iva']);
                $db->existRecord("id='".@number_format($_POST['brand'],0,"","")."'", "brands", "La marca no es válida");

                if (isset($_POST['category'])) {
                    if (is_array($_POST['category'])) {
                        foreach ($_POST['category'] as $key => $cat) {
                            $db->existRecord("id='".@number_format($cat,0,"","")."'", "products_category", "La categoría no es válida");
                        }
                    } else {
                        throw new \Exception("Debes seleccionar una categoría", 1);
                    }
                } else {
                    throw new \Exception("Debes seleccionar una categoría", 1);
                }

                if (isset($_POST['subcategory'])) {
                    if (is_array($_POST['subcategory'])) {
                        foreach ($_POST['subcategory'] as $key => $sub) {
                            $db->existRecord("id='".@number_format($sub,0,"","")."'", "products_sub_category", "La subcategoría no es válida");
                        }
                    }
                }

                if (isset($_POST['size'])) {
                    if (is_array($_POST['size'])) {
                        foreach ($_POST['size'] as $key => $size) {
                            $db->existRecord("id='".@number_format($size,0,"","")."'", "size", "La talla no es válida");
                        }
                    }
                }

                if (isset($_POST['status'])) {
                    isValidOption(0, 1, $_POST['status'], "El estado no es válido");
                } else {
                    $_POST['status'] = "0";
                }

                // Se valida novedad
                if (isset($_POST['novelty'])) {
                    validateNovelty();
                } else {
                    $_POST['novelty'] = "0";
                }

                // Se valida descuento / promoción
                if (isset($_POST['discount_promo'])) {
                    validateDiscountPromo();
                } else {
                    $_POST['discount_promo'] = "0";
                }

                // Se valida recomendación semanal
                if (isset($_POST['weekly_recommendation'])) {
                    validateWeeklyRecommendation();
                } else {
                    $_POST['weekly_recommendation'] = "0";
                }

                $result = $productsService->persist();

                if (verifyTempFiles($admin_pp_code)) {
                    $productsService->moveImages($result);
                }

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente el producto", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se edita una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");
            $arr_required = array('name','description','price');

            try {
                $db->existRecord("id='".$id."'", "products", "La entrada no existe");
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name'], "Nombre: no se permiten caracteres especiales", "#$^*\|");
                isValidString($_POST['content'], "Contenido: no se permiten caracteres especiales", "#$%^*\|");
                isValidDecimal($_POST['price'], "El precio no es válido");
                isValidString($_POST['seo_title'], "SEO Título: no se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['seo_keywords'], "SEO Palabras clave: no se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['seo_description'], "SEO Descripción: no se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['tags'], "Tags: no se permiten caracteres especiales", "#$%^*\|");
                isValidNumber($_POST['iva']);

                $db->existRecord("id='".@number_format($_POST['brand'],0,"","")."'", "brands", "La marca no es válida");

                if (isset($_POST['category'])) {
                    if (is_array($_POST['category'])) {
                        foreach ($_POST['category'] as $key => $cat) {
                            $db->existRecord("id='".@number_format($cat,0,"","")."'", "products_category", "La categoría no es válida");
                        }
                    } else {
                        throw new \Exception("Debes seleccionar una categoría", 1);
                    }
                } else {
                    throw new \Exception("Debes seleccionar una categoría", 1);
                }

                if (isset($_POST['subcategory'])) {
                    if (is_array($_POST['subcategory'])) {
                        foreach ($_POST['subcategory'] as $key => $sub) {
                            $db->existRecord("id='".@number_format($sub,0,"","")."'", "products_sub_category", "La subcategoría no es válida");
                        }
                    }
                }

                if (isset($_POST['size'])) {
                    if (is_array($_POST['size'])) {
                        foreach ($_POST['size'] as $key => $size) {
                            $db->existRecord("id='".@number_format($size,0,"","")."'", "size", "La talla no es válida");
                        }
                    }
                }

                if (isset($_POST['status'])) {
                    isValidOption(0, 1, $_POST['status'], "El estado no es válido");
                } else {
                    $_POST['status'] = "0";
                }

                // Se valida novedad
                if (isset($_POST['novelty'])) {
                    validateNovelty();
                } else {
                    $_POST['novelty'] = "0";
                }

                // Se valida descuento / promoción
                if (isset($_POST['discount_promo'])) {
                    validateDiscountPromo();
                } else {
                    $_POST['discount_promo'] = "0";
                }

                // Se valida recomendación semanal
                if (isset($_POST['weekly_recommendation'])) {
                    validateWeeklyRecommendation();
                } else {
                    $_POST['weekly_recommendation'] = "0";
                }

                $result = $productsService->update($id);

                if (verifyTempFiles($admin_pp_code)) {
                    $productsService->moveImages($result);
                }

                // Se verifican los colores
                if (isset($_POST['cnt_color'])) {
                    $cnt_color = @number_format($_POST['cnt_color'],0,"","");
                    for ($i = 1; $i <= $cnt_color; $i++) {
                        $IDcolor = @number_format($_POST['color_'.$i],0,"","");
                        if ($IDcolor != 0 && $IDcolor != "") {
                            $db->existRecord("id='".$IDcolor."'", "colors", "Un color no es válido");
                            isValidGalleryImageMain("gallery_color_".$i, 150, 150);
                        }
                    }
                }

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente el producto", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3: // Se guarda la imagen de portada
            try {
                isValidImage('cover_image', 150, 150);
                $result = $productsService->prepareCoverImage(1);

                $arr_response = array('status' => 'OK', 'data' => $result);
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 4: // se recorta la imagen
            try {
                isRequiredValuesPost($_POST, array('x','y','w','h'));
                $result = $productsService->prepareCoverImage(2);

                $arr_response = array('status' => 'OK', 'data' => $result);
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 5: // Subir imágenes temporales de la galería
            try {
                isValidGalleryImage(1);

                $result = $productsService->uploadImages();

                $arr_response = array('status' => 'OK', 'message' => 'Se han subido las imagenes temporales', 'data' => $result);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 6: //Eliminar imagen temporal
            $path = APP_IMG_ADMIN."user_".$admin_pp_code."/temp_files/products/gallery/";
            $image = $_POST['img'];

            try {
                if (!fileExist($path.$image)) {
                    throw new \Exception("La imagen no existe", 1);
                }

                @unlink($path.$image);
                @unlink($path."min_".$image);

                $result = $productsService->getTempImages();

                $arr_response = array('status' => 'OK', 'data' => 'Imagen temporal eliminada', 'data' => $result);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 7: // Elimina una imagen de galería
            $porduct_id = @number_format($_GET['id'],0,"","");
            $image_id = @number_format($_POST['image_id'],0,"","");

            try {
                $db->existRecord("id='".$image_id."' AND product_id='".$porduct_id."'", "products_gallery", "La imagen no existe");

                $data = $productsService->removeImageGallery($image_id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha eliminado correctamente la imagen", 'data' => $data);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 8: // Recomendaciones semanales
            $porduct_id = @number_format($_POST['product'],0,"","");

            try {
                isRequiredValuesPost($_POST, array('product', 'range', 'new_price'));
                isValidDecimal($_POST['new_price'], "El precio es inválido");
                $db->existRecord("id='".$porduct_id."'", "products", "El producto no es válido");
                $range = explode(" al ", $_POST['range']);
                if (!isset($range[1])) { throw new \Exception("Debes ingresar un rango de fechas válido", 1); }

                isValidDate($range[0]);
                isValidDate($range[1]);

                $productsService->persistWeeklyRecommendation($porduct_id, $range[0], $range[1]);
                $arr_response = array('status' => 'OK', 'message' => "Se ha agregado correctamente");
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 9: // novedades
            $porduct_id = @number_format($_POST['product'],0,"","");

            try {
                isRequiredValuesPost($_POST, array('product', 'range'));
                $db->existRecord("id='".$porduct_id."'", "products", "El producto no es válido");
                $range = explode(" al ", $_POST['range']);
                if (!isset($range[1])) { throw new \Exception("Debes ingresar un rango de fechas válido", 1); }

                isValidDate($range[0]);
                isValidDate($range[1]);

                $productsService->persistNovelties($porduct_id, $range[0], $range[1]);
                $arr_response = array('status' => 'OK', 'message' => "Se ha agregado correctamente");
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 10: // Descuento
            $porduct_id = @number_format($_POST['product'],0,"","");

            try {
                isRequiredValuesPost($_POST, array('product', 'new_price', 'range'));
                isValidDecimal($_POST['new_price'], "El precio es inválido");
                $range = explode(" al ", $_POST['range']);
                if (!isset($range[1])) { throw new \Exception("Debes ingresar un rango de fechas válido", 1); }

                isValidDate($range[0]);
                isValidDate($range[1]);

                $db->existRecord("id='".$porduct_id."'", "products", "El producto no es válido");

                $productsService->persistDiscount($porduct_id, $range[0], $range[1]);
                $arr_response = array('status' => 'OK', 'message' => "Se ha agregado correctamente");
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 11: // Muestra la subcategoria
            $product_id = "";
            try {
                if ($_POST['product_id'] != "") {
                    $product_id = @number_format($_POST['product_id'],0,"","");
                    $db->existRecord("id='".$product_id."'", "products", "El producto no es válido");
                }

                if (is_array($_POST['category'])) {
                    foreach ($_POST['category'] as $key => $cat) {
                        $db->existRecord("id='".@number_format($cat,0,"","")."'", "products_category", "La categoría no es válida");
                    }
                }

                $result = $productsService->getSubcategories($_POST['category'], $product_id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha agregado correctamente", 'data' => $result);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 12:
            $brand_id = @intval($_POST['brand']);

            try {
                $db->existRecord("id='".$brand_id."'", "brands", "La marca no es válida");

                $iva = $db->getValue("iva", "brands", "id='".$brand_id."'");

                $arr_response = array('status' => 'OK', 'message' => "Se ha agregado correctamente", 'iva' => $iva);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 13: // Colores
            $db->beginTransaction();
            $arr_required = array('name');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name'], "Nombre: no se permiten caracteres especiales", "#$^*\|");

                $result = $productsService->persistColor();

                $arr_response = array('status' => 'OK', 'message' => "Se ha agregado el color correctamente", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 14: // Editar color
            $db->beginTransaction();
            $arr_required = array('name');
            $id = @number_format($_GET['id'],0,"","");

            try {
                $db->existRecord("id='".$id."'", "colors", "El color no existe");
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name'], "Nombre: no se permiten caracteres especiales", "#$^*\|");

                $result = $productsService->updateColor($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha agregado el color correctamente", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 15: // Guarda una talla
            $db->beginTransaction();
            $arr_required = array('name');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name'], "Talla: no se permiten caracteres especiales", "#$^*\|");

                $result = $productsService->persistSize();

                $arr_response = array('status' => 'OK', 'message' => "Se ha agregado la talla correctamente", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 16: // Editar talla
            $db->beginTransaction();
            $arr_required = array('name');
            $id = @number_format($_GET['id'],0,"","");

            try {
                $db->existRecord("id='".$id."'", "size", "La talla no existe");
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name'], "Talla: no se permiten caracteres especiales", "#$^*\|");

                $result = $productsService->updateSize($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha agregado la talla correctamente", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 17: // Elimina una imagen de color
            $porduct_id = @number_format($_GET['id'],0,"","");
            $image_id = @number_format($_POST['image_id'],0,"","");

            try {
                $db->existRecord("id='".$image_id."' AND product_id='".$porduct_id."'", "color_image", "La imagen no existe");

                $data = $productsService->removeImageColor($image_id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha eliminado correctamente la imagen", 'data' => $data);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 18: // Elimina un de color
            $porduct_id = @number_format($_GET['id'],0,"","");
            $color_id = @number_format($_POST['color_id'],0,"","");

            try {
                $db->existRecord("color_id='".$color_id."' AND product_id='".$porduct_id."'", "products_colors", "El color no es válido");

                $data = $productsService->removeColor($color_id, $porduct_id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha eliminado correctamente el color", 'data' => $data);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

/**
 * Se validan las imagenes de galería
 * @param  integer  $act Acción
 * @return boolean
 */
function isValidGalleryImage($act)
{
    if ($act == 1) { // Sube imágenes temporales
        $arr_lenght = count($_FILES['gallery_images']['tmp_name']);

        if ($arr_lenght <= 0) {
            throw new Exception("Debes ingresar al menos una imagen", 1);
        }

        for ($i = 0; $i < $arr_lenght; $i++) {
            if (!empty($_FILES['gallery_images']['tmp_name'][$i])) {
                $img_type = $_FILES['gallery_images']['type'][$i];

                if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
                    list($width, $height) = getimagesize($_FILES['gallery_images']['tmp_name'][$i]);

                    if (($width < 250) || ($height < 250)) {
                        throw new Exception("La imagen debe tener por lo menos 250 x 250px", 1);
                    }
                } else {
                    throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
                }
            } else {
                throw new Exception("La imagen de portada no puede estar vacia", 1);
            }
        }
    }
    return true;
}

/**
 * Verifica si la carpeta temporal esta vacía
 * @return boolean
 */
function verifyTempFiles($admin_pp_code)
{
    $path = APP_IMG_ADMIN."user_".$admin_pp_code."/temp_files/products/gallery/";
    $handle = opendir($path);

    while ($file = readdir($handle)){
        if (is_file($path.$file)) {
            return true;
        }
    }

    return false;
}

/**
 * Valida si es novedad y el rango de fechas
 * @return boolean|exception
 */
function validateNovelty()
{
    isValidOption(0, 1, $_POST['novelty'], "El estado novedad no es válido");
    if ($_POST['novelty'] == "1") {
        if (isset($_POST['novelty_date_range'])) {
            if ($_POST['novelty_date_range'] != "") {
                $range = explode(" al ", $_POST['novelty_date_range']);
                $init = to_date($range[0]);
                $end = to_date($range[1]);

                isValidDate($init, "en");
                isValidDate($end, "en");
            }
        }
    }

    return true;
}

/**
 * Valida la promocion / descuento
 * @return boolean|exception
 */
function validateDiscountPromo()
{
    isValidOption(0, 1, $_POST['discount_promo'], "El estado descuento / promoción no es válido");
    if ($_POST['discount_promo'] == 1) {
        // Se valida las fechas
        if (isset($_POST['discount_date_range']) && @$_POST['discount_date_range'] != "") {
            $range = explode(" al ", $_POST['discount_date_range']);
            $init = to_date($range[0]);
            $end = to_date($range[1]);

            isValidDate($init, "en");
            isValidDate($end, "en");
        } else {
            throw new \Exception("Debes ingresar un rango de fechas para la promoción / descuento", 1);
        }

        // se valida el nuevo precio
        if (isset($_POST['discount_price']) && @$_POST['discount_price'] != "") {
            isValidDecimal($_POST['discount_price'], "Debes ingresar un precio de descuento / promoción válido");
        } else {
            throw new \Exception("Debes ingresar un precio de descuento / promoción", 1);
        }
    }

    return true;
}

/**
 * Valida la recomendación semanal
 * @return boolean|exception
 */
function validateWeeklyRecommendation()
{
    isValidOption(0, 1, $_POST['weekly_recommendation'], "El estado descuento / promoción no es válido");
    if ($_POST['weekly_recommendation'] == 1) {
        // Se valida las fechas
        if (isset($_POST['weekly_date_range']) && @$_POST['weekly_date_range'] != "") {
            $range = explode(" al ", $_POST['weekly_date_range']);
            $init = to_date($range[0]);
            $end = to_date($range[1]);

            isValidDate($init, "en");
            isValidDate($end, "en");
        } else {
            throw new \Exception("Debes ingresar un rango de fechas para la recomendación semanal", 1);
        }

        // se valida el nuevo precio
        if (isset($_POST['weekly_price']) && @$_POST['weekly_price'] != "") {
            isValidDecimal($_POST['weekly_price'], "Debes ingresar un precio de recomendación semanal válido");
        } else {
            // throw new \Exception("Debes ingresar un precio de recomendación semanal", 1);
        }
    }

    return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null;
?>
