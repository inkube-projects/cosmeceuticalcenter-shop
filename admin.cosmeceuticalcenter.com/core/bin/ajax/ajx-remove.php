<?php

$db = new Connection();
$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');

include('core/Model/GeneralMethods.php');
include('core/bin/helpers/RemoveHelper.php');

if ($_GET) {
    $act = @number_format($_GET['act'],0,"","");
    $removeHelper = new RemoveHelper($db);

    switch ($act) {
        case 1: // Usuarios
            $id = @number_format($_GET['r'],0,"","");

            $db->beginTransaction();

            try {
                $db->existRecord("id='".$id."'", "user", "El usuario no es válido");
                $removeHelper->removeUser($id);

                $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null;
?>
