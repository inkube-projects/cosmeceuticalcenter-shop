<?php
include('core/Model/GeneralMethods.php');
include('core/bin/Services/CouponService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $couponService = new CouponService($db);

    switch ($act) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();
            $arr_required = array('name', 'discount', 'canonical_id');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name']);
                isValidDecimal($_POST['discount']);
                isValidNumber($_POST['uses']);
                isValidString($_POST['canonical_id'], "ID del cupón: No se permiten caracteres especiales", "#$%^*\|");
                $db->duplicatedRecord("canonical_id='".$_POST['canonical_id']."'", "coupon", "EL ID del cupón ya se encuentra en uso");

                // Se vaida el rango de fechas si no viene vacío
                if ($_POST['range'] != "") {
                    $range = explode(' al ', $_POST['range']);
                    if (!isset($range[1])) { throw new Exception("Debes ingresar un rango de fechas válido", 1); }

                    isValidDate($range[0]); // Rango inicial
                    isValidDate($range[1]); // Rango final
                }

                $result = $couponService->persist($range[0], $range[1]);

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente el cupón", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se edita una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");
            $arr_required = array('name', 'discount');

            try {
                $db->existRecord("id='".$id."'", "coupon", "El cupón no es válido");
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name']);
                isValidDecimal($_POST['discount']);
                isValidNumber($_POST['uses']);

                // Se vaida el rango de fechas si no viene vacío
                if ($_POST['range'] != "") {
                    $range = explode(' al ', $_POST['range']);
                    if (!isset($range[1])) { throw new Exception("Debes ingresar un rango de fechas válido", 1); }

                    isValidDate($range[0]); // Rango inicial
                    isValidDate($range[1]); // Rango final
                }

                $result = $couponService->update($id, $range[0], $range[1]);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente el cupón", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null;
?>
