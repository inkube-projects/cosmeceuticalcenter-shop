<?php
include('core/Model/GeneralMethods.php');
include('core/bin/Services/ZonesService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $zonesService = new ZonesService($db);

    switch ($act) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();
            $arr_required = array('name','value');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name']);
                isValidDecimal($_POST['value']);

                if (isset($_POST['prov'])) {
                    foreach ($_POST['prov'] as $val) {
                        $db->existRecord("id='".$val."'", "province", "Provincia inválida: ".$val);
                    }
                }
                if (isset($_POST['country'])) {
                    foreach ($_POST['country'] as $val) {
                        $db->existRecord("id='".$val."'", "country", "País inválido: ".$val);
                    }
                }

                $result = $zonesService->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la entrada", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se edita una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");
            $arr_required = array('name','value');

            try {
                $db->existRecord("id='".$id."'", "zones", "La entrada no existe");
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name']);
                isValidDecimal($_POST['value']);

                if (isset($_POST['prov'])) {
                    foreach ($_POST['prov'] as $val) {
                        $db->existRecord("id='".$val."'", "province", "Provincia inválida: ".$val);
                    }
                }
                if (isset($_POST['country'])) {
                    foreach ($_POST['country'] as $val) {
                        $db->existRecord("id='".$val."'", "country", "País inválido: ".$val);
                    }
                }

                $result = $zonesService->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la entrada", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3: // Obtiene la provincia
            $province_id = @number_format($_GET['id'],0,"","");

            try {
                $db->existRecord("id='".$province_id."'", "province", "La provincia no es válida");
                $result = $zonesService->getProvince($province_id);
                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la entrada", 'data' => $result);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null;
?>
