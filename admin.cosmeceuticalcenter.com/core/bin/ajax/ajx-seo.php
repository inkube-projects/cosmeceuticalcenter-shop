<?php
include('core/Model/GeneralMethods.php');
include('core/bin/Services/SeoService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $SeoService = new SeoService($db);

    switch ($act) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();
            $arr_required = array('keywords','description','section');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['keywords'], "Palabras clave: no se permiten caracteres especiales", $not_allowed = "#$%^*\|/-");
                isValidString($_POST['description'], "Palabras clave: no se permiten caracteres especiales", $not_allowed = "#$%^*\|/-");

                $db->existRecord("id='".@number_format($_POST['section'])."'", "seo_section", "La sección no es válida");

                $result = $SeoService->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la entrada", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se guarda una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");
            $arr_required = array('keywords','description','section');

            try {
                $db->existRecord("id='".$id."'", "seo", "La entrada no existe");
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['keywords'], "Palabras clave: no se permiten caracteres especiales", $not_allowed = "#$%^*\|/-");
                isValidString($_POST['description'], "Palabras clave: no se permiten caracteres especiales", $not_allowed = "#$%^*\|/-");

                $db->existRecord("id='".@number_format($_POST['section'])."'", "seo_section", "La sección no es válida");

                $result = $SeoService->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la entrada", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null;
?>
