<?php
include('core/Model/GeneralMethods.php');
include('core/bin/Services/ProductsCategoryService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $ProductsCategoryService = new ProductsCategoryService($db);

    switch ($act) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();
            $arr_required = array('name');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name']);
                isValidString($_POST['description']);

                if ($_FILES["image"]['tmp_name'] != "") {
                    isValidImage('image', 80, 80);
                }

                $result = $ProductsCategoryService->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la categoría", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se guarda una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");
            $arr_required = array('name');

            try {
                $db->existRecord("id='".$id."'", "products_category", "La entrada no existe");
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name']);
                isValidString($_POST['description']);

                if ($_FILES["image"]['tmp_name'] != "") {
                    isValidImage('image', 80, 80);
                }

                $result = $ProductsCategoryService->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la categoría", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null;
?>
