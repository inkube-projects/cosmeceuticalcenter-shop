<?php
include('core/Model/GeneralMethods.php');
include('core/bin/Services/InventoryService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $InventoryService = new InventoryService($db);

    switch ($act) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();
            $arr_required = array('product','quantity','unity');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidDecimal($_POST['quantity']);
                $db->existRecord("id='".@number_format($_POST['product'],0,"","")."'", "products", "El producto no es válido");
                $db->existRecord("id='".@number_format($_POST['unity'])."'", "inventory_unit", "La unidad no es válida");

                $result = $InventoryService->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la entrada", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se guarda una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");
            $arr_required = array('quantity','unity');

            try {
                $db->existRecord("id='".$id."'", "inventory", "La entrada no existe");
                isRequiredValuesPost($_POST, $arr_required);
                isValidDecimal($_POST['quantity']);
                $db->existRecord("id='".@number_format($_POST['unity'])."'", "inventory_unit", "La unidad no es válida");

                $result = $InventoryService->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la entrada", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3:
            $db->beginTransaction();
            $notify_id = @number_format($_POST['id'],0,"","");

            try {
                $db->existRecord("id='".$notify_id."'", "notify_products", "La entrada no existe");
                $result = $InventoryService->removeNotify($notify_id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha eliminado correctamente");
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }

        break;

        case 4: // Actualizacion desde el istado de productos
            $db->beginTransaction();
            $product_id = @number_format($_GET['id'],0,"","");
            $arr_required = array('quantity');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidDecimal($_POST['quantity']);
                $db->existRecord("id='".$product_id."'", "products", "El producto no es válido");

                $result = $InventoryService->fastUpdate($product_id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la entrada");
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 5: // muestra la infromacion de inventario extra de los productos (talla - colores)
            $product_id = @number_format($_GET['id'],0,"","");

            try {
                $db->existRecord("id='".$product_id."'", "products", "El producto no es válido");
                $data = $InventoryService->showInventorySizeColors($product_id);
                $arr_response = array('status' => 'OK', 'message' => "Información cargada correctamente", "data" => $data);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 6: // Guarda el stock de las tallas y colores
            $db->beginTransaction();
            $product_id = @number_format($_POST['product_id'],0,"","");

            try {
                $db->existRecord("id='".$product_id."'", "products", "El producto no es válido");
                $InventoryService->persistExtraStock($product_id);
                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente el stock");
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null;
?>
