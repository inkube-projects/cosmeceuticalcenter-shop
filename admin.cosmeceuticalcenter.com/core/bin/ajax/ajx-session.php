<?php

$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');

if ($_POST) {
    try {
        isRequiredValuesPost($_POST, array('user_email', 'password'));
        $user = isValidCredentials($_POST['user_email']);
        createSession($user);

        $arr_response = array('status' => 'OK', 'message' => 'Inicio de sesión correcto');
    } catch (Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }
}

//------------------------------------------------------------------------------

/**
 * Verifica las credenciales
 * @param  string  $username Nombre de usuario o email
 * @return array
 */
function isValidCredentials($username)
{
    $db = new Connection();
    isValidString($username);

    if (filter_var($username, FILTER_VALIDATE_EMAIL) === false) { // se verifica si es un email o un nombre de usuario
        $cnt_val = $db->getCount("view_user", "username='".$username."' AND role_id IN (1,2)");
        $username = mb_convert_case($username, MB_CASE_UPPER, "UTF-8");

        if ($cnt_val == 1) {
            $user = $db->fetchSQL("SELECT * FROM view_user WHERE username='".$username."' AND role_id IN (1,2)");
            if (!password_verify($_POST['password'], $user[0]['password'])) {

                throw new Exception("Usuario o contraseña incorrecto(s)", 1);
            }
        } else {
            throw new Exception("Usuario o contraseña incorrecto(s)", 1);
        }
    } else {
        $cnt_val = $db->getCount("view_user", "email='".$username."' AND role_id IN (1,2)");
        $username = mb_convert_case($username, MB_CASE_LOWER, "UTF-8");

        if ($cnt_val == 1) {
            $user = $db->fetchSQL("SELECT * FROM view_user WHERE email='".$username."' AND role_id IN (1,2)");
            if (!password_verify($_POST['password'], $user[0]['password'])) {
                throw new Exception("Usuario o contraseña incorrecto(s)", 1);
            }
        } else {
            throw new Exception("Usuario o contraseña incorrecto(s)", 1);
        }
    }

    return $user;
}

/**
 * Crea la sesión de usuario
 * @param  array $user Arreglo del usuario que se le creará la sesión
 * @return boolean
 */
function createSession($user)
{
    $db = new Connection();
    $ip_acces = @$_SERVER['REMOTE_ADDR'];
    $ip_proxy = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $ip_compa = @$_SERVER['HTTP_CLIENT_IP'];
    $arr_values = array(
        'user_id' => $user[0]['id'],
        'username' => $user[0]['username'],
        'action' => 'Inicio de sesión',
        'ip_access' => $ip_acces,
        'ip_proxy' => $ip_proxy,
        'ip_company' => $ip_compa,
        'create_at' => date('Y-m-d H:i:s')
    );

    $db->insertAction("user_logs", $arr_values);
    $db->updateAction("user", array('last_login' => date('Y-m-d H:i:s')), "id='".$user[0]['id']."'");
    $_SESSION['ADMIN_SESSION_COSME'] = $user[0];
    // ini_set('session.cookie_lifetime', time() + (60*60*2));
    setcookie("session.cookie_lifetime", "session expired", time()+3600);

    return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
