<?php
include('core/Model/GeneralMethods.php');
include('core/bin/Services/CarouselService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $CarouselService = new CarouselService($db);

    switch ($act) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();
            $arr_required = array();

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidURL($_POST['url']);

                if ($_FILES["image"]['tmp_name'] != "") {
                    isValidImage('image', 300, 150);
                } else {
                    throw new \Exception("Debes ingresar una imagen", 1);
                }

                if (isset($_POST['status'])) {
                    isValidOption(0, 1, $_POST['status'], "El estado no es válido");
                } else {
                    $_POST['status'] = '0';
                }

                $result = $CarouselService->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la imagen", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se guarda una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");
            $arr_required = array();

            try {
                $db->existRecord("id='".$id."'", "carousel", "La entrada no existe");
                isRequiredValuesPost($_POST, $arr_required);
                isValidURL($_POST['url']);
                isValidImage('image', 300, 150);

                if (isset($_POST['status'])) {
                    isValidOption(0, 1, $_POST['status'], "El estado no es válido");
                } else {
                    $_POST['status'] = '0';
                }

                $result = $CarouselService->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la imagen", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3:
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");
            $order = @number_format($_POST['order'],0,"","");

            try {
                $db->existRecord("id='".$id."'", "carousel", "La imagen no es válida");
                isValidNumber($order);

                $CarouselService->setOrder($id, $order);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la imagen");
                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null;
?>
