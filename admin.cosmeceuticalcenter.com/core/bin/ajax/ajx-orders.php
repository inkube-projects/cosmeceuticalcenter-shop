<?php
include('core/Model/GeneralMethods.php');
include('core/bin/Services/OrdersService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $OrdersService = new OrdersService($db);

    switch ($act) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();
            $arr_required = array('user', 'country');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                $db->existRecord("id='".@number_format($_POST['user'],0,"","")."' AND role='3'", "user", "El usuario no es válido");
                $db->existRecord("id='".@number_format($_POST['country'],0,"","")."'", "country", "El país no es válido");

                if (isset($_POST['province'])) {
                    if ($_POST['province'] != "") {
                        $db->existRecord("id='".@number_format($_POST['province'],0,"","")."'", "province", "La provincia no es válida");
                    }
                } else {
                    $_POST['province'] = "";
                }

                $result = $OrdersService->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la entrada", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se edita una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");
            $arr_required = array('status');


            try {
                $db->existRecord("id='".$id."'", "orders", "La entrada no existe");
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['company'], "Empresa envío: No se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['tracking_number'], "Número de seguimiento: No se permiten caracteres especiales", "#$%^*\|");

                $result = $OrdersService->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la entrada", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3: // Agrea producto en la orden
            $product_id = @number_format($_POST['product'],0,"","");
            $country_id = @number_format($_POST['country'],0,"","");

            try {
                $db->existRecord("id='".$product_id."'", "products", "El producto no es válido");
                $db->existRecord("id='".$country_id."'", "country", "El país no es válido");

                if ($_POST['province'] != "") {
                    $db->existRecord("id='".@number_format($_POST['province'],0,"","")."'", "province", "La provincia no es válida");
                }

                $result = $OrdersService->addProduct($product_id);
                $calculate = $OrdersService->calculate($_POST['province'], $country_id);

                $arr_response = array(
                    'status' => 'OK',
                    'message' => "Se ha editado correctamente la entrada",
                    'data' => $result,
                    'calculate' => array(
                        'sub_total' => $calculate['sub_total'],
                        'shipping' => $calculate['shipping'],
                        'total' => $calculate['total']
                    )
                );
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 4: // Elimina un producto de la orden
            $product_id = @number_format($_POST['product'],0,"","");
            $country_id = @number_format($_POST['country'],0,"","");

            try {
                $db->existRecord("id='".$product_id."'", "products", "El producto no es válido");
                $db->existRecord("id='".$country_id."'", "country", "El país no es válido");
                if ($_POST['province'] != "") {
                    $db->existRecord("id='".@number_format($_POST['province'],0,"","")."'", "province", "La provincia no es válida");
                }

                $result = $OrdersService->removeProduct($product_id);
                $calculate = $OrdersService->calculate($_POST['province'], $country_id);

                $arr_response = array(
                    'status' => 'OK',
                    'message' => "Se ha editado correctamente la entrada",
                    'data' => $result,
                    'calculate' => array(
                        'sub_total' => $calculate['sub_total'],
                        'shipping' => $calculate['shipping'],
                        'total' => $calculate['total']
                    )
                );
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 5: // Agrega el shipping
            $province_id = @number_format($_POST['province'],0,"","");
            $country_id = @number_format($_POST['country'],0,"","");

            try {
                $db->existRecord("id='".$country_id."'", "country", "El país no es válido");
                if ($province_id) { $db->existRecord("id='".$province_id."'", "province", "La provincia no es válida"); }

                $calculate = $OrdersService->calculate($province_id, $country_id);

                $arr_response = array(
                    'status' => 'OK',
                    'message' => "Se ha editado correctamente la entrada",
                    'calculate' => array(
                        'sub_total' => $calculate['sub_total'],
                        'shipping' => $calculate['shipping'],
                        'total' => $calculate['total']
                    )
                );
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null;
?>
