<?php
include('core/Model/GeneralMethods.php');
include('core/bin/Services/NewsletterService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $NewsletterService = new NewsletterService($db);

    switch ($act) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();
            $arr_required = array('email');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidEmail($_POST['email']);
                $db->duplicatedRecord("email='".$_POST['email']."'", "newsletter", "El email ya se encuentra registrado");

                $result = $NewsletterService->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la entrada", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se guarda una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");
            $arr_required = array('email');

            try {
                $db->existRecord("id='".$id."'", "newsletter", "La entrada no existe");
                isRequiredValuesPost($_POST, $arr_required);
                isValidEmail($_POST['email']);
                $db->duplicatedRecord("email='".$_POST['email']."' AND NOT id='".$id."'", "newsletter", "El email ya se encuentra registrado");

                $result = $NewsletterService->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la entrada", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null;
?>
