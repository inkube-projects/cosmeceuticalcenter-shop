<?php
include('core/Model/GeneralMethods.php');
include('core/bin/Services/SubcategoryService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $SubcategoryService = new SubcategoryService($db);

    switch ($act) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();
            $arr_required = array('name','category');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name'], "Nombre: no se permiten caracteres especiales", "#$%^*\|");
                $db->existRecord("id='".@number_format($_POST['category'],0,"","")."'", "products_category", "La categoría no es válida");

                $result = $SubcategoryService->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la entrada", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se guarda una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");
            $arr_required = array('name','category');

            try {
                $db->existRecord("id='".$id."'", "products_sub_category", "La subcategoría no es válida");
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name'], "Nombre: no se permiten caracteres especiales", "#$%^*\|");
                $db->existRecord("id='".@number_format($_POST['category'],0,"","")."'", "products_category", "La categoría no es válida");

                $result = $SubcategoryService->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la entrada", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null;
?>
