<?php
/**
 * Servicios
 */
class ZonesService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Agrega una entrada
     * @return void
     */
    public function persist()
    {
        $db = $this->db;

        $arr_values = array(
            'name' => $_POST['name'],
            'value' => $_POST['value'],
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s'),
        );

        $zone = $db->insertAction("zones", $arr_values);

        // Se guardan las provincias
        if (isset($_POST['prov'])) {
            $formed_array = array_unique($_POST['prov']);
            $db->deleteAction("zone_province", "zone_id='".$zone[0]['id']."'");
            foreach ($formed_array as $val) {
                $arr_values = array(
                    'province_id' => $val,
                    'zone_id' => $zone[0]['id']
                );

                $db->insertAction("zone_province", $arr_values);
            }
        }

        // Se guardan los países
        $flag = false;
        if (isset($_POST['country'])) {
            $db->deleteAction("zone_country", "zone_id='".$zone[0]['id']."'");
            foreach ($_POST['country'] as $val) {
                if ($val == 73) { $flag = true;}
                $arr_values = array(
                    'country_id' => $val,
                    'zone_id' => $zone[0]['id']
                );

                $db->insertAction("zone_country", $arr_values);
            }
        }
        // Se eliminan las provincias de la zona si no tiene a españa
        if (!$flag) { $db->deleteAction("zone_province", "zone_id='".$zone[0]['id']."'"); }

        $this->addLogs(sprintf("Agregando entrada zones - id: %d", $zone[0]['id']));
        return $zone[0];
    }

    /**
     * Edita una entrada
     * @param  integer $id ID de la entrada
     * @return void
     */
    public function update($id)
    {
        $db = $this->db;

        $arr_values = array(
            'name' => $_POST['name'],
            'value' => $_POST['value'],
            'create_at' => date('Y-m-d'),
            'update_at' => date('Y-m-d'),
        );

        $zone = $db->updateAction("zones", $arr_values, "id='".$id."'");
        if (isset($_POST['prov'])) {
            $formed_array = array_unique($_POST['prov']);
            $db->deleteAction("zone_province", "zone_id='".$zone[0]['id']."'");
            foreach ($formed_array as $val) {
                $arr_values = array(
                    'province_id' => $val,
                    'zone_id' => $zone[0]['id']
                );

                $db->insertAction("zone_province", $arr_values);
            }
        }

        // Se guardan los países
        $flag = false;
        if (isset($_POST['country'])) {
            $db->deleteAction("zone_country", "zone_id='".$zone[0]['id']."'");
            foreach ($_POST['country'] as $val) {
                if ($val == 73) { $flag = true;}
                $arr_values = array(
                    'country_id' => $val,
                    'zone_id' => $zone[0]['id']
                );

                $db->insertAction("zone_country", $arr_values);
            }

        }
        // Se eliminan las provincias de la zona si no tiene a españa
        if (!$flag) { $db->deleteAction("zone_province", "zone_id='".$zone[0]['id']."'"); }

        $this->addLogs(sprintf("Editando entrada zones - id: %d", $zone[0]['id']));
        return $zone[0];
    }

    /**
     * Retorna el tag de la provincia
     * @param  integer $id ID de la provincia
     * @return string
     */
    public function getProvince($id)
    {
        $db = $this->db;

        $s = "SELECT * FROM province WHERE id='".$id."'";
        $arr_province = $db->fetchSQL($s);

        $province_name = $arr_province[0]['name'];

        $tag = '<div class="pull-left css-marginR10 css-marginB10">
            <div class="input-group">
                <span class="input-group-addon">
                    <input type="checkbox" id="prov_'.$id.'" name="prov[]" value="'.$id.'" checked>
                </span>
                <input type="text" class="form-control" value="'.$province_name.'" disabled>
            </div>
        </div>';

        return $tag;
    }
}
?>
