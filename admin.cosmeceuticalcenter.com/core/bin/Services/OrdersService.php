<?php
/**
 * Servicios de las ordenes
 */
class OrdersService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Agrega una entrada
     * @return void
     */
    public function persist()
    {
        $db = $this->db;
        $calculate = $this->calculate($_POST['province'], $_POST['country']);

        $arr_values = array(
            'sub_total' => $calculate['sub_total'],
            'iva' => "",
            'iva_percentage' => "",
            'total' => $calculate['total'],
            'status_id' => 1,
            'user_id' => $_POST['user'],
            'country_id' => $_POST['country'],
            'province_id' => $_POST['province'],
            'shipping' => $calculate['shipping'],
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s'),
        );

        $order = $db->insertAction("orders", $arr_values);
        $arr_value = array('code' => 'OR'.$order[0]['id'].$_POST['user'].'-'.$calculate['total_products']);
        $order = $db->updateAction("orders", $arr_value, "id='".$order[0]['id']."'");

        $this->saveDetail($order[0]);

        $this->addLogs(sprintf("Agregando orden id: %d - Código", $order[0]['id'], $order[0]['code']));
        return $order[0];
    }

    /**
     * Edita una entrada
     * @param  integer $id ID de la entrada
     * @return void
     */
    public function update($id)
    {
        $db = $this->db;
        $current_status = $db->getValue("status_id", "orders", "id='".$id."'");

        $arr_values = array(
            'status_id' => $_POST['status'],
            'update_at' => date('Y-m-d'),
        );

        $result = $db->updateAction("orders", $arr_values, "id='".$id."'");

        if ($result[0]['status_id'] == 3) {
            $this->setNotifications($result[0]);
        }

        if ($_POST['company'] != "" || $_POST['tracking_number'] != "") {
            $db->deleteAction("tracking_information", "order_id='".$id."'");
            $arr_values = array(
                'company' => secure_mysql($_POST['company']),
                'tracking_number' => secure_mysql($_POST['tracking_number']),
                'order_id' => $id,
            );
            $db->insertAction("tracking_information", $arr_values);
            $this->sendEmail($result[0]);
        }

        if ($current_status != $_POST['status']) {
            $this->sendEmail($result[0]);
        }

        $this->addLogs(sprintf("Editando Orden de compra id: %d - Código: %s", $result[0]['id'], $result[0]['code']));
        return $result[0];
    }

    /**
     * Agrega las notificaciones
     * @param array $order Arreglo de la orden
     * @return void
     */
    public function setNotifications($order)
    {
        $db = $this->db;

        // Se crea la notificación de orden finalizada
        $arr_values = array(
            'user_id' => $order['user_id'],
            'title' => 'Orden '.$order['code'].' ha finalizado',
            'description' => 'La orden '.$order['code'].' se encuentra finalizada, gracias por tu compra',
            'status' => '0',
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s')
        );

        $db->insertAction('notifications', $arr_values);

        // Se crean las notificaciones de los review
        $s = "SELECT * FROM view_orders_detail WHERE order_id='".$order['id']."'";
        $arr_detail = $db->fetchSQL($s);

        foreach ($arr_detail as $key => $val) {
            // Se crea el feature
            $arr_values = array(
                'user_id' => $order['user_id'],
                'product_id' => $val['product_id'],
                'order_id' => $order['id'],
                'status' => '0',
                'create_at' => date('Y-m-d H:i:s'),
                'update_at' => date('Y-m-d H:i:s')
            );

            $feature = $db->insertAction('features', $arr_values);

            // Se crea la notificación
            $arr_values = array(
                'user_id' => $order['user_id'],
                'title' => '¿Qué te ha parecido el producto '.$val['product_name'].'?',
                'description' => 'Nos gustaria saber tu opinion acerca del producto: '.$val['product_name'],
                'status' => '0',
                'link' => BASE_URL.'OPINION/'.$feature[0]['id'],
                'create_at' => date('Y-m-d H:i:s'),
                'update_at' => date('Y-m-d H:i:s')
            );

            $db->insertAction('notifications', $arr_values);
        }
    }

    /**
     * Agrega los productos al crear una orden
     * @param integer $product_id ID del producto
     * @return string
     */
    public function addProduct($product_id)
    {
        $db = $this->db;

        $s = "SELECT * FROM view_products WHERE id='".$product_id."'";
        $arr_product = $db->fetchSQL($s);

        if (isset($_SESSION['ORDER_PRODUCT'])) {
            $exist = false; $pos = 0; $quantity = 1;

            foreach ($_SESSION['ORDER_PRODUCT'] as $key => $val) {
                if (isset($val['id'])) {
                    if ($val['id'] == $product_id) {
                        $exist = true;
                        $pos = $key;
                        $quantity = $val['quantity'];
                    }
                }
            }

            if ($exist) {
                $arr_product[0]['quantity'] = $quantity + 1;
                $_SESSION['ORDER_PRODUCT'][$pos] = $arr_product[0];
            } else {
                $arr_product[0]['quantity'] = 1;
                $_SESSION['ORDER_PRODUCT'][count($_SESSION['ORDER_PRODUCT'])] = $arr_product[0];
            }

            foreach ($_SESSION['ORDER_PRODUCT'] as $key => $val) {
                if (isset($val['id'])) {
                    // Se verifica su precio en las tablas de descuentos
                    $cnt_discount = $db->getCount('discount', "product_id='".$val['id']."'");
                    $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$val['id']."'");
                    $new_price = NULL;

                    if ($cnt_discount == 1) {
                        $new_price = $db->getValue("new_price", "discount", "product_id='".$val['id']."'");
                    } elseif ($cnt_weekly == 1) {
                        $new_price = $db->getValue("new_price", "weekly_recommendation", "product_id='".$val['id']."'");
                    }

                    if ($new_price != "") {
                        $p_sub_total = number_format(($new_price * $val['quantity']),2,".","");
                    } else {
                        $p_sub_total = number_format(($val['price'] * $val['quantity']),2,".","");
                    }

                    $_SESSION['ORDER_PRODUCT'][$key]['new_price'] = $new_price;
                    $_SESSION['ORDER_PRODUCT'][$key]['sub_total'] = $p_sub_total;
                }
            }

        } else {
            // Se verifica su precio en las tablas de descuentos
            $cnt_discount = $db->getCount('discount', "product_id='".$product_id."'");
            $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$product_id."'");
            $new_price = NULL;

            if ($cnt_discount == 1) {
                $new_price = $db->getValue("new_price", "discount", "product_id='".$product_id."'");
            } elseif ($cnt_weekly == 1) {
                $new_price = $db->getValue("new_price", "weekly_recommendation", "product_id='".$product_id."'");
            }

            if ($new_price != "") {
                $arr_product[0]['sub_total'] = $new_price;
            } else {
                $arr_product[0]['sub_total'] = $arr_product[0]['price'];
            }

            $arr_product[0]['new_price'] = $new_price;
            $arr_product[0]['quantity'] = 1;
            $_SESSION['ORDER_PRODUCT'][0] = $arr_product[0];
        }

        return $this->getHTML();
    }

    /**
     * Elimina un producto de la orden
     * @param  integer $product_id ID del producto
     * @return string
     */
    public function removeProduct($product_id)
    {
        $db = $this->db;
        $arr = $_SESSION['ORDER_PRODUCT'];

        foreach ($arr as $key => $val) {
            if (isset($val['id'])) {
                if ($val['id'] == $product_id) {
                    $total = $val['quantity'] - 1;

                    if ($total >= 1) {
                        $_SESSION['ORDER_PRODUCT'][$key]['quantity'] = $total;

                        // Se verifica su precio en las tablas de descuentos
                        $cnt_discount = $db->getCount('discount', "product_id='".$val['id']."'");
                        $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$val['id']."'");
                        $new_price = NULL;

                        if ($cnt_discount == 1) {
                            $new_price = $db->getValue("new_price", "discount", "product_id='".$val['id']."'");
                        } elseif ($cnt_weekly == 1) {
                            $new_price = $db->getValue("new_price", "weekly_recommendation", "product_id='".$val['id']."'");
                        }

                        if ($new_price != "") {
                            $p_sub_total = number_format(($new_price * $_SESSION['ORDER_PRODUCT'][$key]['quantity']),2,".","");
                        } else {
                            $p_sub_total = number_format(($val['price'] * $_SESSION['ORDER_PRODUCT'][$key]['quantity']),2,".","");
                        }

                        $_SESSION['ORDER_PRODUCT'][$key]['new_price'] = $new_price;
                        $_SESSION['ORDER_PRODUCT'][$key]['sub_total'] = $p_sub_total;
                    } else {
                        unset($_SESSION['ORDER_PRODUCT'][$key]);
                        // $_SESSION['ORDER_PRODUCT'][$key] = "";
                    }
                }
            }
        }

        return $this->getHTML();
    }

    /**
     * Obtiene la tabla con los productos agregados
     * @return string
     */
    public function getHTML()
    {
        $arr = $_SESSION['ORDER_PRODUCT'];
        // showArr($_SESSION['ORDER_PRODUCT']); exit;

        foreach ($arr as $key => $val) {
            $product_iva = ($val['iva']) ? $val['iva'] : APP_IVA;
            $price = ($val['new_price']) ? $val['new_price'] : $val['price'];
            $sub_total = number_format(($price * $val['quantity']),2,".","");
            $d_iva = number_format((($product_iva * $sub_total) / 100),2,".","");


            $arr[$key]['formed_iva'] = "(".$product_iva."%)".$d_iva;
            $arr[$key]['formed_total'] = number_format(($sub_total + $d_iva),2,".","");
        }

        ob_start();
        include('html/ajx_show/products-order.php');
        return ob_get_clean();
    }

    /**
     * Calcula la orden de compra
     * @return array
     */
    public function calculate($province_id = NULL, $country_id = NULL)
    {
        $db = $this->db;
        $iva = 0;
        $sub_total = 0;
        $total = 0;
        $t_products = 0;
        $shipping = 0;

        if (isset($_SESSION['ORDER_PRODUCT'])) {
            foreach ($_SESSION['ORDER_PRODUCT'] as $key => $val) {
                if (isset($val['id'])) {
                    $t_products++;
                    $cnt_discount = $db->getCount('discount', "product_id='".$val['id']."'");
                    $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$val['id']."'");
                    $new_price = "";

                    if ($cnt_discount == 1) {
                        $new_price = $db->getValue("new_price", "discount", "product_id='".$val['id']."'");
                    } elseif ($cnt_weekly == 1) {
                        $new_price = $db->getValue("new_price", "weekly_recommendation", "product_id='".$val['id']."'");
                    }

                    if ($new_price != "") {
                        $p_sub_total = number_format(($new_price * $val['quantity']),2,".","");
                    } else {
                        $p_sub_total = number_format(($val['price'] * $val['quantity']),2,".","");
                    }

                    $product_iva = ($val['iva']) ? $val['iva'] : APP_IVA;
                    $s_iva  = number_format((($product_iva * $p_sub_total) / 100),2,".","");
                    $p_sub_total = number_format(($p_sub_total + $s_iva),2,".","");
                    $sub_total += $p_sub_total;
                }
            }
        }

        if ($country_id && $country_id != "") {
            if ($country_id == 73) {
                if ($province_id && $province_id != "") {
                    $shipping = $db->getValue("value", "view_zones", "province_id='".$province_id."'");
                }
            } else {
                $shipping = $db->getValue("value", "view_zones_country", "country_id='".$country_id."'");
            }
        }



        // $sub_total = number_format($sub_total,2,".","");
        // $iva = number_format(((APP_IVA * $sub_total) / 100),2,".","");
        // $total = @number_format(($sub_total + $iva),2,".","");

        $sub_total = number_format($sub_total,2,".","");
        $total = number_format($sub_total + $shipping,2,".","");

        return array('sub_total' => $sub_total, 'shipping' => $shipping, 'total' => $total, 'total_products' => $t_products);
    }

    /**
     * Guarda el detalle de la orden
     * @param  array $order Arreglo de la orden
     * @return void
     */
    public function saveDetail($order)
    {
        $db = $this->db;

        if (isset($_SESSION['ORDER_PRODUCT'])) {
            foreach ($_SESSION['ORDER_PRODUCT'] as $key => $val) {
                if (isset($val['id'])) {
                    $p_iva = ($val['iva']) ? $val['iva'] : APP_IVA;
                    if ($val['new_price']) {
                        $iva = number_format((($p_iva * ($val['new_price'] * $val['quantity'])) / 100),2,".","");
                        $sub_total = number_format(($val['new_price'] * $val['quantity']),2,".","");
                    } else {
                        $iva = number_format((($p_iva * ($val['price'] * $val['quantity'])) / 100),2,".","");
                        $sub_total = number_format(($val['price'] * $val['quantity']),2,".","");
                    }

                    $arr_values = array(
                        'product_id' => $val['id'],
                        'order_id' => $order['id'],
                        'quantity' => $val['quantity'],
                        'price' => $val['price'],
                        'new_price' => ($val['new_price']) ? $val['new_price'] : "",
                        'product_iva' => $p_iva,
                        'iva' => $iva,
                        'sub_total' => $sub_total + $iva,
                        'user_id' => $order['user_id']
                    );

                    $db->insertAction("order_detail", $arr_values);
                }
            }
        }
    }

    /**
     * Envía los emails
     * @param  array $order Arreglo con la información de la orden
     * @return void
     */
    public function sendEmail($order)
    {
        $db = $this->db;
        $mail = new PHPMailer(true);

        // Usuario
        $s = "SELECT * FROM view_user WHERE id='".$order['user_id']."'";
        $arr_user = $db->fetchSQL($s);
        $u_names = $arr_user[0]['name']." ".$arr_user[0]['last_name'];

        // Tracking
        $s = "SELECT * FROM tracking_information WHERE order_id='".$order['id']."'";
        $arr_tracking = $db->fetchSQL($s);
        $company = @$arr_tracking[0]['company'];
        $tracking_number = @$arr_tracking[0]['tracking_number'];

        $status_name = $db->getValue("name", "order_status", "id='".$order['status_id']."'");

        ob_start();
        include('html/email/order-info.php');
        $template = ob_get_clean();

        //Server settings
        // $mail->SMTPDebug = 2;
        $mail->isSMTP();
        $mail->Host = EMAIL_HOST;
        $mail->SMTPAuth = true;
        $mail->Username = EMAIL_SENDER;
        $mail->Password = EMAIL_PASSWORD;
        $mail->SMTPSecure = 'tls';
        // $mail->Port = EMAIL_PORT;
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        //Recipients
        $mail->setFrom(EMAIL_SENDER, 'Cosmeceutical center');
        $mail->addAddress($arr_user[0]['email'], $u_names);

        //Content
        $mail->isHTML(true);
        $mail->Subject = 'Actualizacion de la Orden | Cosmeceuticalcenter';
        $mail->Body    = $template;
        $mail->AltBody = $template;

        $mail->send();
    }
}
?>
