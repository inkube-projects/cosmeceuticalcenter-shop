<?php
/**
 * Servicios
 */
class BrandsService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Agrega una entrada
     * @return void
     */
    public function persist()
    {
        $db = $this->db;

        $arr_values = array(
            'name' => $_POST['name'],
            'description' => $_POST['description'],
            'status' => $_POST['status'],
            'iva' => $_POST['iva'],
            'name_canonical' => friendlyURL($_POST['name'], "upper"),
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s')
        );

        $brand = $db->insertAction("brands", $arr_values);
        if (isset($_FILES['image']['tmp_name'])) {
            $this->persistImage($brand, 'image');
        }
        $this->addLogs(sprintf("Agregando marca id: %d - %s", $brand[0]['id'], $brand[0]['name']));

        return $brand[0];
    }

    /**
     * Edita una entrada
     * @param  integer $id ID de la entrada
     * @return void
     */
    public function update($id)
    {
        $db = $this->db;

        $arr_values = array(
            'name' => $_POST['name'],
            'description' => $_POST['description'],
            'status' => $_POST['status'],
            'iva' => $_POST['iva'],
            'name_canonical' => friendlyURL($_POST['name'], "upper"),
            'update_at' => date('Y-m-d H:i:s')
        );

        $brand = $db->updateAction("brands", $arr_values, "id='".$id."'");
        if (isset($_FILES['image']['tmp_name'])) {
            $this->persistImage($brand, 'image');
        }

        // Se actualiza el IVA de los productos
        $db->updateAction("products", array('iva' => $_POST['iva'], 'update_at' => date('Y-m-d H:i:s')), "brand_id='".$id."'");
        // Se editan los precios
        $this->setProductsPrice($id, $brand[0]['iva']);

        $this->addLogs(sprintf("Editando marca id: %d - %s", $brand[0]['id'], $brand[0]['name']));
        return $brand[0];
    }

    /**
     * Guarda las imagenes
     * @param  array  $brand   Arreglo de la imagen
     * @param  string $parameter  Nombre del parametro del formulario
     * @return void|exception
     */
    public function persistImage(array $brand, $parameter)
    {
        if ($_FILES[$parameter]['tmp_name'] != "") {
            $path = APP_SYSTEM_BRANDS;

            if (!empty($_FILES[$parameter]['tmp_name'])) {
                $img_type = $_FILES[$parameter]['type'];

                $extens = str_replace("image/",".",$_FILES[$parameter]['type']);
                $file_name = "brand_".rand(00000, 99999).uniqid().$extens;

                if(move_uploaded_file($_FILES[$parameter]['tmp_name'], $path.$file_name)){
                    if ($brand[0]['image']) {
                        @unlink($path.$brand[0]['image']);
                    }

                    $this->db->updateAction("brands", array('image' => $file_name), "id='".$brand[0]['id']."'");
                }
            }
        }
    }

    /**
     * Edita el precio del producto
     * @param integer $brand_id  [description]
     * @param integer $brand_iva [description]
     * @return void
     */
    public function setProductsPrice($brand_id, $brand_iva)
    {
        $db = $this->db;
        $s = "SELECT * FROM products WHERE brand_id='".$brand_id."'";
        $arr_products = $db->fetchSQL($s);
        $part_iva = 100 - $brand_iva;


        foreach ($arr_products as $key => $val) {
            $iva = number_format((($brand_iva * $val['price']) / 100),2, ".", "");
            $total_price = number_format(($val['price'] + $iva),2,".","");

            // if ($val['id'] == 3) {
            //     showArr(($brand_iva * $val['price'])); exit;
            // }

            $arr_values = array(
                'price_iva' => $total_price,
                'update_at' => date('Y-m-d H:i:s')
            );
            $db->updateAction("products", $arr_values, "id='".$val['id']."'");
        }
    }
}
?>
