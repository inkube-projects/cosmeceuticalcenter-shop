<?php
/**
 * Servicios
 */
class SeoService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Agrega una entrada
     * @return void
     */
    public function persist()
    {
        $db = $this->db;

        $arr_values = array(
            'keywords' => secure_mysql($_POST['keywords']),
            'description' => secure_mysql($_POST['description']),
            'section_id' => $_POST['section'],
        );

        $db->deleteAction("seo", "section_id='".$_POST['section']."'");
        $result = $db->insertAction("seo", $arr_values);

        $this->addLogs(sprintf("Agregando entrada seo - id: %d", $result[0]['id']));
        return $result[0];
    }

    /**
     * Edita una entrada
     * @param  integer $id ID de la entrada
     * @return void
     */
    public function update($id)
    {
        $db = $this->db;

        $arr_values = array(
            'keywords' => secure_mysql($_POST['keywords']),
            'description' => secure_mysql($_POST['description']),
            'section_id' => $_POST['section'],
        );

        $db->deleteAction("seo", "section_id='".$_POST['section']."'");
        $result = $db->insertAction("seo", $arr_values);

        $this->addLogs(sprintf("Editando entrada seo - id: %d", $result[0]['id']));
        return $result[0];
    }
}
?>
