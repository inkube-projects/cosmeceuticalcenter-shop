<?php
/**
 * Servicios
 */
class FeaturesService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Agrega una entrada
     * @return void
     */
    public function persist()
    {
        $db = $this->db;

        $arr_values = array(
            'user_id' => $_POST['user_id'],
            'product_id' => $_POST['product_id'],
            'order_id' => $_POST['order_id'],
            'review' => $_POST['review'],
            'score' => $_POST['score'],
            'status' => $_POST['status'],
            'create_at' => date('Y-m-d'),
            'update_at' => date('Y-m-d'),
        );

        $result = $db->insertAction("features", $arr_values);
        $this->addLogs(sprintf("Agregando entrada features - id: %d", $result[0]['id']));

        return $result[0];
    }

    /**
     * Edita una entrada
     * @param  integer $id ID de la entrada
     * @return void
     */
    public function update($id)
    {
        $db = $this->db;

        $arr_values = array(
            'review' => $_POST['review'],
            'status' => $_POST['status'],
            'update_at' => date('Y-m-d'),
        );

        $result = $db->updateAction("features", $arr_values, "id='".$id."'");
        $this->addLogs(sprintf("Editando entrada features - id: %d", $result[0]['id']));

        return $result[0];
    }
}
?>
