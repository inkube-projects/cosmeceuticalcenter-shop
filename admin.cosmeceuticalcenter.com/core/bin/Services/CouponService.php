<?php

/**
 * Servicio para os cupones
 */
class CouponService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Agrega una entrada
     * @param  string $init_date Fecha inicial
     * @param  string $end_date  Fecha final
     * @return array
     */
    public function persist($init_date, $end_date)
    {
        $db = $this->db;

        $arr_values = array(
            'name' => secure_mysql($_POST['name']),
            'description' => secure_mysql($_POST['description']),
            'discount' => secure_mysql($_POST['discount']),
            'uses' => secure_mysql($_POST['uses']),
            'init_date' => to_date($init_date),
            'end_date' => to_date($end_date),
            'canonical_id' => secure_mysql($_POST['canonical_id']),
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s')
        );

        $coupon = $db->insertAction("coupon", $arr_values);

        $this->addLogs(sprintf("Agregando cupon id: %d - %s", $coupon[0]['id'], $coupon[0]['name']));
        return $coupon[0];
    }

    /**
     * Edita una entrada
     * @param  integer $id        [description]
     * @param  string $init_date [description]
     * @param  string $end_date  [description]
     * @return array
     */
    public function update($id, $init_date, $end_date)
    {
        $db = $this->db;
        $arr_values = array(
            'name' => secure_mysql($_POST['name']),
            'description' => secure_mysql($_POST['description']),
            'discount' => secure_mysql($_POST['discount']),
            'uses' => secure_mysql($_POST['uses']),
            'init_date' => to_date($init_date),
            'end_date' => to_date($end_date),
            'update_at' => date('Y-m-d H:i:s')
        );

        $coupon = $db->updateAction("coupon", $arr_values, "id='".$id."'");

        $this->addLogs(sprintf("Editando cupon id: %d - %s", $coupon[0]['id'], $coupon[0]['name']));
        return $coupon[0];
    }
}


?>
