<?php
/**
 * Servicios
 */
class ProductsService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Agrega una entrada
     * @return void
     */
    public function persist()
    {
        $db = $this->db;
        $price = number_format($_POST['price'],2,".","");
        // if ($_POST['iva'] && $_POST['iva'] > 0 && $_POST['iva'] != "") {
        //     $iva = ($_POST['iva'] * $price) / 100;
        // } else {
        //     $iva = 0;
        // }
        // $price_without_iva = number_format(($price - $iva),2,".","");
        $ext = "1.".$_POST['iva'];
        $price_without_iva = number_format(($price / $ext),2,".","");

        $arr_values = array(
            'name' => secure_mysql(addslashes($_POST['name'])),
            'presentation' => secure_mysql(addslashes($_POST['presentation'])),
            'description' => secure_mysql(addslashes($_POST['description'])),
            'description_short' => secure_mysql(addslashes($_POST['description_short'])),
            'active_principles' => secure_mysql(addslashes($_POST['active_principles'])),
            'prescription' => secure_mysql(addslashes($_POST['prescription'])),
            'use_recommendations' => secure_mysql(addslashes($_POST['use_recommendations'])),
            'results_benefits' => secure_mysql(addslashes($_POST['results_benefits'])),
            'price' => $price_without_iva,
            'price_iva' => $price,
            'iva' => $_POST['iva'],
            'content' => $_POST['content'],
            'tags' => secure_mysql(mb_convert_case($_POST['tags'], MB_CASE_UPPER, "UTF-8")),
            'status' => $_POST['status'],
            'brand_id' => $_POST['brand'],
            'novelty' => $_POST['novelty'],
            'discount_promo' => $_POST['discount_promo'],
            'weekly_recommendation' => $_POST['weekly_recommendation'],
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d'),
        );

        $product = $db->insertAction("products", $arr_values);

        if (is_array($_POST['category'])) {
            foreach ($_POST['category'] as $cat) {
                $arr = array(
                    'product_id' => $product[0]['id'],
                    'category_id' => $cat,
                );

                $db->insertAction("products_categories", $arr);
            }
        }

        if (isset($_POST['subcategory'])) {
            if (is_array($_POST['subcategory'])) {
                foreach ($_POST['subcategory'] as $sub) {
                    $arr = array(
                        'product_id' => $product[0]['id'],
                        'subcategory_id' => $sub,
                    );

                    $db->insertAction("products_subcategories", $arr);
                }
            }
        }

        if (isset($_POST['size'])) {
            if (is_array($_POST['size'])) {
                foreach ($_POST['size'] as $size) {
                    $arr = array(
                        'product_id' => $product[0]['id'],
                        'size_id' => $size
                    );

                    $db->insertAction("products_sizes", $arr);
                }
            }
        }

        mkdir(APP_SYSTEM_PRODUCTS."product_".$product[0]['id']."/");
        mkdir(APP_SYSTEM_PRODUCTS."product_".$product[0]['id']."/cover/");
        mkdir(APP_SYSTEM_PRODUCTS."product_".$product[0]['id']."/gallery/");

        if ($_POST['hid-name'] != "") {
            $this->saveImage(
                $_POST['hid-name'],
                "",
                APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/cover/",
                APP_SYSTEM_PRODUCTS."product_".$product[0]['id']."/cover/",
                "products",
                array("cover_image" => $_POST['hid-name']),
                "id='".$product[0]['id']."'"
            );
        }
        // SEO
        $arr_values = array(
            'title' => secure_mysql(addslashes($_POST['seo_title'])),
            'keywords' => secure_mysql(addslashes($_POST['seo_keywords'])),
            'description' => secure_mysql(addslashes($_POST['seo_description'])),
            'product_id' => $product[0]['id']
        );

        $db->insertAction("products_seo", $arr_values);

        // Se guarda el inventario
        $arr_values = array(
            'product_id' => $product[0]['id'],
            'quantity' => "0",
            'unity_id' => "1",
            'update_at' => date('Y-m-d H:i:s')
        );
        $db->insertAction("inventory", $arr_values);

        // Se guarda como novedad
        if ($_POST['novelty'] == "1") {
            $date_init = date('Y-m-d');
            $date_end = strtotime('+30 day', strtotime($date_init));
            $date_end = date('Y-m-d', $date_end);
            $arr_values = array(
                'date_init' => $date_init,
                'date_end' => $date_end,
                'product_id' => $product[0]['id'],
                'create_at' => date('Y-m-d H:i:s')
            );
            $db->insertAction("novelty", $arr_values);
        }

        // Se guarda como promoción / descuento
        if ($_POST['discount_promo'] == 1) {
            $range = explode(" al ", $_POST['discount_date_range']);
            $init = to_date($range[0]);
            $end = to_date($range[1]);
            $price = @number_format($_POST['discount_price'],2,".","");

            $arr_values = array(
                'product_id' => $product[0]['id'],
                'new_price' => $price,
                'date_init' => $init,
                'date_end' => $end,
                'create_at' => date('Y-m-d H:i:s')
            );
            $db->insertAction("discount", $arr_values);
        }

        // Se guarda como recomendación semanal
        if ($_POST['weekly_recommendation'] == 1) {
            $range = explode(" al ", $_POST['weekly_date_range']);
            $init = to_date($range[0]);
            $end = to_date($range[1]);
            if ($_POST['weekly_price']) {
                $price = @number_format($_POST['weekly_price'],2,".","");
            } else {
                $price = @number_format($_POST['price'],2,".","");
            }


            $arr_values = array(
                'product_id' => $product[0]['id'],
                'new_price' => $price,
                'date_init' => $init,
                'date_end' => $end,
                'create_at' => date('Y-m-d H:i:s')
            );
            $db->insertAction("weekly_recommendation", $arr_values);
        }

        $this->addLogs(sprintf("Agregando producto id: %d - %s", $product[0]['id'], $product[0]['name']));
        return $product[0];
    }

    /**
     * Edita una entrada
     * @param  integer $id ID de la entrada
     * @return void
     */
    public function update($id)
    {
        $db = $this->db;
        $price = number_format($_POST['price'],2,".","");
        // if ($_POST['iva'] && $_POST['iva'] > 0 && $_POST['iva'] != "") {
        //     $iva = number_format((($_POST['iva'] * $price) / 100),2,".","");
        // } else {
        //     $iva = 0;
        // }
        // $price_without_iva = number_format(($price - $iva),2,".","");
        $ext = "1.".$_POST['iva'];
        $price_without_iva = number_format(($price / $ext),2,".","");

        $arr_values = array(
            'name' => secure_mysql(addslashes($_POST['name'])),
            'presentation' => secure_mysql(addslashes($_POST['presentation'])),
            'description' => secure_mysql(addslashes($_POST['description'])),
            'description_short' => secure_mysql(addslashes($_POST['description_short'])),
            'active_principles' => secure_mysql(addslashes($_POST['active_principles'])),
            'prescription' => secure_mysql(addslashes($_POST['prescription'])),
            'use_recommendations' => secure_mysql(addslashes($_POST['use_recommendations'])),
            'results_benefits' => secure_mysql(addslashes($_POST['results_benefits'])),
            'price' => $price_without_iva,
            'price_iva' => $price,
            'iva' => $_POST['iva'],
            'content' => $_POST['content'],
            'tags' => secure_mysql(mb_convert_case($_POST['tags'], MB_CASE_UPPER, "UTF-8")),
            'status' => $_POST['status'],
            'brand_id' => $_POST['brand'],
            'novelty' => $_POST['novelty'],
            'discount_promo' => $_POST['discount_promo'],
            'weekly_recommendation' => $_POST['weekly_recommendation'],
            'update_at' => date('Y-m-d H:i:s'),
        );

        $product = $db->updateAction("products", $arr_values, "id='".$id."'");

        $db->deleteAction('products_categories', "product_id='".$id."'");
        if (is_array($_POST['category'])) {
            foreach ($_POST['category'] as $cat) {
                $arr = array(
                    'product_id' => $product[0]['id'],
                    'category_id' => $cat,
                );

                $db->insertAction("products_categories", $arr);
            }
        }

        $db->deleteAction('products_subcategories', "product_id='".$id."'");
        if (isset($_POST['subcategory'])) {
            if (is_array($_POST['subcategory'])) {
                foreach ($_POST['subcategory'] as $sub) {
                    $arr = array(
                        'product_id' => $product[0]['id'],
                        'subcategory_id' => $sub,
                    );

                    $db->insertAction("products_subcategories", $arr);
                }
            }
        }

        $db->deleteAction('products_sizes', "product_id='".$id."'");
        if (isset($_POST['size'])) {
            if (is_array($_POST['size'])) {
                foreach ($_POST['size'] as $size) {
                    $arr = array(
                        'product_id' => $product[0]['id'],
                        'size_id' => $size
                    );

                    $db->insertAction("products_sizes", $arr);
                }
            }
        }

        if ($_POST['hid-name'] != "") {
            $this->saveImage(
                $_POST['hid-name'],
                $product[0]['cover_image'],
                APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/cover/",
                APP_SYSTEM_PRODUCTS."product_".$product[0]['id']."/cover/",
                "products",
                array("cover_image" => $_POST['hid-name']),
                "id='".$id."'"
            );
        }

        // SEO
        $db->deleteAction("products_seo", "product_id='".$id."'");
        $arr_values = array(
            'title' => secure_mysql(addslashes($_POST['seo_title'])),
            'keywords' => secure_mysql(addslashes($_POST['seo_keywords'])),
            'description' => secure_mysql(addslashes($_POST['seo_description'])),
            'product_id' => $id
        );
        $db->insertAction("products_seo", $arr_values);

        // Se guarda el inventario si no posee entrada
        $cnt_val = $db->getCount("inventory", "product_id='".$product[0]['id']."'");
        if ($cnt_val == 0) {
            $arr_values = array(
                'product_id' => $product[0]['id'],
                'quantity' => "0",
                'unity_id' => "1",
                'update_at' => date('Y-m-d H:i:s')
            );
            $db->insertAction("inventory", $arr_values);
        }

        // Se guarda como novedad
        $db->deleteAction("novelty", "product_id='".$id."'");
        if ($_POST['novelty'] == "1") {
            $range = explode(" al ", $_POST['novelty_date_range']);
            if (!isset($range[0]) || !isset($range[1])) {
                throw new Exception("Debes agregar el rango de fechas para la Novedad", 1);
            }
            $init = to_date($range[0]);
            $end = to_date($range[1]);
            $arr_values = array(
                'date_init' => $init,
                'date_end' => $end,
                'product_id' => $id,
                'create_at' => date('Y-m-d H:i:s')
            );
            $db->insertAction("novelty", $arr_values);
        }

        // Se guarda como promoción / descuento
        $db->deleteAction("discount", "product_id='".$id."'");
        if ($_POST['discount_promo'] == 1) {
            $range = explode(" al ", $_POST['discount_date_range']);
            $init = to_date($range[0]);
            $end = to_date($range[1]);
            $price = @number_format($_POST['discount_price'],2,".","");

            $arr_values = array(
                'product_id' => $id,
                'new_price' => $price,
                'date_init' => $init,
                'date_end' => $end,
                'create_at' => date('Y-m-d H:i:s')
            );
            $db->insertAction("discount", $arr_values);
        }

        // Se guarda como recomendación semanal
        $db->deleteAction("weekly_recommendation", "product_id='".$id."'");
        if ($_POST['weekly_recommendation'] == 1) {
            $range = explode(" al ", $_POST['weekly_date_range']);
            $init = to_date($range[0]);
            $end = to_date($range[1]);
            if ($_POST['weekly_price']) {
                $price = @number_format($_POST['weekly_price'],2,".","");
            } else {
                $price = @number_format($_POST['price'],2,".","");
            }

            $arr_values = array(
                'product_id' => $id,
                'new_price' => $price,
                'date_init' => $init,
                'date_end' => $end,
                'create_at' => date('Y-m-d H:i:s')
            );
            $db->insertAction("weekly_recommendation", $arr_values);
        }
        // Se verifican los colores
        if (isset($_POST['cnt_color'])) {
            $cnt_color = @number_format($_POST['cnt_color'],0,"","");


            for ($i = 1; $i <= $cnt_color; $i++) {
                $IDcolor = @number_format($_POST['color_'.$i],0,"","");
                if ($IDcolor != 0 && $IDcolor != "") {
                    $cnt_val = $db->getCount("products_colors", "product_id='".$id."' AND color_id='".$IDcolor."'");
                    if ($cnt_val == 0) { // si el color no ha sido agregado se procesa sino se ignora
                        $arr_values_c = array(
                            'product_id' => $id,
                            'color_id' => $IDcolor
                        );

                        $db->insertAction("products_colors", $arr_values_c);
                        // Se cargan las imagenes
                    }
                    $this->uploadImagesColors("gallery_color_".$i, $id, $IDcolor);
                }
            }
        }

        $this->addLogs(sprintf("Editando entrada products - id: %d", $product[0]['id']));
        return $product[0];
    }

    /**
    * Prepara imagen
    * @param  integer $acc Acción a ejecutar [1: Guarda la imagen base, 2: Recorta la imagen]
    * @return object
    */
    public function prepareCoverImage($acc)
    {
        if ($acc == 1) {
            $tempIMG = $this->prepareCoverTemp();
            $form = $this->showFormCoverIMG($tempIMG);
            return $form;
        } elseif ($acc == 2) {
            $directory_temp = APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/cover/";
            $directory_profile = APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/cover/";
            return $this->createIMG($directory_temp, $directory_profile);
        }
    }

    /**
    * Guarda la imagen base
    * @return string
    */
    public function prepareCoverTemp()
    {
        // se elimina las imagenes temporales que esten en la carpeta
        $dir = APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/cover/";
        $handle = opendir($dir);
        while ($file = readdir($handle)){
            if (is_file($dir.$file)) {
                @unlink($dir.$file);
            }
        }

        // se sube la nueva imagen temporal
        $extens = str_replace("image/",".",$_FILES['cover_image']['type']);
        $nb_img = "cover_".rand(00000, 99999).uniqid().$extens;
        list($width, $height) = getimagesize($_FILES["cover_image"]['tmp_name']);

        if($width > 1000){
            ImagenTemp('cover_image', 1000, $nb_img, $dir);
        }else{
            ImagenTemp('cover_image', $width, $nb_img, $dir);
        }

        return $nb_img;
    }

    /**
    * Retorna el fomulario de la imagen de perfil a recortar
    * @param  string $img_name Nombre asignado a la imagen
    * @return string
    */
    public function showFormCoverIMG($img_name)
    {
        $directory = APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/cover/";
        $directory_post = $directory;
        $path = "ajx/adm/products/4/0";

        list($width, $height) = getimagesize($directory.$img_name);

        if($height > 230) {
            $altura = 230;
        } else {
            $altura = $height;
        }
        if($width > 360) {
            $ancho = 360;
        } else {
            $ancho = $width;
        }

        ob_start();
        include('html/ajx_show/jcrop-image.php');

        return ob_get_clean();
    }

    /**
     * Guardando imagenes temporales
     * @return void
     */
    public function uploadImages()
    {
        $arr_lenght = count($_FILES['gallery_images']['tmp_name']);

        // se elimina las imagenes temporales que esten en la carpeta
        $path = APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/gallery/";

        for ($i = 0; $i < $arr_lenght; $i++) {
            // se sube la nueva imagen temporal
            $extens = str_replace("image/",".",$_FILES['gallery_images']['type'][$i]);
            $img_name = "product_".rand(00000, 99999).uniqid().$extens;
            list($width, $height) = getimagesize($_FILES["gallery_images"]['tmp_name'][$i]);

            if($width > 800){
                UpPictureArray('gallery_images', 800, $img_name, $path, $i);
            }else{
                UpPictureArray('gallery_images', $width, $img_name, $path, $i);
            }
        }

        return $this->getTempImages();
    }

    /**
     * Guarda las imagenes de los colores
     * @param  string  $param       Nombre del parametro
     * @param  integer $product_id  ID del producto
     * @param  integer $color_id    ID del color
     * @return void
     */
    public function uploadImagesColors($param, $product_id, $color_id)
    {
        $db = $this->db;
        $arr_lenght = count($_FILES[$param]['tmp_name']);

        // se elimina las imagenes temporales que esten en la carpeta
        $path = APP_SYSTEM_PRODUCTS."product_".$product_id."/gallery/";

        for ($i = 0; $i < $arr_lenght; $i++) {
            if ($_FILES[$param]['tmp_name'][$i] != "") {
                // se sube la nueva imagen temporal
                $extens = str_replace("image/",".",$_FILES[$param]['type'][$i]);
                $img_name = "color_".rand(00000, 99999).uniqid().$extens;
                list($width, $height) = getimagesize($_FILES[$param]['tmp_name'][$i]);

                if($width > 800){
                    UpPictureArray($param, 800, $img_name, $path, $i);
                }else{
                    UpPictureArray($param, $width, $img_name, $path, $i);
                }

                $arr_values = array(
                    'product_id' => $product_id,
                    'color_id' => $color_id,
                    'image' => $img_name
                );

                $db->insertAction("color_image", $arr_values);
            }
        }
    }

    /**
     * Retorna las miniaturas de todas las imágenes temporales en html
     * @return string
     */
    public function getTempImages()
    {
        $path = APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/gallery/";
        $handle = opendir($path);
        $result = "";
        $cnt = 0;
        $arr_temp_images = array();

        while ($file = readdir($handle)){
            if (is_file($path.$file)) {
                if (substr($file, 0,4) != "min_") {
                    $arr_temp_images[] = array(
                        'file' => $file,
                        'cnt' => $cnt
                    );
                    $cnt++;
                }
            }
        }

        ob_start();
        include('html/ajx_show/products-temp-gallery.php');
        return ob_get_clean();
    }

    /**
     * Se encarga de mover las imágenes de galería a su ubicación final
     * @param  array  $arr Arreglo de las propiedad
     * @return void
     */
    public function moveImages(array $arr)
    {
        $temp_path = APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/gallery/";
        $project_path = APP_SYSTEM_PRODUCTS."product_".$arr['id']."/gallery/";
        $handle = opendir($temp_path);
        $cnt = 0;

        while ($file = readdir($handle)){
            if (is_file($temp_path.$file)) {
                if (rename($temp_path.$file, $project_path.$file)) {
                    if (substr($file, 0,4) != "min_") {
                        $arr_values = array(
                            'product_id' => $arr['id'],
                            'image' => $file
                        );

                        $this->db->insertAction('products_gallery', $arr_values);

                        $cnt++;
                    }
                }
            }
        }
    }

    /**
     * Elimina una imagen de galería
     * @param  integer $image_id ID imagen de galería
     * @return string
     */
    public function removeImageGallery($image_id)
    {
        $s = "SELECT * FROM products_gallery WHERE id='".$image_id."'";
        $arr_gallery = $this->db->fetchSQL($s);
        $path = APP_SYSTEM_PRODUCTS."product_".$arr_gallery[0]['product_id']."/gallery/";
        $product_name = $this->db->getValue("name", "products", "id='".$arr_gallery[0]['product_id']."'");
        $delete = $this->db->deleteAction("products_gallery", "id='".$image_id."'");

        if ($delete) {
            @unlink($path.$arr_gallery[0]['image']);
            @unlink($path."min_".$arr_gallery[0]['image']);
        }

        $this->addLogs(sprintf("Eliminando imagen de galería del producto: %s - ID: %d", $product_name, $arr_gallery[0]['product_id']));

        return $this->getGalleryImages($arr_gallery[0]['product_id']);
    }

    /**
     * Retorna las miniaturas de las imágenes de galería de una propiedad
     * @return string
     */
    public function getGalleryImages($id)
    {
        $path = APP_IMG_PRODUCTS."product_".$id."/gallery/";
        $s = "SELECT * FROM products_gallery WHERE product_id='".$id."'";
        $arr_gallery = $this->db->fetchSQL($s);

        ob_start();
        include('html/ajx_show/products-gallery.php');
        return ob_get_clean();
    }

    /**
     * Agregar la recoemndación semanal
     * @param  integer $product_id ID del producto
     * @return void
     */
    public function persistWeeklyRecommendation($product_id, $date_init, $date_end)
    {
        $db = $this->db;
        $price = ($_POST['new_price'] != "") ? number_format($_POST['new_price'],2,".","") : NULL ;

        $arr_values = array(
            'product_id' => $product_id,
            'new_price' => $price,
            'date_init' => to_date($date_init),
            'date_end' => to_date($date_end),
            'create_at' => date('Y-m-d H:i:s')
        );

        $db->deleteAction("weekly_recommendation", "product_id='".$product_id."'");
        $weekly = $db->insertAction("weekly_recommendation", $arr_values);
        $product_name = $db->getValue("name", "products", "id='".$product_id."'");

        $this->addLogs(sprintf("Agregando recomendación semanal: %s - ID: %d", $product_name, $weekly[0]['product_id']));
    }

    /**
     * Agrega un articulo como novedad
     * @param  integer $product_id ID del producto
     * @return void
     */
    public function persistNovelties($product_id, $date_init, $date_end)
    {
        $db = $this->db;

        $arr_values = array(
            'product_id' => $product_id,
            'date_init' => to_date($date_init),
            'date_end' => to_date($date_end),
            'create_at' => date('Y-m-d H:i:s')
        );

        $db->deleteAction("novelty", "product_id='".$product_id."'");
        $novelty = $db->insertAction("novelty", $arr_values);
        $product_name = $db->getValue("name", "products", "id='".$product_id."'");

        $this->addLogs(sprintf("Agregando novedad: %s - ID: %d", $product_name, $novelty[0]['product_id']));
    }

    /**
     * Guarda el descuento
     * @param  integer $product_id ID del producto
     * @return void
     */
    public function persistDiscount($product_id, $date_init, $date_end)
    {
        $db = $this->db;
        $price = ($_POST['new_price'] != "") ? number_format($_POST['new_price'],2,".","") : NULL ;

        $arr_values = array(
            'product_id' => $product_id,
            'new_price' => $price,
            'date_init' => to_date($date_init),
            'date_end' => to_date($date_end),
            'create_at' => date('Y-m-d H:i:s')
        );

        $db->deleteAction("discount", "product_id='".$product_id."'");
        $discount = $db->insertAction("discount", $arr_values);
        $product_name = $db->getValue("name", "products", "id='".$product_id."'");

        $this->addLogs(sprintf("Agregando recomendación semanal: %s - ID: %d", $product_name, $discount[0]['product_id']));
    }

    /**
     * Obtiene la subcategorías
     * @param  array   $arr_category Arreglo con las categorías
     * @param  integer $product_id   ID del producto
     * @return string
     */
    public function getSubcategories($arr_category, $product_id)
    {
        $db = $this->db;
        $arr_subcategories = array();
        $arr_p_categories = array();
        $arr_s_cat = array();

        if (is_array($arr_category)) {
            $s = "SELECT * FROM products_sub_category WHERE category_id IN (".implode(", ", $arr_category).")";
            $arr_subcategories = $db->fetchSQL($s);
        }

        // Categorías seleccionadas
        if ($product_id != "") {
            $s = "SELECT * FROM products_categories WHERE product_id='".$product_id."'";
            $arr_p_categories = $db->fetchSQL($s);
        }

        foreach ($arr_p_categories as $val) {
            $arr_s_cat[] = $val['category_id'];
        }

        // Sub categorías seleccionadas
        $s = "SELECT * FROM products_subcategories WHERE product_id='".$product_id."'";
        $arr_p_subcategories = $db->fetchSQL($s);
        $arr_s_subcat = array();

        foreach ($arr_p_subcategories as $val) {
            $arr_s_subcat[] = $val['subcategory_id'];
        }

        ob_start();
        include('html/ajx_show/subcategories.php');

        return ob_get_clean();
    }

    /**
     * Crea un color
     * @return array
     */
    public function persistColor()
    {
        $db = $this->db;
        $arr_values = array(
            'name' => secure_mysql(sanitize(addslashes($_POST['name']))),
            'color' => secure_mysql(sanitize($_POST['color'])),
        );

        $color = $db->insertAction("colors", $arr_values);

        $this->addLogs(sprintf("Agregando color id: %d - %s", $color[0]['id'], $color[0]['name']));
        return $color[0];
    }

    /**
     * Editar un color
     * @param  integer $id ID del color
     * @return array
     */
    public function updateColor($id)
    {
        $db = $this->db;
        $arr_values = array(
            'name' => secure_mysql(sanitize(addslashes($_POST['name']))),
            'color' => secure_mysql(sanitize($_POST['color'])),
        );

        $color = $db->updateAction("colors", $arr_values, "id='".$id."'");

        $this->addLogs(sprintf("Editando color id: %d - %s", $color[0]['id'], $color[0]['name']));
        return $color[0];
    }

    /**
     * Se guarda una talla
     * @return array
     */
    public function persistSize()
    {
        $db = $this->db;
        $arr_values = array(
            'name' => secure_mysql(sanitize($_POST['name']))
        );

        $size = $db->insertAction("size", $arr_values);

        $this->addLogs(sprintf("Agregando talla id: %d - %s", $size[0]['id'], $size[0]['name']));
        return $size[0];
    }

    /**
     * Actualiza una talla
     * @param  integer $id ID de la talla
     * @return array
     */
    public function updateSize($id)
    {
        $db = $this->db;
        $arr_values = array(
            'name' => secure_mysql(sanitize($_POST['name'])),
        );

        $size = $db->updateAction("size", $arr_values, "id='".$id."'");

        $this->addLogs(sprintf("Editando talla id: %d - %s", $size[0]['id'], $size[0]['name']));
        return $size[0];
    }

    /**
     * Elimina la imagen del color
     *
     * @param integer $image_id
     * @return void
     */
    public function removeImageColor($image_id)
    {
        $s = "SELECT * FROM color_image WHERE id='".$image_id."'";
        $arr_gallery = $this->db->fetchSQL($s);
        $path = APP_SYSTEM_PRODUCTS."product_".$arr_gallery[0]['product_id']."/gallery/";
        $product_name = $this->db->getValue("name", "products", "id='".$arr_gallery[0]['product_id']."'");
        $delete = $this->db->deleteAction("color_image", "id='".$image_id."'");

        if ($delete) {
            @unlink($path.$arr_gallery[0]['image']);
            @unlink($path."min_".$arr_gallery[0]['image']);
        }

        $this->addLogs(sprintf("Eliminando imagen de color del producto: %s - ID: %d", $product_name, $arr_gallery[0]['product_id']));
    }

    /**
     * Elimina un color
     *
     * @param integer $image_id
     * @param integer $porduct_id
     * @return void
     */
    public function removeColor($color_id, $porduct_id)
    {
        $db = $this->db;
        $path = APP_SYSTEM_PRODUCTS."product_".$porduct_id."/gallery/";

        $db->deleteAction("products_colors", "color_id='".$color_id."' AND product_id='".$porduct_id."'");

        $s = "SELECT * FROM color_image WHERE color_id='".$color_id."' AND product_id='".$porduct_id."'";
        $arr_color_image = $db->fetchSQL($s);

        foreach ($arr_color_image as $key => $val) {
            @unlink($path.$val['image']);
            @unlink($path."min_".$val['image']);
        }

        $db->deleteAction("color_image", "color_id='".$color_id."' AND product_id='".$porduct_id."'");
    }
}

?>
