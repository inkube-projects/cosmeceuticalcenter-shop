<?php
/**
 * Servicios
 */
class ProductsCategoryService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Agrega una entrada
     * @return void
     */
    public function persist()
    {
        $db = $this->db;

        $arr_values = array(
            'name' => $_POST['name'],
            'description' => $_POST['description'],
            'name_canonical' => friendlyURL($_POST['name'], "upper")
        );
        $category = $db->insertAction("products_category", $arr_values);

        if (isset($_FILES['image']['tmp_name'])) {
            $this->persistImage($category, 'image');
        }

        $this->addLogs(sprintf("Agregando categoría de producto id: %d - %s", $category[0]['id'], $category[0]['name']));
        return $category[0];
    }

    /**
     * Edita una entrada
     * @param  integer $id ID de la entrada
     * @return void
     */
    public function update($id)
    {
        $db = $this->db;

        $arr_values = array(
            'name' => $_POST['name'],
            'description' => $_POST['description'],
            'name_canonical' => friendlyURL($_POST['name'], "upper")
        );
        $category = $db->updateAction("products_category", $arr_values, "id='".$id."'");

        if (isset($_FILES['image']['tmp_name'])) {
            $this->persistImage($category, 'image');
        }

        $this->addLogs(sprintf("Editando categoría de producto id: %d - %s", $id, $category[0]['name']));
        return $category[0];
    }

    /**
     * Guarda las imagenes
     * @param  array  $category   Arreglo de la imagen
     * @param  string $parameter  Nombre del parametro del formulario
     * @return void|exception
     */
    public function persistImage(array $category, $parameter)
    {
        if ($_FILES[$parameter]['tmp_name'] != "") {
            $path = APP_SYSTEM_CATEGORY;

            if (!empty($_FILES[$parameter]['tmp_name'])) {
                $img_type = $_FILES[$parameter]['type'];

                $extens = str_replace("image/",".",$_FILES[$parameter]['type']);
                $file_name = "cat_".rand(00000, 99999).uniqid().$extens;

                if(move_uploaded_file($_FILES[$parameter]['tmp_name'], $path.$file_name)){
                    if ($category[0]['image']) {
                        @unlink($path.$category[0]['image']);
                    }

                    $this->db->updateAction("products_category", array('image' => $file_name), "id='".$category[0]['id']."'");
                }
            }
        }
    }
}
?>
