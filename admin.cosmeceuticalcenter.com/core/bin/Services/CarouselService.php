<?php
/**
 * Servicios
 */
class CarouselService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Agrega una entrada
     * @return void
     */
    public function persist()
    {
        $db = $this->db;

        $arr_values = array(
            'description' => secure_mysql($_POST['description']),
            'color' => secure_mysql($_POST['color']),
            'url' => $_POST['url'],
            'status' => secure_mysql($_POST['status']),
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s'),
        );

        $carousel = $db->insertAction("carousel", $arr_values);

        if (isset($_FILES['image']['tmp_name'])) {
            $this->persistImage($carousel, 'image');
        }

        $this->addLogs(sprintf("Agregando imagen de carrusel - id: %d", $carousel[0]['id']));

        return $carousel[0];
    }

    /**
     * Edita una entrada
     * @param  integer $id ID de la entrada
     * @return void
     */
    public function update($id)
    {
        $db = $this->db;

        $arr_values = array(
            'description' => secure_mysql($_POST['description']),
            'color' => secure_mysql($_POST['color']),
            'url' => $_POST['url'],
            'status' => secure_mysql($_POST['status']),
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s'),
        );

        $carousel = $db->updateAction("carousel", $arr_values, "id='".$id."'");
        if (isset($_FILES['image']['tmp_name'])) {
            $this->persistImage($carousel, 'image');
        }

        $this->addLogs(sprintf("Editando imagen de carrusel - id: %d", $carousel[0]['id']));

        return $carousel[0];
    }

    /**
     * Guarda las imagenes
     * @param  array  $carousel   Arreglo de la imagen
     * @param  string $parameter  Nombre del parametro del formulario
     * @return void|exception
     */
    public function persistImage(array $carousel, $parameter)
    {
        if ($_FILES[$parameter]['tmp_name'] != "") {
            $path = APP_SYSTEM_CAROUSEL;

            if (!empty($_FILES[$parameter]['tmp_name'])) {
                $img_type = $_FILES[$parameter]['type'];

                $extens = str_replace("image/",".",$_FILES[$parameter]['type']);
                $file_name = "slide_".rand(00000, 99999).uniqid().$extens;

                if(move_uploaded_file($_FILES[$parameter]['tmp_name'], $path.$file_name)){
                    if ($carousel[0]['image']) {
                        @unlink($path.$carousel[0]['image']);
                    }

                    $this->db->updateAction("carousel", array('image' => $file_name), "id='".$carousel[0]['id']."'");
                }
            }
        }
    }

    /**
     * Cambia el orden de las imagenes
     * @param integer $img_id ID de la imagen de carrusel
     * @param integer $value  Valor del lugar
     * @return void
     */
    public function setOrder($img_id, $value)
    {
        $db = $this->db;

        $arr_values = array(
            'order_image' => $value,
            'update_at' => date('Y-m-d H:i:s')
        );
        $db->updateAction("carousel", $arr_values, "id='".$img_id."'");

        $this->addLogs(sprintf("Cambiando orden de la imagen id: %d al lugar: %d", $img_id, $value));
    }
}
?>
