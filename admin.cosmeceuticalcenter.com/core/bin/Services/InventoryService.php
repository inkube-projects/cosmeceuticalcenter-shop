<?php
/**
 * Servicios
 */
class InventoryService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Agrega una entrada
     * @return void
     */
    public function persist()
    {
        $db = $this->db;

        $arr_values = array(
            'product_id' => $_POST['product'],
            'quantity' => $_POST['quantity'],
            'unity_id' => $_POST['unity'],
            'update_at' => date('Y-m-d H:i:s')
        );

        $result = $db->insertAction("inventory", $arr_values);
        $this->addLogs(sprintf("Agregando Inventario id: %d - producto: %d", $result[0]['id'], $_POST['product']));

        return $result[0];
    }

    /**
     * Edita una entrada
     * @param  integer $id ID de la entrada
     * @return void
     */
    public function update($id)
    {
        $db = $this->db;

        $arr_values = array(
            'quantity' => $_POST['quantity'],
            'unity_id' => $_POST['unity'],
            'update_at' => date('Y-m-d H:i:s')
        );

        $result = $db->updateAction("inventory", $arr_values, "id='".$id."'");

        if ($result[0]['quantity'] > 0) {
            $s = "SELECT * FROM notify_products WHERE product_id='".$result[0]['product_id']."'";
            $arr_notify = $db->fetchSQL($s);

            foreach ($arr_notify as $key => $not) {
                $product_name = $db->getValue('name', 'products', "id='".$result[0]['product_id']."'");
                $user_id = $db->getValue('id', 'user', "email='".$not['email']."'");
                $arr_values = array(
                    'user_id' => $user_id,
                    'title' => 'Producto disponible: '.$product_name,
                    'description' => 'El producto '.$product_name.' ya se encuentra disponible nuevamente',
                    'link' => BASE_URL.'PRODUCTO/'.$result[0]['product_id'].'/'.friendlyURL($product_name, 'upper'),
                    'status' => '0',
                    'create_at' => date('Y-m-d H:i:s'),
                    'update_at' => date('Y-m-d H:i:s')
                );

                $db->insertAction('notifications', $arr_values);

                $arr_values = array(
                    'status' => '1'
                );
                $db->updateAction('notify_products', $arr_values, "id='".$not['id']."'");

                // Se envía el email
                $s = "SELECT * FROM products WHERE id='".$result[0]['product_id']."'";
                $arr_product = $db->fetchSQL($s);

                // Informacion del usuario
                $s = "SELECT * FROM view_user WHERE id='".$user_id."'";
                $arr_user = $db->fetchSQL($s);

                $arr_data = array(
                    'names' => $arr_user[0]['name']." ".$arr_user[0]['last_name'],
                    'product_name' => $arr_product[0]['name'],
                    'product_url' => BASE_URL."PRODUCTO/".$arr_product[0]['id']."/".friendlyURL($arr_product[0]['name'], 'upper'),
                    'image' => APP_IMG_PRODUCTS."/product_".$arr_product[0]['id']."/cover/".$arr_product[0]['cover_image']
                );

                $this->sendEmailTemplate("html/email/inventory-notify.php", $arr_data, $arr_user[0]['email'], $arr_data['names'], "Inventario respuesto");
            }
        }

        $this->addLogs(sprintf("Editando Inventario id: %d", $id));
        return $result[0];
    }

    /**
     * Elimina un email para notficar
     * @param  integer $id ID de la notificación
     * @return void
     */
    public function removeNotify($id)
    {
        $db = $this->db;
        $db->deleteAction("notify_products", "id='".$id."'");
    }

    /**
     * Actualiza de forma rápida el inventario
     * @param  integer  $product_id ID del producto
     * @return void
     */
    public function fastUpdate($product_id)
    {
        $db = $this->db;
        $cnt_val = $db->getCount("inventory", "product_id='".$product_id."'");

        if ($cnt_val == 1) {
            $s = "SELECT * FROM inventory WHERE product_id='".$product_id."'";
            $arr_inventory = $db->fetchSQL($s);
            $i_quantity = $arr_inventory[0]['quantity'];

            $arr_values = array(
                'quantity' => $_POST['quantity'],
                'update_at' => date('Y-m-d H:i:s')
            );

            $db->updateAction("inventory", $arr_values, "product_id='".$product_id."'");
        } else {
            $i_quantity = 0;
            $arr_values = array(
                'product_id' => $product_id,
                'quantity' => $_POST['quantity'],
                'unity_id' => 1,
                'update_at' => date('Y-m-d H:i:s')
            );

            $db->insertAction("inventory", $arr_values);
        }

        if ($i_quantity == 0 && $arr_values['quantity'] > 0) {
            $s = "SELECT * FROM notify_products WHERE product_id='".$product_id."'";
            $arr_notify = $db->fetchSQL($s);

            foreach ($arr_notify as $key => $not) {
                $product_name = $db->getValue('name', 'products', "id='".$product_id."'");
                $user_id = $db->getValue('id', 'user', "email='".$not['email']."'");
                $arr_values = array(
                    'user_id' => $user_id,
                    'title' => 'Producto disponible: '.$product_name,
                    'description' => 'El producto '.$product_name.' ya se encuentra disponible nuevamente',
                    'link' => BASE_URL.'PRODUCTO/'.$product_id.'/'.friendlyURL($product_name, 'upper'),
                    'status' => '0',
                    'create_at' => date('Y-m-d H:i:s'),
                    'update_at' => date('Y-m-d H:i:s')
                );

                $db->insertAction('notifications', $arr_values);

                $arr_values = array(
                    'status' => '1'
                );
                $db->updateAction('notify_products', $arr_values, "id='".$not['id']."'");

                // Se envía el email
                $s = "SELECT * FROM products WHERE id='".$product_id."'";
                $arr_product = $db->fetchSQL($s);

                // Informacion del usuario
                $s = "SELECT * FROM view_user WHERE id='".$user_id."'";
                $arr_user = $db->fetchSQL($s);

                $arr_data = array(
                    'names' => $arr_user[0]['name']." ".$arr_user[0]['last_name'],
                    'product_name' => $arr_product[0]['name'],
                    'product_url' => BASE_URL."PRODUCTO/".$arr_product[0]['id']."/".friendlyURL($arr_product[0]['name'], 'upper'),
                    'image' => APP_IMG_PRODUCTS."/product_".$arr_product[0]['id']."/cover/".$arr_product[0]['cover_image']
                );

                $this->sendEmailTemplate("html/email/inventory-notify.php", $arr_data, $arr_user[0]['email'], $arr_data['names'], "Inventario respuesto");
            }
        }
    }

    /**
     * Muestra el formulario para modificar el inventario de colores y tallas
     *
     * @param integer $product_id
     * @return string
     */
    public function showInventorySizeColors($product_id)
    {
        $db = $this->db;

        // Colores
        $s = "SELECT * FROM view_colors WHERE product_id='".$product_id."'";
        $arr_product_colors = $db->fetchSQL($s);

        // Tallas
        $s = "SELECT * FROM view_size WHERE product_id='".$product_id."'";
        $arr_product_size = $db->fetchSQL($s);

        ob_start();
        include('html/ajx_show/products-extra-stock.php');
        return ob_get_clean();
    }

    /**
     * Guarda el stock de tallas y colores
     *
     * @param integer $productID ID del producto
     * @return void
     */
    public function persistExtraStock($productID)
    {
        $db = $this->db;

        // guardar stock de los colores
        $s = "SELECT * FROM view_colors WHERE product_id='".$productID."'";
        $arr_product_colors = $db->fetchSQL($s);

        foreach ($arr_product_colors as $key => $val) {
            $stock = @number_format($_POST['color_'.$val['color_id']],0,"","");
            $arr_values = array(
                'stock' => $stock
            );
            
            $db->updateAction("products_colors", $arr_values, "product_id='".$productID."' AND color_id='".$val['color_id']."'");
        }

        // Guardar stock de las tallas
        $s = "SELECT * FROM view_size WHERE product_id='".$productID."'";
        $arr_product_size = $db->fetchSQL($s);

        foreach ($arr_product_size as $key => $val) {
            $stock = @number_format($_POST['size_'.$val['size_id']],0,"","");
            $arr_values = array(
                'stock' => $stock
            );

            $db->updateAction("products_sizes", $arr_values, "product_id='".$productID."' AND size_id='".$val['size_id']."'");
        }
    }
}
?>
