<?php
/**
 * Servicios
 */
class SubcategoryService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Agrega una entrada
     * @return void
     */
    public function persist()
    {
        $db = $this->db;

        $arr_values = array(
            'name' => secure_mysql($_POST['name']),
            'category_id' => $_POST['category'],
        );

        $result = $db->insertAction("products_sub_category", $arr_values);
        $this->addLogs(sprintf("Agregando entrada subcategoría id: %d - Nombre: %s", $result[0]['id'], $result[0]['name']));

        return $result[0];
    }

    /**
     * Edita una entrada
     * @param  integer $id ID de la entrada
     * @return void
     */
    public function update($id)
    {
        $db = $this->db;

        $arr_values = array(
            'name' => secure_mysql($_POST['name']),
            'category_id' => $_POST['category'],
        );

        $result = $db->updateAction("products_sub_category", $arr_values, "id='".$id."'");
        $this->addLogs(sprintf("Editando entrada subcategoría id: %d - Nombre: %s", $result[0]['id'], $result[0]['name']));

        return $result[0];
    }
}
?>
