<?php
/**
 * Servicios
 */
class NewsletterService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Agrega una entrada
     * @return void
     */
    public function persist()
    {
        $db = $this->db;

        $arr_values = array(
            'email' => mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s')
        );

        $result = $db->insertAction("newsletter", $arr_values);
        $this->addLogs(sprintf("Agregando entrada newsletter - id: %d", $result[0]['id']));

        return $result[0];
    }

    /**
     * Edita una entrada
     * @param  integer $id ID de la entrada
     * @return void
     */
    public function update($id)
    {
        $db = $this->db;

        $arr_values = array(
            'email' => mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            'update_at' => date('Y-m-d H:i:s')
        );

        $result = $db->updateAction("newsletter", $arr_values, "id='".$id."'");
        $this->addLogs(sprintf("Editando entrada newsletter - id: %d", $result[0]['id']));

        return $result[0];
    }
}
?>
