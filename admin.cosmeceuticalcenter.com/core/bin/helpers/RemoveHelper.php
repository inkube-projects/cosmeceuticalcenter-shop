<?php

/**
* Clase que se encarga de eliminar entradas en la base de datos
*/
class RemoveHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
    * Elimina global
    * @param  integer $id ID de la imagen a eliminar
    * @return void
    */
    public function removeGlobal($id, $table, $where, $log, $param, $path = "", $img_param = array())
    {
        $s = "SELECT * FROM ".$table." WHERE ".$where;
        $arr_sql = $this->db->fetchSQL($s);
        $remove = $this->db->deleteAction($table, $where);

        if ($path != "") {
            foreach ($img_param as $val) {
                @unlink($path.$arr_sql[0][$val]);
                @unlink($path."min_".$arr_sql[0][$val]);
            }
        }

        $this->addLogs(sprintf($log.": %s - ID: %d", $arr_sql[0][$param], $id));
    }


    /**
     * Elimina los directorios especificados en el arreglo
     * @param  array  $arr_directory Arreglo de directorios
     * @return boolean
     */
    public function removeDirectory(array $arr_directory)
    {
        foreach ($arr_directory as $val) {
            clearDirectory($val);
            rmdir($val);
        }

        return true;
    }

    /**
     * Elimina especificamente a un usuario
     * @param  integer $id ID del usuario
     * @return void
     */
    public function removeUser($id)
    {
        $db = $this->db;

        $s = "SELECT * FROM user WHERE id='".$id."'";
        $arr_user = $db->fetchSQL($s);

        $remove = $this->db->deleteAction("user", "id='".$id."'");

        $arr_paths = array(
            APP_SYSTEM_USERS."user_".$arr_user[0]['code']."/profile/",
            APP_SYSTEM_USERS."user_".$arr_user[0]['code']."/"
        );

        if ($arr_user[0]['role'] == 1 || $arr_user[0]['role'] == 2) {
            array_push(
                $arr_paths,
                APP_IMG_ADMIN."user_".$arr_user[0]['code']."/temp_files/products/banner",
                APP_IMG_ADMIN."user_".$arr_user[0]['code']."/temp_files/products/cover",
                APP_IMG_ADMIN."user_".$arr_user[0]['code']."/temp_files/products/gallery",
                APP_IMG_ADMIN."user_".$arr_user[0]['code']."/temp_files/products/",
                APP_IMG_ADMIN."user_".$arr_user[0]['code']."/temp_files/users/profile/",
                APP_IMG_ADMIN."user_".$arr_user[0]['code']."/temp_files/users/",
                APP_IMG_ADMIN."user_".$arr_user[0]['code']."/temp_files/",
                APP_IMG_ADMIN."user_".$arr_user[0]['code']."/"
            );
        }

        $this->removeDirectory($arr_paths);

        $this->addLogs(sprintf("Eliminando usuario: %s - ID: %d", $arr_user[0]['username'], $id));
    }
}

?>
