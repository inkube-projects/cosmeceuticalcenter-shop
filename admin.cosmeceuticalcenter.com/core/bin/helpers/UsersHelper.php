<?php

/**
 * Helper para los usuarios
 */
class UsersHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Registra un Usuario
     * @return array
     */
    public function persist()
    {
        $db = $this->db;

        $arr_values = array(
            'username' => mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            'email' => mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            'password' => hashPass($_POST['pass_1'], 12),
            'role' => @number_format($_POST['role'],0,"",""),
            'status_id' => @number_format($_POST['status'],0,"",""),
            'username_canonical' => mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            'email_canonical' => mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s')
        );

        $user = $db->insertAction("user", $arr_values);
        $code = genPass($user[0]['id'], 5);
        $user = $this->db->updateAction("user", array('code' => $code), "id='".$user[0]['id']."'");

        $arr_values = array(
            'name' => $_POST['name'],
            'last_name' => $_POST['last_name'],
            'birthdate' => to_date($_POST['birthdate']),
            'company' => $_POST['company'],
            'user_id' => $user[0]['id'],
            'country_id' => @number_format($_POST['country'],0,"",""),
            'province_id' => @number_format($_POST['province'],0,"",""),
            'city' => $_POST['city'],
            'direction' => $_POST['location_1'],
            'direction_2' => $_POST['location_2'],
            'phone_1' => $_POST['phone_1'],
            'phone_2' => $_POST['phone_2'],
            'note' => $_POST['note'],
            'dni' => $_POST['dni'],
            'postal_code' => $_POST['postal_code'],
        );

        $user_information = $db->insertAction("user_information", $arr_values);

        if ($user[0]['role'] == '1' || $user[0]['role'] == '2') {
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/profile/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/cover/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/gallery/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/banner/");
        }

        mkdir(APP_SYSTEM_USERS."user_".$user[0]['code']."/");
        mkdir(APP_SYSTEM_USERS."user_".$user[0]['code']."/profile/");

        if (($user) && ($_POST['hid-name'] != "")) {
            $this->saveImage(
                $_POST['hid-name'],
                "",
                APP_IMG_ADMIN."user_".$this->user_code."/temp_files/users/profile/",
                APP_SYSTEM_USERS."user_".$user[0]['code']."/profile/",
                "user",
                array("profile_image" => $_POST['hid-name']),
                "id='".$user[0]['id']."'"
            );
        }

        // showArr($user); exit;

        $this->addLogs(sprintf("Editando usuario: %s - id: %d", $user[0]['username'], $user[0]['id']));

        return $user[0];
    }

    /**
     * Edita un usuario
     * @param  integer $id ID del usuario
     * @return array
     */
    public function update($id)
    {
        $db = $this->db;

        $arr_values = array(
            'username' => mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            'email' => mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            'role' => @number_format($_POST['role'],0,"",""),
            'status_id' => @number_format($_POST['status'],0,"",""),
            'username_canonical' => mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            'email_canonical' => mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            'update_at' => date('Y-m-d H:i:s')
        );

        $user = $db->updateAction("user", $arr_values, "id='".$id."'");

        if ($_POST['pass_1'] != "") {
            $user = $this->db->updateAction("user", array('password' => hashPass($_POST['pass_1'], 12),), "id='".$user[0]['id']."'");
        }

        $arr_values = array(
            'name' => $_POST['name'],
            'last_name' => $_POST['last_name'],
            'birthdate' => to_date($_POST['birthdate']),
            'company' => $_POST['company'],
            'user_id' => $user[0]['id'],
            'country_id' => @number_format($_POST['country'],0,"",""),
            'province_id' => @number_format($_POST['province'],0,"",""),
            'city' => $_POST['city'],
            'direction' => $_POST['location_1'],
            'direction_2' => $_POST['location_2'],
            'phone_1' => $_POST['phone_1'],
            'phone_2' => $_POST['phone_2'],
            'note' => $_POST['note'],
            'dni' => $_POST['dni'],
            'postal_code' => $_POST['postal_code'],
        );

        $user_information = $db->updateAction("user_information", $arr_values, "user_id='".$id."'");

        if ($user[0]['role'] == '1' || $user[0]['role'] == '2' && !directoryExist(APP_IMG_ADMIN."user_".$user[0]['code']."/")) {
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/profile/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/cover/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/gallery/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/banner/");
        }

        if (!directoryExist(APP_SYSTEM_USERS."user_".$user[0]['code']."/")) {
            mkdir(APP_SYSTEM_USERS."user_".$user[0]['code']."/");
            mkdir(APP_SYSTEM_USERS."user_".$user[0]['code']."/profile/");
        }

        if (($user) && ($_POST['hid-name'] != "")) {
            $this->saveImage(
                $_POST['hid-name'],
                $user[0]['profile_image'],
                APP_IMG_ADMIN."user_".$this->user_code."/temp_files/users/profile/",
                APP_SYSTEM_USERS."user_".$user[0]['code']."/profile/",
                "user",
                array("profile_image" => $_POST['hid-name']),
                "id='".$user[0]['id']."'"
            );
        }

        $this->addLogs(sprintf("Editando usuario: %s - id: %d", $user[0]['username'], $user[0]['id']));

        return $user[0];
    }

    /**
    * Prepara imagen
    * @param  integer $acc Acción a ejecutar [1: Guarda la imagen base, 2: Recorta la imagen]
    * @return object
    */
    public function prepareCoverImage($acc)
    {
        if ($acc == 1) {
            $tempIMG = $this->prepareCoverTemp();
            $form = $this->showFormCoverIMG($tempIMG);
            return $form;
        } elseif ($acc == 2) {
            $directory_temp = APP_IMG_ADMIN."user_".$this->user_code."/temp_files/users/profile/";
            $directory_profile = APP_IMG_ADMIN."user_".$this->user_code."/temp_files/users/profile/";
            return $this->createIMG($directory_temp, $directory_profile);
        }
    }

    /**
    * Guarda la imagen base
    * @return string
    */
    public function prepareCoverTemp()
    {
        // se elimina las imagenes temporales que esten en la carpeta
        $dir = APP_IMG_ADMIN."user_".$this->user_code."/temp_files/users/profile/";
        $handle = opendir($dir);
        while ($file = readdir($handle)){
            if (is_file($dir.$file)) {
                @unlink($dir.$file);
            }
        }

        // se sube la nueva imagen temporal
        $extens = str_replace("image/",".",$_FILES['cover_image']['type']);
        $nb_img = "cover_".rand(00000, 99999).uniqid().$extens;
        list($width, $height) = getimagesize($_FILES["cover_image"]['tmp_name']);

        if($width > 1000){
            ImagenTemp('cover_image', 1000, $nb_img, $dir);
        }else{
            ImagenTemp('cover_image', $width, $nb_img, $dir);
        }

        return $nb_img;
    }

    /**
    * Retorna el fomulario de la imagen de perfil a recortar
    * @param  string $img_name Nombre asignado a la imagen
    * @return string
    */
    public function showFormCoverIMG($img_name)
    {
        $directory = APP_IMG_ADMIN."user_".$this->user_code."/temp_files/users/profile/";
        $directory_post = APP_IMG_ADMIN."user_".$this->user_code."/temp_files/users/profile/";

        list($width, $height) = getimagesize($directory.$img_name);

        if($height > 600) {
            $altura = 600;
        } else {
            $altura = $height;
        }
        if($width > 600) {
            $ancho = 600;
        } else {
            $ancho = $width;
        }

        $form = '
        <link href="'.APP_ASSETS.'plugins/jcrop/jquery.Jcrop.css" type="text/css" rel="stylesheet" />
        <script src="'.APP_ASSETS.'plugins/jcrop/jquery.Jcrop.js"></script>
        <script type="text/javascript">
        $(function(){
            $(\'#img-recortar\').Jcrop({
                setSelect: [ 0, 0, 0, 0],
                maxSize: ['.$width.', '.$height.'],
                minSize: ['.$ancho.', '.$altura.'],
                onSelect: updateCoords
            });
        });
        function updateCoords(c){
            $(\'#x\').val(c.x);
            $(\'#y\').val(c.y);
            if($(\'#w\').val()==0){ $(\'#w\').val('.$ancho.');}else{ $(\'#w\').val(c.w);}
            if($(\'#h\').val()==0){ $(\'#h\').val('.$altura.');}else{ $(\'#h\').val(c.h);}
        }
        function recortarImagen(){
            var i = $("#frm-img").serialize();
            $.ajax({
                type: \'POST\',
                url: base_admin + \'ajax.php?m=user&act=4\',
                dataType: \'json\',
                data: i,
                success: function(r){
                    $(\'#mod-img-cover\').modal(\'hide\');
                    if (r.status === "OK") {
                        $("#hid-name").val("'.$this->user_code."_".$img_name.'");
                        $("#img-cover").attr("src", "'.BASE_URL_ADMIN.$directory_post.'"+r.data);
                    }
                }
            });
            return false;
        }
        </script>
        <img src="'.BASE_URL_ADMIN.$directory.$img_name.'" id="img-recortar" class="img-responsive">
        <form method="post" name="frm-img" id="frm-img" onSubmit="return recortarImagen();">
            <input type="hidden" id="x" name="x" value="0"/>
            <input type="hidden" id="y" name="y" value="0"/>
            <input type="hidden" id="w" name="w" value="0"/>
            <input type="hidden" id="h" name="h" value="0"/>
            <div class="clearfix css-espacio10"></div>
            <button type="submit" class="btn btn-default pull-right">Cortar Imagen <i class="fa fa-scissors"></i></button>
        </form>
        ';

        return $form;
    }

    /**
    * Crea la imagen de Portada
    * @return string
    */
    public function createIMG($directory_temp, $directory_profile)
    {
        $dir = $directory_temp;
        $handle = opendir($dir);
        $file = readdir($handle);
        while ($file = readdir($handle)){
            if (is_file($dir.$file)) {
                $image = $file;
            }
        }
        $ext = explode(".", $image)[1];

        $x = sanitize($_POST['x']);
        $y = sanitize($_POST['y']);
        $w = sanitize($_POST['w']);
        $h = sanitize($_POST['h']);
        $targ_w = $w;
        $targ_h = $h;
        $jpeg_quality = 90;
        $src = $directory_temp.$image;
        $src_min = $directory_profile."min_".$image;

        if(($ext == "jpeg")||($ext == "jpg")){
            $img_r = imagecreatefromjpeg($src);
        }if($ext == "png"){
            $img_r = imagecreatefrompng($src);
        } if($ext == "gif"){
            $img_r = imagecreatefromgif($src);
        }

        $src = $directory_temp.$this->user_code."_".$image;
        $dst_r = imagecreatetruecolor($targ_w, $targ_h);
        imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);

        if (($ext == "jpeg") || ($ext== "jpg ")){
            imagejpeg($dst_r,$src,90);
        } elseif ($ext == "png") {
            imagepng($dst_r, $src);
        } elseif ($ext=="gif") {
            imagegif($dst_r,$src);
        }

        // se crea la miniatura
        $src_min = $directory_profile."min_".$this->user_code."_".$image;
        list($width, $height) = getimagesize($src);
        $new_width = 200;
        $new_height = floor($height * ($new_width / $width));
        $targ_w = $new_width;
        $targ_h = $new_height;
        $dst_r = imagecreatetruecolor($targ_w, $targ_h);
        imagecopyresampled($dst_r,$img_r,0,0,$x,$y, $targ_w,$targ_h,$w,$h);

        if(($ext=="jpeg")||($ext=="jpg")){
            imagejpeg($dst_r,$src_min,90);
        } if($ext=="png"){
            imagepng($dst_r,$src_min);
        } if($ext=="gif"){
            imagegif($dst_r,$src_min);
        }

        imagedestroy($dst_r);

        // rename($src, $directory_profile.$image);
        return $this->user_code."_".$image;
    }

    /**
     * Registra un Usuario
     * @return array
     */
    public function persistAdmin()
    {
        $db = $this->db;

        $arr_values = array(
            'username' => mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            'email' => mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            'password' => hashPass($_POST['pass_1'], 12),
            'role' => @number_format($_POST['role'],0,"",""),
            'status_id' => @number_format($_POST['status'],0,"",""),
            'username_canonical' => mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            'email_canonical' => mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s')
        );

        $user = $db->insertAction("user", $arr_values);
        $code = genPass($user[0]['id'], 5);
        $user = $this->db->updateAction("user", array('code' => $code), "id='".$user[0]['id']."'");

        $arr_values = array(
            'name' => $_POST['name'],
            'last_name' => $_POST['last_name'],
            'user_id' => $user[0]['id'],
            'country_id' => @number_format($_POST['country'],0,"",""),
            'phone_1' => $_POST['phone_1'],
            'phone_2' => $_POST['phone_2']
        );

        $user_information = $db->insertAction("user_information", $arr_values);

        if ($user[0]['role'] == '1' || $user[0]['role'] == '2') {
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/profile/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/cover/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/gallery/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/banner/");
        }

        mkdir(APP_SYSTEM_USERS."user_".$user[0]['code']."/");
        mkdir(APP_SYSTEM_USERS."user_".$user[0]['code']."/profile/");

        if (($user) && ($_POST['hid-name'] != "")) {
            $this->saveImage(
                $_POST['hid-name'],
                "",
                APP_IMG_ADMIN."user_".$this->user_code."/temp_files/users/profile/",
                APP_SYSTEM_USERS."user_".$user[0]['code']."/profile/",
                "user",
                array("profile_image" => $_POST['hid-name']),
                "id='".$user[0]['id']."'"
            );
        }

        // showArr($user); exit;
        $this->addLogs(sprintf("Agregando Administrador / moderador: %s - id: %d", $user[0]['username'], $user[0]['id']));
        return $user[0];
    }

    /**
     * Edita un usuario
     * @param  integer $id ID del usuario
     * @return array
     */
    public function updateAdmin($id)
    {
        $db = $this->db;

        $arr_values = array(
            'username' => mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            'email' => mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            'role' => @number_format($_POST['role'],0,"",""),
            'status_id' => @number_format($_POST['status'],0,"",""),
            'username_canonical' => mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            'email_canonical' => mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            'update_at' => date('Y-m-d H:i:s')
        );

        $user = $db->updateAction("user", $arr_values, "id='".$id."'");

        if ($_POST['pass_1'] != "") {
            $user = $this->db->updateAction("user", array('password' => hashPass($_POST['pass_1'], 12),), "id='".$user[0]['id']."'");
        }

        $arr_values = array(
            'name' => $_POST['name'],
            'last_name' => $_POST['last_name'],
            'user_id' => $user[0]['id'],
            'country_id' => @number_format($_POST['country'],0,"",""),
            'phone_1' => $_POST['phone_1'],
            'phone_2' => $_POST['phone_2'],
        );

        $user_information = $db->updateAction("user_information", $arr_values, "user_id='".$id."'");
        if ($user[0]['role'] == '1' || $user[0]['role'] == '2' && !directoryExist(APP_IMG_ADMIN."user_".$user[0]['code']."/")) {
            @mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/");
            @mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/");
            @mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/");
            @mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/profile/");
            @mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/");
            @mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/cover/");
            @mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/gallery/");
            @mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/banner/");
        }

        if (!directoryExist(APP_SYSTEM_USERS."user_".$user[0]['code']."/")) {
            @mkdir(APP_SYSTEM_USERS."user_".$user[0]['code']."/");
            @mkdir(APP_SYSTEM_USERS."user_".$user[0]['code']."/profile/");
        }

        if (($user) && ($_POST['hid-name'] != "")) {
            $this->saveImage(
                $_POST['hid-name'],
                $user[0]['profile_image'],
                APP_IMG_ADMIN."user_".$this->user_code."/temp_files/users/profile/",
                APP_SYSTEM_USERS."user_".$user[0]['code']."/profile/",
                "user",
                array("profile_image" => $_POST['hid-name']),
                "id='".$user[0]['id']."'"
            );
        }

        $this->addLogs(sprintf("Editando usuario: %s - id: %d", $user[0]['username'], $user[0]['id']));

        return $user[0];
    }
}


?>
