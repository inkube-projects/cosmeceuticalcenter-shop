<?php
/**
 * Handler que controla el acceso a los archivos ajax
*/
$db = new Connection();
$current_ajax = $_GET['m'];

$role = "ROLE_ANONYMOUS";

$arr_ajax = array(
   'login' => array('ROLE_ANONYMOUS'),
   'remove' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'user' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'products-category' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'products' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'brands' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'orders' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'zones' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'seo' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'subcategory' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'inventory' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'carousel' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'sub-category' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'newsletter' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'features' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'coupon' => array('ROLE_MODERATOR', 'ROLE_ADMIN'),
   'admin' => array('ROLE_ADMIN'),
);

/**
 * Se verifica si existe una sesión, sino se le asigna un Rol anónimo
*/
if (isset($_SESSION['ADMIN_SESSION_COSME']['id'])) {
   $role = $_SESSION['ADMIN_SESSION_COSME']['role'];
   $admin_pp_id = $_SESSION['ADMIN_SESSION_COSME']['id'];
   $admin_pp_code = $_SESSION['ADMIN_SESSION_COSME']['code'];
}

try {
    foreach ($arr_ajax as $key => $val) {
       if ($key == $current_ajax) {
          if (!in_array($role, $val)) {
            throw new \Exception("No tienes acceso a esta acción", 1);
          }
       }
    }
} catch (Exception $e) {
    $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    header('Content-Type: application/json');
    echo json_encode($arr_response);
    exit;
}
?>
