<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');
include('core/Model/classConnectionPrestashop.php');

/**
 * Controlador para sincronizar los productos
 */
class SynchronizeController extends ControllerAware
{
    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Sincronizador
     * @return string
     */
    public function syncProductsAction()
    {
        // $db = new Connection;
        // $dbPresta = new ConnectionPrestashop;
        // $s = "SELECT
        // 	p.`id_product`,
        // 	p.`id_manufacturer`,
        // 	p.`id_category_default`,
        // 	p.`price`,
        // 	p.`active`,
        // 	pl.`name`,
        // 	pl.`description`,
        // 	pl.`link_rewrite`,
        //     pl.principios,
        //     pl.prescripcion,
        //     pl.recomendaciones,
        //     pl.resultados,
        // 	pc.name AS category_name,
        // 	pc.id_lang AS category_lang
        // FROM
        // 	ps_product p
        // INNER JOIN ps_product_lang pl ON p.id_product = pl.id_product AND pl.id_lang='3'
        // INNER JOIN ps_category_lang pc ON p.id_category_default = pc.id_category AND pc.id_lang='3'
        // GROUP BY
        // 	p.id_product;
        // ";
        // $arr_presta = $dbPresta->fetchSQL($s);
        // showArr($arr_presta); exit;

        // foreach ($arr_presta as $key => $val) {
        //     $cnt_cat = $db->getCount("products_category", "name='".$val['category_name']."'");
        //     if ($cnt_cat == 0) {
        //         $arr_values = array(
        //             'name' => $val['category_name'],
        //         );
        //         // $category = $db->insertAction("products_category", $arr_values);
        //         $cat_id = $category[0]['id'];
        //     } else {
        //         $cat_id = $db->getValue('id', "products_category", "name='".$val['category_name']."'");
        //     }
        //
        //     $arr_values = array(
        //         'name' => addslashes(secure_mysql($val['name'])),
        //         'presentation' => addslashes(secure_mysql($val['description'])),
        //         'description' => addslashes(secure_mysql($val['description'])),
        //         'active_principles' => addslashes(secure_mysql($val['principios'])),
        //         'prescription' => addslashes(secure_mysql($val['prescripcion'])),
        //         'use_recommendations' => addslashes(secure_mysql($val['recomendaciones'])),
        //         'results_benefits' => addslashes(secure_mysql($val['resultados'])),
        //         'price' => number_format($val['price'],2,".",""),
        //         'category_id' => $cat_id,
        //         'create_at' => date('Y-m-d H:i:s'),
        //         'update_at' => date('Y-m-d'),
        //     );
        //
        //     // $product = $db->insertAction("products", $arr_values);
        //
        //     mkdir(APP_SYSTEM_PRODUCTS."product_".$product[0]['id']."/");
        //     mkdir(APP_SYSTEM_PRODUCTS."product_".$product[0]['id']."/cover/");
        //     mkdir(APP_SYSTEM_PRODUCTS."product_".$product[0]['id']."/gallery/");
        // }

        echo 'OK';
    }

    /**
     * Sincroniza las categorias
     * @return void
     */
    public function categoriesAction()
    {
        // $db = new Connection;
        // $dbPresta = new ConnectionPrestashop;
        // $s = "SELECT
        // 	p.`id_product`,
        // 	p.`id_manufacturer`,
        // 	p.`id_category_default`,
        // 	p.`price`,
        // 	p.`active`,
        // 	pl.`name`,
        // 	pl.`description`,
        // 	pl.`link_rewrite`,
        //     pl.principios,
        //     pl.prescripcion,
        //     pl.recomendaciones,
        //     pl.resultados,
        // 	pc.name AS category_name,
        // 	pc.id_lang AS category_lang
        // FROM
        // 	ps_product p
        // INNER JOIN ps_product_lang pl ON p.id_product = pl.id_product AND pl.id_lang='3'
        // INNER JOIN ps_category_lang pc ON p.id_category_default = pc.id_category AND pc.id_lang='3'
        // GROUP BY
        // 	p.id_product;
        // ";
        // $arr_presta = $dbPresta->fetchSQL($s);

        // foreach ($arr_presta as $key => $val) {
        //     // Se obtiene el producto guarado
        //     $s = "SELECT * FROM products WHERE name='".$val['name']."'";
        //     $arr_product = $db->fetchSQL($s);

        //     if (isset($arr_product[0]['id'])) {
        //         // Se busca la categoria del producto en prestashop
        //         $presta_cat = $val['category_name'];
        //         $cat_id = $db->getValue("id", "products_category", "name='".$presta_cat."'");

        //         // SI la categoria no existe se crea
        //         if (!$cat_id) {
        //             $arr_values = array(
        //                 'name' => $val['category_name']
        //             );
        //             $db->insertAction("products_category", $arr_values);
        //         }

        //         // Se guardan las categorias con su relación de productos
        //         $arr_values = array(
        //             'product_id' => $arr_product[0]['id'],
        //             'category_id' => $cat_id
        //         );
        //         $db->insertAction("products_categories", $arr_values);
        //     }
        // }
    }

    public function statusAction()
    {
        // $db = new Connection;
        // $dbPresta = new ConnectionPrestashop;
        // $s = "SELECT
        // 	p.`id_product`,
        // 	p.`id_manufacturer`,
        // 	p.`id_category_default`,
        // 	p.`price`,
        // 	p.`active`,
        // 	pl.`name`,
        // 	pl.`description`,
        // 	pl.`link_rewrite`,
        //     pl.principios,
        //     pl.prescripcion,
        //     pl.recomendaciones,
        //     pl.resultados,
        //     pl.description_short
        // FROM
        // 	ps_product p
        // LEFT JOIN ps_product_lang pl ON p.id_product = pl.id_product AND pl.id_lang='3'
        // GROUP BY
        // 	p.id_product;
        // ";
        // $arr_presta = $dbPresta->fetchSQL($s);

        // foreach ($arr_presta as $key => $val) {
        //     // Se obtiene el producto guarado
        //     $s = "SELECT * FROM products WHERE name='".addslashes($val['name'])."'";
        //     // echo $s;
        //     $arr_product = $db->fetchSQL($s);

        //     if (isset($arr_product[0]['id'])) {
        //         $arr_values = array(
        //             'description_short' => addslashes($val['description_short']),
        //             'status' => $val['active']
        //         );
        //         $db->updateAction("products", $arr_values, "id='".$arr_product[0]['id']."'");
        //     }
        // }
    }

    public function syncAction()
    {
        $db = new Connection;
        //$dbPresta = new ConnectionPrestashop;
        $act = @number_format($_GET['act'],0,"","");

        switch ($act) {
            case 1:
                $s = "SELECT * FROM ps_customer";
                $arr_p_users = $dbPresta->fetchSQL($s);

                foreach ($arr_p_users as $key => $val) {
                    $cnt_val = $db->getCount("user", "email='".$val['email']."'");
                    if ($cnt_val == 0) {
                        // Información de la cuenta
                        $arr_values = array(
                            'username' => mb_convert_case($val['email'], MB_CASE_LOWER, "UTF-8"),
                            'email' => mb_convert_case($val['email'], MB_CASE_LOWER, "UTF-8"),
                            'password' => hashPass(rand(00000000, 99999999), 12),
                            'role' => 3,
                            'status_id' => 1,
                            'username_canonical' => mb_convert_case($val['email'], MB_CASE_LOWER, "UTF-8"),
                            'email_canonical' => mb_convert_case($val['email'], MB_CASE_LOWER, "UTF-8"),
                            'account_type' => 1,
                            'create_at' => $val['date_add'],
                            'update_at' => date('Y-m-d H:i:s')
                        );
                        $user = $db->insertAction("user", $arr_values);
                        $code = genPass($user[0]['id'], 5);
                        $user = $db->updateAction("user", array('code' => $code), "id='".$user[0]['id']."'");

                        // Informacion personal
                        $arr_values = array(
                            'name' => mb_convert_case($val['firstname'], MB_CASE_UPPER, "UTF-8"),
                            'last_name' => mb_convert_case($val['lastname'], MB_CASE_UPPER, "UTF-8"),
                            'birthdate' => $val['birthday'],
                            'company' => "",
                            'user_id' => $user[0]['id'],
                            'country_id' => 73,
                            'province_id' => 1,
                        );
                        $user_information = $db->insertAction("user_information", $arr_values);

                        mkdir(APP_SYSTEM_USERS."user_".$user[0]['code']."/");
                        mkdir(APP_SYSTEM_USERS."user_".$user[0]['code']."/profile/");

                        if ($val['newsletter'] == 1) {
                            $arr_values = array(
                                'email' => mb_convert_case($val['email'], MB_CASE_LOWER, "UTF-8"),
                                'status' => '1',
                                'create_at' => ($val['newsletter_date_add']) ? $val['newsletter_date_add'] : date('Y-m-d H:i:s'),
                                'update_at' => date('Y-m-d H:i:s')
                            );
                            $newsletter = $db->insertAction("newsletter", $arr_values);
                        }
                    }
                }
                echo "Sincronizado"; exit;
                break;

            case 2: // sincroniza precios e impuestos
                $result = $this->productsPrice();
                echo "Sincronizado precios e impuestos de los productos: ".$result; exit;
                break;

            case 3:
                $result = $this->productsContent();
                echo "Sincronizado precios e impuestos de los productos: ".$result; exit;
                break;

            case 4:
                $result = $this->canonicalAction();
                echo "Nombres sincronizados"; exit;
                break;

            case 5: // DNI
                $result = $this->setDNI();
                echo "DNI sincornizados"; exit;
                break;

            case 6: // direcciones
                $result = $this->setAddres();
                echo "Direcciones sincronizadas"; exit;
                break;

            default:
                echo "Elige una opción"; exit;
                break;
        }
    }

    public function setAddres()
    {
        $db = new Connection;
        $dbPresta = new ConnectionPrestashop;

        // Se obtienen los emails de los ususarios registrados en la nueva web
        $s = "SELECT * FROM view_user";
        $arr_users = $db->fetchSQL($s);

        foreach ($arr_users as $key => $val) {
            $email = $val['email'];
            $id = $val['id'];
            $dni = $val['dni'];
            $name = $val['name'];
            $last_name = $val['last_name'];
            $company = $val['company'];
            $country = $val['country_id'];

            $cnt_bill_address = $db->getCount("bill_information", "user_id='".$id."'");
            $cnt_shipping_address = $db->getCount("shipping_address", "user_id='".$id."'");

            // informacion de dirección en el prstashop
            if ($cnt_bill_address == 0 && $cnt_shipping_address == 0) {
                $ps = "SELECT * FROM ps_customer WHERE email='".$email."'";
                $arr_presta_user = $dbPresta->fetchSQL($ps);
                if ($arr_presta_user) {
                    $presta_user_id = $arr_presta_user[0]['id_customer'];

                    // Dirección en prestashop
                    $pa = "SELECT * FROM ps_address WHERE id_customer='".$presta_user_id."'";
                    $arr_presta_address = $dbPresta->fetchSQL($pa);

                    if ($arr_presta_address) {
                        $presta_address_1 = $arr_presta_address[0]['address1'];
                        $presta_address_2 = $arr_presta_address[0]['address2'];
                        $presta_postal_code = $arr_presta_address[0]['postcode'];
                        $presta_city = $arr_presta_address[0]['city'];
                        $presta_state_id = $arr_presta_address[0]['id_state'];

                        // Provincia
                        $presta_state_name = $dbPresta->getValue("name", "ps_state", "id_state='".$presta_state_id."'");
                        $p = "SELECT * FROM province WHERE name='".$presta_state_name."'";
                        $arr_province = $db->fetchSQL($p);



                        if ($presta_state_name == "Avila") {
                            $id_province = 5;
                            $province_name = $db->getValue("name", "province", "id='".$id_province."'");
                        } elseif ($presta_state_name == "Orense") {
                            $id_province = 32;
                            $province_name = $db->getValue("name", "province", "id='".$id_province."'");
                        } elseif ($presta_state_name == "Islas Baleares") {
                            $id_province = 7;
                            $province_name = $db->getValue("name", "province", "id='".$id_province."'");
                        } elseif ($presta_state_name == "Gerona") {
                            $id_province = 17;
                            $province_name = $db->getValue("name", "province", "id='".$id_province."'");
                        } elseif ($presta_state_name == "Castellón de la Plana") {
                            $id_province = 12;
                            $province_name = $db->getValue("name", "province", "id='".$id_province."'");
                        } elseif ($presta_state_name == "Gipuzcoa") {
                            $id_province = 20;
                            $province_name = $db->getValue("name", "province", "id='".$id_province."'");
                        } elseif ($presta_state_name == "La Coruña") {
                            $id_province = 15;
                            $province_name = $db->getValue("name", "province", "id='".$id_province."'");
                        } elseif ($presta_state_name == "Sta. Cruz de Tenerife") {
                            $id_province = 38;
                            $province_name = $db->getValue("name", "province", "id='".$id_province."'");
                        } else {
                            $id_province = $arr_province[0]['id'];
                            $province_name = $arr_province[0]['name'];
                        }


                        echo "usuario: ".$val['email']."<br>";
                        echo "dirección 1: ".$presta_address_1."<br>";
                        echo "dirección 2: ".$presta_address_2."<br>";
                        echo "codigo postal: ".$presta_postal_code."<br>";
                        echo "ciudad: ".$presta_city."<br>";
                        echo "ID presta provincia: ".$presta_state_id."<br>";
                        echo "Nombre presta provincia: ".$presta_state_name."<br>";
                        echo "ID provincia: ".$id_province."<br>";

                        $arr_values = array(
                            'identification' => $dni,
                            'name' => $name,
                            'last_name' => $last_name,
                            'company' => $company,
                            'country_id' => $country,
                            'province_id' => $id_province,
                            'address_1' => addslashes($presta_address_1),
                            'address_2' => addslashes($presta_address_2),
                            'city' => addslashes($presta_city),
                            'postal_code' => $presta_postal_code,
                            'user_id' => $id,
                            'update_at' => date('Y-m-d')
                        );
                        $db->insertAction("bill_information", $arr_values);

                        $arr_values_2 = array(
                            'name_location' => "Direccion 1",
                            'country_id' => $country,
                            'province_id' => $id_province,
                            'address_1' => addslashes($presta_address_1),
                            'address_2' => addslashes($presta_address_2),
                            'city' => addslashes($presta_city),
                            'postal_code' => $presta_postal_code,
                            'user_id' => $id,
                            'create_at' => date('Y-m-d'),
                            'update_at' => date('Y-m-d')
                        );
                        $db->insertAction("shipping_address", $arr_values_2);

                        echo "Sincronizado";
                        echo "Nombre provincia: ".$province_name."<br><br><br><br>------------------------------------------------------------------<br><br><br>";
                    }
                }
            }
        }
    }

    public function setDNI()
    {
        $db = new Connection;
        $dbPresta = new ConnectionPrestashop;

        // Se obtienen los emails de los ususarios registrados en la nueva web
        $s = "SELECT * FROM view_user";
        $arr_users = $db->fetchSQL($s);

        foreach ($arr_users as $key => $val) {
            // Si el DNI esta vacio se sincroniza del prestashop
            if (!$val['dni']) {
                // Se obtiene el ID del usuario en prestashop
                $customerID = $dbPresta->getValue("id_customer", "ps_customer", "email='".$val['email']."'");
                $customerDNI = $dbPresta->getValue("dni", "ps_address", "id_customer='".$customerID."'");

                if ($customerDNI) {
                    echo "- ".strtoupper($customerDNI)."<br>";
                    $arr_values = array(
                        'dni' => strtoupper($customerDNI)
                    );

                    $db->updateAction("user_information", $arr_values, "user_id='".$val['id']."'");
                }
            }
        }
    }

    /**
     * Sincroniza los precio de los productos
     * @return void
     */
    public function productsPrice()
    {
        // $db = new Connection;
        // $dbPresta = new ConnectionPrestashop;
        // $s = "SELECT
        // 	p.`id_product`,
        // 	p.`id_manufacturer`,
        // 	p.`id_category_default`,
        // 	p.`price`,
        //     p.`id_tax_rules_group`,
        //     p.`active`,
        // 	pl.`name`,
        // 	pl.`description`,
        // 	pl.`link_rewrite`,
        //     pl.principios,
        //     pl.prescripcion,
        //     pl.recomendaciones,
        //     pl.resultados
        // FROM
        // 	ps_product p
        // INNER JOIN ps_product_lang pl ON p.id_product = pl.id_product AND pl.id_lang='3'
        // GROUP BY
        // 	p.id_product;
        // ";
        // $arr_presta = $dbPresta->fetchSQL($s);
        // $cnt = 0;
        // foreach ($arr_presta as $key => $val) {
        //     $cnt++;
        //     // Se obtiene el producto guardado
        //     $s = "SELECT * FROM products WHERE name='".addslashes($val['name'])."'";
        //     echo $s."<br><br>";
        //     $arr_product = $db->fetchSQL($s);

        //     if (isset($arr_product[0]['id'])) {
        //         // Se busca la categoria del producto en prestashop
        //         $price = number_format($val['price'],2,".","");
        //         $id_tax_rules_group = $val['id_tax_rules_group'];
        //         $tax = "";
        //         if ($id_tax_rules_group == 7) {
        //             $tax = 21;
        //         } elseif ($id_tax_rules_group == 6) {
        //             $tax = 10;
        //         }

        //         // Se guardan las categorias con su relación de productos
        //         $arr_values = array(
        //             'price' => $price,
        //             'iva' => $tax
        //         );
        //         $db->updateAction("products", $arr_values, "id='".$arr_product[0]['id']."'");
        //     }
        // }

        // return $cnt;
    }

    public function productsContent()
    {
        // $db = new Connection;
        // $dbPresta = new ConnectionPrestashop;

        // $s = "SELECT
        //     p.id_product,
        //     fp.id_feature_value,
        //     fvl.id_feature_value,
        //     fvl.value,
        //     pl.name
        // FROM
        //     ps_product p
        // LEFT JOIN ps_feature_product fp ON p.id_product = fp.id_product
        // LEFT JOIN ps_feature_value_lang fvl ON fp.id_feature_value = fvl.id_feature_value
        // LEFT JOIN ps_product_lang pl ON p.id_product = pl.id_product AND pl.id_lang='3'
        // GROUP BY p.id_product";
        // $arr_presta = $dbPresta->fetchSQL($s);
        // $cnt = 0;

        // foreach ($arr_presta as $key => $val) {
        //     $cnt++;
        //     // Se obtiene el producto guardado
        //     $s = "SELECT * FROM products WHERE name='".addslashes($val['name'])."'";
        //     echo $s."<br><br>";
        //     $arr_product = $db->fetchSQL($s);

        //     if (isset($arr_product[0]['id'])) {
        //         $content = $val['value'];

        //         // Se guardan las categorias con su relación de productos
        //         $arr_values = array(
        //             'content' => $content,
        //         );
        //         $db->updateAction("products", $arr_values, "id='".$arr_product[0]['id']."'");
        //     }
        // }

        // return $cnt;
    }

    public function canonicalAction()
    {
        $db = $this->db;

        $s = "SELECT * FROM products_category";
        $arr_cat = $db->fetchSQL($s);

        foreach ($arr_cat as $key => $val) {
            $arr_values = array(
                'name_canonical' => friendlyURL($val['name'], "upper")
            );

            $db->updateAction("products_category", $arr_values, "id='".$val['id']."'");
        }

        $s = "SELECT * FROM brands";
        $arr_brand = $db->fetchSQL($s);

        foreach ($arr_brand as $key => $val) {
            $arr_values = array(
                'name_canonical' => friendlyURL($val['name'], "upper")
            );

            $db->updateAction("brands", $arr_values, "id='".$val['id']."'");
        }
    }
}


?>
