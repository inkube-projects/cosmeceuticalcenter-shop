<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');

/**
* Controlador BrandsController
*/
class BrandsController extends ControllerAware
{
   /**
    * Constructor
    */
    public function __construct()
    {
        parent::__construct();
    }

   /**
     * Listados de brands
     *
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;
        $q = "SELECT * FROM brands";
        $arr_brands = $db->fetchSQL($q);

        foreach ($arr_brands as $key => $val) {
            $created = datetime_format($val['create_at']);
            $arr_brands[$key]['formed_create'] = $created['date'];
            $arr_brands[$key]['formed_image'] = ($val['image']) ? APP_IMG_BRANDS.$val['image'] : APP_NO_IMAGES."no_image.jpg";
        }

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once("html/brands/brands-list.php");
    }

   /**
     * Formulario para agregar datos en brands
     *
     * @return void
     */
    public function addAction()
    {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $act = 1;
        $name = "";
        $description = "";
        $image = APP_NO_IMAGES."no_image.jpg";
        $status = 1;
        $iva = "";
        $create_at = "";
        $update_at = "";
        $flash_message = "";

        require_once('html/brands/brands-form.php');
    }

   /**
     * Formulario para editar datos en brands
     *
     * @return void
     */
    public function editAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."'", "brands", BASE_URL."404");
        $frm = "frm-edit"; $act = 2;
        $s = "SELECT * FROM brands WHERE id='".$id."'";
        $arr_brands = $db->fetchSQL($s);

        $name = $arr_brands[0]['name'];
        $description = $arr_brands[0]['description'];
        $image = ($arr_brands[0]['image']) ? APP_IMG_BRANDS.$arr_brands[0]['image'] : APP_NO_IMAGES."no_image.jpg";
        $status = $arr_brands[0]['status'];
        $iva = $arr_brands[0]['iva'];
        $create_at = $arr_brands[0]['create_at'];
        $update_at = $arr_brands[0]['update_at'];
        $flash_message = @$this->flashMessageGlobal($_GET);

        require_once('html/brands/brands-form.php');
    }

    /**
     * Elimina una entrada de brands
     *
     * @return object
     */
    public function removeAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "brands", "La entrada no existe");
            $s = "SELECT * FROM brands WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);

            $db->deleteAction("brands", "id='".$id."'");
            if ($arr_sql[0]['image']) {
                @unlink(APP_SYSTEM_BRANDS.$arr_sql[0]['image']);
            }
            $logs->addLogs(sprintf("Eliminando entrada de brands ID: %d", $id));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }
}
?>
