<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');

/**
 * Controlador de los cupones
 */
class CouponController extends ControllerAware
{
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Listado de cupones
     * @return void
     */
    public function listAction()
    {
        $db = $this->db;

        $s = "SELECT * FROM coupon";
        $arr_coupon = $db->fetchSQL($s);

        foreach ($arr_coupon as $key => $val) {
            $create = datetime_format($val['create_at']);

            $arr_coupon[$key]['formed_created'] = $create['date'];
            $arr_coupon[$key]['formed_init'] = to_date($val['init_date']);
            $arr_coupon[$key]['formed_end'] = to_date($val['end_date']);
        }

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once("html/coupon/coupon-list.php");
    }

    /**
     * Agrega un cupón
     * @return void
     */
    public function addAction()
    {
        $db = $this->db;
        $id = ""; $frm = "frm-add"; $act = 1;
        $name = "";
        $canonical_id = "";
        $description = "";
        $discount = "";
        $uses = "";
        $init_date = "";
        $end_date = "";
        $create_at = "";
        $update_at = "";
        $range = "";

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once("html/coupon/coupon-form.php");
    }

    /**
     * Edita un cupón
     * @return void
     */
    public function editAction()
    {
        $db = $this->db;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."'", "coupon", BASE_URL."404");
        $frm = "frm-edit"; $act = 2;

        $s = "SELECT * FROM coupon WHERE id='".$id."'";
        $arr_coupon = $db->fetchSQL($s);

        $name = $arr_coupon[0]['name'];
        $canonical_id = $arr_coupon[0]['canonical_id'];
        $description = $arr_coupon[0]['description'];
        $discount = $arr_coupon[0]['discount'];
        $uses = $arr_coupon[0]['uses'];
        $init_date = $arr_coupon[0]['init_date'];
        $end_date = $arr_coupon[0]['end_date'];
        $create_at = $arr_coupon[0]['create_at'];
        $update_at = $arr_coupon[0]['update_at'];
        $range = to_date($init_date)." al ".to_date($end_date);

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once("html/coupon/coupon-form.php");
    }

    /**
     * Elimina una entrada de coupon
     *
     * @return object
     */
    public function removeAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "coupon", "La entrada no existe");
            $s = "SELECT * FROM coupon WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);

            $db->deleteAction("coupon", "id='".$id."'");
            
            $logs->addLogs(sprintf("Eliminando entrada de coupon ID: %d", $id));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }
}


?>
