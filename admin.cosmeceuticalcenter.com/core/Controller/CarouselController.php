<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');

/** 
* Controlador CarouselController
*/
class CarouselController extends ControllerAware
{
   /**
    * Constructor
    */
    public function __construct()
    {
        parent::__construct();
    }

   /**
     * Listados de carousel
     *
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;
        $q = "SELECT * FROM carousel";
        $arr_carousel = $db->fetchSQL($q);

        foreach ($arr_carousel as $key => $val) {
            $create = datetime_format($val['create_at']);
            $arr_carousel[$key]['formed_created'] = $create['date'];
            $arr_carousel[$key]['formed_status'] = ($val['status'] == 0) ? '<span class="text-danger">Inactivo</span>' : '<span class="text-success">Activo</span>';
            $arr_carousel[$key]['formed_color'] = ($val['color'] == "css_background_Black") ? "Negro" : "Blanco";
            $arr_carousel[$key]['formed_image'] = APP_IMG_CAROUSEL.$val['image'];
        }

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once("html/carousel/carousel-list.php");
    }

   /**
     * Formulario para agregar datos en carousel
     *
     * @return void
     */
    public function addAction()
    {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $act = 1;
        $image = APP_NO_IMAGES."no_image.jpg";
        $description = "";
        $color = "";
        $url = "";
        $status = "";
        $create_at = "";
        $update_at = "";
        $flash_message = "";

        require_once('html/carousel/carousel-form.php');
    }

   /**
     * Formulario para editar datos en carousel
     *
     * @return void
     */
    public function editAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."'", "carousel", BASE_URL."404");
        $frm = "frm-edit"; $act = 2;
        $s = "SELECT * FROM carousel WHERE id='".$id."'";
        $arr_carousel = $db->fetchSQL($s);

        $image = APP_IMG_CAROUSEL.$arr_carousel[0]['image'];
        $description = $arr_carousel[0]['description'];
        $color = $arr_carousel[0]['color'];
        $url = $arr_carousel[0]['url'];
        $status = $arr_carousel[0]['status'];
        $create_at = $arr_carousel[0]['create_at'];
        $update_at = $arr_carousel[0]['update_at'];
        $flash_message = @$this->flashMessageGlobal($_GET);

        require_once('html/carousel/carousel-form.php');
    }

    /**
     * Elimina una entrada de carousel
     *
     * @return void
     */
    public function removeAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "carousel", "La entrada no existe");
            $s = "SELECT * FROM carousel WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);

            $db->deleteAction("carousel", "id='".$id."'");

            if ($arr_sql[0]['image']) {
                @unlink(APP_SYSTEM_CAROUSEL.$arr_sql[0]['image']);
            }

            $logs->addLogs(sprintf("Eliminando Imagen de Carrusel ID: %d", $id));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }
}
?>

