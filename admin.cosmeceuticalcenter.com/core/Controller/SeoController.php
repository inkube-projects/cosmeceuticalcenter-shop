<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');

/**
* Controlador SeoController
*/
class SeoController extends ControllerAware
{
   /**
    * Constructor
    */
    public function __construct()
    {
        parent::__construct();
    }

   /**
     * Listados de seo
     *
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;
        $q = "SELECT * FROM view_seo";
        $arr_seo = $db->fetchSQL($q);
        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once("html/seo/seo-list.php");
    }

   /**
     * Formulario para agregar datos en seo
     *
     * @return void
     */
    public function addAction()
    {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $act = 1;
        $keywords = "";
        $description = "";
        $section_id = "";

        // Secciones del SEO
        $s = "SELECT * FROM seo_section";
        $arr_section = $db->fetchSQL($s);

        $flash_message = "";
        require_once('html/seo/seo-form.php');
    }

   /**
     * Formulario para editar datos en seo
     *
     * @return void
     */
    public function editAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."'", "seo", BASE_URL."404");
        $frm = "frm-edit"; $act = 2;
        $s = "SELECT * FROM seo WHERE id='".$id."'";
        $arr_seo = $db->fetchSQL($s);

        $keywords = $arr_seo[0]['keywords'];
        $description = $arr_seo[0]['description'];
        $section_id = $arr_seo[0]['section_id'];

        // Secciones del SEO
        $s = "SELECT * FROM seo_section";
        $arr_section = $db->fetchSQL($s);

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/seo/seo-form.php');
    }

    /**
     * Elimina una entrada de seo
     *
     * @return object
     */
    public function removeAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "seo", "La entrada no existe");
            $s = "SELECT * FROM seo WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);

            $db->deleteAction("seo", "id='".$id."'");
            $logs->addLogs(sprintf("Eliminando entrada de seo ID: %d", $id));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }
}
?>
