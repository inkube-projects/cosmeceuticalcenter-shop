<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');

/**
* Controlador FeaturesController
*/
class FeaturesController extends ControllerAware
{
   /**
    * Constructor
    */
    public function __construct()
    {
        parent::__construct();
    }

   /**
     * Listados de features
     *
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;
        $q = "SELECT * FROM view_features";
        $arr_features = $db->fetchSQL($q);

        foreach ($arr_features as $key => $val) {
            $create = datetime_format($val['feature_create']);
            $arr_features[$key]['u_name'] = $val['user_name']." ".$val['last_name'];
            $arr_features[$key]['formed_status'] = ($val['status'] == "1") ? "Habilitada" : "No habilitada";
            $arr_features[$key]['formed_create'] = $create['date'];
        }

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once("html/features/features-list.php");
    }

   /**
     * Formulario para agregar datos en features
     *
     * @return void
     */
    public function addAction()
    {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $act = 1;
        $user_id = "";
        $product_id = "";
        $order_id = "";
        $review = "";
        $score = "";
        $status = "";
        $create_at = "";
        $update_at = "";
        $flash_message = "";

        require_once('html/features/features-form.php');
    }

   /**
     * Formulario para editar datos en features
     *
     * @return void
     */
    public function editAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."'", "features", BASE_URL."404");
        $frm = "frm-edit"; $act = 2;
        $s = "SELECT * FROM view_features WHERE id='".$id."'";
        $arr_features = $db->fetchSQL($s);

        $user_id = $arr_features[0]['user_id'];
        $product_id = $arr_features[0]['product_id'];
        $product_name = $arr_features[0]['product_name'];
        $order_id = $arr_features[0]['order_id'];
        $order_code = $arr_features[0]['order_code'];
        $review = $arr_features[0]['review'];
        $score = $arr_features[0]['score'];
        $status = $arr_features[0]['status'];
        $create_at = $arr_features[0]['feature_create'];
        $u_name = $arr_features[0]['user_name']." ".$arr_features[0]['last_name'];
        $create = datetime_format($arr_features[0]['feature_create']);

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/features/features-form.php');
    }

    /**
     * Elimina una entrada de features
     *
     * @return object
     */
    public function removeAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "features", "La entrada no existe");
            $s = "SELECT * FROM features WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);

            $db->deleteAction("features", "id='".$id."'");
            $logs->addLogs(sprintf("Eliminando entrada de features ID: %d", $id));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }
}
?>
