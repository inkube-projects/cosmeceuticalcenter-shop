<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');

/**
 * Controlador para el index y el Home
 */
class IndexController extends ControllerAware
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Pantalla de Inicio de sesión
     * @return void
     */
    public function indexAction()
    {
        if (isset($_SESSION['ADMIN_SESSION_COSME']) && $_SESSION['ADMIN_SESSION_COSME'] != "") {
            header('location: '.BASE_URL.'admin/dashboard');
        } else {
            require_once('html/index/login.php');
        }
    }

    /**
     * Pantalla principal del gestor
     * @return void
     */
    public function homeAction()
    {
        $db = new Connection;

        if (isset($_SESSION['ADMIN_SESSION_COSME']) && $_SESSION['ADMIN_SESSION_COSME'] != "") {
            $cnt_products = $db->getCount("products", "");
            $cnt_users = $db->getCount("user", "");
            $cnt_brands = $db->getCount("brands", "");
            $cnt_categories = $db->getCount("products_category", "");

            // Articulos agitados y por agotarse
            $s = "SELECT * FROM view_inventory WHERE quantity < 3 AND status='1'";
            $arr_inventory = $db->fetchSQL($s);

            foreach ($arr_inventory as $key => $val) {
                $arr_inventory[$key]['brand_name'] = $db->getValue("name", "brands", "id='".$val['brand_id']."'");
            }

            foreach ($arr_inventory as $key => $val) {
                $arr_inventory[$key]['icon'] = ($val['quantity'] == 0) ? '<i class="fas fa-times-circle text-danger"></i>' : '<i class="fas fa-exclamation-circle text-warning"></i>';
            }

            // Ordenes nuevas
            $s = "SELECT * FROM view_orders WHERE status_id='1' ORDER BY id DESC";
            $arr_orders = $db->fetchSQL($s);

            foreach ($arr_orders as $key => $val) {
                $create = datetime_format($val['create_at']);
                $q = "SELECT * FROM view_user WHERE id='".$val['user_id']."'";
                $arr_user = $db->fetchSQL($q);

                $arr_orders[$key]['formed_date'] = $create['date'];
                $arr_orders[$key]['user_names'] = $arr_user[0]['name']." ".$arr_user[0]['last_name'];
                if ($val['status_id'] == 1) {
                    $arr_orders[$key]['formed_status'] = '<span class="text-danger"><i class="fa fa-hourglass-start css-fonsize16"></i> '.$val['status_name'].'</span>';
                } elseif ($val['status_id'] == 2) {
                    $arr_orders[$key]['formed_status'] = '<span class="text-info"><i class="fa fas fa-shipping-fast css-fonsize16"></i> '.$val['status_name'].'</span>';
                } elseif ($val['status_id'] == 3) {
                    $arr_orders[$key]['formed_status'] = '<span class="text-success"><i class="fas fa-check-circle css-fonsize16"></i> '.$val['status_name'].'</span>';
                }

                $label = "";
                $cnt_orders = $db->getCount("orders", "user_id='".$val['user_id']."'");
                $cnt_newsletter = $db->getCount("newsletter", "email='".$arr_user[0]['email']."'");

                if ($cnt_orders == 1 && $cnt_newsletter == 0) {
                    $label = '<span class="label label-success">Nuevo</span>';
                }
                $arr_orders[$key]['label'] = $label;

            }
            // Comentarios
            $q = "SELECT * FROM view_features";
            $arr_features = $db->fetchSQL($q);

            foreach ($arr_features as $key => $val) {
                $create = datetime_format($val['feature_create']);
                $arr_features[$key]['u_name'] = $val['user_name']." ".$val['last_name'];
                $arr_features[$key]['formed_status'] = ($val['status'] == "1") ? "Habilitada" : "No habilitada";
                $arr_features[$key]['formed_create'] = $create['date'];
            }

            require_once('html/index/home.php');
        } else {
            header('location: '.BASE_URL.'admin/login');
        }
    }

    /**
     * Cerrar sesión
     * @return void
     */
    public function logOutAction()
    {
        $_SESSION['ADMIN_SESSION_COSME'] = "";
        session_unset(); //para eliminar las variables de sesion
        session_destroy(); //con esto destruyes la sesion
        header('location: '.BASE_URL.'admin/login');
    }
}

?>
