<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');

/**
* Controlador InventoryController
*/
class InventoryController extends ControllerAware
{
   /**
    * Constructor
    */
    public function __construct()
    {
        parent::__construct();
    }

   /**
     * Listados de inventory
     *
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;
        $q = "SELECT * FROM view_inventory";
        $arr_inventory = $db->fetchSQL($q);

        foreach ($arr_inventory as $key => $val) {
            $update = datetime_format($val['update_at']);
            $arr_inventory[$key]['formed_update'] = $update['date'];
        }

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once("html/inventory/inventory-list.php");
    }

   /**
     * Formulario para agregar datos en inventory
     *
     * @return void
     */
    public function addAction()
    {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $act = 1;
        $product = "";
        $quantity = 0;
        $unity = "";

        // Productos que no poseen inventario
        $s = "SELECT * FROM products WHERE NOT id IN (SELECT product_id FROM inventory)";
        $arr_products = $db->fetchSQL($s);

        // Unidad
        $s = "SELECT * FROM inventory_unit";
        $arr_unit = $db->fetchSQL($s);

        $flash_message = "";
        require_once('html/inventory/inventory-form.php');
    }

   /**
     * Formulario para editar datos en inventory
     *
     * @return void
     */
    public function editAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."'", "inventory", BASE_URL."404");
        $frm = "frm-edit"; $act = 2;

        $s = "SELECT * FROM inventory WHERE id='".$id."'";
        $arr_inventory = $db->fetchSQL($s);

        // Unidad
        $s = "SELECT * FROM inventory_unit";
        $arr_unit = $db->fetchSQL($s);

        $product = $db->getValue("name", "products", "id='".$arr_inventory[0]['product_id']."'");
        $quantity = $arr_inventory[0]['quantity'];
        $unity = $arr_inventory[0]['unity_id'];

        // Usuarios a notificar
        $s = "SELECT * FROM notify_products WHERE product_id='".$arr_inventory[0]['product_id']."'";
        $arr_notify = $db->fetchSQL($s);

        foreach ($arr_notify as $key => $val) {
            $create = datetime_format($val['create_at']);

            $arr_notify[$key]['formed_date'] = $create['date'];
            if ($val['status'] == 0) {
                $arr_notify[$key]['formed_status'] = '<span class="text-danger">No notificado</span>';
            } elseif ($val['status'] == 1) {
                $arr_notify[$key]['formed_status'] = '<span class="text-success">Notificado en WEB</span>';
            } elseif ($val['status'] == 2) {
                $arr_notify[$key]['formed_status'] = '<span class="text-success">Notificado por E-mail</span>';
            } elseif ($val['status'] == 3) {
                $arr_notify[$key]['formed_status'] = '<span class="text-success">Notificado</span>';
            }
        }

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/inventory/inventory-form.php');
    }

    /**
     * Elimina una entrada de inventory
     *
     * @return void
     */
    public function removeAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "inventory", "La entrada no existe");
            $s = "SELECT * FROM inventory WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);

            $db->deleteAction("inventory", "id='".$id."'");
            $logs->addLogs(sprintf("Eliminando entrada de inventory ID: %d", $id));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }
}
?>
