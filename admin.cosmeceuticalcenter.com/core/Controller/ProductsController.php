<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');

/**
* Controlador ProductsController
*/
class ProductsController extends ControllerAware
{
   /**
    * Constructor
    */
    public function __construct()
    {
        parent::__construct();
    }

   /**
     * Listados de products
     *
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;
        $src_brand = (isset($_POST['src_brand']) && (!empty($_POST['src_brand']))) ? @number_format($_POST['src_brand'],0,'','') : "" ;
        $src_cat = (isset($_POST['src_cat']) && (!empty($_POST['src_cat']))) ? @number_format($_POST['src_cat'],0,'','') : "" ;
        $src_status = (isset($_POST['src_status']) && (!empty($_POST['src_status']))) ? @number_format($_POST['src_status'],0,'','') : "" ;
        $arr_search = array();
        $w = "";
        $page = 0;

        if (isset($_SESSION['formed_filter']['brand'])) {
            $src_brand = ($src_brand == "") ? $_SESSION['formed_filter']['brand'] : "";
        }
        if (isset($_SESSION['formed_filter']['category'])) {
            $src_cat = ($src_cat == "") ? $_SESSION['formed_filter']['category'] : "";
        }
        if (isset($_SESSION['formed_filter']['status'])) {
            $src_status = ($src_status == "") ? $_SESSION['formed_filter']['status'] : "";
        }

        $q = "SELECT * FROM view_products WHERE NOT id=''";

        if ($src_brand != "") { // Por marca
            $q .= " AND brand_id='".$src_brand."'";
            $w .= " AND brand_id='".$src_brand."'";
            $arr_search['brand'] = $src_brand;
        }
        if ($src_cat != "") { // Por categoría
            $q .= " AND id IN (SELECT product_id FROM products_categories WHERE category_id='".$src_cat."')";
            $w .= " AND id IN (SELECT product_id FROM products_categories WHERE category_id='".$src_cat."')";
            $arr_search['category'] = $src_cat;
        }
        if ($src_status != "") { // Por estado
            $arr_search['status'] = $src_status;
            $q .= " AND status='".($src_status - 1)."'";
            $w .= " AND status='".($src_status - 1)."'";
        }

        if (isset($_SESSION['formed_filter']['product_id'])) {
            $product_number = $db->getCount("products", "id <= '".$_SESSION['formed_filter']['product_id']."' ".$w);
            $page = (ceil($product_number / 10) * 10) - 10;
        }

        $arr_products = $db->fetchSQL($q);
        foreach ($arr_products as $key => $val) {
            // inventario
            $q = "SELECT * FROM view_inventory WHERE product_id='".$val['id']."'";
            $arr_inventory = $db->fetchSQL($q);

            $create = datetime_format($val['product_create_at']);
            $arr_products[$key]['formed_date'] = $create['date'];
            $arr_products[$key]['inventory'] = '<a href="admin/inventory/edit/'.@$arr_inventory[0]['inventory_id'].'">'.@$arr_inventory[0]['quantity']." ".@$arr_inventory[0]['unit_name'].'</a>';
            $arr_products[$key]['inventory_quantity'] = @number_format($arr_inventory[0]['quantity'],0,"","");

            // Inventarrio de colores y tallas
            $cnt_colors = $db->getCount("products_colors", "product_id='".$val['id']."'");
            $cnt_size = $db->getCount("products_sizes", "product_id='".$val['id']."'");
            $arr_products[$key]['inventory_extra'] = ($cnt_colors > 0 || $cnt_size > 0) ? true : false;


            // categorias
            $arr_products[$key]['formed_categories'] = "";
            $s = "SELECT
            	pcs.id,
            	pcs.product_id,
            	pcs.category_id,
            	pc.`name` AS category_name,
            	pc.description AS category_description
            FROM
            	products_categories pcs
            LEFT JOIN products_category pc ON pcs.category_id = pc.id WHERE pcs.product_id='".$val['id']."'";
            $arr_p_categories = $db->fetchSQL($s);
            foreach ($arr_p_categories as $i => $cat) {
                $arr_products[$key]['formed_categories'] .= ($i == 0) ? $cat['category_name'] : ", ".$cat['category_name'];
            }
            $arr_products[$key]['formed_image'] = ($val['cover_image']) ? APP_IMG_PRODUCTS."product_".$val['id']."/cover/min_".$val['cover_image'] : APP_NO_IMAGES."no_image.jpg";
            $arr_products[$key]['status_icon'] = ($val['status'] == "0") ? '<i class="fas fa-times-circle text-danger"></i>' : '<i class="fas fa-check-circle text-success"></i>';
        }

        $s = "SELECT * FROM brands";
        $arr_brands = $db->fetchSQL($s);

        $s = "SELECT * FROM products_category";
        $arr_categories = $db->fetchSQL($s);

        // Se agregan los filtros a una variable de sesion
        $_SESSION['products_filter'] = $arr_search;
        if (isset($_SESSION['formed_filter'])) { unset($_SESSION['formed_filter']); }

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once("html/products/products-list.php");
    }

   /**
     * Formulario para agregar datos en products
     *
     * @return void
     */
    public function addAction()
    {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $act = 1;
        $name = "";
        $presentation = "";
        $description = "";
        $description_short = "";
        $active_principles = "";
        $prescription = "";
        $use_recommendations = "";
        $results_benefits = "";
        $price_iva = "";
        $content = "";
        $cover_image = APP_NO_IMAGES."no_image.jpg";
        $status = "1";
        $category = "";
        $subcategory = "";
        $brand = "";
        $product_create_at = "";
        $update_at = "";
        $flash_message = "";
        $seo_keywords = "";
        $seo_description = "";
        $seo_title = "";
        $arr_subcategories = array();
        $arr_s_cat = array();
        $tags = "";
        $iva = "";
        $novelty = "1";
        $novelty_range = "";
        $discount_promo = "0";
        $discount_price = "";
        $discount_range = "";
        $weekly_recommendation = "0";
        $weekly_price = "";
        $weekly_range = "";
        $arr_s_size = array();

        // Categorías
        $s = "SELECT * FROm products_category";
        $arr_category = $db->fetchSQL($s);

        // Marcas
        $s = "SELECT * FROM brands";
        $arr_brands = $db->fetchSQL($s);

        // Tallas
        $s = "SELECT * FROM size";
        $arr_sizes = $db->fetchSQL($s);

        @clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/gallery/");
        @clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/cover/");
        require_once('html/products/products-form.php');
    }

   /**
     * Formulario para editar datos en products
     *
     * @return void
     */
    public function editAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."'", "products", BASE_URL."404");
        $frm = "frm-edit"; $act = 2;
        $s = "SELECT * FROM products WHERE id='".$id."'";
        $arr_products = $db->fetchSQL($s);
        $arr_filter = array();


        if (isset($_SESSION['products_filter'])) {
            $arr_filter = $_SESSION['products_filter'];
            $arr_filter['product_id'] = $id;
            $_SESSION['formed_filter'] = $arr_filter;
            // unset($_SESSION['products_filter']);
        } else {
            $arr_filter['product_id'] = $id;
            $_SESSION['formed_filter'] = $arr_filter;
        }

        $name = $arr_products[0]['name'];
        $presentation = $arr_products[0]['presentation'];
        $description = $arr_products[0]['description'];
        $description_short = $arr_products[0]['description_short'];
        $active_principles = $arr_products[0]['active_principles'];
        $prescription = $arr_products[0]['prescription'];
        $use_recommendations = $arr_products[0]['use_recommendations'];
        $results_benefits = $arr_products[0]['results_benefits'];
        $price = $arr_products[0]['price'];
        $price_iva = $arr_products[0]['price_iva'];
        $content = $arr_products[0]['content'];
        $cover_image = ($arr_products[0]['cover_image']) ? APP_IMG_PRODUCTS."product_".$id."/cover/".$arr_products[0]['cover_image'] : APP_NO_IMAGES."no_image.jpg";
        $status = $arr_products[0]['status'];
        $category = $arr_products[0]['category_id'];
        $subcategory = $arr_products[0]['sub_category_id'];
        $brand = $arr_products[0]['brand_id'];
        $tags = $arr_products[0]['tags'];
        $iva = $arr_products[0]['iva'];
        $novelty = ($db->getCount("novelty", "product_id='".$id."'") > 0 || $arr_products[0]['novelty'] == "1") ? "1" : "0";
        $discount_promo = ($db->getCount("discount", "product_id='".$id."'") > 0 || $arr_products[0]['discount_promo'] == "1") ? "1" : "0";
        $weekly_recommendation = ($db->getCount("weekly_recommendation", "product_id='".$id."'") > 0 || $arr_products[0]['weekly_recommendation'] == "1") ? "1" : "0";
        $create_at = $arr_products[0]['create_at'];
        $update_at = $arr_products[0]['update_at'];

        // Galería
        $s = "SELECT * FROM products_gallery WHERE product_id='".$id."'";
        $arr_gallery = $db->fetchSQL($s);

        foreach ($arr_gallery as $key => $val) {
            $arr_gallery[$key]['formed_image'] = APP_IMG_PRODUCTS."product_".$id."/gallery/min_".$val['image'];
        }

        // Marcas
        $s = "SELECT * FROM brands";
        $arr_brands = $db->fetchSQL($s);

        $s = "SELECT * FROM products_seo WHERE product_id='".$id."'";
        $arr_seo = $db->fetchSQL($s);

        $seo_keywords = @$arr_seo[0]['keywords'];
        $seo_description = @$arr_seo[0]['description'];
        $seo_title = @$arr_seo[0]['title'];

        // Categorías
        $s = "SELECT * FROm products_category";
        $arr_category = $db->fetchSQL($s);

        $arr_cat = array();
        foreach ($arr_category as $val) {
            $arr_cat[] = $val['id'];
        }

        // Categorías seleccionadas
        $s = "SELECT * FROM products_categories WHERE product_id='".$id."'";
        $arr_p_categories = $db->fetchSQL($s);
        $arr_s_cat = array();

        foreach ($arr_p_categories as $val) {
            $arr_s_cat[] = $val['category_id'];
        }

        // Subcategoría
        $s = "SELECT * FROM products_sub_category WHERE category_id IN (".implode(", ", $arr_cat).")";
        $arr_subcategories = $db->fetchSQL($s);

        // Sub categorías seleccionadas
        $s = "SELECT * FROM products_subcategories WHERE product_id='".$id."'";
        $arr_p_subcategories = $db->fetchSQL($s);
        $arr_s_subcat = array();

        foreach ($arr_p_subcategories as $val) {
            $arr_s_subcat[] = $val['subcategory_id'];
        }

        // Rango para novedad
        $s = "SELECT * FROM novelty WHERE product_id='".$id."'";
        $arr_novelty = $db->fetchSQL($s);
        $novelty_range = (isset($arr_novelty[0]['date_init'])) ? to_date($arr_novelty[0]['date_init'])." al ".to_date($arr_novelty[0]['date_end']) : "";

        // Informacion de descuento / promoción
        $s = "SELECT * FROM discount WHERE product_id='".$id."'";
        $arr_discount = $db->fetchSQL($s);
        $discount_price = "";
        $discount_range = "";

        if (isset($arr_discount[0]['id'])) {
            $discount_price = $arr_discount[0]['new_price'];
            $discount_range = to_date($arr_discount[0]['date_init'])." al ".to_date($arr_discount[0]['date_end']);
        }

        // Informacion de recomendación semanal
        $s = "SELECT * FROM weekly_recommendation WHERE product_id='".$id."'";
        $arr_weekly = $db->fetchSQL($s);
        $weekly_price = "";
        $weekly_range = "";

        if (isset($arr_weekly[0]['id'])) {
            $weekly_price = $arr_weekly[0]['new_price'];
            $weekly_range = to_date($arr_weekly[0]['date_init'])." al ".to_date($arr_weekly[0]['date_end']);
        }
        // showArr($weekly_range); exit;

        // Tallas
        $s = "SELECT * FROM size";
        $arr_sizes = $db->fetchSQL($s);

        $s = "SELECT * FROM products_sizes WHERE product_id='".$id."'";
        $arr_products_sizes = $db->fetchSQL($s);

        $arr_s_size = array();
        foreach ($arr_products_sizes as $key => $val) {
            $arr_s_size[] = $val['size_id'];
        }

        // Colores
        $s = "SELECT * FROM colors";
        $arr_colors = $db->fetchSQL($s);

        $s = "SELECT * FROM products_colors WHERE product_id='".$id."'";
        $arr_p_colors = $db->fetchSQL($s);

        foreach ($arr_p_colors as $key => $p_color) {
            $q = "SELECT * FROM color_image WHERE product_id='".$p_color['product_id']."' AND color_id='".$p_color['color_id']."'";
            $arr_image = $db->fetchSQL($q);

            $arr_p_colors[$key]['gallery'] = $arr_image;
        }


        $cnt_color = $db->getCount("products_colors", "product_id='".$id."'");

        $flash_message = @$this->flashMessageGlobal($_GET);
        @clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/gallery/");
        @clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/cover/");
        require_once('html/products/products-form.php');
    }

    /**
     * Elimina una entrada de products
     *
     * @return void
     */
    public function removeAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "products", "La entrada no existe");
            $s = "SELECT * FROM products WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);

            $db->deleteAction("products", "id='".$id."'");

            removeDir(APP_SYSTEM_PRODUCTS."product_".$id."/");

            $logs->addLogs(sprintf("Eliminando producto ID: %d - %s", $id, $arr_sql[0]['name']));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }

    /**
      * Listados de products_category
      *
      * @return void
      */
     public function pcListAction()
     {
         $db = new Connection;
         $q = "SELECT * FROM products_category";
         $arr_products_category = $db->fetchSQL($q);
         $flash_message = @$this->flashMessageGlobal($_GET);
         require_once("html/products/products-category-list.php");
     }

    /**
      * Formulario para agregar datos en products_category
      *
      * @return void
      */
     public function pcAddAction()
     {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $act = 1;
        $name = "";
        $description = "";
        $image = APP_NO_IMAGES."no_image.jpg";

        $flash_message = "";
        require_once('html/products/products-category-form.php');
     }

    /**
      * Formulario para editar datos en products_category
      *
      * @return void
      */
     public function pcEditAction()
     {
         $db = new Connection;
         $id = @number_format($_GET['id'],0,"","");
         $this->validRecordCustom("id='".$id."'", "products_category", BASE_URL."404");
         $frm = "frm-edit"; $act = 2;
         $s = "SELECT * FROM products_category WHERE id='".$id."'";
         $arr_products_category = $db->fetchSQL($s);

         $name = $arr_products_category[0]['name'];
         $description = $arr_products_category[0]['description'];
         $image = ($arr_products_category[0]['image']) ? APP_IMG_CATEGORY.$arr_products_category[0]['image'] : APP_NO_IMAGES."no_image.jpg";

         $flash_message = @$this->flashMessageGlobal($_GET);
         require_once('html/products/products-category-form.php');
     }

     /**
      * Elimina una entrada de products_category
      *
      * @return object
      */
     public function pcRemoveAction()
     {
         $db = new Connection;
         $logs = new GeneralMethods($db);
         $id = @number_format($_GET['r'],0,"","");

         $db->beginTransaction();
         try {
             $db->existRecord("id='".$id."'", "products_category", "La entrada no existe");
             $s = "SELECT * FROM products_category WHERE id='".$id."'";
             $arr_sql = $db->fetchSQL($s);

             $db->deleteAction("products_category", "id='".$id."'");
             $logs->addLogs(sprintf("Eliminando categoría de producto ID: %d - %s", $id, $arr_sql[0]['name']));
             $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
             $db->commit();
         } catch (\Exception $e) {
             $db->rollBack();
             $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
         }

         header('Content-Type: application/json');
         $db = null;
         echo json_encode($arr_response);
     }

    /**
     * Recomendaciones semanales
     * @return void
     */
    public function weeklyAction()
    {
        $db = new Connection;

        $s = "SELECT * FROM products WHERE NOT id IN (SELECT product_id FROM weekly_recommendation)";
        $arr_products = $db->fetchSQL($s);

        $s = "SELECT * FROM view_weekly";
        $arr_weekly = $db->fetchSQL($s);

        foreach ($arr_weekly as $key => $val) {
            $arr_weekly[$key]['range'] = @to_date($val['date_init'])." al ".@to_date($val['date_end']);
            $arr_weekly[$key]['icon'] = (check_in_range($val['date_init'], $val['date_end'], date('Y-m-d'))) ? 'Activo <i class="fas fa-check-circle text-success"></i>' : 'Vencido <i class="fas fa-times-circle text-danger"></i>';
        }

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/products/weekly-recommendation.php');
    }

    /**
     * Elimina la recoemndación semanal
     * @return object
     */
    public function removeWeeklyRecommendationAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "weekly_recommendation", "La entrada no existe");
            $s = "SELECT * FROM weekly_recommendation WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);
            $product_name = $db->getValue("name", "products", "id='".$arr_sql[0]['product_id']."'");

            $db->deleteAction("weekly_recommendation", "id='".$id."'");
            $logs->addLogs(sprintf("Eliminando recomendación semanal ID: %d - %s", $id, $product_name));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }

    /**
     * pantalla de las novedades
     * @return void
     */
    public function noveltyAction()
    {
        $db = new Connection;

        $s = "SELECT * FROM products WHERE NOT id IN (SELECT product_id FROM novelty)";
        $arr_products = $db->fetchSQL($s);

        $s = "SELECT * FROM view_novelty";
        $arr_novelty = $db->fetchSQL($s);

        foreach ($arr_novelty as $key => $val) {
            $arr_novelty[$key]['range'] = @to_date($val['date_init'])." al ".@to_date($val['date_end']);
            $arr_novelty[$key]['icon'] = (check_in_range($val['date_init'], $val['date_end'], date('Y-m-d'))) ? 'Activo <i class="fas fa-check-circle text-success"></i>' : 'Vencido <i class="fas fa-times-circle text-danger"></i>';
        }

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/products/novelty.php');
    }

    /**
     * Elimina una novedad
     * @return void
     */
    public function removeNoveltyAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "novelty", "La entrada no existe");
            $s = "SELECT * FROM novelty WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);
            $product_name = $db->getValue("name", "products", "id='".$arr_sql[0]['product_id']."'");

            $db->deleteAction("novelty", "id='".$id."'");
            $logs->addLogs(sprintf("Eliminando novedad ID: %d - %s", $id, $product_name));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }

    /**
     * Pantalla de descuentos
     * @return void
     */
    public function discountAction()
    {
        $db = new Connection;

        $s = "SELECT * FROM products WHERE NOT id IN (SELECT product_id FROM discount)";
        $arr_products = $db->fetchSQL($s);

        $s = "SELECT * FROM view_discount";
        $arr_discount = $db->fetchSQL($s);

        foreach ($arr_discount as $key => $val) {
            $arr_discount[$key]['range'] = @to_date($val['date_init'])." al ".@to_date($val['date_end']);
            $arr_discount[$key]['icon'] = (check_in_range($val['date_init'], $val['date_end'], date('Y-m-d'))) ? 'Activo <i class="fas fa-check-circle text-success"></i>' : 'Vencido <i class="fas fa-times-circle text-danger"></i>';
        }

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/products/discount.php');
    }

    /**
     * Elimina un descuento
     * @return void
     */
    public function removeDiscountAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "discount", "La entrada no existe");
            $s = "SELECT * FROM discount WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);
            $product_name = $db->getValue("name", "products", "id='".$arr_sql[0]['product_id']."'");

            $db->deleteAction("discount", "id='".$id."'");
            $logs->addLogs(sprintf("Eliminando novedad ID: %d - %s", $id, $product_name));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }

    /**
     * Listad de colores
     * @return void
     */
    public function colorsAction()
    {
        $db = new Connection;

        $s = "SELECT * FROM colors";
        $arr_colors = $db->fetchSQL($s);

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/products/colors-list.php');
    }

    /**
     * Agrega un color
     * @return void
     */
    public function colorsAddAction()
    {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $act = 1; $name = ""; $color = "";

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/products/colors-form.php');
    }

    /**
     * Edita un color
     * @return void
     */
    public function colorsEditAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."'", "colors", BASE_URL."404");
        $frm = "frm-edit"; $act = 2;

        $s = "SELECT * FROM colors WHERE id='".$id."'";
        $arr_color = $db->fetchSQL($s);

        $name = str_replace("\\", "", $arr_color[0]['name']);
        $color = "#".$arr_color[0]['color'];

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/products/colors-form.php');
    }

    /**
     * Eimina un color
     * @return void
     */
    public function removeColorAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "colors", "La entrada no existe");
            $s = "SELECT * FROM colors WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);
            $color_name = $db->getValue("name", "colors", "id='".$arr_sql[0]['id']."'");

            $db->deleteAction("colors", "id='".$id."'");
            $logs->addLogs(sprintf("Eliminando color ID: %d - %s", $id, $color_name));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }

    /**
     * Listad de tallas
     * @return void
     */
    public function sizeAction()
    {
        $db = new Connection;

        $s = "SELECT * FROM size";
        $arr_size = $db->fetchSQL($s);

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/products/size-list.php');
    }

    /**
     * Agrega una talla
     * @return void
     */
    public function sizeAddAction()
    {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $act = 1; $name = "";

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/products/size-form.php');
    }

    /**
     * Edita una talla
     * @return void
     */
    public function sizeEditAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."'", "size", BASE_URL."404");
        $frm = "frm-edit"; $act = 2;

        $s = "SELECT * FROM size WHERE id='".$id."'";
        $arr_size = $db->fetchSQL($s);

        $name = $arr_size[0]['name'];

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/products/size-form.php');
    }

    /**
     * Eimina una talla
     * @return void
     */
    public function removeSizeAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "size", "La entrada no existe");
            $s = "SELECT * FROM size WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);
            $name = $db->getValue("name", "size", "id='".$arr_sql[0]['id']."'");

            $db->deleteAction("size", "id='".$id."'");
            $logs->addLogs(sprintf("Eliminando talla ID: %d - %s", $id, $name));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }
}
?>
