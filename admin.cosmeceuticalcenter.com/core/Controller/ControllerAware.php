<?php

/**
 * Clase contenedora de metodos para los controladores
 */
class ControllerAware
{
    function __construct()
    {
        $this->db = new Connection;
        $this->user_id = @$_SESSION['ADMIN_SESSION_COSME']['id'];
        $this->user_code = @$_SESSION['ADMIN_SESSION_COSME']['code'];
        $this->user_username = @$_SESSION['ADMIN_SESSION_COSME']['username'];
        $this->user_name = @$_SESSION['ADMIN_SESSION_COSME']['name'];
        $this->user_last_name = @$_SESSION['ADMIN_SESSION_COSME']['last_name'];
        $this->user_role = @$_SESSION['ADMIN_SESSION_COSME']['role'];
        $this->user_role_name = @$_SESSION['ADMIN_SESSION_COSME']['role_name'];
        $this->user_view = (isset($_GET['view']) && (!empty($_GET['view']))) ? $_GET['view'] : "index";
        $this->user_method = (isset($_GET['meth']) && (!empty($_GET['meth']))) ? $_GET['meth'] : "index";
        $this->inventory_alert = $this->inventoryAlert();
    }

    /**
     * Retorna un mensaje flash
     * @param  string $class   clase para el alert
     * @param  string $message Mensjae a mostrar
     * @return string
     */
    public function flashMessage($class, $message, $path = "")
    {
        $flash_message = '<div class="alert '.$class.' alert-dismissible" role="alert">
        <button type="button" class="close" onclick="location.href=\''.$path.'\'"><span aria-hidden="true">&times;</span></button>
        '.$message.'
        </div>';

        return $flash_message;
    }

    /**
     * Muestra una forma global de los mensajes flash
     * @param  array $get Variable $_GET
     * @return string
     */
    public function flashMessageGlobal($get)
    {
        $flash_message = "";

        if (isset($get['m'])) {
            if ($get['m'] != "") {
                $url = @explode(BASE_PATH, $_SERVER["REQUEST_URI"]);
                $path = BASE_URL.str_replace('/'.$get['m'], "", $url[1]);

                if ($get['m'] == 'OK1') {
                    $flash_message = $this->flashMessage('alert-success', 'La entrada ha sido <b>creada</b> correctamente.', $path);
                } elseif ($get['m'] == 'OK2') {
                    $flash_message = $this->flashMessage('alert-info', 'La entrada ha sido <b>editada</b> correctamente.', $path);
                } elseif ($get['m'] == 'OK3') {
                    $flash_message = $this->flashMessage('alert-warning', 'La entrada ha sido <b>eliminada</b> correctamente.', $path);
                }
            }
        }

        return $flash_message;
    }

    /**
     * Verifica si existe un registro en la base de datos
     * @param  array  $param Parametros de busqueda [columna - valor]
     * @param  string $table Tabla donde se va a buscar
     * @param  string $url   URL destino en caso de que no exista
     * @return void
     */
    public function validRecord(array $param, $table, $url)
    {
        $db = $this->db;

        foreach ($param as $key => $val) {
            $cnt_val = $db->getCount($table, $key."='".$val."'");

            if ($cnt_val == 0) {
                header("location: ".$url); exit;
            }
        }
    }

    /**
     * Verifica si la entrada existe en una tabla
     * @param  string $where Condición
     * @param  string $table Tabla
     * @param  string $url   URL destino en caso de que no exista
     * @return void
     */
    public function validRecordCustom($where, $table, $url)
    {
        $db = $this->db;

        $cnt_val = $db->getCount($table, $where);

        if ($cnt_val == 0) {
            header("location: ".$url); exit;
        }
    }

    /**
     * Retorna un icono de alerta en el inventario
     * @return string
     */
    public function inventoryAlert()
    {
        $db = $this->db;

        $cnt_inventory = $db->getCount("inventory", "quantity < 10");
        if ($cnt_inventory > 0) {
            $alert = '<i class="fas fa-exclamation-triangle pull-right text-warning css-marginR20"></i>';
        } else {
            $alert = "";
        }

        return $alert;
    }
}


?>
