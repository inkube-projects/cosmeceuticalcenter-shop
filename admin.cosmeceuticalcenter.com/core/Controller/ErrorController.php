<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');

/**
 * Controlador para los errores
 */
class ErrorController extends ControllerAware
{
    function __construct()
    {
        parent::__construct();
    }

   public function notFoundAction()
   {
      require_once('html/error/404.php');
   }

   public function serverErrorAction()
   {
      require_once('html/error/500.php');
   }
}
?>
