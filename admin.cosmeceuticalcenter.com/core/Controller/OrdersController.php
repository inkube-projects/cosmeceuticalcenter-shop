<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');

/**
* Controlador OrdersController
*/
class OrdersController extends ControllerAware
{
   /**
    * Constructor
    */
    public function __construct()
    {
        parent::__construct();
    }

   /**
     * Listados de orders
     *
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;

        $src_status = (isset($_POST['src_status']) && (!empty($_POST['src_status']))) ? @number_format($_POST['src_status'],0,'','') : "" ;
        $src_start_date = (isset($_POST['src_start_date']) && (!empty($_POST['src_start_date']))) ? secure_mysql($_POST['src_start_date']) : "" ;
        $src_end_date = (isset($_POST['src_end_date']) && (!empty($_POST['src_end_date']))) ? secure_mysql($_POST['src_end_date']) : "" ;
        $src_start_value = (isset($_POST['src_start_value']) && (!empty($_POST['src_start_value']))) ? @number_format($_POST['src_start_value'],2,'.','') : "" ;
        $src_end_value = (isset($_POST['src_end_value']) && (!empty($_POST['src_end_value']))) ? @number_format($_POST['src_end_value'],2,'.','') : "" ;
        $src_method = (isset($_POST['src_method']) && (!empty($_POST['src_method']))) ? @number_format($_POST['src_method'],0,'','') : "" ;
        $src_province = (isset($_POST['src_province']) && (!empty($_POST['src_province']))) ? @number_format($_POST['src_province'],0,'','') : "" ;

        $s = "SELECT * FROM view_orders WHERE NOT id=''";

        if (!empty($src_status)) { // Por estado
            $s .= " AND status_id='".$src_status."'";
        }
        if ($src_start_date != "") { // Por fecha
            $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') >= '".to_date($src_start_date)."'";
        }
        if ($src_end_date != "") {
            $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') <= '".to_date($src_end_date)."'";
        }
        if ($src_start_value) { // Por valor
            $s .= " AND total >= '".$src_start_value."'";
        }
        if ($src_end_value) {
            $s .= " AND total <= '".$src_end_value."'";
        }
        if (!empty($src_method)) { // Por método de pago
            $s .= " AND method_payment_id = '".$src_method."'";
        }
        if (!empty($src_province)) { // Por provincia
            $s .= " AND province_id = '".$src_province."'";
        }

        $arr_orders = $db->fetchSQL($s);

        foreach ($arr_orders as $key => $val) {
            $create = datetime_format($val['create_at']);
            $q = "SELECT * FROM view_user WHERE id='".$val['user_id']."'";
            $arr_user = $db->fetchSQL($q);

            $arr_orders[$key]['user_names'] = $arr_user[0]['name']." ".$arr_user[0]['last_name'];
            $arr_orders[$key]['formed_date'] = $create['date'];
            $arr_orders[$key]['formed_province'] = $db->getValue('name', 'province', "id='".$val['province_id']."'");

            if ($val['status_id'] == 1) {
                $arr_orders[$key]['formed_status'] = '<span class="text-danger"><i class="fa fa-hourglass-start css-fonsize16"></i> '.$val['status_name'].'</span>';
            } elseif ($val['status_id'] == 2) {
                $arr_orders[$key]['formed_status'] = '<span class="text-info"><i class="fa fas fa-shipping-fast css-fonsize16"></i> '.$val['status_name'].'</span>';
            } elseif ($val['status_id'] == 3) {
                $arr_orders[$key]['formed_status'] = '<span class="text-success"><i class="fas fa-check-circle css-fonsize16"></i> '.$val['status_name'].'</span>';
            } elseif ($val['status_id'] == 4) {
                $arr_orders[$key]['formed_status'] = '<span class="text-warning"><i class="fas fa-lock css-fonsize16"></i> '.$val['status_name'].'</span>';
            }

            $label = "";
            $cnt_orders = $db->getCount("orders", "user_id='".$val['user_id']."'");
            $cnt_newsletter = $db->getCount("newsletter", "email='".$arr_user[0]['email']."'");

            if ($cnt_orders == 1 && $cnt_newsletter == 0) {
                $label = '<span class="label label-success">Nuevo</span>';
            }
            $arr_orders[$key]['label'] = $label;
            $arr_orders[$key]['invoice_id'] = "CU".str_pad($val['id'], 6, '0', STR_PAD_LEFT);
            $arr_orders[$key]['formed_present'] = ($val['present'] == 1) ? "Si" : "No";
            $arr_orders[$key]['user_email'] = $arr_user[0]['email'];
            $arr_orders[$key]['user_phone'] = $arr_user[0]['phone_1'];
        }

        // Estados
        $s = "SELECT * FROM order_status";
        $arr_status = $db->fetchSQL($s);

        // Métodos de pago
        $s = "SELECT * FROM method_payment";
        $arr_methods = $db->fetchSQL($s);

        // Provincias
        $s = "SELECT * FROM province";
        $arr_province = $db->fetchSQL($s);

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once("html/orders/orders-list.php");
    }

   /**
     * Formulario para agregar datos en orders
     *
     * @return void
     */
    public function addAction()
    {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $act = 1;
        $code = "";
        $sub_total = "";
        $iva = "";
        $iva_percentage = "";
        $total = "";
        $status_id = "";
        $user_id = "";
        $create_at = "";
        $update_at = "";
        $p_disabled = "disabled";

        $s = "SELECT * FROM view_user WHERE role_id='3'";
        $arr_user = $db->fetchSQL($s);

        $s = "SELECT * FROM view_products";
        $arr_products = $db->fetchSQL($s);

        foreach ($arr_products as $key => $val) {
            $create = datetime_format($val['product_create_at']);
            $arr_products[$key]['formed_date'] = $create['date'];
        }

        // país
        $s = "SELECT * FROM country";
        $arr_country = $db->fetchSQL($s);

        // provincia
        $s = "SELECT * FROM province";
        $arr_province = $db->fetchSQL($s);

        if (isset($_SESSION['ORDER_PRODUCT'])) { unset($_SESSION['ORDER_PRODUCT']); }
        $flash_message = "";
        require_once('html/orders/orders-form.php');
    }

   /**
     * Formulario para editar datos en orders
     *
     * @return void
     */
    public function editAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."'", "orders", BASE_URL."404");
        $frm = "frm-edit"; $act = 2;

        $s = "SELECT * FROM orders WHERE id='".$id."'";
        $arr_orders = $db->fetchSQL($s);

        $invoice_id = "CU".str_pad($id, 6, '0', STR_PAD_LEFT);
        $code = $arr_orders[0]['code'];
        $sub_total = $arr_orders[0]['sub_total'];
        $iva = $arr_orders[0]['iva'];
        $iva_percentage = $arr_orders[0]['iva_percentage'];
        $total = $arr_orders[0]['total'];
        $status_id = $arr_orders[0]['status_id'];
        $user_id = $arr_orders[0]['user_id'];
        $shipping = $arr_orders[0]['shipping'];
        $order_note = $arr_orders[0]['note'];
        $present = $arr_orders[0]['present'];
        $present_note = $arr_orders[0]['present_note'];
        $coupon_id = $arr_orders[0]['coupon_id'];
        $coupon_code = $arr_orders[0]['coupon_code'];
        $discount_percent = $arr_orders[0]['discount_percent'];
        $discount = $arr_orders[0]['discount'];
        $create_at = $arr_orders[0]['create_at'];
        $update_at = $arr_orders[0]['update_at'];
        $method = $db->getValue("method", "method_payment", "id='".$arr_orders[0]['method_payment_id']."'");

        $charge = 0.00;
        if ($arr_orders[0]['method_payment_id'] == 2) {
            //$charge = (2 * $total) / 100;
            $charge = $total - ($sub_total + $shipping);
            $charge = number_format($charge,2,".","");
        }

        $s = "SELECT * FROM order_status";
        $arr_status = $db->fetchSQL($s);

        $s = "SELECT * FROM view_user WHERE id='".$user_id."'";
        $arr_user = $db->fetchSQL($s);
        $names = $arr_user[0]['name']." ".$arr_user[0]['last_name'];
        $email = $arr_user[0]['email'];
        $phone = $arr_user[0]['phone_1'];

        $s = "SELECT * FROM view_orders_detail WHERE order_id='".$id."'";
        $arr_detail = $db->fetchSQL($s);

        foreach ($arr_detail as $key => $val) {
            $arr_detail[$key]['size'] = ($val['size_id']) ? $db->getValue("name", "size", "id='".$val['size_id']."'") : "No Aplica";
            $arr_detail[$key]['color'] = ($val['color_id']) ? $db->getValue("name", "colors", "id='".$val['color_id']."'") : "No Aplica";

            if ($val['order_new_price']) {
                if ($val['order_price'] != $val['order_new_price']) {
                    $arr_detail[$key]['price'] = $val['order_price']."<br>".$val['order_new_price'];
                } else {
                    $arr_detail[$key]['price'] = $val['order_price'];
                }

                $price_without_iva = (100 * $val['order_new_price']) / (100 + $val['product_iva']);
                $iva = $val['order_new_price'] - $price_without_iva;
                $arr_detail[$key]['unity_price'] = $price_without_iva;
                $arr_detail[$key]['formed_iva'] = $iva;
            } else {
                $arr_detail[$key]['price'] = $val['order_price'];
                $arr_detail[$key]['unity_price'] = $val['order_price'] - $val['iva'];
                $arr_detail[$key]['formed_iva'] = $val['iva'] * $val['quantity'];
            }

            $arr_detail[$key]['formed_categories'] = "";
            $s = "SELECT
            	pcs.id,
            	pcs.product_id,
            	pcs.category_id,
            	pc.`name` AS category_name,
            	pc.description AS category_description
            FROM
            	products_categories pcs
            LEFT JOIN products_category pc ON pcs.category_id = pc.id WHERE pcs.product_id='".$val['product_id']."'";
            $arr_p_categories = $db->fetchSQL($s);
            foreach ($arr_p_categories as $i => $cat) {
                $arr_detail[$key]['formed_categories'] .= ($i == 0) ? $cat['category_name'] : ", ".$cat['category_name'];
            }
        }

        // Dirección de envío
        $s = "SELECT * FROM shipping_address WHERE user_id='".$arr_orders[0]['user_id']."'";
        $arr_shipping_address = $db->fetchSQL($s);

        $country_name = $db->getValue('country', 'country', "id='".@$arr_shipping_address[0]['country_id']."'");
        $province_name = $db->getValue('name', 'province', "id='".@$arr_shipping_address[0]['province_id']."'");
        $address_1 = @$arr_shipping_address[0]['address_1'];
        $address_2 = @$arr_shipping_address[0]['address_2'];
        $city = @$arr_shipping_address[0]['city'];
        $postal_code = @$arr_shipping_address[0]['postal_code'];
        $note = @$arr_shipping_address[0]['note'];

        // Dirección de facturación
        $s = "SELECT * FROM bill_information WHERE user_id='".$arr_orders[0]['user_id']."'";
        $arr_b_information = $db->fetchSQL($s);

        if (count($arr_b_information) > 0) {
            $b_identification = @$arr_b_information[0]['identification'];
            $b_name = @$arr_b_information[0]['name'];
            $b_last_name = @$arr_b_information[0]['last_name'];
            $b_company = @$arr_b_information[0]['company'];
            $b_country = $db->getValue('country', 'country', "id='".@$arr_b_information[0]['country_id']."'");
            $b_province = $db->getValue('country', 'country', "id='".@$arr_b_information[0]['province_id']."'");
            $b_address_1 = @$arr_b_information[0]['address_1'];
            $b_address_2 = @$arr_b_information[0]['address_2'];
            $b_city = @$arr_b_information[0]['city'];
            $b_postal_code = @$arr_b_information[0]['postal_code'];
        } else {
            $b_identification = @$arr_user[0]['dni'];
            $b_name = @$arr_user[0]['name'];
            $b_last_name = @$arr_user[0]['last_name'];
            $b_company = @$arr_user[0]['company'];
            $b_country = $country_name;
            $b_province = $province_name;
            $b_address_1 = $address_1;
            $b_address_2 = $address_2;
            $b_city = $city;
            $b_postal_code = $postal_code;
        }


        // Información de seguimiento
        $s = "SELECT * FROM tracking_information WHERE order_id='".$id."'";
        $arr_tracking = $db->fetchSQL($s);

        $t_company = @$arr_tracking[0]['company'];
        $t_tracking_number = @$arr_tracking[0]['tracking_number'];

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/orders/orders-form.php');
    }

    /**
     * Elimina una entrada de orders
     *
     * @return object
     */
    public function removeAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "orders", "La entrada no existe");
            $s = "SELECT * FROM orders WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);

            $db->deleteAction("orders", "id='".$id."'");
            $logs->addLogs(sprintf("Eliminando entrada de orders ID: %d", $id));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }

    /**
     * Get invoice
     * @return void
     */
    public function invoiceAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."'", "orders", BASE_URL."404");

        // Orden
        $s = "SELECT * FROM view_orders WHERE id='".$id."'";
        $arr_order = $db->fetchSQL($s);

        $order_code = $arr_order[0]['code'];
        $user_id = $arr_order[0]['user_id'];
        $invoice_id = "CU".str_pad($id, 6, '0', STR_PAD_LEFT);
        $create = datetime_format($arr_order[0]['create_at']);
        $sub_total = $arr_order[0]['sub_total'];
        $iva = $arr_order[0]['iva'];
        $shipping = $arr_order[0]['shipping'];
        $total = $arr_order[0]['total'];

        // Detalle de la orden
        $s = "SELECT * FROM view_orders_detail WHERE order_id='".$id."'";
        $arr_detail = $db->fetchSQL($s);

        $total_products = count($arr_detail);

        foreach ($arr_detail as $key => $val) {
            $arr_detail[$key]['formed_price'] = ($val['order_new_price']) ? $val['order_new_price'] : $val['order_price'];
        }

        // Usuario
        $s = "SELECT * FROM view_user WHERE id='".$user_id."'";
        $arr_user = $db->fetchSQL($s);

        $name = $arr_user[0]['name'];
        $last_name = $arr_user[0]['last_name'];

        // Datos de facturación
        $s = "SELECT * FROM bill_information WHERE user_id='".$user_id."'";
        $arr_bill = $db->fetchSQL($s);

        if (count($arr_bill) > 0) {
            $cnf = @$arr_bill[0]['identification'];
            $address = @$arr_bill[0]['address_1']." ".@$arr_bill[0]['address_2'];
        } else {
            $s = "SELECT * FROM shipping_address WHERE user_id='".$user_id."'";
            $arr_shipping_address = $db->fetchSQL($s);

            $cnf = $arr_user[0]['dni'];
            $address = @$arr_shipping_address[0]['address_1']." ".@$arr_shipping_address[0]['address_2'];
        }

        $total_products = $db->getSum("quantity", "order_detail", "order_id='".$id."'");

        require_once('html/orders/invoice.php');
    }
}
?>
