<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');

/**
 * Controlador de los usuarios
 */
class AdministratorController extends ControllerAware
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Lista los usuarios que no tengan rol administrador
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;

        $src_status = (isset($_POST['src_status']) && (!empty($_POST['src_status']))) ? number_format($_POST['src_status'],0,'','') : "" ;
        $src_account = (isset($_POST['src_account']) && (!empty($_POST['src_account']))) ? number_format($_POST['src_account'],0,'','') : "" ;
        $src_start_date = (isset($_POST['src_start_date']) && (!empty($_POST['src_start_date']))) ? sanitize($_POST['src_start_date']) : "" ;
        $src_end_date = (isset($_POST['src_end_date']) && (!empty($_POST['src_end_date']))) ? sanitize($_POST['src_end_date']) : "" ;
        
        $s = "SELECT * FROM view_user WHERE role_id IN (1,2)";
        
        if (($src_status != "") && ($src_status != 0)) {
            $s .= " AND status_id = '".$src_status."'";
        }

        if ($src_start_date != "") {
            $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') >= '".to_date($src_start_date)."'";
        }

        if ($src_end_date != "") {
            $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') <= '".to_date($src_end_date)."'";
        }

        if (($src_account != "") && ($src_account != 0)) {
            $s .= " AND account_type_id = '".$src_account."'";
        }

        $arr_sql = $db->fetchSQL($s);
        $arr_users = array();

        foreach ($arr_sql as $val) {
            $create = datetime_format($val['create_at']);
            $image = APP_NO_IMAGES."profile.jpg";
            if ($val['profile_image']) {
                $image = APP_IMG_USERS."user_".$val['code']."/profile/min_".$val['profile_image'];
            }

            $arr_users[] = array(
                'id' => $val['id'],
                'username' => $val['username'],
                'email' => $val['email'],
                'profile_image' => $image,
                'name' => $val['name'],
                'last_name' => $val['last_name'],
                'role' => $val['role_name'],
                'status' => $db->getValue("description", "user_status", "id='".$val['status_id']."'"),
                'code' => $val['code'],
                'create' => $create['date']
            );
        }

        // Estados
        $s = "SELECT * FROM user_status";
        $arr_status = $db->fetchSQL($s);

        // Se carga el mensaje flash
        $flash_message = $this->flashMessageGlobal($_GET);

        require_once('html/users/admin-list.php');
    }

    /**
     * Acción para agregar un usuario
     * @return void
     */
    public function addAction()
    {
        $db = new Connection;

        // Estados
        $s = "SELECT * FROM user_status";
        $arr_status = $db->fetchSQL($s);

        // Roles
        $s = "SELECT * FROM role WHERE id IN (1,2)";
        $arr_role = $db->fetchSQL($s);

        // Paises
        $s = "SELECT * FROM country";
        $arr_country = $db->fetchSQL($s);

        // Provincias
        $s = "SELECT * FROM province";
        $arr_province = $db->fetchSQL($s);

        // Variables base
        $frm = "frm-admin"; $act = 1; $flash_message = ""; $username = ""; $email = ""; $name = ""; $last_name = ""; $location = ""; $role = ""; $status = ""; $country = "";
        $phone_1 = ""; $phone_2 = ""; $postal_code = ""; $id = ""; $account = ""; $arr_company = array(); $note = ""; $birthdate = ""; $country = ""; $province = ""; $city = "";
        $company = ""; $dni = ""; $location_1 = ""; $location_2 = ""; $phone_1 = ""; $phone_2 = "";
        $profile_image = APP_NO_IMAGES."profile.jpg";

        // Limpiar el directorio temporal
        clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/users/profile/");

        require_once('html/users/admin-form.php');
    }

    /**
     * Acción para editar usuario
     * @return void
     */
    public function editAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $frm = "frm-admin"; $act = 2;
        $this->validRecordCustom("id='".$id."' AND role IN (1,2)", "user", BASE_URL."admin/administrator/list");

        // Estados
        $s = "SELECT * FROM user_status";
        $arr_status = $db->fetchSQL($s);

        // Roles
        $s = "SELECT * FROM role WHERE id IN (1,2)";
        $arr_role = $db->fetchSQL($s);

        // Paises
        $s = "SELECT * FROM country";
        $arr_country = $db->fetchSQL($s);

        $s = "SELECT * FROM view_user WHERE id='".$id."' AND role_id IN (1,2)";
        $arr_user = $db->fetchSQL($s);

        $create = datetime_format($arr_user[0]['create_at']);

        $user_code = $arr_user[0]['code'];
        $username = $arr_user[0]['username'];
        $email = $arr_user[0]['email'];
        $name = $arr_user[0]['name'];
        $last_name = $arr_user[0]['last_name'];
        $role = $arr_user[0]['role_id'];
        $status = $arr_user[0]['status_id'];
        $country = $arr_user[0]['country_id'];
        $phone_1 = $arr_user[0]['phone_1'];
        $phone_2 = $arr_user[0]['phone_2'];
        $create_at = $create['date'];
        $profile_image = APP_NO_IMAGES."profile.jpg";
        
        if ($arr_user[0]['profile_image']) {
            $profile_image = APP_IMG_USERS."user_".$arr_user[0]['code']."/profile/".$arr_user[0]['profile_image'];
        }

        // Se carga el mensaje flash
        $flash_message = $this->flashMessageGlobal($_GET);

        // Limpiar el directorio temporal
        clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/users/profile/");
        require_once('html/users/admin-form.php');
    }

    /**
     * Perfil de usuario
     * @return void
     */
    public function profileAction()
    {
        $db = new Connection;
        $frm = "frm-profile";

        $s = "SELECT * FROM view_user WHERE id='".$this->user_id."'";
        $arr_user = $db->fetchSQL($s);

        $username = $arr_user[0]['username'];
        $email = $arr_user[0]['email'];
        $status = $db->getValue("description", "user_status", "id='".$arr_user[0]['status_id']."'");
        $code = $arr_user[0]['code'];
        $create = datetime_format($arr_user[0]['create_at']);
        $name = $arr_user[0]['name'];
        $last_name = $arr_user[0]['last_name'];
        $country = $arr_user[0]['country_id'];
        $location = $arr_user[0]['direction'];
        $phone_1 = $arr_user[0]['phone_1'];
        $phone_2 = $arr_user[0]['phone_2'];
        $postal_code = $arr_user[0]['postal_code'];
        $role_name = $arr_user[0]['role_name'];

        $profile_image = APP_NO_IMAGES."profile.jpg";

        if ($arr_user[0]['profile_image']) {
            $profile_image = APP_IMG_USERS."user_".$arr_user[0]['code']."/profile/".$arr_user[0]['profile_image'];
        }

        // Paises
        $s = "SELECT * FROM country";
        $arr_country = $db->fetchSQL($s);

        $flash_message = "";
        if (isset($_GET['m'])) {
            if ($_GET['m'] == "OK") {
                $flash_message = $this->flashMessage("alert-success", "El perfil se ha <b>editado</b> correctamente");
            }
        }

        require_once('html/users/profile-form.php');
    }
}


?>
