<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');

/**
* Controlador NewsletterController
*/
class NewsletterController extends ControllerAware
{
   /**
    * Constructor
    */
    public function __construct()
    {
        parent::__construct();
    }

   /**
     * Listados de newsletter
     *
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;
        $q = "SELECT * FROM newsletter";
        $arr_newsletter = $db->fetchSQL($q);

        foreach ($arr_newsletter as $key => $val) {
            $create = datetime_format($val['create_at']);
            $arr_newsletter[$key]['formed_date'] = $create['date'];
        }

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once("html/newsletter/newsletter-list.php");
    }

   /**
     * Formulario para agregar datos en newsletter
     *
     * @return void
     */
    public function addAction()
    {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $act = 1;
        $email = "";
        $create_at = "";
        $update_at = "";
        $flash_message = "";

        require_once('html/newsletter/newsletter-form.php');
    }

   /**
     * Formulario para editar datos en newsletter
     *
     * @return void
     */
    public function editAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."'", "newsletter", BASE_URL."404");
        $frm = "frm-edit"; $act = 2;
        $s = "SELECT * FROM newsletter WHERE id='".$id."'";
        $arr_newsletter = $db->fetchSQL($s);

        $email = $arr_newsletter[0]['email'];
        $create_at = $arr_newsletter[0]['create_at'];
        $update_at = $arr_newsletter[0]['update_at'];
        $flash_message = @$this->flashMessageGlobal($_GET);

        require_once('html/newsletter/newsletter-form.php');
    }

    /**
     * Elimina una entrada de newsletter
     *
     * @return object
     */
    public function removeAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "newsletter", "La entrada no existe");
            $s = "SELECT * FROM newsletter WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);

            $db->deleteAction("newsletter", "id='".$id."'");
            $logs->addLogs(sprintf("Eliminando entrada de newsletter ID: %d", $id));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }
}
?>
