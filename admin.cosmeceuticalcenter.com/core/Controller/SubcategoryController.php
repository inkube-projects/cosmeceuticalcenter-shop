<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');

/**
* Controlador SubcategoryController
*/
class SubcategoryController extends ControllerAware
{
   /**
    * Constructor
    */
    public function __construct()
    {
        parent::__construct();
    }

   /**
     * Listados de products_sub_category
     *
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;
        $q = "SELECT * FROM products_sub_category";
        $arr_products_sub_category = $db->fetchSQL($q);

        foreach ($arr_products_sub_category as $key => $val) {
            $arr_products_sub_category[$key]['category_name'] = $db->getValue("name", "products_category", "id='".$val['category_id']."'");
        }

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once("html/products-sub-category/products-sub-category-list.php");
    }

   /**
     * Formulario para agregar datos en products_sub_category
     *
     * @return void
     */
    public function addAction()
    {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $act = 1;
        $name = "";
        $category = "";

        $s = "SELECT * FROM products_category";
        $arr_categories = $db->fetchSQL($s);

        $flash_message = "";
        require_once('html/products-sub-category/products-sub-category-form.php');
    }

   /**
     * Formulario para editar datos en products_sub_category
     *
     * @return void
     */
    public function editAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."'", "products_sub_category", BASE_URL."404");
        $frm = "frm-edit"; $act = 2;
        $s = "SELECT * FROM products_sub_category WHERE id='".$id."'";
        $arr_products_sub_category = $db->fetchSQL($s);

        $name = $arr_products_sub_category[0]['name'];
        $category = $arr_products_sub_category[0]['category_id'];

        $s = "SELECT * FROM products_category";
        $arr_categories = $db->fetchSQL($s);

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/products-sub-category/products-sub-category-form.php');
    }

    /**
     * Elimina una entrada de products_sub_category
     *
     * @return object
     */
    public function removeAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "products_sub_category", "La entrada no existe");
            $s = "SELECT * FROM products_sub_category WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);

            $db->deleteAction("products_sub_category", "id='".$id."'");
            $logs->addLogs(sprintf("Eliminando entrada de products_sub_category ID: %d", $id));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }
}
?>
