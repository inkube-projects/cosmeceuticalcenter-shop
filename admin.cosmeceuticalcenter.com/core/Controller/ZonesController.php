<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');

/**
* Controlador ZonesController
*/
class ZonesController extends ControllerAware
{
   /**
    * Constructor
    */
    public function __construct()
    {
        parent::__construct();
    }

   /**
     * Listados de zones
     *
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;
        $q = "SELECT * FROM zones";
        $arr_zones = $db->fetchSQL($q);

        foreach ($arr_zones as $key => $zone) {
            $create = datetime_format($zone['create_at']);
            $arr_zones[$key]['formed_date'] = $create['date'];
        }

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once("html/zones/zones-list.php");
    }

   /**
     * Formulario para agregar datos en zones
     *
     * @return void
     */
    public function addAction()
    {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $act = 1;
        $name = "";
        $value = "";
        $p_disabled = "disabled";
        $create_at = "";
        $update_at = "";
        $arr_zones_province = array();
        $arr_s_country = array();

        $s = "SELECT * FROM province";
        $arr_province = $db->fetchSQL($s);

        $s = "SELECT * FROM country";
        $arr_country = $db->fetchSQL($s);

        $flash_message = "";
        require_once('html/zones/zones-form.php');
    }

   /**
     * Formulario para editar datos en zones
     *
     * @return void
     */
    public function editAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."'", "zones", BASE_URL."404");
        $frm = "frm-edit"; $act = 2;

        $s = "SELECT * FROM zones WHERE id='".$id."'";
        $arr_zones = $db->fetchSQL($s);

        $name = $arr_zones[0]['name'];
        $value = $arr_zones[0]['value'];
        $create_at = $arr_zones[0]['create_at'];
        $update_at = $arr_zones[0]['update_at'];

        // Provincias
        $s = "SELECT * FROM province";
        $arr_province = $db->fetchSQL($s);

        // Provincias seleccionadas
        $s = "SELECT * FROM zone_province WHERE zone_id='".$id."'";
        $arr_zones_province = $db->fetchSQL($s);

        // Paises
        $s = "SELECT * FROM country";
        $arr_country = $db->fetchSQL($s);

        // Paises seleccionados
        $s = "SELECT * FROM zone_country WHERE zone_id='".$id."'";
        $arr_zones_country = $db->fetchSQL($s);
        $arr_s_country = array();

        foreach ($arr_zones_country as $key => $val) {
            $arr_s_country[] = $val['country_id'];
        }

        $flash_message = @$this->flashMessageGlobal($_GET);
        require_once('html/zones/zones-form.php');
    }

    /**
     * Elimina una entrada de zones
     *
     * @return object
     */
    public function removeAction()
    {
        $db = new Connection;
        $logs = new GeneralMethods($db);
        $id = @number_format($_GET['r'],0,"","");

        $db->beginTransaction();
        try {
            $db->existRecord("id='".$id."'", "zones", "La entrada no existe");
            $s = "SELECT * FROM zones WHERE id='".$id."'";
            $arr_sql = $db->fetchSQL($s);

            $db->deleteAction("zones", "id='".$id."'");
            $logs->addLogs(sprintf("Eliminando entrada de zones ID: %d", $id));
            $arr_response = array('status' => 'OK', 'message' => 'Se ha eliminado correctamente');
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
        }

        header('Content-Type: application/json');
        $db = null;
        echo json_encode($arr_response);
    }
}
?>
