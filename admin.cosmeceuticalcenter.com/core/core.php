<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


// nucleo del sitio web
@session_start();

// define('DB_USER', 'root');
// define('DB_PASS', 'AJF_19466118');
// define('DB_DSN', 'mysql:dbname=local_cosmeceuticalcenter_db;host=localhost;charset=utf8');

// ---- Preproducción
define('DB_USER', 'shop_user');
define('DB_PASS', '5zF2rz0@');
define('DB_DSN', 'mysql:dbname=cosmeceutical_shop;host=localhost;charset=utf8');

define('APP_TITLE', 'Administrador de Contenido');
define('APP_COPY', '<strong>Copyright &copy; '.date('Y').' <a href="#">Cosmeceutical center</a>.</strong> All rights reserved.');
define('HTML_DIR', 'html/');

// Local
// define('BASE_DOMAIN', 'http://localhost/');
// define('BASE_PATH', 'cosmeceuticalcenter-shop/');

// Producción
define('BASE_DOMAIN', 'https://shop.cosmeceuticalcenter.com/');
define('BASE_PATH', '');

define('BASE_URL', BASE_DOMAIN.BASE_PATH);
define('BASE_URL_ADMIN', BASE_URL.'admin.cosmeceuticalcenter.com/');
define('BASE_URL_PUBLIC', BASE_URL.'cosmeceuticalcenter.com/');
define('APP_ASSETS', BASE_URL_ADMIN.'views/app/');
define('APP_PLUGINS', BASE_URL_ADMIN.'views/app/plugins/');
define('APP_PUBLIC_PATH_IMAGES', '../cosmeceuticalcenter.com/views/images/');


define('APP_IMG', BASE_URL_ADMIN.'views/images/');
define('APP_IMG_PUBLIC', BASE_URL_PUBLIC.'views/images/');
define('APP_NO_IMAGES', BASE_URL_PUBLIC.'views/images/no_images/');

// Usuarios - backend
define('APP_IMG_ADMIN', 'views/images/users/');
define('APP_IMG_ADMIN_USER', BASE_URL_ADMIN.'views/images/users/');

// Usuarios
define('APP_SYSTEM_USERS', APP_PUBLIC_PATH_IMAGES.'users/');
define('APP_IMG_USERS', APP_IMG_PUBLIC.'users/');

// Productos
define('APP_SYSTEM_PRODUCTS', APP_PUBLIC_PATH_IMAGES.'products/');
define('APP_IMG_PRODUCTS', APP_IMG_PUBLIC.'products/');

// Imagenes del carrusel
define('APP_SYSTEM_CAROUSEL', APP_PUBLIC_PATH_IMAGES.'slider/');
define('APP_IMG_CAROUSEL', APP_IMG_PUBLIC.'slider/');

// Imagenes del carrusel
define('APP_SYSTEM_BRANDS', APP_PUBLIC_PATH_IMAGES.'brands/');
define('APP_IMG_BRANDS', APP_IMG_PUBLIC.'brands/');

// Imagenes del categoría
define('APP_SYSTEM_CATEGORY', APP_PUBLIC_PATH_IMAGES.'category/');
define('APP_IMG_CATEGORY', APP_IMG_PUBLIC.'category/');

// IVA
define('APP_IVA', 21);
// Datos de email
define('EMAIL_HOST', 'mail.cosmeceuticalcenter.com');
define('EMAIL_SENDER', 'enviosweb@cosmeceuticalcenter.com');
define('EMAIL_PASSWORD', 'oopXe2A587Lmo123z');

setlocale(LC_TIME, 'es_VE');
date_default_timezone_set('Europe/Madrid');

require('core/Model/classConnection.php');
require('core/bin/functions/main-lib.php');
require('vendor/autoload.php');

?>
