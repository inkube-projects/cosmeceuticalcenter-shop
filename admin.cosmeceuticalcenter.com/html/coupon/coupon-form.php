<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- daterange picker -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/daterangepicker/daterangepicker.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Vales de descuento
                            <?php if ($act == 1): ?>
                                <small>Agregar Vale de descuento</small>
                            <?php else: ?>
                                <small>Editar Vale de descuento</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>"  method="post" action="">
                        <div class="box box-solid">
                            <div class="box-header">
                                <?php if ($act == 1): ?>
                                    <h2 class="box-title">Agregar Vale de descuento</h2>
                                <?php else: ?>
                                    <h2 class="box-title">Editar Vale de descuento</h2>
                                <?php endif; ?>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Nombre:</label>
                                            <input type="text" class="form-control" name="name" id="name" value="<?=$name?>">
                                            <div class="text-danger" id="name_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Descuento (%):</label>
                                            <input type="text" class="form-control only-decimal" name="discount" id="discount" value="<?=$discount?>">
                                            <div class="text-danger" id="discount_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Usos:</label>
                                            <input type="text" class="form-control only-number" name="uses" id="uses" value="<?=$uses?>">
                                            <div class="text-danger" id="uses_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Rango de fechas:</label>
                                            <input type="text" class="form-control" name="range" id="range" value="<?=$range?>">
                                            <div class="text-danger" id="range_validate"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Código:</label>
                                            <?php if ($act == 1): ?>
                                                <input type="text" class="form-control" name="canonical_id" id="canonical_id" value="<?=$canonical_id?>">
                                            <?php else: ?>
                                                <p><?=$canonical_id?></p>
                                            <?php endif; ?>
                                            <div class="text-danger" id="canonical_id_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Descripción:</label>
                                            <textarea name="description" id="description" class="form-control" rows="8"><?=$description?></textarea>
                                            <div class="text-danger" id="description_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                    </form>
                </section>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

        <div id="key" data-form="<?=$frm?>" data-act="<?=$act?>" data-id="<?=$id?>"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- date-range-picker -->
        <script src="<?=APP_ASSETS?>plugins/daterangepicker/moment.min.js"></script>
        <script src="<?=APP_ASSETS?>plugins/daterangepicker/daterangepicker.js"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-coupon.js"></script>
    </body>
</html>
