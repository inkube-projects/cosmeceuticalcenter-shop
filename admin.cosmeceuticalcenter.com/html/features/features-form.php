<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Comentarios
                            <?php if ($act == 1): ?>
                                <small>Agregar comentario</small>
                            <?php else: ?>
                                <small>Editar comentario</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>"  method="post" action="">
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <?php if ($act == 1): ?>
                                    <h2 class="box-title">Agregar comentario</h2>
                                <?php else: ?>
                                    <h2 class="box-title">Editar comentario</h2>
                                <?php endif; ?>

                                <div class="pull-right">
                                    <div class="form-group">
                                        <span class="button-checkbox">
                                            <button type="button" class="btn btn-flat css-marginR10 btn-primary active css-width100" data-color="primary" data-input="status">
                                                <i class="state-icon glyphicon glyphicon-check"></i> Habilitada
                                            </button>
                                            <input class="hidden" type="checkbox" name="status" id="status" value="1" <?=isChecked(array('1'), $status)?>>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Usuario:</label>
                                            <p><?=$u_name?> - <a href="admin/user/edit/<?=$user_id?>" target="_blank">Ver</a></p>
                                            <div class="text-danger" id="user_id_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Orden:</label>
                                            <p><?=$order_code?> - <a href="admin/orders/edit/<?=$order_id?>" target="_blank">Ver</a></p>
                                            <div class="text-danger" id="order_id_validate"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Producto:</label>
                                            <p><?=$product_name?> - <a href="admin/products/edit/<?=$product_id?>" target="_blank">Ver</a></p>
                                            <div class="text-danger" id="product_id_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Score:</label>
                                            <p><?=$score?> <i class="fas fa-star text-warning"></i></p>
                                            <div class="text-danger" id="score_validate"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Comentario:</label>
                                            <textarea name="review" id="review" class="form-control" rows="8"><?=$review?></textarea>
                                            <div class="text-danger" id="review_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                    </form>
                </section>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

                    <div id="key" data-form="<?=$frm?>" data-act="<?=$act?>" data-id="<?=$id?>"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Checkbox -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-checkbox.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-features.js"></script>
    </body>
</html>
