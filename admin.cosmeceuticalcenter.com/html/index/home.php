<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="" />
        <title><?=APP_TITLE?></title>
        <!-- Datatables -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <!-- SIDEBAR
            ================================================== -->
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header">
                            Escritorio <small>Actividad</small>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <h4>Resumen</h4>
                <div class="row">
                    <div class="col-xs-12 col-sm-3">

                        <div class="dashboard-stats__item bg-teal">
                            <i class="fa fa-comment"></i>
                            <h3 class="dashboard-stats__title">
                                <span class="count-to"><?=$cnt_products?></span>
                                <small>Productos</small>
                            </h3>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-3">

                        <div class="dashboard-stats__item bg-pink">
                            <i class="fa fa-user"></i>
                            <h3 class="dashboard-stats__title">
                                <span class="count-to"><?=$cnt_users?></span>
                                <small>Usuarios</small>
                            </h3>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-3">

                        <div class="dashboard-stats__item bg-accent">
                            <i class="fa fas fa-file-alt"></i>
                            <h3 class="dashboard-stats__title">
                                <span class="count-to"><?=$cnt_brands?></span>
                                <small>Marcas</small>
                            </h3>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-3">

                        <div class="dashboard-stats__item bg-orange">
                            <i class="fa fa-exclamation-circle"></i>
                            <h3 class="dashboard-stats__title">
                                <span class="count-to"><?=$cnt_categories?></span>
                                <small>Categorías</small>
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="box box-solid">
                            <div class="box-header">
                                <h2 class="box-title">Artículos por agotarse o agotados</h2>
                            </div>

                            <div class="box-body">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Producto</th>
                                            <th>Cantidad</th>
                                            <th>Marca</th>
                                            <th>Unidad</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($arr_inventory as $key => $val): ?>
                                        <tr>
                                            <td><?=$val['icon']?> <a href="admin/products/edit/<?=$val['product_id']?>"><?=$val['product_name']?></a></td>
                                            <td><?=$val['quantity']?></td>
                                            <td><?=$val['brand_name']?></td>
                                            <td><?=$val['unit_name']?></td>
                                            <td>
                                                <a href="admin/inventory/edit/<?=$val['inventory_id']?>" class="btn btn-info btn-flat">Editar</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="box box-solid">
                            <div class="box-header">
                                <h2 class="box-title">Últimos pedidos</h2>
                            </div>

                            <div class="box-body">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Código</th>
                                            <th>Usuario</th>
                                            <th>Estado</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($arr_orders as $key => $val): ?>
                                        <tr>
                                            <td><a href="admin/orders/edit/<?=$val['id']?>"><?=$val['code']?></a></td>
                                            <td><?=$val['user_names']." ".$val['label']?></td>
                                            <td><?=$val['formed_status']?></td>
                                            <td>
                                                <a href="admin/inventory/edit/<?=$val['id']?>" class="btn btn-info btn-flat">Editar</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row css-marginT20">
                    <div class="col-md-6">
                        <div class="box box-solid">
                            <div class="box-header">
                                <h2 class="box-title">Comentarios de clientes</h2>
                            </div>

                            <div class="box-body">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Ref.</th>
                                            <th>Usuario</th>
                                            <th>Producto</th>
                                            <th>Puntaje</th>
                                            <th>Creada</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($arr_features as $key => $val): ?>
                                        <tr>
                                            <td><?=$val['id']?></td>
                                            <td><a href="admin/user/edit/<?=$val['user_id']?>"><?=$val['u_name']?></a> </td>
                                            <td><a href="admin/products/edit/<?=$val['product_id']?>"><?=$val['product_name']?></a></td>
                                            <td><?=$val['score']?> <i class="fas fa-star text-warning"></i></td>
                                            <td><?=$val['formed_create']?></td>
                                            <td>
                                                <a href="admin/features/edit/<?=$val['id']?>" class="btn btn-info btn-flat">Editar</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- DataTables -->
        <script src="<?=APP_ASSETS?>plugins/datatables/jquery.dataTables.js"></script>
        <script src="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip({
                    placement : 'top'
                });

                $('.table').DataTable({
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ entradas por página",
                        "zeroRecords": "No se han encontrado resultados",
                        "info": "Mostrando página _PAGE_ de _PAGES_",
                        "infoEmpty": "No se han encontrado resultados",
                        "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
                        "search": "Buscar: ",
                        "paginate": {
                            "previous": "Anterior",
                            "next": "Siguiente"
                        }
                    }
                });
            });
        </script>
    </body>
</html>
