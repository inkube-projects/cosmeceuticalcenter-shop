<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <?php include('html/overall/header.php'); ?>
        <style media="screen">
            td {
                background-color: #eeeeee;
                padding: 0.6em 0.4em;
            }
        </style>
    </head>

    <body>
        <div class="css-invoice-container">
            <img src="<?=APP_IMG?>logos/logo-white.png" alt="" class="center-block css-marginB20">

            <div class="css-invoice-body">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="css-h1">Factura: <?=$invoice_id?></h1>
                        <b>Usuario:</b> <?=$name." ".$last_name?><br>
                        <b>NIF/CIF/NIE:</b> <?=$cnf?><br>
                        <b>Orden:</b> <?=$order_code?><br>
                        <b>Fecha:</b> <?=$create['date']?><br>
                        <b>Pago:</b> <?=$arr_order[0]['method']?> <br>
                        <b>Dirección:</b> <?=$address?>
                    </div>

                    <div class="col-md-6">
                        <img src="<?=APP_IMG?>logos/logo.png" alt="" class="pull-right css-marginB20">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <h2 class="css-h2 css-marginT20">Detalles de la compra</h2>
                    </div>

                    <div class="col-md-6 text-right">
                        <h2 class="css-h2 css-marginT20">N° de productos: <?=$total_products?></h2>
                    </div>
                </div>
                <table style="width: 100%; font-size: 11px; color: #374953;" bordercolor="FFFFFF" border="1"><!-- Title -->
                    <tbody>
                        <tr style="background-color:#eeeeee; text-align: center;">
                            <th style="width: 15%; padding: 1em 0;">Referencia</th>
                            <th>Producto</th>
                            <th style="width: 15%;">Precio Unidad</th>
                            <th style="width: 15%;">Cantidad</th>
                            <th>IVA</th>
                            <th style="width: 20%;">Precio Total</th>
                        </tr>
                        <!-- Products -->
                        <?php foreach ($arr_detail as $key => $val): ?>
                            <tr style="background-color:#eeeeee; text-align: center;">
                                <td style="width: 15%; padding: 1em 0;"><?=$key?></td>
                                <td><?=$val['product_name']?></td>
                                <td style="width: 15%;">
                                    <?php if ($val['order_new_price']): ?>
                                        <?=$val['order_new_price']?> €
                                    <?php else: ?>
                                        <?=$val['order_price']?> €
                                    <?php endif; ?>
                                </td>
                                <td style="width: 15%;"><?=$val['quantity']?></td>
                                <td>(<?=$val['product_iva']?>%) <?=$val['iva']?> €</td>
                                <td style="width: 20%;"><?=number_format($val['sub_total'],2,".","")?> €</td>
                            </tr>
                        <?php endforeach; ?>
                        <!-- Footer: prices -->
                        <tr style="text-align: right;">
                            <td>&nbsp;</td>
                            <td style="background-color: #eeeeee; padding: 0.6em 0.4em;" colspan="4">Productos</td>
                            <td style="background-color: #eeeeee; padding: 0.6em 0.4em;"><?=$total_products?></td>
                        </tr>
                        <tr style="text-align: right;">
                            <td>&nbsp;</td>
                            <td style="background-color: #eeeeee; padding: 0.6em 0.4em;" colspan="4">Descuentos</td>
                            <td style="background-color: #eeeeee; padding: 0.6em 0.4em;"><?=($arr_order[0]['discount']) ? $arr_order[0]['discount'] : "0.00"; ?> €</td>
                        </tr>
                        <tr style="text-align: right;">
                            <td>&nbsp;</td>
                            <td style="background-color: #eeeeee; padding: 0.6em 0.4em;" colspan="4">Env&iacute;os</td>
                            <td style="background-color: #eeeeee; padding: 0.6em 0.4em;"><?=number_format($arr_order[0]['shipping'],2,".","")?> €</td>
                        </tr>
                        <tr style="text-align: right; font-weight: bold;">
                            <td>&nbsp;</td>
                            <td style="background-color: #c4c4bc; padding: 0.6em 0.4em;" colspan="4">TOTAL</td>
                            <td style="background-color: #c4c4bc; padding: 0.6em 0.4em;"><?=$arr_order[0]['total']?> €</td>
                        </tr>
                    </tbody>
                </table>

                COSMECEUTICAL CENTER S.L <br>
                CIF B21507405 <br>
                C/ Jesús de la Vera-Cruz, 27 Bajo - 41002 Sevilla. <br>
            </div>

            <div class="css-btn-container">
                <button type="button" class="btn btn-info btn-flat pull-right" onclick="genPDF()">Descargar como pdf <i class="fas fa-file-pdf"></i></button>
            </div>
        </div>
    </body>
    <!-- JS Global -->
    <?php include('html/overall/js.php'); ?>
    <!-- html2canvas -->
    <script src="<?=APP_ASSETS?>plugins/html2canvas/html2canvas.js"></script>
    <script src="<?=APP_ASSETS?>plugins/canvas2image/canvas2image.js"></script>
    <!-- jsPDF -->
    <script src="<?=APP_ASSETS?>plugins/jsPDF-master/dist/jspdf.min.js"></script>
    <!-- Custom JS -->
    <script src="<?=APP_ASSETS?>js/js-invoice.js" type="text/javascript"></script>
</html>
