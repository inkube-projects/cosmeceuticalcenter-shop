<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Datatables -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/select2/select2.min.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Pedidos
                            <?php if ($act == 1): ?>
                                <small>Agregar pedido</small>
                            <?php else: ?>
                                <small>Editar pedido</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>
                    <?php if ($act == 1): ?>
                        <form name="<?=$frm?>" id="<?=$frm?>"  method="post" action="">
                            <div class="box box-solid">
                                <div class="box-header">
                                    <h2 class="box-title">Crear pedido</h2>
                                </div>

                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Usuario:</label>
                                                <select class="form-control select2" name="user" id="user">
                                                    <option value="">Seleccione un Usuario</option>
                                                    <?php foreach ($arr_user as $key => $val): ?>
                                                        <option value="<?=$val['id']?>"><?=$val['email']." - ".$val['name']." ".$val['last_name']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <div class="text-danger" id="user_validate"></div>
                                            </div>

                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Provincia:</label>
                                                <select class="form-control select2" name="province" id="province" <?=$p_disabled?>>
                                                    <option value="">No aplica</option>
                                                    <?php foreach ($arr_province as $key => $val): ?>
                                                        <option value="<?=$val['id']?>"><?=$val['name']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <div class="text-danger" id="province_validate"></div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="css-bold css-text-black">País:</label>
                                                <select class="form-control select2" name="country" id="country">
                                                    <option value="">Seleccione un país</option>
                                                    <?php foreach ($arr_country as $key => $val): ?>
                                                        <option value="<?=$val['id']?>"><?=$val['country']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <div class="text-danger" id="country_validate"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="box box-solid">
                                        <div class="box-header">
                                            <h2 class="box-title">Productos</h2>
                                        </div>

                                        <div class="box-body">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Ref.</th>
                                                        <th>Nombre</th>
                                                        <th>Precio</th>
                                                        <th>Marca</th>
                                                        <th>Categoría</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($arr_products as $key => $val): ?>
                                                    <tr>
                                                        <td><?=$val['id']?></td>
                                                        <td><?=$val['product_name']?></td>
                                                        <td><?=$val['price']?></td>
                                                        <td><?=$val['brand_name']?></td>
                                                        <td><?=$val['category_name']?></td>
                                                        <td>
                                                            <button type="button" class="btn btn-info btn-flat" onclick="addProduct(<?=$val['id']?>)">Agregar</button>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="box box-solid">
                                        <div class="box-header">
                                            <h2 class="box-title">Productos agregados</h2>
                                        </div>

                                        <div class="box-body" id="ajx-order">

                                        </div>
                                    </div>

                                    <div class="box box-solid">
                                        <div class="box-body">
                                            <ul class="list-group col-md-8 pull-right">
                                                <li class="list-group-item"><b>Sub total:</b> <span id="ajx-sub-total" class="pull-right"></span></li>
                                                <li class="list-group-item"><b>Importe:</b> <span id="ajx-shipping" class="pull-right">0.00</span></li>
                                                <li class="list-group-item"><b>Total:</b> <span id="ajx-total" class="pull-right"></span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                        </form>
                    <?php elseif ($act == 2): ?>
                        <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>"  method="post" action="">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="box box-solid">
                                        <div class="box-header with-border">
                                            <h2 class="box-title">Editar pedido</h2>
                                            <?php if ($present == 1): ?>
                                                <i class="fas fa-gift text-success css-fontSize25 pull-right"></i>
                                            <?php endif; ?>
                                        </div>

                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Código:</label>
                                                        <p><?=$code?></p>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Factura:</label>
                                                        <p><?=$invoice_id?></p>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Usuario:</label>
                                                        <p><?=$names?></p> - <a href="admin/user/edit/<?=$user_id?>" target="_blank">Info</a>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">E-mail:</label>
                                                        <p><?=$email?></p>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Teléfono:</label>
                                                        <p><?=$phone?></p>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Estado:</label>
                                                        <select class="form-control select2" name="status" id="status">
                                                            <option value="">Seleccione un estado</option>
                                                            <?php foreach ($arr_status as $val): ?>
                                                                <option value="<?=$val['id']?>" <?=isSelected($val['id'], $status_id)?>><?=$val['name']?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="text-danger" id="status_validate"></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Forma de pago:</label>
                                                        <p><?=$method?></p>
                                                        <div class="text-danger" id="user_id_validate"></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Regalo:</label>
                                                        <?php if ($present == 1): ?>
                                                            <p>Si</p>
                                                        <?php else: ?>
                                                            <p>No</p>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Sub total:</label>
                                                        <p><?=$sub_total?> &euro;</p>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Vale de descuento:</label>
                                                        <?php if ($discount): ?>
                                                            <p><?=$discount?> &euro;</p>
                                                        <?php else: ?>
                                                            <p>0.00 &euro;</p>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Envío:</label>
                                                        <p><?=$shipping?> &euro;</p>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Recargo por gestión (2%):</label>
                                                        <p><?=$charge?> &euro;</p>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Total:</label>
                                                        <p><?=$total?> &euro;</p>
                                                        <div class="text-danger" id="total_validate"></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Nota del cliente:</label>
                                                        <p><?=$order_note?></p>
                                                        <div class="text-danger" id="user_id_validate"></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Nota de regalo:</label>
                                                        <p><?=$present_note?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php if ($coupon_id): ?>
                                        <div class="box box-solid">
                                            <div class="box-header">
                                                <h2 class="box-title">Vale de descuento</h2>
                                            </div>

                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="css-bold css-text-black">Código:</label>
                                                            <p><a href="admin/coupon/edit/<?=$coupon_id?>"><?=$coupon_code?></a></p>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="css-bold css-text-black">Descuento (&euro;):</label>
                                                            <p><?=$discount?></p>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="css-bold css-text-black">Descuento (%):</label>
                                                            <p><?=$discount_percent?>%</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>

                                <div class="col-md-6">
                                    <div class="box box-solid">
                                        <div class="box-header with-border">
                                            <h2 class="box-title">Dirección de envío</h2>
                                        </div>

                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">País:</label>
                                                        <p><?=$country_name?></p>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Dirección 1 (Calle, código postal, nombre de empresa, c / o):</label>
                                                        <p><?=$address_1?></p>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Ciudad:</label>
                                                        <p><?=$city?></p>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Nota adicional:</label>
                                                        <p><?=$note?></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Provincia:</label>
                                                        <p><?=$province_name?></p>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Dirección 2 (Apartamento, suite, unidad, edificio, piso, etc):</label>
                                                        <p><?=$address_2?></p>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Código postal:</label>
                                                        <p><?=$postal_code?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box box-solid">
                                        <div class="box-header">
                                            <h2 class="box-title">Información de seguimiento</h2>
                                        </div>

                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Empresa:</label>
                                                        <input type="text" name="company" id="company" class="form-control" value="<?=$t_company?>">
                                                        <div class="text-danger" id="company_validate"></div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="css-bold css-text-black">Número de seguimiento:</label>
                                                        <input type="text" name="tracking_number" id="tracking_number" class="form-control" value="<?=$t_tracking_number?>">
                                                        <div class="text-danger" id="tracking_number_validate"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h2 class="box-title">Datos de facturación</h2>
                                </div>

                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="css-bold css-text-black">NIF/CIF/NIE:</label>
                                                <p><?=$b_identification?></p>
                                            </div>

                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Empresa:</label>
                                                <p><?=$b_company?></p>
                                            </div>

                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Nombre(s):</label>
                                                <p><?=$b_name?></p>
                                            </div>

                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Apellidos:</label>
                                                <p><?=$b_last_name?></p>
                                            </div>

                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Ciudad:</label>
                                                <p><?=$b_city?></p>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Pais:</label>
                                                <p><?=$b_country?></p>
                                            </div>

                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Provincia:</label>
                                                <p><?=$b_province?></p>
                                            </div>

                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Dirección:</label>
                                                <p><?=$b_address_1?>, <?=$b_address_2?></p>
                                            </div>

                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Código postal:</label>
                                                <p><?=$b_postal_code?></p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h2 class="box-title">Detalle del pedido</h2>
                                </div>

                                <div class="box-body">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Producto</th>
                                                <th>Talla</th>
                                                <th>Color</th>
                                                <th>Marca</th>
                                                <th>Categoría</th>
                                                <th>Cantidad</th>
                                                <th>Precio U.</th>
                                                <th>IVA</th>
                                                <th>Sub total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($arr_detail as $key => $val): ?>
                                            <tr>
                                                <td><a href="admin/products/edit/<?=$val['product_id']?>" target="_blank"><?=$val['product_name']?></a></td>
                                                <td><?=$val['size']?></td>
                                                <td><?=$val['color']?></td>
                                                <td><?=$val['brand_name']?></td>
                                                <td><?=$val['formed_categories']?></td>
                                                <td><?=$val['quantity']?></td>
                                                <td><?=number_format($val['unity_price'],2,".","")?></td>
                                                <td>(<?=$val['product_iva']?>%) <?=number_format($val['formed_iva'],2,".","")?></td>
                                                <td><?=number_format($val['sub_total'],2,".","")?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                            <a href="admin/orders/invoice/<?=$id?>" class="btn btn-warning btn-flat pull-right css-marginR10" target="_blank">Factura</a>
                        </form>
                    <?php endif; ?>
                </section>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

                    <div id="key" data-form="<?=$frm?>" data-act="<?=$act?>" data-id="<?=$id?>"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- DataTables -->
        <script src="<?=APP_ASSETS?>plugins/datatables/jquery.dataTables.js"></script>
        <script src="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- Select2 -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-orders.js"></script>
    </body>
</html>
