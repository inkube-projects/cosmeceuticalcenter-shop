<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Select 2 -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/select2/select2.min.css">
        <!-- Datatables -->
        <link rel="stylesheet" type="text/css" href="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Pedidos
                            <small>Listado de pedidos</small>
                        </h3>

                        <div class="col-md-2 pull-right css-marginT35">
                            <a href="admin/orders/add" class="btn btn-info btn-flat pull-right">Nuevo pedido</a>
                        </div>

                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <form name="frm-search" id="frm-search" method="post" action="">
                        <div class="box box-solid">
                            <div class="box-header">
                                <h2 class="box-title">Búsqueda avanzada</h2>
                            </div>

                            <div class="box-body">
                                <div class="row css-marginB20">
                                    <div class="col-md-4">
                                        <label class="css-bold css-text-black">Estado</label>
                                        <select class="form-control select2" name="src_status" id="src_status">
                                            <option value="">Todos los estados</option>
                                            <?php foreach ($arr_status as $val): ?>
                                                <option value="<?=$val['id']?>" <?=isSelected($val['id'], $src_status)?>><?=$val['name']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="css-bold css-text-black">Fecha de alta</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control js-date" name="src_start_date" id="src_start_date" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask placeholder="Desde" value="<?=$src_start_date?>">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control js-date" name="src_end_date" id="src_end_date" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask placeholder="Hasta" value="<?=$src_end_date?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="css-bold css-text-black">Precio total</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control only-decimal" name="src_start_value" id="src_start_value" placeholder="Desde" value="<?=$src_start_value?>">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control only-decimal" name="src_end_value" id="src_end_value" placeholder="Hasta" value="<?=$src_end_value?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="css-bold css-text-black">Método de pago</label>
                                        <select class="form-control select2" name="src_method" id="src_method">
                                            <option value="">Todos los métodos</option>
                                            <?php foreach ($arr_methods as $val): ?>
                                                <option value="<?=$val['id']?>" <?=isSelected($val['id'], $src_method)?>><?=$val['method']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="css-bold css-text-black">Provincias</label>
                                        <select class="form-control select2" name="src_province" id="src_province">
                                            <option value="">Todas las provincias</option>
                                            <?php foreach ($arr_province as $val): ?>
                                                <option value="<?=$val['id']?>" <?=isSelected($val['id'], $src_province)?>><?=$val['name']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button type="button" class="btn btn-warning btn-flat btn-reset" onclick="formReset('frm-search')"><i class="fa fa-brush"></i> Limpiar formulario</button>
                                <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fas fa-search"></i> Buscar</button>
                            </div>
                        </div>
                    </form>

                    <div class="js-alert"><?=$flash_message?></div>

                    <div class="box box-solid">
                        <div class="box-header">
                            <h2 class="box-title">Listado de pedidos</h2>
                        </div>
                        <div class="box-body">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Ref.</th>
                                        <th>code</th>
                                        <th>Factura</th>
                                        <th>Usuario</th>
                                        <th>Teléfono</th>
                                        <th>Email</th>
                                        <th>Estado</th>
                                        <th>Método de pago</th>
                                        <!-- <th>Provincia</th> -->
                                        <th>Total</th>
                                        <th>Regalo</th>
                                        <!-- <th>Creada</th> -->
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($arr_orders as $key => $val): ?>
                                    <tr>
                                        <td><?=$val['id']?></td>
                                        <td><?=$val['code']?></td>
                                        <td><?=$val['invoice_id']?> <br> <?=$val['formed_date']?></td>
                                        <td><?=$val['user_names']?> <?=$val['label']?></td>
										<td><?=$val['user_phone']?></td>
										<td><?=$val['user_email']?></td>
                                        <td><?=$val['formed_status']?></td>
                                        <td><?=$val['method']?></td>
                                        <!-- <td><?=$val['formed_province']?></td> -->
                                        <td><?=$val['total']?></td>
                                        <td><?=$val['formed_present']?></td>
                                        <!-- <td><?=$val['formed_date']?></td> -->
                                        <td>
                                            <a href="admin/orders/edit/<?=$val['id']?>" class="btn btn-xs btn-info btn-flat">Editar</a>
                                            <a href="admin/orders/invoice/<?=$val['id']?>" class="btn btn-xs btn-warning btn-flat" target="_blank">Factura</a>
                                            <?php if ($this->user_role === "ROLE_ADMIN"): ?>
                                                <button type="button" class="btn btn-xs btn-danger btn-flat" onclick="SelfdeleteBoostrapAction('admin/orders/remove/<?=$val['id']?>', 1)"><i class="far fa-trash-alt"></i></button>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>

                <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Eliminar Pedido</h4>
                            </div>
                            <div class="modal-body">
                                <p>Estas seguro de eliminar <b>Pedido</b></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                                <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="SelfdeleteBoostrapAction('', 2, '<?=BASE_URL?>admin/orders/list/OK3')"><i class="fa fa-trash"></i> Eliminar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->


        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- Select2 -->
        <script src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js"></script>
        <!-- DataTables -->
        <script src="<?=APP_ASSETS?>plugins/datatables/jquery.dataTables.js"></script>
        <script src="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- InputMask -->
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.js"></script>
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-orders-list.js"></script>
    </body>
</html>
