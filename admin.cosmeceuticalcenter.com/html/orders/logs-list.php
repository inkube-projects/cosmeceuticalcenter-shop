<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Select 2 -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/select2/select2.min.css">
        <!-- Datatables -->
        <link rel="stylesheet" type="text/css" href="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Pedidos
                            <small>Listado de logs</small>
                        </h3>

                        <div class="col-md-2 pull-right css-marginT35">
                            <a href="admin/orders/add" class="btn btn-info btn-flat pull-right">Nuevo pedido</a>
                        </div>

                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="box box-solid">
                        <div class="box-header">
                            <h2 class="box-title">Listado de logs</h2>
                        </div>
                        <div class="box-body">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Ref.</th>
                                        <th>E-mail</th>
                                        <th>Nombres</th>
                                        <th>Logs</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($arr_users as $key => $val): ?>
                                        <tr>
                                            <td><?=$val['id']?></td>
                                            <td><?=$val['email']?></td>
                                            <td><?=$val['formed_name']?></td>
                                            <td><?=$val['cnt_logs']?></td>
                                            <td>
                                                <?php if ($val['cnt_logs'] > 0): ?>
                                                    <a href="admin/orders/log/<?=$val['id']?>" class="btn btn-xs btn-info btn-flat">Ver</a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                
                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->


        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- Select2 -->
        <script src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js"></script>
        <!-- DataTables -->
        <script src="<?=APP_ASSETS?>plugins/datatables/jquery.dataTables.js"></script>
        <script src="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- InputMask -->
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.js"></script>
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-orders-list.js"></script>
    </body>
</html>
