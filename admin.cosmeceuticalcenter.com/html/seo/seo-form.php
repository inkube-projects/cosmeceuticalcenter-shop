<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Select2 -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/select2/select2.min.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Seo
                            <?php if ($act == 1): ?>
                                <small>Agregar seo</small>
                            <?php else: ?>
                                <small>Editar seo</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>"  method="post" action="">
                        <div class="box box-solid">
                            <div class="box-header">
                                <?php if ($act == 1): ?>
                                    <h2 class="box-title">Agregar seo</h2>
                                <?php else: ?>
                                    <h2 class="box-title">Editar seo</h2>
                                <?php endif; ?>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Palabras clave <small>Separadas por coma(,)</small>:</label>
                                            <textarea name="keywords" id="keywords" class="form-control" rows="8"><?=$keywords?></textarea>
                                            <div class="text-danger" id="keywords_validate"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Descripción:</label>
                                            <textarea name="description" id="description" class="form-control" rows="8"><?=$description?></textarea>
                                            <div class="text-danger" id="description_validate"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Sección:</label>
                                            <select class="form-control select2" name="section" id="section">
                                                <option value="">Seleccione una sección</option>
                                                <?php foreach ($arr_section as $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelected($val['id'], $section_id)?>><?=$val['view']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <div class="text-danger" id="section_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                    </form>
                </section>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

                    <div id="key" data-form="<?=$frm?>" data-act="<?=$act?>" data-id="<?=$id?>"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- Select2 -->
        <script src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js" type="text/javascript"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-seo.js"></script>
    </body>
</html>
