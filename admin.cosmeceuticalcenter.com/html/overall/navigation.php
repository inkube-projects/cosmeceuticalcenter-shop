<!-- Inicio -->
<li class="" data-h-opt="Index">
    <a href="admin/dashboard"><i class="fa fa-th-large icoG"></i> <span class="txtMenu">DASHBOARD</span></a>
</li>

<!-- Carousel -->
<li class="sidebar-nav__dropdown" data-h-opt="Carousel">
    <a href="#"><i class="fas fa-images icoG"></i><span class="txtMenu">Carrusel<i class="fa fa-angle-down"></i></span></a>
    <ul class="sidebar-nav__submenu">
        <li class="" data-h-subopt="list"><a href="admin/carousel/list">Listado de Imágenes</a></li>
        <li class="" data-h-subopt="add"><a href="admin/carousel/add">Agregar Imagen</a></li>
    </ul>
</li>

<!-- Inventory -->
<li class="sidebar-nav__dropdown" data-h-opt="Inventory">
    <a href="#"><i class="fas fa-dolly-flatbed icoG"></i></i><span class="txtMenu">Stock<i class="fa fa-angle-down"></i></span></a>
    <ul class="sidebar-nav__submenu">
        <li class="" data-h-subopt="list"><a href="admin/inventory/list">Listado de Productos</a></li>
        <li class="" data-h-subopt="add"><a href="admin/inventory/add">Agregar Stock</a></li>
    </ul>
</li>

<!-- Marcas -->
<li class="sidebar-nav__dropdown" data-h-opt="Brands">
    <a href="#"><i class="fas fa-clipboard-check icoG"></i></i><span class="txtMenu">Marcas<i class="fa fa-angle-down"></i></span></a>
    <ul class="sidebar-nav__submenu">
        <li class="" data-h-subopt="list"><a href="admin/brands/list">Listado de Marcas</a></li>
        <li class="" data-h-subopt="add"><a href="admin/brands/add">Agregar Marca</a></li>
    </ul>
</li>

<!-- Usuarios -->
<li class="sidebar-nav__dropdown" data-h-opt="Users">
    <a href="#"><i class="fas fa-users icoG"></i><span class="txtMenu">Clientes<i class="fa fa-angle-down"></i></span></a>
    <ul class="sidebar-nav__submenu">
        <li class="" data-h-subopt="list"><a href="admin/users/list">Listado de clientes</a></li>
        <li class="" data-h-subopt="add"><a href="admin/user/add">Agregar cliente</a></li>
    </ul>
</li>


<!-- productos -->
<li class="sidebar-nav__dropdown" data-h-opt="Products">
    <a href="#"><i class="fas fa-box-open icoG"></i><span class="txtMenu">Productos<i class="fa fa-angle-down"></i></span></a>
    <ul class="sidebar-nav__submenu">
        <li class="" data-h-subopt="list"><a href="admin/products/list">Listado de Productos</a></li>
        <li class="" data-h-subopt="add"><a href="admin/products/add">Agregar Producto</a></li>
        <li class="" data-h-subopt="pcList"><a href="admin/products/category/list">Listado de Categorías</a></li>
        <li class="" data-h-subopt="pcAdd"><a href="admin/products/category/add">Agregar Categoría</a></li>
        <li class="" data-h-subopt="weekly"><a href="admin/products/weekly">Recomendaciones Semanales</a></li>
        <li class="" data-h-subopt="novelty"><a href="admin/products/novelty">Novedades</a></li>
        <li class="" data-h-subopt="discount"><a href="admin/products/discount">Descuentos</a></li>
        <li class="" data-h-subopt="discount"><a href="admin/products/colors">Colores</a></li>
        <li class="" data-h-subopt="discount"><a href="admin/products/size">Tallas</a></li>
    </ul>
</li>

<!-- Ordenes -->
<li class="sidebar-nav__dropdown" data-h-opt="Orders">
    <a href="#"><i class="fas fa-shipping-fast icoG"></i><span class="txtMenu">Pedidos<i class="fa fa-angle-down"></i></span></a>
    <ul class="sidebar-nav__submenu">
        <li class="" data-h-subopt="list"><a href="admin/orders/list">Listado de pedidos</a></li>
        <!-- <li class="" data-h-subopt="add"><a href="admin/orders/add">Agregar Orden</a></li> -->
    </ul>
</li>

<!-- Seo -->
<li class="sidebar-nav__dropdown" data-h-opt="Seo">
    <a href="#"><i class="fas fa-bullhorn icoG"></i><span class="txtMenu">Seo<i class="fa fa-angle-down"></i></span></a>
    <ul class="sidebar-nav__submenu">
        <li class="" data-h-subopt="list"><a href="admin/seo/list">Listado de Seo</a></li>
        <li class="" data-h-subopt="add"><a href="admin/seo/add">Agregar Seo</a></li>
    </ul>
</li>

<!-- Subcategory -->
<li class="sidebar-nav__dropdown" data-h-opt="Subcategory">
    <a href="#"><i class="fas fa-cubes icoG"></i><span class="txtMenu">Sub-categoría<i class="fa fa-angle-down"></i></span></a>
    <ul class="sidebar-nav__submenu">
        <li class="" data-h-subopt="list"><a href="admin/subcategory/list">Listado de Subcategorias</a></li>
        <li class="" data-h-subopt="add"><a href="admin/subcategory/add">Agregar Subcategoria</a></li>
    </ul>
</li>

<!-- Zones -->
<li class="sidebar-nav__dropdown" data-h-opt="Zones">
    <a href="#"><i class="fas fa-map-signs icoG"></i><span class="txtMenu">Zonas<i class="fa fa-angle-down"></i></span></a>
    <ul class="sidebar-nav__submenu">
        <li class="" data-h-subopt="list"><a href="admin/zones/list">Listado de Zonas</a></li>
        <li class="" data-h-subopt="add"><a href="admin/zones/add">Agregar Zona</a></li>
    </ul>
</li>

<!-- Newsletter -->
<li class="sidebar-nav__dropdown" data-h-opt="Newsletter">
    <a href="#"><i class="fas fa-envelope-square icoG"></i><span class="txtMenu">Newsletter<i class="fa fa-angle-down"></i></span></a>
    <ul class="sidebar-nav__submenu">
        <li class="" data-h-subopt="list"><a href="admin/newsletter/list">Listado de Emails</a></li>
        <li class="" data-h-subopt="add"><a href="admin/newsletter/add">Agregar email</a></li>
    </ul>
</li>

<!-- Features -->
<li class="sidebar-nav__dropdown" data-h-opt="Features">
    <a href="#"><i class="fas fa-star icoG"></i><span class="txtMenu">Comentarios<i class="fa fa-angle-down"></i></span></a>
    <ul class="sidebar-nav__submenu">
        <li class="" data-h-subopt="list"><a href="admin/features/list">Listado de Comentarios</a></li>
    </ul>
</li>

<!-- coupon -->
<li class="sidebar-nav__dropdown" data-h-opt="Coupon">
    <a href="#"><i class="fas fa-ticket-alt icoG"></i><span class="txtMenu">Vales de descuento<i class="fa fa-angle-down"></i></span></a>
    <ul class="sidebar-nav__submenu">
        <li class="" data-h-subopt="list"><a href="admin/coupon/list">Listado de vales</a></li>
        <li class="" data-h-subopt="add"><a href="admin/coupon/add">Agregar vale</a></li>
    </ul>
</li>

<!-- navoptions -->
