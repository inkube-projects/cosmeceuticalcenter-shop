<div class="sidebar">
    <!-- Close button (mobile devices) -->
    <div class="sidebar__close">
        <img src="<?=APP_IMG?>close.svg" alt="Close sidebar">
    </div>

    <!-- Sidebar user -->
    <div class="sidebar__user center-block text-center">
        <div class="sidebar-user__avatar center-block">
            <img src="<?=APP_IMG?>logos/logo-white.png" alt="" class="img-responsive">
        </div>
        <a class="sidebar-user__info">
            <h4 class="txtMenu">
                <?=mb_convert_case($this->user_username, MB_CASE_UPPER, "UTF-8")?>
            </h4>
            <p>Opciones <i class="fa fa-caret-down"></i></p>
        </a>
        <a href="admin/logout" data-toggle="tooltip" data-placement="top" title="" data-original-title="Salir">
            <span class="glyphicon glyphicon-off text-red" aria-hidden="true"></span>
        </a>
    </div>

    <!-- Sidebar user nav -->
    <nav class="sidebar-user__nav">
        <ul class="sidebar__nav">
            <li><a href="#"><i class="fa fa-user icoG"></i><span class="txtMenu">Mi Perfil</span></a></li>
            <li><a href="admin/administrator/list"><i class="fa fa-users icoG"></i> <span class="txtMenu">Perfil Usuairos</span></a></li>
            <!-- <li><a href="#"><i class="fa fa-briefcase icoG"></i><span class="txtMenu">Perfil Empresas</span></a></li> -->
        </ul>
    </nav>

    <!-- Sidebar nav -->
    <nav>
        <ul class="sidebar__nav">
            <ul class="sidebar__nav">
                <?php include('html/overall/navigation.php') ?>
            </ul>

        </ul>
    </nav>

    <!-- <div class="sidebar-footer hidden-small">
        <a href="admin/logout" data-toggle="tooltip" data-placement="top" title="" data-original-title="Salir">
            <span class="glyphicon glyphicon-off text-red" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Settings">
            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        </a>
    </div> -->
</div>

<div id="h_m" data-hm="<?=$this->user_method?>" data-hv="<?=$this->user_view?>"></div>

<div class="row">
    <div class="col-xs-12">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="navbar_main">
                    <a href="index.html#" class="btn btn-default navbar-btn navbar-left" id="sidebar__toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="hidden-xs">Accesos</span> <i class="fa fa-briefcase fa-2x visible-xs-inline-block"></i>
                            </a>
                            <div class="dropdown-menu navbar-messages">
                            <a href="http://niu.inkube.net/HOME-NIU" class="navbar-messages__item active" target="_blank">
                            <div class="navbar-messages__avatar">
                            <img src="<?=APP_IMG?>access/niu.jpg" alt="...">
                            </div>
                            <div class="navbar-messages__body">
                            <h5 class="navbar-messages__sender">
                            NIU
                            </h5>
                            </div>
                            </a>
                            </div> -->
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
</div> <!-- / .row -->
