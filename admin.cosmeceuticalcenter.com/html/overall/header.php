<!-- CSS Plugins -->
<link rel="stylesheet" href="<?=APP_ASSETS?>css/perfect-scrollbar.min.css">
<!-- Bootstrap -->
<link href="<?=APP_ASSETS?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Font-awesome 5.0.6 -->
<link rel="stylesheet" href="<?=APP_ASSETS?>plugins/font-awesome/css/all.css">
<!-- Google Fonts -->
<!-- <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'> -->
<!-- CSS Global -->
<link href="<?=APP_ASSETS?>css/styles.css" rel="stylesheet">
<!--CSS Custom-->
<link href="<?=APP_ASSETS?>css/BFT.css" rel="stylesheet">
