<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Select2 -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/select2/select2.min.css">
        <!-- Datatables -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Stock
                            <?php if ($act == 1): ?>
                                <small>Agregar Stock</small>
                            <?php else: ?>
                                <small>Editar Stock</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>"  method="post" action="">
                        <div class="box box-solid">
                            <div class="box-header">
                                <?php if ($act == 1): ?>
                                    <h2 class="box-title">Agregar Stock</h2>
                                <?php else: ?>
                                    <h2 class="box-title">Editar Stock</h2>
                                <?php endif; ?>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Producto:</label>
                                            <?php if ($act == 1): ?>
                                                <select class="form-control select2" name="product" id="product">
                                                    <option value="">Debes seleccionar un producto</option>
                                                    <?php foreach ($arr_products as $key => $val): ?>
                                                        <option value="<?=$val['id']?>" <?=isSelected($val['id'], $product)?>><?=$val['name']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <div class="text-danger" id="product_validate"></div>
                                            <?php else: ?>
                                                <p><a href="admin/products/edit/<?=$arr_inventory[0]['product_id']?>" target="_blank"><?=$product?></a></p>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">quantity:</label>
                                            <input type="text" class="form-control only-decimal" name="quantity" id="quantity" value="<?=$quantity?>">
                                            <div class="text-danger" id="quantity_validate"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Unidad:</label>
                                            <select class="form-control select2" name="unity" id="unity">
                                                <?php foreach ($arr_unit as $key => $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelected($val['id'], $unity)?>><?=$val['name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <div class="text-danger" id="unity_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                        <div class="clearfix css-marginB10"></div>
                    </form>

                    <?php if ($act == 2): ?>
                        <div class="box box-solid">
                            <div class="box-header">
                                <h2 class="box-title">Usuarios a notificar</h2>
                            </div>

                            <div class="box-body">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Email</th>
                                            <th>Fecha</th>
                                            <th>Estado</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($arr_notify as $key => $val): ?>
                                            <tr>
                                                <td><?=($key + 1)?></td>
                                                <td><?=$val['email']?></td>
                                                <td><?=$val['formed_date']?></td>
                                                <td><?=$val['formed_status']?></td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-remove" data-id="<?=$val['id']?>"><i class="fas fa-trash-alt"></i></button>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php endif; ?>
                </section>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

        <div id="key" data-form="<?=$frm?>" data-act="<?=$act?>" data-id="<?=$id?>"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- DataTables -->
        <script src="<?=APP_ASSETS?>plugins/datatables/jquery.dataTables.js"></script>
        <script src="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- Select2 -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-inventory.js"></script>
    </body>
</html>
