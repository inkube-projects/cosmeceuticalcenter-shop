<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Datatables -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/datetables1.10/DataTables-1.10.18/css/dataTables.bootstrap.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/select2/select2.min.css">
        <?php include('html/overall/header.php'); ?>
        <style media="screen">
            form input[type="radio"] { display: block; }
        </style>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Productos
                            <small>Listado de productos</small>
                        </h3>

                        <div class="col-md-2 pull-right css-marginT35">
                            <a href="admin/products/add" class="btn btn-info btn-flat pull-right">Nuevo producto</a>
                        </div>

                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <form name="frm-search" id="frm-search" method="post" action="">
                        <div class="box box-solid">
                            <div class="box-header">
                                <h2 class="box-title">Búsqueda avanzada</h2>
                            </div>

                            <div class="box-body">
                                <div class="row css-marginB20">
                                    <div class="col-md-4">
                                        <label class="css-bold css-text-black">Marcas</label>
                                        <select class="form-control select2" name="src_brand" id="src_brand">
                                            <option value="">Todas las marcas</option>
                                            <?php foreach ($arr_brands as $val): ?>
                                                <option value="<?=$val['id']?>" <?=isSelected($val['id'], $src_brand)?>><?=$val['name']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="css-bold css-text-black">Categorías</label>
                                        <select class="form-control select2" name="src_cat" id="src_cat">
                                            <option value="">Todas las categorías</option>
                                            <?php foreach ($arr_categories as $val): ?>
                                                <option value="<?=$val['id']?>" <?=isSelected($val['id'], $src_cat)?>><?=$val['name']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="css-bold css-text-black">Estados</label>
                                        <select class="form-control select2" name="src_status" id="src_status">
                                            <option value="">Todos los estados</option>
                                            <option value="2" <?=isSelected(2, $src_status)?>>Habilitados</option>
                                            <option value="1" <?=isSelected(1, $src_status)?>>Inhabilitados</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button type="button" class="btn btn-warning btn-flat btn-reset" onclick="formReset('frm-search')"><i class="fa fa-brush"></i> Limpiar formulario</button>
                                <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fas fa-search"></i> Buscar</button>
                            </div>
                        </div>
                    </form>

                    <div class="js-alert"><?=$flash_message?></div>

                    <div class="box box-solid">
                        <div class="box-header">
                            <h2 class="box-title">Listado de productos</h2>
                        </div>
                        <div class="box-body">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Ref.</th>
                                        <th>Imagen</th>
                                        <th width="30%">Nombre</th>
															<th>Precio <span>(con iva)</span></th>
                                        <th width="20%">Marca</th>
                                        <th>Categorías</th>
                                        <th>Activo</th>
                                        <th>Stock</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($arr_products as $key => $val): ?>
                                    <tr>
                                        <td><?=$val['id']?></td>
                                        <td>
                                            <img src="<?=$val['formed_image']?>" alt="" width="80">
                                        </td>
                                        <td><?=$val['product_name']?></td>
                                        <td><?=$val['price_iva']?></td>
                                        <td><?=$val['brand_name']?></td>
                                        <td><?=$val['formed_categories']?></td>
                                        <td><?=$val['status_icon']?></td>
                                        <td>
                                            <?php if ($val['inventory_extra']): ?>
                                                <button class="btn btn-warning btn-flat" onclick="showModalExtra('<?=$val['id']?>')"><i class="fas fa-dolly-flatbed"></i></button>
                                            <?php else: ?>
                                                <input type="text" name="inventory" class="form-control inventory-<?=$val['id']?>" value="<?=$val['inventory_quantity']?>" style="width: 60px;" onfocusout="setInventory(<?=$val['id']?>)">
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <a href="admin/products/edit/<?=$val['id']?>" class="btn btn-xs btn-info btn-flat">Editar</a>
                                            <?php if ($this->user_role === "ROLE_ADMIN"): ?>
                                                <button type="button" class="btn btn-xs btn-danger btn-flat" onclick="SelfdeleteBoostrapAction('admin/products/remove/<?=$val['id']?>', 1)"><i class="far fa-trash-alt"></i></button>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </section>

                <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Eliminar producto</h4>
                            </div>
                            <div class="modal-body">
                                <p>Estas seguro de eliminar <b>producto</b></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                                <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="SelfdeleteBoostrapAction('', 2, '<?=BASE_URL?>admin/products/list/OK3')"><i class="fa fa-trash"></i> Eliminar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" tabindex="-1" role="dialog" id="mod-stock-extra">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Stock</h4>
                            </div>
                            <div class="modal-body" id="ext-stock-body">

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->


        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- Select2 -->
        <script src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js" type="text/javascript"></script>
        <!-- DataTables -->
        <script src="<?=APP_ASSETS?>plugins/datetables1.10/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
        <script src="<?=APP_ASSETS?>plugins/datetables1.10/DataTables-1.10.18/js/dataTables.bootstrap.min.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-products-list.js"></script>
        <script>
            $(function(){
                $('.table').DataTable({
                    "displayStart": <?=$page?>,
                    "order": [[ 0, "desc" ]],
                    "pageLength": 100,
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ entradas por página",
                        "zeroRecords": "No se han encontrado resultados",
                        "info": "Mostrando página _PAGE_ de _PAGES_",
                        "infoEmpty": "No se han encontrado resultados",
                        "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
                        "search": "Buscar: ",
                        "paginate": {
                            "previous": "Anterior",
                            "next": "Siguiente"
                        }
                    },
                    "columns": [
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        { "orderDataType": "dom-text-numeric" },
                        null,
                    ]
                });
            });
        </script>
    </body>
</html>
