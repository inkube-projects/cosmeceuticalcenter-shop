<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Productos
                            <?php if ($act == 1): ?>
                                <small>Agregar categoría</small>
                            <?php else: ?>
                                <small>Editar categoría</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>"  method="post" action="">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <?php if ($act == 1): ?>
                                            <h2 class="box-title">Agregar categoría</h2>
                                        <?php else: ?>
                                            <h2 class="box-title">Editar categoría</h2>
                                        <?php endif; ?>
                                    </div>

                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="css-bold css-text-black">Nombre:</label>
                                                    <input type="text" class="form-control" name="name" id="name" value="<?=$name?>">
                                                    <div class="text-danger" id="name_validate"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="css-bold css-text-black">Descripción:</label>
                                                    <textarea name="description" id="description" class="form-control" rows="8"><?=$description?></textarea>
                                                    <div class="text-danger" id="description_validate"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h2 class="box-title">Imagen</h2>
                                    </div>

                                    <div class="box-body">
                                        <div class="col-xs-12 text-center">
                                            <img class="img-responsive css-marginT20 css-marginB20 css-noFloat center-block" src="<?=$image?>" alt="">
                                        </div>

                                        <div class="btn btn-default btn-file css-width100 btn-files-adj" id="img-image">
                                            <i class="far fa-image"></i> <span>Seleccione una imagen</span>
                                            <input type="file" name="image" id="image" class="img-section">
                                        </div>
                                        <div class="text-danger" id="image_validate"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                    </form>
                </section>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

                    <div id="key" data-form="<?=$frm?>" data-act="<?=$act?>" data-id="<?=$id?>"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-products-category.js"></script>
    </body>
</html>
