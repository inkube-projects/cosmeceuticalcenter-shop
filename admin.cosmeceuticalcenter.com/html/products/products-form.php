<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Select2 -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/select2/select2.min.css">
        <!-- daterange picker -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/daterangepicker/daterangepicker.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Productos
                            <?php if ($act == 1): ?>
                                <small>Agregar producto</small>
                            <?php else: ?>
                                <small>Editar producto</small>
                            <?php endif; ?>

                            <div class="css-marginT10">
                                <a href="admin/products/list" class="btn btn-warning btn-flat">Volver</a>
                            </div>
                        </h3>
                    </div>
                </div>

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>"  method="post" action="">
                        <input type="hidden" name="hid-name" id="hid-name" value="">

                        <div class="box box-solid">
                            <div class="box-header">
                                <?php if ($act == 1): ?>
                                    <h2 class="box-title">Agregar producto</h2>
                                <?php else: ?>
                                    <h2 class="box-title">Editar producto</h2>
                                <?php endif; ?>

                                <div class="pull-right">
                                    <div class="form-group">
                                        <span class="button-checkbox">
                                            <button type="button" class="btn btn-flat css-marginR10 btn-primary active css-width100" data-color="primary" data-input="status">
                                                <i class="state-icon glyphicon glyphicon-check"></i> Habilitada
                                            </button>
                                            <input class="hidden" type="checkbox" name="status" id="status" value="1" <?=isChecked(array('1'), $status)?>>
                                        </span>
                                    </div>
                                </div>

                                <div class="pull-right">
                                    <div class="form-group">
                                        <span class="button-checkbox">
                                            <button type="button" class="btn btn-flat css-marginR10 btn-primary active css-width100" data-color="primary" data-input="novelty">
                                                <i class="state-icon glyphicon glyphicon-check"></i> Novedad
                                            </button>
                                            <input class="hidden" type="checkbox" name="novelty" id="novelty" value="1" <?=isChecked(array('1'), $novelty)?>>
                                        </span>
                                    </div>
                                </div>

                                <div class="pull-right">
                                    <div class="form-group">
                                        <span class="button-checkbox">
                                            <button type="button" class="btn btn-flat css-marginR10 btn-primary active css-width100" data-color="primary" data-input="discount_promo">
                                                <i class="state-icon glyphicon glyphicon-check"></i> Descuento / Promoción
                                            </button>
                                            <input class="hidden" type="checkbox" name="discount_promo" id="discount_promo" value="1" <?=isChecked(array('1'), $discount_promo)?>>
                                        </span>
                                    </div>
                                </div>

                                <div class="pull-right">
                                    <div class="form-group">
                                        <span class="button-checkbox">
                                            <button type="button" class="btn btn-flat css-marginR10 btn-primary active css-width100" data-color="primary" data-input="weekly_recommendation">
                                                <i class="state-icon glyphicon glyphicon-check"></i> Recomendación semanal
                                            </button>
                                            <input class="hidden" type="checkbox" name="weekly_recommendation" id="weekly_recommendation" value="1" <?=isChecked(array('1'), $weekly_recommendation)?>>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Nombre:</label>
                                            <input type="text" class="form-control" name="name" id="name" value="<?=$name?>">
                                            <div class="text-danger" id="name_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Marcas:</label>
                                            <select class="select2 form-control" name="brand" id="brand">
                                                <option value="">Seleccione una marca</option>
                                                <?php foreach ($arr_brands as $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelected($val['id'], $brand)?>><?=$val['name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <div class="text-danger" id="brand_validate"></div>
                                        </div>

                                        <?php if ($act == 2): ?>
                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Precio (sin iva):</label>
                                                <p><?=$price?></p>
                                                <div class="text-danger" id="iva_validate"></div>
                                            </div>
                                        <?php endif; ?>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">IVA (%):</label>
                                            <input type="text" class="form-control only-number" name="iva" id="iva" value="<?=$iva?>">
                                            <div class="text-danger" id="iva_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Precio (con iva):</label>
                                            <input type="text" class="form-control only-decimal" name="price" id="price" value="<?=$price_iva?>">
                                            <div class="text-danger" id="price_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Contenido:</label>
                                            <input type="text" class="form-control" name="content" id="content" value="<?=$content?>">
                                            <div class="text-danger" id="content_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Categorías:</label>
                                            <select class="select2 form-control" name="category[]" id="category" multiple="multiple">
                                                <option value="">Seleccione una categoría</option>
                                                <?php foreach ($arr_category as $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelectedMultiple($arr_s_cat, $val['id'])?>><?=$val['name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <div class="text-danger" id="category_validate"></div>
                                        </div>

                                        <div class="form-group" id="ajx-subcategory">
                                            <label class="css-bold css-text-black">Sub-categoría:</label>
                                            <select class="select2 form-control" name="subcategory[]" id="subcategory" multiple="multiple">
                                                <option value="">Seleccione una categoría primero</option>
                                                <?php foreach ($arr_subcategories as $key => $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelectedMultiple($arr_s_subcat, $val['id'])?>><?=$val['name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <div class="text-danger" id="subcategory_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Tallas:</label>
                                            <select class="select2 form-control" name="size[]" id="size" multiple="multiple">
                                                <option value="">Seleccione una talla</option>
                                                <?php foreach ($arr_sizes as $key => $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelectedMultiple($arr_s_size, $val['id'])?>><?=$val['name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <div class="text-danger" id="size_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Tags (separada por comas ","):</label>
                                            <textarea name="tags" id="tags" class="form-control" rows="8"><?=$tags?></textarea>
                                            <div class="text-danger" id="tags_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Descripción:</label>
                                            <textarea class="form-control" name="description" id="description" rows="8"><?=$description?></textarea>
                                            <div class="text-danger" id="description_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Principios Activos:</label>
                                            <textarea class="form-control" name="active_principles" id="active_principles" rows="8"><?=$active_principles?></textarea>
                                            <div class="text-danger" id="active_principles_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Descripción corta:</label>
                                            <textarea class="form-control" name="description_short" id="description_short" rows="2"><?=$description_short?></textarea>
                                            <div class="text-danger" id="description_short_validate"></div>
                                        </div>

                                        <?php if ($act == 2): ?>
                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Rango de fechas (novedad):</label>
                                                <input type="text" id="novelty_date_range" name="novelty_date_range" class="form-control date_range" value="<?=$novelty_range?>">
                                                <div class="text-danger" id="novelty_date_range_validate"></div>
                                            </div>
                                        <?php endif; ?>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Presentación:</label>
                                            <textarea class="form-control" name="presentation" id="presentation" rows="8"><?=$presentation?></textarea>
                                            <div class="text-danger" id="presentation_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Prescripción:</label>
                                            <textarea class="form-control" name="prescription" id="prescription" rows="8"><?=$prescription?></textarea>
                                            <div class="text-danger" id="prescription_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Recomendaciones de uso:</label>
                                            <textarea class="form-control" name="use_recommendations" id="use_recommendations" rows="8"><?=$use_recommendations?></textarea>
                                            <div class="text-danger" id="use_recommendations_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Resultados y beneficios:</label>
                                            <textarea class="form-control" name="results_benefits" id="results_benefits" rows="8"><?=$results_benefits?></textarea>
                                            <div class="text-danger" id="results_benefits_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h2 class="box-title">Descuento / Promoción</h2>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Nuevo precio:</label>
                                            <input type="text" name="discount_price" id="discount_price" class="form-control only-decimal" value="<?=$discount_price?>">
                                            <div class="text-center" id="discount_price_validate"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Rango de fechas:</label>
                                            <input type="text" id="discount_date_range" name="discount_date_range" class="form-control date_range" value="<?=$discount_range?>">
                                            <div class="text-center" id="discount_price_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h2 class="box-title">Recomendación semanal</h2>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Nuevo precio:</label>
                                            <input type="text" id="weekly_price" name="weekly_price" class="form-control only-decimal" value="<?=$weekly_price?>">
                                            <div class="text-center" id="weekly_price_validate"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Rango de fechas:</label>
                                            <input type="text" id="weekly_date_range" name="weekly_date_range" class="form-control date_range" value="<?=$weekly_range?>">
                                            <div class="text-center" id="weekly_date_range_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h2 class="box-title">SEO</h2>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Título:</label>
                                            <input type="text" class="form-control" name="seo_title" id="seo_title" value="<?=$seo_title?>">
                                            <div class="text-danger" id="seo_title_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Palabras clave:</label> <small>Separado por comas (,)</small>
                                            <textarea class="form-control" name="seo_keywords" id="seo_keywords" rows="8"><?=$seo_keywords?></textarea>
                                            <div class="text-danger" id="seo_keywords_validate"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Descripción:</label>
                                            <textarea class="form-control" name="seo_description" id="seo_description" rows="13"><?=$seo_description?></textarea>
                                            <div class="text-danger" id="seo_description_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h2 class="box-title">Imagen de portada</h2>
                                    </div>

                                    <div class="panel-body">
                                        <div class="col-xs-12 text-center">
                                            <img id="img-cover" class="img-responsive css-marginT20 css-marginB20" src="<?=$cover_image?>" alt="">
                                        </div>
                                        <br>
                                        <hr>
                                        <div class="col-md-12 text-center">
                                            <button type="button" class="btn btn-success btn-flat btn-submit-cover-image" disabled style="width: 100%">Debes agregar una imagen</button>
                                            <div class="clearfix css-espacio10"></div>

                                            <div class="btn btn-default btn-file btn-flat" style="width: 100%">
                                                <i class="fa fa-camera"></i> Subir foto
                                                <input type="file" name="cover_image" id="cover_image" class="cover_image">
                                            </div>
                                            <div class="text-danger" id="cover_image_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="box box-solid">
                                    <div class="box-header with-border">
                                        <h2 class="box-title">Galería de imágenes</h2>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>

                                    <div class="box-body">
                                        <div class="col-md-10 css-noFloat center-block text-center well">
                                            <div class="btn btn-default btn-file css-width100 btn-file-gallery">
                                                <i class="fa fa-camera"></i> <span>Agregar imágenes</span>
                                                <input type="file" name="gallery_images[]" id="gallery_images" class="gallery_images" multiple>
                                            </div>
                                            <p>Las imagenes deben tener mínimo 500 x 250px</p>
                                            <div class="text-danger" id="cover_image_validate"></div>
                                        </div>

                                        <div class="row css-marginT40" id="ajx-gallery"></div>
                                    </div>
                                </div>

                                <?php if ($act == 2): ?>
                                    <div class="box box-solid">
                                        <div class="box-header">
                                            <h2 class="box-title">Imagenes en la galería</h2>
                                        </div>

                                        <div class="box-body" id="js-gallery">
                                            <?php foreach ($arr_gallery as $key => $val): ?>
                                                <div class="col-md-3 css-marginB20 container-thumb">
                                                    <input type="hidden" name="gal_id[]" value="<?=$val['id']?>">
                                                    <div class="css-background-image" style="background-image: url('<?=$val['formed_image']?>')"></div>
                                                    <a href="javascript: removeGalleryImage('<?=$val['id']?>', this.event)" class="btn btn-danger btn-flat center-block btn-remove-gallery" data-id="<?=$val['id']?>"><i class="fas fa-trash-alt"></i> Eliminar</a>
                                                    <a href="#" class="btn btn-info btn-flat center-block btn-show" data-image="<?=$val['formed_image']?>"><i class="fas fa-search"></i> Ver imagen</a>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>

                                    <div class="box box-solid">
                                        <div class="box-header">
                                            <h2 class="box-title">Colores</h2>
                                        </div>

                                        <div class="box-body">
                                            <input type="hidden" name="cnt_color" id="cnt_color" value="<?=$cnt_color?>">

                                            <div id="ajx-colors" class="css-marginT20">
                                                <?php $cnt = 0; foreach ($arr_p_colors as $key => $p_color): $cnt++; ?>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select class="form-control select2" name="color_<?=$cnt?>" id="color_<?=$cnt?>">
                                                                <option value="">Seleccione un color</option>
                                                                <?php foreach ($arr_colors as $key => $color): ?>
                                                                    <option value="<?=$color['id']?>" <?=isSelected($color['id'], $p_color['color_id'])?>><?=$color['name']?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <a href="javascript: removeColor(<?=$p_color['color_id']?>, this.event)" class="btn btn-danger btn-flat pull-right">Eliminar</a>
                                                        </div>
                                                    </div>

                                                    <div class="row css-marginT20">
                                                        <?php foreach ($p_color['gallery'] as $val): ?>
                                                            <div class="col-md-3 css-marginB20 container-thumb">
                                                                <input type="hidden" name="gal_id[]" value="<?=$val['id']?>">
                                                                <div class="css-background-image" style="background-image: url('<?=APP_IMG_PRODUCTS."product_".$id."/gallery/".$val['image']?>')"></div>
                                                                <a href="javascript: removeColorImage('<?=$val['id']?>', this.event)" class="btn btn-danger btn-flat center-block btn-remove-gallery" data-id="<?=$val['id']?>"><i class="fas fa-trash-alt"></i> Eliminar</a>
                                                                <a href="javascript: showImage('<?=APP_IMG_PRODUCTS."product_".$id."/gallery/".$val['image']?>')" class="btn btn-info btn-flat center-block btn-show"><i class="fas fa-search"></i> Ver imagen</a>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>

                                                    <div class="col-md-11 css-noFloat center-block text-center well css-marginT20">
                                                        <div class="btn btn-default btn-file css-width100 btn-file-gallery" id="btn-color-<?=$cnt?>">
                                                            <i class="fa fa-camera"></i> <span>Agregar imágenes</span>
                                                            <input type="file" name="gallery_color_<?=$cnt?>[]" id="gallery_color_<?=$cnt?>" class="gallery_color_<?=$cnt?> css-gallery-image" onChange="changeClass('btn-color-<?=$cnt?>')" multiple>
                                                        </div>
                                                        <p>Las imagenes deben tener mínimo 150 x 150px</p>
                                                    </div>
                                                    <div class="row css-marginT40" id="ajx-gallery-color-<?=$cnt?>"></div>
                                                    <hr>
                                                <?php endforeach; ?>
                                            </div>

                                            <a href="#" class="js-add-color css-fontSize20">Agregar color <i class="fas fa-plus"></i></a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                    </form>
                </section>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

        <div id="key" data-form="<?=$frm?>" data-act="<?=$act?>" data-id="<?=$id?>"></div>

        <!-- Modal para la imagen de Perfil -->
        <div class="modal fade" tabindex="-1" role="dialog" id="mod-img-cover">
            <div class="modal-dialog modal-lg css-width1100">
                <div class="modal-content">
                    <div class="modal-header">
                        Imagen
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12" id="ajx-img-cover"></div>
                        <div class="clearfix css-marginB10"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal para la imagen de galería -->
        <div class="modal fade" tabindex="-1" role="dialog" id="mod-image">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Imagen de galería</h4>
                    </div>
                    <div class="modal-body" id="js-image"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- CKeditor -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/ckeditor/ckeditor.js"></script>
        <!-- date-range-picker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="<?=APP_ASSETS?>plugins/daterangepicker/daterangepicker.js"></script>
        <!-- Select2 -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Checkbox -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-checkbox.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-products.js"></script>
        <script type="text/javascript">
            var colorOption = [
                <?php foreach ($arr_colors as $key => $val): ?>
                    {
                        "id": "<?=$val['id']?>",
                        "color": "<?=$val['name']?>"
                    },
                <?php endforeach; ?>
            ];
        </script>
    </body>
</html>
