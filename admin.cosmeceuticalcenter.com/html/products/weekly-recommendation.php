<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Select2 -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/select2/select2.min.css">
        <!-- Datatables -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.css">
        <!-- daterange picker -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/daterangepicker/daterangepicker.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Productos
                            <small>Recomendaciones semanales</small>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="box box-solid">
                        <div class="box-header">
                            <h2 class="box-title">Productos</h2>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control select2" name="product" id="product">
                                            <option value="">Seleccione un producto</option>
                                            <?php foreach ($arr_products as $val): ?>
                                                <option value="<?=$val['id']?>"><?=$val['name']." - ".$val['price']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <div class="text-danger" id="product_validate"></div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" id="new_price" name="new_price" class="form-control only-decimal" placeholder="Nuevo precio">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <button type="button" class="btn btn-success btn-flat btn-submit">Agregar Producto</button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" id="date_range" name="date_range" class="form-control" placeholder="Rango de fechas">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="js-alert"><?=$flash_message?></div>

                    <div class="box box-solid">
                        <div class="box-header">
                            <h2 class="box-title">Recomendaciones semanales</h2>
                        </div>
                        <div class="box-body">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Ref.</th>
                                        <th>Producto</th>
                                        <th>Precio</th>
                                        <th>Rango de fechas</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($arr_weekly as $key => $val): ?>
                                    <tr>
                                        <td><?=$val['id']?></td>
                                        <td><?=$val['product_name']?></td>
                                        <td><?=$val['new_price']?></td>
                                        <td><?=$val['range']?></td>
                                        <td><?=$val['icon']?></td>
                                        <td>
                                            <a href="admin/products/edit/<?=$val['product_id']?>" class="btn btn-info btn-flat" target="_blank">Editar</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </section>

                <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Eliminar recomendación</h4>
                            </div>
                            <div class="modal-body">
                                <p>Estas seguro de eliminar <b>recomendación</b></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                                <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="SelfdeleteBoostrapAction('', 2, '<?=BASE_URL?>admin/products/weekly/OK3')"><i class="fa fa-trash"></i> Eliminar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->


        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- date-range-picker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="<?=APP_ASSETS?>plugins/daterangepicker/daterangepicker.js"></script>
        <!-- Select2 -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js"></script>
        <!-- DataTables -->
        <script src="<?=APP_ASSETS?>plugins/datatables/jquery.dataTables.js"></script>
        <script src="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript">
            $(function(){
                // Configuración de la paginación con DataTable
                $('.table').DataTable({
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ entradas por página",
                        "zeroRecords": "No se han encontrado resultados",
                        "info": "Mostrando página _PAGE_ de _PAGES_",
                        "infoEmpty": "No se han encontrado resultados",
                        "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
                        "search": "Buscar: ",
                        "paginate": {
                            "previous": "Anterior",
                            "next": "Siguiente"
                        }
                    },
                    "order": [
                        [4,'asc']
                    ]
                });

                // Limpiar el formulario de búsqueda
                $('.btn-reset').on('click', function(e){
                    $('#frm-search select').val('').trigger('change');
                    $('#frm-search input[type=text]').val('');
                });

                // Select2
                $(".select2").select2();

                // DAte Rangue
                $('#date_range').daterangepicker({
                    "locale": {
                        "format": "DD-MM-YYYY",
                        "separator": " al ",
                        "applyLabel": "Aplicar",
                        "cancelLabel": "Cancelar",
                        "fromLabel": "Desde",
                        "toLabel": "hasta",
                        "customRangeLabel": "Custom",
                        "daysOfWeek": [
                            "Do",
                            "Lu",
                            "Ma",
                            "Mi",
                            "Ju",
                            "Vi",
                            "Sa"
                        ],
                        "monthNames": [
                            "Enero",
                            "Febrero",
                            "Marzo",
                            "Abril",
                            "Mayo",
                            "Junio",
                            "Julio",
                            "Agosto",
                            "Septiembre",
                            "Octubre",
                            "Noviembre",
                            "Deciembre"
                        ],
                        "firstDay": 1
                    }
                });

                $(".btn-submit").on('click', function(){
                    var product = $("#product").val();
                    var price = $("#new_price").val();
                    var date_range = $("#date_range").val();

                    $.ajax({
                        type: 'POST',
                        url: base_url + 'ajx/adm/products/8/0',
                        data: {
                            product: product,
                            new_price: price,
                            range: date_range
                        },
                        dataType: 'json',
                        beforeSend: function(){
                            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
                        },
                        success: function(r){
                            if (r.status == 'OK') {
                                $(location).attr("href", base_url + "admin/products/weekly/OK1");
                            } else {
                                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                                $(".btn-submit").attr("disabled", false).html('Guardar');
                                $(document).scrollTop(0);
                            }
                        }
                    });
                });
            });
        </script>
    </body>
</html>
