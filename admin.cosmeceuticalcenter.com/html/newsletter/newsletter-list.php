<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Datatables -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Newsletter
                            <small>Listado de E-mails</small>
                        </h3>

                        <div class="col-md-2 pull-right css-marginT35">
                            <a href="admin/newsletter/add" class="btn btn-info btn-flat pull-right">Nuevo email</a>
                        </div>

                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <div class="box box-solid">
                        <div class="box-header">
                            <h2 class="box-title">Listado de E-mails</h2>
                        </div>
                        <div class="box-body">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Ref.</th>
                                        <th>E-mail</th>
                                        <th>Registrado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($arr_newsletter as $key => $val): ?>
                                    <tr>
                                        <td><?=$val['id']?></td>
                                        <td><?=$val['email']?></td>
                                        <td><?=$val['formed_date']?></td>
                                        <td>
                                            <a href="admin/newsletter/edit/<?=$val['id']?>" class="btn btn-info btn-flat">Editar</a>
                                            <?php if ($this->user_role === "ROLE_ADMIN"): ?>
                                                <button type="button" class="btn btn-danger btn-flat" onclick="SelfdeleteBoostrapAction('admin/newsletter/remove/<?=$val['id']?>', 1)"><i class="far fa-trash-alt"></i></button>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </section>

                <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Eliminar entrada</h4>
                            </div>
                            <div class="modal-body">
                                <p>Estas seguro de eliminar <b>Entrada</b></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                                <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="SelfdeleteBoostrapAction('', 2, '<?=BASE_URL?>admin/newsletter/list/OK3')"><i class="fa fa-trash"></i> Eliminar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->


        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- DataTables -->
        <script src="<?=APP_ASSETS?>plugins/datatables/jquery.dataTables.js"></script>
        <script src="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-newsletter-list.js"></script>
    </body>
</html>
