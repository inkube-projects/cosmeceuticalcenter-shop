<?php foreach ($arr_temp_images as $key => $val): ?>
    <div class="col-md-3 css-marginB20 container-thumb">
        <div class="css-background-image" style="background-image: url('<?=BASE_URL_ADMIN.$path.'min_'.$val['file']?>')"></div>
        <a href="javascript: removeImage('<?=$val['file']?>')" class="btn btn-danger btn-flat center-block"><i class="fas fa-trash-alt"></i> Eliminar</a>
        <a href="#" class="btn btn-info btn-flat center-block btn-show" data-image="<?=BASE_URL_ADMIN.$path.$val['file']?>"><i class="fas fa-search"></i> Ver imagen</a>
    </div>
<?php endforeach; ?>
