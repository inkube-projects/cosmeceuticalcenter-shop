<table class="table table-hover">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Cantidad</th>
            <th>IVA(%)</th>
            <th>Sub total</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($arr as $key => $val): ?>
        <?php if (isset($val['id'])): ?>
            <tr>
                <td><?=$val['product_name']?></td>
                <?php if ($val['new_price']): ?>
                    <td><span style="text-decoration: line-through;"><?=$val['price']?></span> <?=$val['new_price']?></td>
                <?php else: ?>
                    <td><?=$val['price']?></td>
                <?php endif; ?>
                <td><?=$val['quantity']?></td>
                <td><?=$val['formed_iva']?></td>
                <td><?=$val['formed_total']?></td>
                <td>
                    <button type="button" class="btn btn-danger btn-flat" onclick="removeProduct(<?=$val['id']?>)"><i class="fas fa-times"></i></button>
                </td>
            </tr>
        <?php endif; ?>
    <?php endforeach; ?>
    </tbody>
</table>
