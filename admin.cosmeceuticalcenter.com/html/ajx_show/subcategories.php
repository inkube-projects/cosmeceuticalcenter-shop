<label class="css-bold css-text-black">Sub-categoría:</label>
<select class="select2 form-control" name="subcategory[]" id="subcategory" multiple="multiple">
    <option value="">No aplica</option>
    <?php foreach ($arr_subcategories as $val): ?>
        <option value="<?=$val['id']?>" <?=isSelectedMultiple($arr_s_subcat, $val['id'])?>><?=$val['name']?></option>
    <?php endforeach; ?>
</select>
<div class="text-danger" id="subcategory_validate"></div>
