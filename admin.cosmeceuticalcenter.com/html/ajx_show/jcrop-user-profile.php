<link href="<?=APP_ASSETS?>bower_components/Jcrop/css/Jcrop.min.css" type="text/css" rel="stylesheet" />
<script src="<?=APP_ASSETS?>bower_components/Jcrop/js/Jcrop.min.js"></script>
<script type="text/javascript">
$(function(){
    $('#img-cut').Jcrop({
        setSelect: [ 0, 0, 0, 0],
        maxSize: [<?=$width?>, <?=$height?>],
        minSize: [200, 200],
        onSelect: updateCoords
    });
});
function updateCoords(c){
    $('#x').val(c.x);
    $('#y').val(c.y);
    if($('#w').val() == 0){ $('#w').val(<?=$width?>);}else{ $('#w').val(c.w);}
    if($('#h').val() == 0){ $('#h').val(<?=$height?>);}else{ $('#h').val(c.h);}
}
function recortarImagen(){
    var i = $("#frm-img").serialize();
    $.ajax({
        type: 'POST',
        url: base_admin + 'ajax.php?m=users&act=4',
        dataType: 'json',
        data: i,
        success: function(r){
            $('#mod-img-cover').modal('hide');
            if (r.status === "OK") {
                $("#hid-name").val("<?=$this->admin_code."_".$img_name?>");
                $("#img-cover").attr("src", "<?=BASE_URL_ADMIN.$directory_post?>" + r.data);
            }
        }
    });
    return false;
}
</script>
<img src="<?=BASE_URL_ADMIN.$directory.$img_name?>" id="img-cut" class="img-responsive">
<form method="post" name="frm-img" id="frm-img" onSubmit="return recortarImagen();">
    <input type="hidden" id="x" name="x" value="0"/>
    <input type="hidden" id="y" name="y" value="0"/>
    <input type="hidden" id="w" name="w" value="0"/>
    <input type="hidden" id="h" name="h" value="0"/>
    <div class="clearfix css-espacio10"></div>
    <button type="submit" class="btn btn-default pull-right">Cortar Imagen <i class="fa fa-scissors"></i></button>
</form>
