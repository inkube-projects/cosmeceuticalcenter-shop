<div class="js-alert-stock"></div>

<form name="frm-stock-extra" id="frm-stock-extra" method="post" action="">
    <input type="hidden" name="product_id" id="product_id" value="<?=$product_id?>">

    <?php if (count($arr_product_colors) > 0): ?>
        <h4>Colores:</h4>

        <?php foreach ($arr_product_colors as $key => $val): ?>
            <div class="col-md-6">
                <div class="form-group">
                    <label><?=$val['name']?></label>
                    <input type="text" class="form-control only-number" name="color_<?=$val['color_id']?>" id="color_<?=$val['color_id']?>" value="<?=$val['stock']?>">
                </div>
            </div>
        <?php endforeach; ?>

        <div class="clearfix css-marginB10"></div>
        <hr>
    <?php endif; ?>

    <?php if (count($arr_product_size) > 0): ?>
        <h4>Tallas:</h4>

        <?php foreach ($arr_product_size as $key => $val): ?>
            <div class="col-md-6">
                <div class="form-group">
                    <label><?=$val['name']?></label>
                    <input type="text" class="form-control only-number" name="size_<?=$val['size_id']?>" id="size_<?=$val['size_id']?>" value="<?=$val['stock']?>">
                </div>
            </div>
        <?php endforeach; ?>

        <div class="clearfix css-marginB10"></div>
        <hr>
    <?php endif; ?>

    <button type="button" class="btn btn-success btn-flat pull-right btn-submit" onclick="persistExtraStock()">Guardar stock</button>
    <div class="clearfix css-marginB10"></div>
</form>