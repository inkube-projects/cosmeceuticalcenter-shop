<?php foreach ($arr_gallery as $key => $val): ?>
    <div class="col-md-3 css-marginB20 container-thumb">
        <div class="css-background-image" style="background-image: url('<?=$path.$val['image']?>')"></div>
        <a href="javascript: removeGalleryImage('<?=$val['id']?>', this.event)" class="btn btn-danger btn-flat center-block btn-remove-gallery" data-id="<?=$val['id']?>"><i class="fas fa-trash-alt"></i> Eliminar</a>
        <a href="#" class="btn btn-info btn-flat center-block btn-show" data-image="<?=$path.$val['image']?>"><i class="fas fa-search"></i> Ver imagen</a>
    </div>
<?php endforeach; ?>
