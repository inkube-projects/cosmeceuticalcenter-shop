<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Select 2 -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/select2/select2.min.css">
        <?php include('html/overall/header.php'); ?>

        <style media="screen">
            form input[type="checkbox"] { display: block; }
        </style>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Zonas
                            <?php if ($act == 1): ?>
                                <small>Agregar zona</small>
                            <?php else: ?>
                                <small>Editar zona</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>"  method="post" action="">
                        <div class="box box-solid">
                            <div class="box-header">
                                <?php if ($act == 1): ?>
                                    <h2 class="box-title">Agregar zona</h2>
                                <?php else: ?>
                                    <h2 class="box-title">Editar zona</h2>
                                <?php endif; ?>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Nombre:</label>
                                            <input type="text" class="form-control" name="name" id="name" value="<?=$name?>">
                                            <div class="text-danger" id="name_validate"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Valor de envío:</label>
                                            <input type="text" class="form-control only-decimal" name="value" id="value" value="<?=$value?>">
                                            <div class="text-danger" id="value_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box box-solid">
                            <div class="box-header">
                                <h2 class="box-title">Agregar provincias a la zona</h2>
                            </div>

                            <div class="box-body">
                                <div class="row css-marginB20">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">País</label>
                                            <select class="form-control select2" name="country[]" id="country" multiple>
                                                <option value="">Seleccione un país</option>
                                                <?php foreach ($arr_country as $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelectedMultiple($arr_s_country, $val['id'])?>><?=$val['country']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Provincia</label>
                                            <select class="form-control select2" name="province" id="province" <?=$p_disabled?>>
                                                <option value="">Seleccione una provincia</option>
                                                <?php foreach ($arr_province as $val): ?>
                                                    <option value="<?=$val['id']?>"><?=$val['name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="well">
                                    <div class="js-province-container">
                                        <?php foreach ($arr_zones_province as $val): ?>
                                            <div class="pull-left css-marginR10 css-marginB10">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" id="prov_<?=$val['province_id']?>" name="prov[]" value="<?=$val['province_id']?>" checked>
                                                    </span>
                                                    <input type="text" class="form-control" value="<?=$db->getValue('name', 'province', "id='".$val['province_id']."'")?>" disabled>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>

                                    <div class="clearfix css-marginB10"></div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                    </form>
                </section>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

        <div id="key" data-form="<?=$frm?>" data-act="<?=$act?>" data-id="<?=$id?>"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- Select2 -->
        <script src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-zones.js"></script>
    </body>
</html>
