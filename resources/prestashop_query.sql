SELECT
	p.`id_product`,
	p.`id_manufacturer`,
	p.`id_category_default`,
	p.`price`,
	p.`active`,
	pl.`name`,
	pl.`description`,
	pl.`link_rewrite`,
	pc.name AS category_name,
	pc.id_lang AS category_lang
FROM
	ps_product p
INNER JOIN ps_product_lang pl ON p.id_product = pl.id_product
INNER JOIN ps_category_lang pc ON p.id_category_default = pc.id_category AND pc.id_lang='3'
WHERE
	pl.`name`='Aventus for Him'
GROUP BY
	p.id_product;
