-- ----------------------------
-- View structure for view_user
-- ----------------------------
DROP VIEW IF EXISTS view_user;
CREATE VIEW view_user AS
SELECT
	u.id,
	u.username,
	u.email,
	u.`password`,
	u.status_id,
	u.username_canonical,
	u.email_canonical,
	u.profile_image,
	u.`code`,
	u.last_login,
	u.password_requested_at,
	u.account_type,
	u.create_at,
	u.update_at,
	ui.`name`,
	ui.last_name,
	ui.birthdate,
	ui.company,
	ui.province_id,
	ui.city,
	ui.country_id,
	ui.direction,
	ui.direction_2,
	ui.phone_1,
	ui.phone_2,
	ui.dni,
	ui.note,
	ui.postal_code,
	r.id AS role_id,
	r.role,
	r.role_name
FROM
	user u
INNER JOIN user_information ui ON ui.user_id = u.id
INNER JOIN user_status us ON u.status_id = us.id
INNER JOIN role r ON u.role = r.id;

-- ----------------------------
-- View structure for view_products
-- ----------------------------
DROP VIEW IF EXISTS view_products;
CREATE VIEW view_products AS
SELECT
	p.id,
	p.`name` AS product_name,
	p.presentation,
	p.description AS product_description,
	p.description_short,
	p.active_principles,
	p.prescription,
	p.use_recommendations,
	p.results_benefits,
	p.price,
	p.price_iva,
	p.iva,
	p.content,
	p.cover_image,
	p.tags,
	p.status,
	p.category_id,
	p.brand_id,
	p.create_at AS product_create_at,
	p.update_at AS product_update_at,
	pc.`name` AS category_name,
	pc.`name_canonical` AS category_name_canonical,
	pc.description AS category_description,
	b.`name` AS brand_name,
	b.`name_canonical` AS brand_name_canonical,
	b.description AS brand_description,
	b.image,
	b.status AS brand_status,
	b.create_at AS brand_create_at,
	b.update_at AS brand_update_at
FROM
	products p
LEFT JOIN products_category pc ON p.category_id = pc.id
LEFT JOIN brands b ON p.brand_id = b.id;

-- ----------------------------
-- View structure for view_inventory
-- ----------------------------
DROP VIEW IF EXISTS view_inventory;
CREATE VIEW view_inventory AS
SELECT
	p.`name` AS product_name,
	p.presentation,
	p.description,
	p.description_short,
	p.active_principles,
	p.prescription,
	p.use_recommendations,
	p.results_benefits,
	p.price,
	p.price_iva,
	p.iva,
	p.content,
	p.cover_image,
	p.status,
	p.category_id,
	p.brand_id,
	p.create_at,
	p.update_at,
	i.id AS inventory_id,
	i.product_id,
	i.quantity,
	i.unity_id,
	iu.`name` AS unit_name
FROM
	products p
INNER JOIN inventory i ON i.product_id = p.id
INNER JOIN inventory_unit iu ON i.unity_id = iu.id;

-- ----------------------------
-- View structure for view_weekly
-- ----------------------------
DROP VIEW IF EXISTS view_weekly;
CREATE VIEW view_weekly AS
SELECT
	wr.id,
	wr.product_id,
	wr.new_price,
	wr.date_init,
	wr.date_end,
	wr.create_at,
	p.`name` AS product_name,
	p.presentation,
	p.description AS product_description,
	p.description_short,
	p.active_principles,
	p.prescription,
	p.use_recommendations,
	p.results_benefits,
	p.price,
	p.price_iva,
	p.iva,
	p.content,
	p.cover_image,
	p.status,
	p.category_id,
	p.brand_id,
	p.create_at AS product_create_at,
	p.update_at AS product_update_at,
	b.`name` AS brand_name,
	b.`name_canonical` AS brand_name_canonical,
	b.description AS brand_description,
	b.image,
	b.status AS brand_status,
	b.create_at AS brand_create_at,
	b.update_at AS brand_update_at,
	pc.`name` AS category_name,
	pc.`name_canonical` AS category_name_canonical,
	pc.description AS category_description
FROM
	weekly_recommendation wr
LEFT JOIN products p ON wr.product_id = p.id
LEFT JOIN brands b ON p.brand_id = b.id
LEFT JOIN products_category pc ON p.category_id = pc.id;

-- ----------------------------
-- View structure for view_novelty
-- ----------------------------
DROP VIEW IF EXISTS view_novelty;
CREATE VIEW view_novelty AS
SELECT
	n.id,
	n.product_id,
	n.date_init,
	n.date_end,
	n.create_at,
	p.`name` AS product_name,
	p.presentation,
	p.description AS product_description,
	p.description_short,
	p.active_principles,
	p.prescription,
	p.use_recommendations,
	p.results_benefits,
	p.price,
	p.price_iva,
	p.iva,
	p.content,
	p.cover_image,
	p.status,
	p.category_id,
	p.brand_id,
	p.create_at AS product_create_at,
	p.update_at AS product_update_at,
	b.`name` AS brand_name,
	b.`name_canonical` AS brand_name_canonical,
	b.description AS brand_description,
	b.image,
	b.status AS brand_status,
	b.create_at AS brand_create_at,
	b.update_at AS brand_update_at,
	pc.`name` AS category_name,
	pc.`name_canonical` AS category_name_canonical,
	pc.description AS category_description
FROM
	novelty n
LEFT JOIN products p ON n.product_id = p.id
LEFT JOIN brands b ON p.brand_id = b.id
LEFT JOIN products_category pc ON p.category_id = pc.id;


-- ----------------------------
-- View structure for view_discount
-- ----------------------------
DROP VIEW IF EXISTS view_discount;
CREATE VIEW view_discount AS
SELECT
	d.id,
	d.product_id,
	d.new_price,
	d.date_init,
	d.date_end,
	d.create_at,
	p.`name` AS product_name,
	p.presentation,
	p.description AS product_description,
	p.description_short,
	p.active_principles,
	p.prescription,
	p.use_recommendations,
	p.results_benefits,
	p.price,
	p.price_iva,
	p.iva,
	p.content,
	p.cover_image,
	p.status,
	p.category_id,
	p.brand_id,
	p.create_at AS product_create_at,
	p.update_at AS product_update_at,
	b.`name` AS brand_name,
	b.`name_canonical` AS brand_name_canonical,
	b.description AS brand_description,
	b.image,
	b.status AS brand_status,
	b.create_at AS brand_create_at,
	b.update_at AS brand_update_at,
	pc.`name` AS category_name,
	pc.`name_canonical` AS category_name_canonical,
	pc.description AS category_description
FROM
	discount d
LEFT JOIN products p ON d.product_id = p.id
LEFT JOIN brands b ON p.brand_id = b.id
LEFT JOIN products_category pc ON p.category_id = pc.id;

-- ----------------------------
-- View structure for view_cart
-- ----------------------------
DROP VIEW IF EXISTS view_cart;
CREATE VIEW view_cart AS
SELECT
	cd.id AS cart_id,
	cd.user_id,
	cd.product_id,
	cd.quantity,
	cd.size_id,
	cd.color_id,
	cd.create_at AS cart_create_at,
	cd.update_at AS cart_update_at,
	p.`name`,
	p.presentation,
	p.description,
	p.description_short,
	p.active_principles,
	p.prescription,
	p.use_recommendations,
	p.results_benefits,
	p.price,
	p.price_iva,
	p.iva,
	p.content,
	p.cover_image,
	p.status,
	p.category_id,
	p.brand_id,
	p.create_at AS product_create_at,
	p.update_at AS product_update_at
FROM
	cart_detail cd
INNER JOIN products p ON cd.product_id = p.id;

-- ----------------------------
-- View structure for view_orders
-- ----------------------------
DROP VIEW IF EXISTS view_orders;
CREATE VIEW view_orders AS
SELECT
	o.id,
	o.`code`,
	o.sub_total,
	o.iva,
	o.iva_percentage,
	o.shipping,
	o.total,
	o.status_id,
	o.user_id,
	o.method_payment_id,
	o.province_id,
	o.present,
	o.present_note,
	o.coupon_code,
	o.coupon_id,
	o.discount_percent,
	o.discount,
	o.create_at,
	o.update_at,
	os.`name` AS status_name,
	mp.method,
	mp.icon
FROM
	orders o
INNER JOIN order_status os ON o.status_id = os.id
LEFT JOIN method_payment mp ON o.method_payment_id = mp.id;

-- ----------------------------
-- View structure for view_orders_detail
-- ----------------------------
DROP VIEW IF EXISTS view_orders_detail;
CREATE VIEW view_orders_detail AS
SELECT
	od.id,
	od.product_id,
	od.order_id,
	od.quantity,
	od.price AS order_price,
	od.new_price AS order_new_price,
	od.product_iva,
	od.iva,
	od.sub_total,
	od.user_id,
	od.color_id,
	od.size_id,
	od.create_at AS order_create_at,
	od.update_at AS order_update_at,
	p.`name` AS product_name,
	p.presentation,
	p.description,
	p.description_short,
	p.active_principles,
	p.prescription,
	p.use_recommendations,
	p.results_benefits,
	p.price AS product_price,
	p.price_iva AS product_price_iva,
	p.content,
	p.cover_image,
	p.status,
	p.category_id,
	p.brand_id,
	p.create_at AS product_create_at,
	p.update_at AS product_update_at,
	b.`name` AS brand_name,
	b.`name_canonical` AS brand_name_canonical,
	b.description AS brand_description,
	b.image AS brand_image,
	pc.`name` AS category_name,
	pc.`name_canonical` AS category_name_canonical,
	pc.description AS category_description
FROM
	order_detail od
LEFT JOIN products p ON od.product_id = p.id
LEFT JOIN brands b ON p.brand_id = b.id
LEFT JOIN products_category pc ON p.category_id = pc.id;

-- ----------------------------
-- View structure for view_favorites
-- ----------------------------
DROP VIEW IF EXISTS view_favorites;
CREATE VIEW view_favorites AS
SELECT
	f.id,
	f.product_id,
	f.user_id,
	p.`name` AS product_name,
	p.presentation,
	p.description AS product_description,
	p.description_short,
	p.active_principles,
	p.prescription,
	p.use_recommendations,
	p.results_benefits,
	p.price,
	p.price_iva,
	p.content,
	p.cover_image,
	p.status,
	p.category_id,
	p.brand_id,
	p.create_at AS product_create_at,
	p.update_at AS product_update_at,
	b.`name` AS brand_name,
	b.`name_canonical` AS brand_name_canonical,
	b.description AS brand_description,
	b.image,
	b.create_at AS brand_create_at,
	b.update_at AS brand_update_at,
	pc.`name` AS category_name,
	pc.`name_canonical` AS category_name_canonical,
	pc.description AS category_description
FROM
	favorites f
LEFT JOIN products p ON f.product_id = p.id
LEFT JOIN brands b ON p.brand_id = b.id
LEFT JOIN products_category pc ON p.category_id = pc.id;

-- ----------------------------
-- View structure for view_seo
-- ----------------------------
DROP VIEW IF EXISTS view_seo;
CREATE VIEW view_seo AS
SELECT
	s.id,
	s.keywords,
	s.description,
	s.section_id,
	ss.`view`
FROM
	seo s
INNER JOIN seo_section ss ON s.section_id = ss.id;

-- ----------------------------
-- View structure for view_zones
-- ----------------------------
DROP VIEW IF EXISTS view_zones;
CREATE VIEW view_zones AS
SELECT
	zp.province_id,
	zp.zone_id,
	z.`name`,
	z.`value`,
	z.create_at,
	z.update_at
FROM
	zone_province zp
INNER JOIN zones z ON zp.zone_id = z.id;

-- ----------------------------
-- View structure for view_zones_country
-- ----------------------------
DROP VIEW IF EXISTS view_zones_country;
CREATE VIEW view_zones_country AS
SELECT
	zc.country_id,
	zc.zone_id,
	z.`name`,
	z.`value`,
	z.create_at,
	z.update_at
FROM
	zone_country zc
INNER JOIN zones z ON zc.zone_id = z.id;

-- ----------------------------
-- View structure for view_products_categories
-- ----------------------------
DROP VIEW IF EXISTS view_products_categories;
CREATE VIEW view_products_categories AS
SELECT
	pcs.id,
	pcs.product_id,
	pcs.category_id,
	p.`name` AS product_name,
	pc.`name` AS category_name,
	pc.`name_canonical` AS category_name_canonical,
	pc.description AS category_description
FROM
	products_categories pcs
LEFT JOIN products p ON pcs.product_id = p.id
LEFT JOIN products_category pc ON pcs.category_id = pc.id
AND p.category_id = pc.id;

-- ----------------------------
-- View structure for view_features
-- ----------------------------
DROP VIEW IF EXISTS view_features;
CREATE VIEW view_features AS
SELECT
	f.id,
	f.user_id,
	f.product_id,
	f.order_id,
	f.review,
	f.score,
	f.`status`,
	f.create_at AS feature_create,
	f.update_at AS feature_update,
	u.username,
	u.email,
	ui.`name` AS user_name,
	ui.last_name,
	o.`code` AS order_code,
	p.`name` AS product_name
FROM
	features f
INNER JOIN `user` u ON f.user_id = u.id
INNER JOIN user_information ui ON ui.user_id = u.id
INNER JOIN orders o ON o.user_id = u.id
AND f.order_id = o.id
INNER JOIN products p ON f.product_id = p.id;

-- ----------------------------
-- View structure for view_coupon_history
-- ----------------------------
DROP VIEW IF EXISTS view_coupon_history;
CREATE VIEW view_coupon_history AS
SELECT
	ch.id,
	ch.user_id,
	ch.coupon_id,
	ch.create_at,
	c.`name`,
	c.canonical_id,
	c.description,
	c.discount,
	c.uses,
	c.init_date,
	c.end_date,
	c.create_at AS coupon_create,
	c.update_at AS coupon_update
FROM
	coupon_user_history ch
INNER JOIN coupon c ON ch.coupon_id = c.id;

-- ----------------------------
-- View structure for view_category
-- ----------------------------
DROP VIEW IF EXISTS view_category;
CREATE VIEW view_category AS
SELECT
	pc.id,
	pc.product_id,
	pc.category_id,
	c.`name`,
	c.`name_canonical` AS category_name_canonical,
	c.description,
	c.image
FROM
	products_categories pc
INNER JOIN products_category c ON pc.category_id = c.id;

-- ----------------------------
-- View structure for view_colors
-- ----------------------------
DROP VIEW IF EXISTS view_colors;
CREATE VIEW view_colors AS
SELECT
	pc.id AS products_colors_id,
	pc.product_id,
	pc.color_id,
	pc.stock,
	c.`name`,
	c.color
FROM
	products_colors pc
INNER JOIN colors c ON pc.color_id = c.id;

-- ----------------------------
-- View structure for view_size
-- ----------------------------
DROP VIEW IF EXISTS view_size;
CREATE VIEW view_size AS
SELECT
	ps.id AS products_sizes_id,
	ps.product_id,
	ps.size_id,
	ps.stock,
	s.`name`
FROM
	products_sizes ps
INNER JOIN size s ON ps.size_id = s.id;