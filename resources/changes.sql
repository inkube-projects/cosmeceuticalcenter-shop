SET FOREIGN_KEY_CHECKS=0;

SET AUTOCOMMIT = 0;
START TRANSACTION;

CREATE TABLE `unprocess_orders` ( `id` INT(11) NOT NULL AUTO_INCREMENT ,  `sub_total` VARCHAR(20) NOT NULL ,  `shipping` VARCHAR(20) NOT NULL ,  `coupon_code` VARCHAR(180) NULL ,  `coupon_id` INT(11) NULL ,  `discount_percent` VARCHAR(20) NULL ,  `discount` VARCHAR(20) NULL ,  `total` VARCHAR(20) NOT NULL ,  `user_id` INT(11) NOT NULL ,  `method_payment_id` INT(11) NULL ,  `country_id` INT(11) NOT NULL ,  `province_id` INT(11) NOT NULL ,  `note` TEXT NULL ,  `present` INT(2) NULL ,  `present_note` TEXT NULL ,  `create_at` DATETIME NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;
CREATE TABLE `unprocess_orders_detail` ( `id` INT(11) NOT NULL AUTO_INCREMENT ,  `product_id` INT(11) NOT NULL ,  `unprocess_order_id` INT(11) NOT NULL ,  `quantity` INT(11) NOT NULL ,  `price` VARCHAR(20) NOT NULL ,  `new_price` VARCHAR(20) NULL ,  `product_iva` VARCHAR(20) NOT NULL ,  `iva` VARCHAR(20) NOT NULL ,  `sub_total` VARCHAR(20) NOT NULL ,  `user_id` INT(11) NOT NULL ,  `size_id` INT(11) NULL ,  `color_id` INT(11) NULL ,  `origin_offer` VARCHAR(200) NULL ,  `create_at` DATETIME NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;
ALTER TABLE `unprocess_orders` ADD FOREIGN KEY (`country_id`) REFERENCES `country`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE; ALTER TABLE `unprocess_orders` ADD FOREIGN KEY (`coupon_id`) REFERENCES `coupon`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE; ALTER TABLE `unprocess_orders` ADD FOREIGN KEY (`method_payment_id`) REFERENCES `method_payment`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE; ALTER TABLE `unprocess_orders` ADD FOREIGN KEY (`province_id`) REFERENCES `province`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE; ALTER TABLE `unprocess_orders` ADD FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE `unprocess_orders_detail` ADD FOREIGN KEY (`color_id`) REFERENCES `colors`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE; ALTER TABLE `unprocess_orders_detail` ADD FOREIGN KEY (`product_id`) REFERENCES `products`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE; ALTER TABLE `unprocess_orders_detail` ADD FOREIGN KEY (`size_id`) REFERENCES `size`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE; ALTER TABLE `unprocess_orders_detail` ADD FOREIGN KEY (`unprocess_order_id`) REFERENCES `orders`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE; ALTER TABLE `unprocess_orders_detail` ADD FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE `orders` ADD `shipping_address_id` INT(11) NULL AFTER `province_id`;
ALTER TABLE `orders` ADD INDEX(`shipping_address_id`);
ALTER TABLE `orders` ADD FOREIGN KEY (`shipping_address_id`) REFERENCES `shipping_address`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

COMMIT;
SET FOREIGN_KEY_CHECKS=1;
