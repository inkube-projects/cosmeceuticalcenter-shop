<header class="header_area header_three">
    <div class="header_top">
        <div class="container">
            <div class="row align-items-center">
                <div class="d-none d-sm-none d-md-block col-lg-5 col-md-5">
                    <div class="social_icone text-left">
                        <ul>
                            <li><a href="https://www.facebook.com/CosmeceuticalCenter" target="_blank"><i class="ion-social-facebook"></i></a></li>
                            <li><a href="https://twitter.com/CosmeceuticalC" target="_blank"><i class="ion-social-twitter"></i></a></li>
                            <li><a href="https://www.pinterest.es/cosmeceuticalc/" target="_blank"><i class="ion-social-pinterest"></i></a></li>
                            <li><a href="https://www.instagram.com/cosmeceuticalcenter/?hl=es" target="_blank"><i class="ion-social-instagram"></i></a></li>
                            <li class="css-tel"><i class="fa fa-phone css-fonsize16" aria-hidden="true"></i> <span class="css-fonsize12">Pedidos - <a href="tel:+34954378643">954 378 643</a> - <a href="teñ:+34673375357">673 375 357</a></span> </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-7 col-md-7">
                    <div class="middel_right">
                        <div class="d-none d-sm-none d-md-block box_setting">
                            <div class="search_btn">
                                <form name="frm-search" id="frm-search" method="get" action="">
                                    <input type="text" name="src_search" id="src_search" placeholder="¿Qué buscas...?">
                                    <button type="submit" class="btn-search"><i class="ion-ios-search-strong"></i></button>
                                </form>
                            </div>
                        </div>

                        <div class="d-block d-sm-block d-md-none search_btn">
                            <form name="frm-search-mobile" id="frm-search-mobile" method="get" action="">
                                <input type="text" name="src_search_mobile" id="src_search_mobile" placeholder="¿Qué buscas...?">
                                <button type="submit" class="btn-search"><i class="ion-ios-search-strong"></i></button>
                            </form>
                        </div>

                        <div class="cart_link">
                            <a href=" " class="js-icon-cart"><i class="ion-android-cart"></i><?=$this->cart['sub_total']?> €<i class="fa fa-angle-down"></i></a>
                            <span class="cart_quantity"><?=$this->cart['total_products']?></span>

                            <div class="mini_cart">
                                <div class="mini_cart_inner">
                                    <?php foreach ($this->cart['cart'] as $key => $val): ?>
                                        <div class="cart_item">
                                            <div class="cart_img">
                                                <a href="PRODUCTO/<?=$val['brand_name']?>/<?=friendlyURL($val['name'], 'upper')?>/<?=$val['product_id']?>">
                                                    <img src="<?=$val['formed_image']?>" alt="<?=$val['name']?>">
                                                </a>
                                            </div>

                                            <div class="cart_info">
                                                <a href="PRODUCTO/<?=$val['brand_name']?>/<?=friendlyURL($val['name'], 'upper')?>/<?=$val['product_id']?>">
                                                    <?=$val['name']?> <br>
                                                    <?php if (@$val['size']): echo "<b>Talla</b>: ".@$val['size']."<br>"; endif; ?>
                                                    <?php if (@$val['color']): echo "<b>Color</b>: ".@$val['color']; endif; ?>
                                                </a>

                                                <span class="quantity">Cant: <?=$val['quantity']?> x <?=$val['formed_price']?> €</span>
                                                <?php if ($val['calculated_iva'] == 0): ?>
                                                    <span>PROMOCIÓN</span>
                                                <?php else: ?>
                                                    <span>IVA (<?=$val['product_iva']?>%): <?=$val['calculated_iva']?> €</span>
                                                <?php endif; ?>
                                                <span class="price_cart"><?=$val['sub_total']?> €</span>
                                            </div>

                                            <div class="cart_remove">
                                                <a href="#" class="js-cart-remove" data-value="<?=$val['product_id']?>" data-color="<?=@$val['color_id']?>" data-size="<?=@$val['size_id']?>"><i class="ion-android-close"></i></a>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>

                                    <div class="cart_total">
                                        <span>Subtotal:</span>
                                        <span><?=$this->cart['sub_total']?> €</span>
                                    </div>
                                </div>
                                <div class="mini_cart_footer">
                                    <div class="cart_button view_cart">
                                        <a href="CARRITO">Ver cesta</a>
                                    </div>
                                </div>
                            </div>

                            <!--SOLO VISIBLE EN MÓVIL-->
                            <div class="main_menu d-block d-sm-block d-md-none floatRightDisplay">
                                <nav>
                                    <ul>
                                        <li class="position-relative">
                                            <a href="#" class="no-click css-relative">
                                                <i class="fa fa-user css-fonsize20">
                                                    <i class="fa fa-angle-down"></i>
                                                </i>
                                                <?php if (isset($_SESSION['USER_SESSION_COSME']['id'])): ?>
                                                    <div class="css-notifications"><?=$this->notifications?></div>
                                                <?php endif; ?>
                                            </a>

                                            <ul class="sub_menu css-left-78">
                                                <?php if (isset($_SESSION['USER_SESSION_COSME']['id'])): ?>
                                                    <li><a href="DASHBOARD">Mi cuenta</a></li>
                                                    <li><a href="NOTIFICACIONES">Notificaciones <label class="css-notifications"><?=$this->notifications?></label></a></li>
                                                    <li><a href="LOGOUT">Cerrar Sesión</a></li>
                                                <?php else: ?>
                                                    <li><a href="REGISTRO">Registro</a></li>
                                                    <li><a href="LOGIN">Iniciar Sesión</a></li>
                                                <?php endif; ?>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <!--END USSR VISIBLE MOVIL-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="header_middel">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 col-md-4"></div>
                <div class="col-lg-4 col-md-4">
                    <div class="logo text-center">
                        <a href="HOME"><img src="<?=APP_LOGO?>logo.png" alt=""></a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4"></div>
            </div>
        </div>
    </div>

    <div class="header_bottom sticky-header">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="main_menu_inner">
                        <div class="logo_sticky">
                            <a href="HOME"><img src="<?=APP_LOGO?>logo.jpg" alt=""></a>
                        </div>
                        <div class="main_menu d-none d-lg-block">
                            <nav>
                                <ul>
                                    <li><a href="HOME"><i class="fa fa-home css-fonsize16  css-padding-left20"></i></a></li>
                                    <li><a href="#" class="no-click">CATEGORÍAS <i class="fa fa-angle-down"></i></a>
                                        <ul class="mega_menu">
                                            <li>
                                                <div class="ocultar1100" style="width: 300px;">
                                                    <?php if (isset($cat_info)): ?>
                                                        <div class="css-categoryimage-menu" style="background-image: url('<?=$cat_info['formed_image']?>')"></div>
                                                    <?php else: ?>
                                                        <img src="<?=APP_IMG?>banners/imgMenuCategoria.jpg" width="300" height="170" alt=""/>
                                                    <?php endif; ?>
                                                </div>
                                            </li>
                                            <li class="boder-right1" style="margin-right: -15px;">
                                                <ul>
                                                    <li><a href="PRODUCTOS/NOVEDADES" class="css-bold-lowercase">Novedades <i class="fa fa-angle-right"></i></a></li>
                                                    <li><a href="PRODUCTOS/DESCUENTOS" class="css-bold-lowercase">Con descuento <i class="fa fa-angle-right"></i></a></li>
                                                </ul>
                                            </li>
                                            <?php foreach ($this->formatedArray($this->categories, ceil(count($this->brands) / 4)) as $key => $val): ?>
                                                <li class="boder-right1">
                                                    <ul>
                                                        <?php for ($i = 0; $i < count($val); $i++): ?>
                                                            <!-- <li><a href="PRODUCTOS/CATEGORIA/<?=$val[$i]['id']?>/<?=friendlyURL($val[$i]['name'], 'upper')?>"><?=$val[$i]['name']?></a></li> -->
                                                            <li><a href="CATEGORIA/<?=$val[$i]['name_canonical']?>"><?=$val[$i]['name']?></a></li>
                                                        <?php endfor; ?>
                                                    </ul>
                                                </li>
                                            <?php endforeach; ?>

                                        </ul>
                                    </li>

                                    <li><a href="#" class="no-click">MARCAS <i class="fa fa-angle-down"></i></a>
                                        <ul class="mega_menu">
                                            <li>
                                                <div class="ocultar1100" style="width: 300px;">
                                                    <img src="<?=APP_IMG_BANNERS?>imgMenuMarca.jpg" width="300" height="170" alt=""/>
                                                </div>
                                            </li>
                                            <li class="boder-right1" style="margin-right: -15px;">
                                                <ul>
                                                    <li><a href="PRODUCTOS/NOVEDADES" class="css-bold-lowercase">Novedades <i class="fa fa-angle-right"></i></a></li>
                                                    <li><a href="PRODUCTOS/DESCUENTOS" class="css-bold-lowercase">Con descuento <i class="fa fa-angle-right"></i></a></li>
                                                </ul>
                                            </li>
                                            <?php foreach ($this->formatedArray($this->brands, ceil(count($this->brands) / 4)) as $key => $val): ?>
                                                <li class="boder-right1">
                                                    <ul>
                                                        <?php for ($i = 0; $i < count($val); $i++): ?>
                                                            <li><a href="MARCA/<?=$val[$i]['name_canonical']?>"><?=$val[$i]['name']?></a></li>
                                                        <?php endfor; ?>
                                                    </ul>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>

                                    <li><a href="http://www.cosmeceuticalcenter.com/NEW-CONTACTO-COSMECEUTICALCENTER">Contacto</a></li>
                                    <li class="position-relative"><a href="#" class="no-click">Servicios <i class="fa fa-angle-down"></i></a>
                                        <ul class="sub_menu css-left-78">
                                            <li><a href="https://www.cosmeceuticalcenter.com/NEW-TRATAMIENTOS">Tratamientos en clínica</a></li>
                                            <li><a href="https://www.cosmeceuticalcenter.com/ASESORIA-COSMECEUTICA">Asesoria Cosmecéutica</a></li>
                                        </ul>
                                    </li>

                                    <li class="position-relative">
                                        <a href="#" class="no-click css-relative">
                                            <i class="fa fa-user css-fonsize16">
                                                <i class="fa fa-angle-down"></i>
                                            </i>
                                            <?php if (isset($_SESSION['USER_SESSION_COSME']['id'])): ?>
                                                <div class="css-notifications"><?=$this->notifications?></div>
                                            <?php endif; ?>
                                        </a>

                                        <ul class="sub_menu css-left-78">
                                            <?php if (isset($_SESSION['USER_SESSION_COSME']['id'])): ?>
                                                <li><a href="DASHBOARD">Mi cuenta</a></li>
                                                <li><a href="NOTIFICACIONES">Notificaciones <label class="css-notifications"><?=$this->notifications?></label></a></li>
                                                <li><a href="LOGOUT">Cerrar Sesión</a></li>
                                            <?php else: ?>
                                                <li><a href="REGISTRO">Registro</a></li>
                                                <li><a href="LOGIN">Iniciar Sesión</a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <div class="mobile-menu d-lg-none">
                            <nav>
                                <ul>
                                    <li><a href="#" class="no-click">MARCAS</a>
                                        <ul class="sub_menu">
                                            <?php foreach ($this->formatedArray($this->brands, ceil(count($this->brands) / 4)) as $key => $val): ?>
                                                <?php for ($i = 0; $i < count($val); $i++): ?>
                                                    <li><a href="MARCA/<?=$val[$i]['name_canonical']?>"><?=$val[$i]['name']?></a></li>
                                                <?php endfor; ?>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>

                                    <li><a href="#" class="no-click">CATEGORIAS</a>
                                        <ul class="sub_menu">
                                            <?php foreach ($this->formatedArray($this->categories, 7) as $key => $val): ?>
                                                <?php for ($i = 0; $i < count($val); $i++): ?>
                                                    <li><a href="CATEGORIA/<?=$val[$i]['name_canonical']?>"><?=$val[$i]['name']?></a></li>
                                                <?php endfor; ?>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                    <li><a href="http://www.cosmeceuticalcenter.com/NEW-CONTACTO-COSMECEUTICALCENTER">Contacto</a></li>
                                    <li class="position-relative"><a href="#" class="no-click">Servicios <i class="fa fa-angle-down"></i></a>
                                        <ul class="sub_menu css-left-78">
                                            <li><a href="https://www.cosmeceuticalcenter.com/NEW-TRATAMIENTOS">Tratamientos en clínica</a></li>
                                            <li><a href="https://www.cosmeceuticalcenter.com/ASESORIA-COSMECEUTICA">Asesoria Cosmecéutica</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
