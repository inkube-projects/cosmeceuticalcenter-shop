<ul>
    <li><a href="#"><i class="fa fa-home css-fonsize16  css-padding-left20"></i></a></li>
    <li><a href="#" class="no-click">CATEGORÍAS <i class="fa fa-angle-down"></i></a>
        <ul class="mega_menu">
            <li>
                <div style="width: 300px;">
                    <?php if (isset($cat_info)): ?>
                        <div class="css-categoryimage-menu" style="background-image: url('<?=$cat_info['formed_image']?>')"></div>
                    <?php else: ?>
                        <img src="<?=APP_IMG?>banners/imgMenuCategoria.jpg" width="300" height="170" alt=""/>
                    <?php endif; ?>
                </div>
            </li>
            <li class="boder-right1" style="margin-right: 40px;">
                <ul>
                    <li><a href="PRODUCTOS/NOVEDADES" class="css-bold-lowercase">Novedades <i class="fa fa-angle-right"></i></a></li>
                    <li><a href="PRODUCTOS/DESCUENTOS" class="css-bold-lowercase">Con descuento <i class="fa fa-angle-right"></i></a></li>
                </ul>
            </li>
            <?php foreach ($this->formatedArray($this->categories, 7) as $key => $val): ?>
                <li class="boder-right1">
                    <ul>
                        <?php for ($i = 0; $i < count($val); $i++): ?>
                            <li><a href="CATEGORIA/<?=friendlyURL($val[$i]['name_canonical'], 'upper')?>"><?=$val[$i]['name']?></a></li>
                        <?php endfor; ?>
                    </ul>
                </li>
            <?php endforeach; ?>
        </ul>
    </li>

    <li><a href="#" class="no-click">MARCAS <i class="fa fa-angle-down"></i></a>
        <ul class="mega_menu">
            <li>
                <div style="width: 300px;">
                    <img src="<?=APP_IMG_BANNERS?>imgMenuMarca.jpg" width="300" height="170" alt=""/>
                </div>
            </li>
            <li class="boder-right1" style="margin-right: -15px;">
                <ul>
                    <li><a href="PRODUCTOS/NOVEDADES" class="css-bold-lowercase">Novedades <i class="fa fa-angle-right"></i></a></li>
                    <li><a href="PRODUCTOS/DESCUENTOS" class="css-bold-lowercase">Con descuento <i class="fa fa-angle-right"></i></a></li>
                </ul>
            </li>
            <?php foreach ($this->formatedArray($this->brands, ceil(count($this->brands) / 4)) as $key => $val): ?>
                <li class="boder-right1">
                    <ul>
                        <?php for ($i = 0; $i < count($val); $i++): ?>
                            <li><a href="MARCA/<?=friendlyURL($val[$i]['name_canonical'], 'upper')?>"><?=$val[$i]['name']?></a></li>
                        <?php endfor; ?>
                    </ul>
                </li>
            <?php endforeach; ?>
        </ul>
    </li>

    <li><a href="http://www.cosmeceuticalcenter.com/NEW-CONTACTO-COSMECEUTICALCENTER">Contacto</a></li>

    <li class="position-relative"><a href="#" class="no-click">Servicios <i class="fa fa-angle-down"></i></a>
        <ul class="sub_menu css-left-78">
            <li><a href="https://www.cosmeceuticalcenter.com/NEW-TRATAMIENTOS">Tratamientos en clínica</a></li>
            <li><a href="https://www.cosmeceuticalcenter.com/ASESORIA-COSMECEUTICA">Asesoria Cosmecéutica</a></li>
        </ul>
    </li>

    <li class="position-relative">
        <a href="#" class="no-click css-relative">
            <i class="fa fa-user css-fonsize16">
                <i class="fa fa-angle-down"></i>
            </i>
            <?php if (isset($_SESSION['USER_SESSION_COSME']['id'])): ?>
                <div class="css-notifications"><?=$this->notifications?></div>
            <?php endif; ?>
        </a>

        <ul class="sub_menu css-left-78">
            <?php if (isset($_SESSION['USER_SESSION_COSME']['id'])): ?>
                <li><a href="DASHBOARD">Mi cuenta</a></li>
                <li><a href="NOTIFICACIONES">Notificaciones <label class="css-notifications"><?=$this->notifications?></label></a></li>
                <li><a href="LOGOUT">Cerrar Sesión</a></li>
            <?php else: ?>
                <li><a href="REGISTRO">Registro</a></li>
                <li><a href="LOGIN">Iniciar Sesión</a></li>
            <?php endif; ?>
        </ul>
    </li>
</ul>