<!-- modal area start-->
<div class="modal fade" id="modal_box" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal_body" id="ajx-product-mod">

            </div>
        </div>
    </div>
</div>

<!-- Newsletter Modal -->
<div class="newletter-popup">
    <div id="boxes" class="newletter-container whidthModal">
        <div id="dialog" class="window">
            <div id="popup2">
                <span class="b-close"><span>cerrar</span></span>
            </div>
            <div class="box">
                <div class="newletter-title">
                    <img src="<?=APP_LOGO?>logo.jpg" alt=""/><br><br>
                    <h4>Newsletter</h4>
                </div>
                <div class="box-content newleter-content">
                    <label class="newletter-label">Siempre estarás informado de todo lo que ocurre en Cosmeceutical Center (Descuentos, Novedades, Consejos, Promociones, etc...)&amp;</label>
                    <div id="frm_subscribe">
                        <form name="subscribe" id="subscribe_popup">
                            <!-- <span class="required">*</span><span>Enter you email address here...</span>-->
                            <input type="text" value="" name="subscribe_pemail" id="subscribe_pemail" placeholder="Escribe tu email...">
                            <input type="hidden" value="" name="subscribe_pname" id="subscribe_pname">
                            <div id="notification"></div>
                            <a class="theme-btn-outlined" onclick="email_subscribepopup()"><span>Subscribete</span></a>
                        </form>
                        <div class="subscribe-bottom">
                            <input type="checkbox" id="newsletter_popup_dont_show_again">
                            <label for="newsletter_popup_dont_show_again">
                                Soy mayor de 14 años y ACEPTO las condiciones establecidas en Aviso Legal y Política de Privacidad
                                Suscribirme a la lista de correo para conocer las novedades de COSMECEUTICAL CENTER - nuevos productos, promociones, descuentos y eventos.
                            </label>
                        </div>
                    </div>
                    <!-- /#frm_subscribe -->
                </div>
                <!-- /.box-content -->
            </div>
        </div>
    </div>
    <!-- /.box -->
</div>

<!-- Registro Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="mod-newsletter">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: block;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title"><b>Valida tu suscripción al newsletter</b></h4>
            </div>
            <div class="modal-body">
                <div class="text-center" id="ajx-newsletter">
                    <i class="fa fa-address-card css-fontSize40 css-color-crema" aria-hidden="true"></i>
                    <h4 class="css-text-black css-marginT10">Valida tu registro al newsletter</h4>
                    <p>Para validar tu registro sólo tendrás que pulsar el vinculo que recibirás en la cuenta de correo con la que te has suscrito</p>
                    <i class="fa fa-exclamation-circle css-fontSize20 css-icon-yellow" aria-hidden="true"></i>
                    <p>Recuerda revisar tu bandeja de correo no deseado. Añade <span class="css-icon-yellow">enviosweb@cosmeceuticalcenter.com</span> a tus contactos y llegaremos siempre a tu bandeja de entrada</p>
                </div>

                <div class="text-center">
                    <img src="<?=APP_LOGO?>logo.jpg" alt=""/><br><br>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal destacados -->
<div class="modal fade" id="mod-featured" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered whidthModal" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal_body" id="js-featured-content">

            </div>
        </div>
    </div>
</div>

<!-- Modal notificaciones -->
<div class="modal fade" id="mod-notifications" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered whidthModal" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal_body" id="ajx-notification">

            </div>
        </div>
    </div>
</div>

<!-- MOdal para agregar productos -->
<div class="modal fade" tabindex="-1" role="dialog" id="mod-cart-add">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Aviso</h4>
            </div>
            <div class="modal-body text-center ajx-add-cart-message">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para avisame -->
<div class="modal fade" tabindex="-1" role="dialog" id="mod-let-me-know">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-left">
                <h4 class="modal-title">Avísame</h4>
            </div>
            <div class="modal-body">
                <form name="frm-let-me-know" id="frm-let-me-know" method="post" action="">
                    <div class="js-alert-mod"></div>
                    <div class="input-group">
                        <input type="text" name="notification_email" id="notification_email" class="form-control" placeholder="Introduce tu E-mail">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-black lmk-submit js-notify" type="button" style="border-radius:0;">Envíar</button>
                        </span>
                    </div>
                    <div class="text-danger css-marginB10 notification_email_validate" id="notification_email_validate"></div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
