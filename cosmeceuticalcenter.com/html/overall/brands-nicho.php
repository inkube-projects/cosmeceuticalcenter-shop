<!--banner fullwidth start-->
<section class="banner_fullwidth banner_bg04">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <div class="slider_content text-center mx-auto widthNicheo">
                    <div class="slider_content_info">
                        <p>Cosmética, Belleza y Antiaging</p>
                        <h4>MARCAS NICHO</h4>
                        <p class="slider_price" style="font-size: 13px; line-height: 18px;">Antes de incluir un producto en nuestro catálogo, siempre comprobamos la eficacia de los principios activos, la penetración, su forma de liberación y el análisis de excipientes, vehículos y coadyuvantes.</p>
                        <a class="button" href="http://www.cosmeceuticalcenter.com/NEW-MARCAS-NICHO-BELLEZA-COSMETICA" target="_blank">+ ver todas las marcas</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--banner area end-->
