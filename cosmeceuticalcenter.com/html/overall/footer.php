<!--footer area start-->
<footer class="footer_widgets css-paddingT50">
    <div class="container-fluid">
        <div class="footer_top">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="widgets_container contact_us">
                        <h3><img src="<?=APP_LOGO?>logo.png" alt=""/></h3>
                        <div class="footer_contact">
                            <p>C/ Jesús de la Vera Cruz, 27 <br>41002 Sevilla</p>
                            <p><a href="tel:+34954378643">+34 954 378 643 </a> - <a href="tel:+34673375357"> +34 673 375 357</a></p>
                            <p>Email: info@cosmeceuticalcenter.com </p>
                            <ul>
                                <!-- <li><a href="https://www.facebook.com/CosmeceuticalCenter" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/CosmeceuticalC" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="ion-social-rss"></i></a></li>
                                <li><a href="#"><i class="ion-social-googleplus"></i></a></li>
                                <li><a href="#"><i class="ion-social-youtube"></i></a></li> -->
                                <li><a href="https://www.facebook.com/CosmeceuticalCenter" target="_blank"><i class="ion-social-facebook"></i></a></li>
                                <li><a href="https://twitter.com/CosmeceuticalC" target="_blank"><i class="ion-social-twitter"></i></a></li>
                                <li><a href="https://www.pinterest.es/cosmeceuticalc/" target="_blank"><i class="ion-social-pinterest"></i></a></li>
                                <li><a href="https://www.instagram.com/cosmeceuticalcenter/?hl=es" target="_blank"><i class="ion-social-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="d-none d-md-block col-md-6 col-lg-3">
                    <div class="widgets_container widget_menu">
                        <h3>Productos por Marcas</h3>
                        <div class="footer_menu">
                            <div class="row">
                                <?php foreach ($this->formatedArray($this->brands, ceil(count($this->brands) / 2)) as $key => $val): ?>
                                    <div class="col-6">
                                        <ul>
                                            <?php for ($i = 0; $i < count($val); $i++): ?>
                                                <li><a href="MARCA/<?=friendlyURL($val[$i]['name_canonical'], 'upper')?>"><?=$val[$i]['name']?></a></li>
                                            <?php endfor; ?>
                                        </ul>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-none d-md-block col-md-6 col-lg-3">
                    <div class="widgets_container widget_menu">
                        <h3>Productos por Categorías</h3>
                        <div class="footer_menu">
                            <div class="row">
                                <?php if (count($this->categories)): ?>
                                    <?php foreach ($this->formatedArray($this->categories, ceil(count($this->categories) / 2)) as $key => $val): ?>
                                        <div class="col-6">
                                            <ul>
                                                <?php for ($i = 0; $i < count($val); $i++): ?>
                                                    <li><a href="CATEGORIA/<?=friendlyURL($val[$i]['name_canonical'], 'upper')?>"><?=$val[$i]['name']?></a></li>
                                                <?php endfor; ?>
                                            </ul>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="widgets_container widget_menu">
                        <h3>Secciones</h3>
                        <div class="footer_menu">
                            <ul>
                                <li><a href="https://www.cosmeceuticalcenter.com/NEW-TRATAMIENTOS">Tratamientos en clínica</a></li>
                                <li><a href="https://www.cosmeceuticalcenter.com/ASESORIA-COSMECEUTICA">Asesoría Antiaging</a></li>
                                <li><a href="http://www.cosmeceuticalcenter.com/NEW-CONTACTO-COSMECEUTICALCENTER">Contacto</a></li>
                                <li><a href="https://shop.cosmeceuticalcenter.com/LOGIN">Mi cuenta</a></li>
                                <li><a href="https://shop.cosmeceuticalcenter.com/REGISTRO">Registro</a></li>
												<li><a href="https://www.cosmeceuticalcenter.com/SHOP-TAX-FREE" target="_blank">Shop Tax Free</a></li>
                                <!--<li><a href="#">Faqs/Ayuda</a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_middel">
            <div class="row">
                <div class="col-12">
                    <div class="footer_middel_menu">
                        <ul>
                            <li><a href="HOME">Shop Online</a></li>
                            <li><a href="https://www.cosmeceuticalcenter.com/">Web Corporativa</a></li>
                            <li><a href="http://www.cosmeceuticalcenter.com/blog/">Blog</a></li>
                            <li><a href="http://www.cosmeceuticalcenter.com/NEW-CONTACTO-COSMECEUTICALCENTER">Contacto</a></li>
										<li><a href="https://www.cosmeceuticalcenter.com/SHOP-TAX-FREE" target="_blank">Shop Tax Free</a></li>
                            <li><a href="https://www.cosmeceuticalcenter.com/NEW-NOTA-LEGAL">Aviso Legal-RGPD</a></li>
                            <li><a href="https://www.cosmeceuticalcenter.com/NEW-COOKIES">Cookies</a></li>
                            <li><a href="http://www.cosmeceuticalcenter.com/condiciones-venta.php">Condiciones de Venta</a></li>
                            <!--<li><a href="#">Cambios y devoluciones</a></li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_bottom">
            <div class="row">
                <div class="col-12">
                    <div class="copyright_area">
                        <p>Copyright &copy; 2019 <a href="#">Coemceutial Center S.L.</a>  Todo los derechos reservados.</p>
                        <img src="<?=APP_IMG_ICONS?>papyel2.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--footer area end-->
