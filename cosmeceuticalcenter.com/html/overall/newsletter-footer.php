<!--Newsletter area start-->
<div class="newsletter_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="newsletter_content">
                    <h4>Newsletter - </h4>
                    <p>Descuentos, promociones y novedades de Cosmecéutica y Nutracéutica en tu email</p>
                    <form name="frm-newsletter" id="frm-newsletter" method="post" action="">
                        <div class="css-frm-newsletter">
                            <input type="text" name="newsletter_email" id="newsletter_email" placeholder="Escribe tu email">
                            <button type="submit" class="btn-submit">suscríbete </button>
                        </div>

                        <div class="text-danger" id="newsletter_email_validate"></div>

                        <div class="text-danger" id="newsletter_terms_validate"></div>
                        <div class="checkbox col-xs-12 col-md-8 css-center-block css-noFloat css-marginT10">
                            <label class="css-fonsize12 css-lineheigt13">
                                <input type="checkbox" id="newsletter_terms" name="newsletter_terms[]" value="1" > Soy mayor de 14 años y ACEPTO las condiciones establecidas en Aviso Legal y Política de Privacidad. Suscribirme a la lista de correo para conocer las novedades de COSMECEUTICAL CENTER - nuevos productos, promociones, descuentos y eventos.
                            </label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Newsletter area start-->
