<!DOCTYPE html>
<html class="no-js" lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <title> Mi Carrito | <?=APP_TITLE?> </title>
        <meta name="Description" CONTENT=" " />
        <meta name="Keywords" CONTENT="" />
        <?php include('html/overall/header.php'); ?>
        <link rel="stylesheet" href="<?=APP_ASSETS?>css/css-cart.css">
    </head>

    <body>
        <div class="css-overlay-load" id="js-overlay"><i class="fa fa-spinner fa-spin"></i></div>
        <div class="home_three_body_wrapper">
            <!--header area start-->
            <?php include("html/overall/navbar.php"); ?>
            <!--header area end-->

            <!--breadcrumbs-->
            <div class="breadcrumbs_area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb_content">
                                <h3>Cesta de la compra</h3>
                                <ul>
                                    <li><a href="HOME">home</a></li>
                                    <li>></li>
                                    <li>Cesta Compra</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end.breadcrumbs-->

            <!--CONTENIDO-->
            <div class="Checkout_section textura_Gris" id="ajx-cart">
                <div class="container">
                    <div class="row">
                        <!--datos user-->
                        <div class="col-lg-7 col-md-7">
                            <form name="frm-cart" id="frm-cart" class="css-frm" method="post" action="">
                                <div class="js-alert"></div>

                                <div class="row">
                                    <?php foreach ($arr_cart as $val): ?>
                                        <input type="hidden" name="c[]" value="<?=$val['cart_id']?>">
                                        <!--ITEM CART -->
                                        <div class="col-12 itemCart">
                                            <div class="row">
                                                <div  class="col-xs-4 col-ms-2 col-sm-2">
                                                    <a href="PRODUCTO/<?=$val['product_id']?>/<?=friendlyURL($val['name'], 'upper')?>">
                                                        <img src="<?=$val['formed_image']?>" alt="">
                                                    </a>
                                                </div>

                                                <div class="col-xs-8 col-ms-3 col-sm-4 borderRightN bottomxs bottommd">
                                                    <h4><?=$val['brand_name']?> </h4>
                                                    <p><span class="verde"><?=$val['name']?></span></p>
                                                    <?php if ($val['size']): echo "<b>Talla</b>: ".$val['size']."<br>"; endif; ?>
                                                    <?php if ($val['color']): echo "<b>Color</b>: ".$val['color']; endif; ?>
                                                    <div class="imgMarca">
                                                        <?=$val['category_name']?>
                                                    </div>
                                                </div>

                                                <div  class="col-xs-3 col-ms-2 col-sm-2 borderRightN cantidadCart">
                                                    <div class="form-group ">
                                                        <label for="cantidad">Cantidad</label>
                                                        <?php if ($session): ?>
                                                            <input type="number" name="qty[]" id="qty_<?=$val['cart_id']?>" class="css-ipt-quantity only-number" min="1" max="100" value="<?=$val['quantity']?>" data-value="<?=$val['cart_id']?>" data-color="<?=$val['color_id']?>" data-size="<?=$val['size_id']?>">
                                                        <?php else: ?>
                                                            <!-- <p><?=$val['quantity']?></p> -->
                                                            <input type="number" name="qty[]" id="qty_<?=$val['product_id']?>" class="css-ipt-quantity only-number" min="1" max="100" value="<?=$val['quantity']?>" data-value="<?=$val['product_id']?>" data-color="<?=$val['color_id']?>" data-size="<?=$val['size_id']?>">
                                                        <?php endif; ?>
                                                    </div>
                                                </div>

                                                <div  class="col-xs-9 col-ms-5 col-sm-4 ">
                                                    <div class="borderRightN" style="width:75%; float:left;">
                                                        <?php if ($val['new_price'] != ""): ?>
                                                            <?php if ($val['new_price'] != $val['price_iva']): ?>
                                                                <small class="css-line-through"><?=$val['price_iva']?> €</small>
                                                            <?php endif; ?>

                                                            <h4><?=$val['new_price']?> €</h4>
                                                        <?php else: ?>
                                                            <h4><?=$val['price_iva']?> €</h4>
                                                        <?php endif; ?>

                                                        <?php if ($val['type_price'] != ""): ?>
                                                            <span><?=$val['type_price']?></span><br>
                                                        <?php else: ?>
                                                            <!-- <span>IVA (<?=$val['product_iva']?>%): <?=$val['calculated_iva']?> €</span><br> -->
                                                        <?php endif; ?>

                                                        <small>Sub total: <b><?=$val['sub_total']?> &euro;</b></small>
                                                    </div>

                                                    <div style="width:25%; float:left; position:relative; height:100%; text-align:center;">
                                                        <button type="button" class="btn btn-link btn-xs btn-remove" data-value="<?=$val['cart_id']?>" data-size="<?=$val['size_id']?>" data-color="<?=$val['color_id']?>">
                                                            <i class="fa fa-trash-o borrarCart"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--END ITEM CART -->
                                    <?php endforeach; ?>

                                    <!--CALCULO PORTES-->
                                    <div class="col-12 col-ms-8 col-ms-offset-4  col-sm-10 col-sm-offset-2 col-md-8 col-md-offset-4 calculoPortes">
                                        <i class="fa fa-truck fa-1x css-color-grisIco absoluteIco"></i>
                                        <div class="txtPortes">
                                            <p><br>Elije tu provincia<br>para calcular los portes</p>
                                        </div>
                                        <div class="ProvinciaPortes" style="width:54%; float:right;">
                                            <div class="form-group">
                                                <label for="country">País</label>
                                                <select class="form-control shipping" id="country" name="country">
                                                    <?php foreach ($arr_country as $val): ?>
                                                        <option value="<?=$val['id']?>" <?=isSelected($val['id'], $country_id)?>><?=$val['country']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="ProvinciaPortes" style="width:54%; float:right;">
                                            <div class="form-group">
                                                <label for="provincia">Provincia</label>
                                                <?php $disabled = ($country_id == 73) ? "" : "disabled" ?>
                                                <select class="form-control shipping" id="province" name="province" <?=$disabled?>>
                                                    <?php foreach ($arr_province as $val): ?>
                                                        <option value="<?=$val['id']?>" <?=isSelected($val['id'], $province_id)?>><?=$val['name']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!--END CALCULO PORTES-->
                                </div>
                            </form>
                        </div>

                        <div class="col-lg-4 offset-lg-1 col-md-4 offset-md-1">
                            <div class="row">
                                <!--CALCULO CART-->
                                <div class="col-12 ResumenCart">
                                    <h3><i class="fa fa-calculator fa-1x css-color-grisIco"></i> Cálculo cesta</h3><br>
                                    <div class="camposCart">
                                        <p>Subtotal:</p>
                                        <p>Portes:</p>
                                    </div>
                                    <div class="importesCart">
                                        <p><?=$sub_total?> €</p>
                                        <p id="ajx-shipping"><?=$shipping?> €</p>
                                    </div>
                                    <div  class="hr"></div>
                                    <div class="totalCart">
                                        <p>Total:</p>
                                    </div>
                                    <div class="pvpCart">
                                        <h2 id="ajx-total"><?=$total?> €</h2>
                                    </div>

                                    <div class="checkout_form" style="width: 100%; float: left;">
                                        <div class="col-12"></div>
                                    </div>

                                    <div  class="hr"></div>

                                    <div class="buttonSeguir">
                                        <a class="btn btn-warning" href="PRODUCTOS" role="button">seguir comprando</a>
                                    </div>
                                    <div class="buttonPago">
                                        <?php
                                        $disable = ($this->cart['total_products'] > 0) ? "" : "disabled";
                                        if ($session) {
                                            $url = ($this->cart['total_products'] > 0) ? "PASO-2" : "";
                                        } else {
                                            $url = "PASO-1";
                                        }
                                        ?>
                                        <a href="<?=$url?>" class="btn btn-lg customButtonCar">Continuar</a>
                                    </div>

														<!--<img style="margin-top: 20px;" src="<?//=APP_IMG_ICONS?>VERANO.jpg" alt="">-->
                                </div>
                                <!--END.CALCULO CART-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--END.CONTENIDO-->

            <?php include("html/overall/footer.php"); ?>
        </div>

        <!-- JS
        ============================================ -->
        <?php include("html/overall/js.php"); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-cart.js"></script>
    </body>
</html>
