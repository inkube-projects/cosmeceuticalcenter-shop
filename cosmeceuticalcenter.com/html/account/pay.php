<!DOCTYPE html>
<html class="no-js" lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <title> Pagar | <?=APP_TITLE?> </title>
        <meta name="Description" CONTENT=" " />
        <meta name="Keywords" CONTENT="" />
        <?php include('html/overall/header.php'); ?>
        <link rel="stylesheet" href="<?=APP_ASSETS?>css/css-cart.css">
    </head>

    <body>
        <div class="css-overlay-load" id="js-overlay"><i class="fa fa-spinner fa-spin"></i></div>
        <div class="home_three_body_wrapper">
            <!--header area start-->
            <?php include("html/overall/navbar.php"); ?>
            <!--header area end-->

            <!--breadcrumbs-->
            <div class="breadcrumbs_area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb_content">
                                <h3>Cesta de la compra</h3>
                                <ul>
                                    <li><a href="HOME">Home</a></li>
                                    <li>></li>
                                    <li><a href="CARRITO">Carrito</a></li>
                                    <li>></li>
                                    <li>Pagar</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end.breadcrumbs-->

            <!--PASOS CESTA ================================================== -->
            <div class="contenidos">
                <div class="container">
                    <div class="row">
                        <div class=" col-12 col-sm-8 offset-sm-2">
                            <div class="process">
                                <div class="process-row">
                                    <div class="process-step">
                                        <button id="iconBasicos" type="button" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-user fa-3x "></i></button>
                                        <h2>PASO 01</h2><p>ACCESO / REGISTRO</p>
                                    </div>
                                    <div class="process-step">
                                        <button id="iconUbicacion" type="button" class="btn btn-success  btn-circle" disabled="disabled"><i class="fa fa-shopping-cart fa-3x "></i></button>
                                        <h2>PASO 02</h2><p>REALIZAR PAGO</p>
                                    </div>
                                    <div class="process-step">
                                        <button type="button" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-thumbs-up fa-3x "></i></button>
                                        <h2>PASO 03</h2><p>VALIDACIÓN PAGO</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.END PASO CESTA -->

            <!--CONTENIDO-->
            <div class="Checkout_section textura_Gris" id="ajx-cart">
                <form name="frm-cart" id="frm-cart" class="css-frm" method="post" action="">
                    <input type="hidden" name="o" id="o" value="">
                    <input type="hidden" name="amount" id="amount" value="">
                    <div class="container">
                        <div class="row">
                            <!--datos user-->
                            <div class="col-lg-7 col-md-7">
                                <div class="text-right">
                                    <a href="CARRITO" class="css-fontSize16">Editar cesta <i class="ion-android-cart"></i></a>
                                </div>
                                <div class="js-alert"><?=$alert?></div>

                                <div class="row">
                                    <?php foreach ($arr_cart as $val): ?>
                                        <input type="hidden" name="c[]" value="<?=$val['cart_id']?>">
                                        <!--ITEM CART -->
                                        <div class="col-12 itemCart">
                                            <div class="row">
                                                <div  class="col-xs-4 col-ms-2 col-sm-2">
                                                    <a href="PRODUCTO/<?=$val['product_id']?>/<?=friendlyURL($val['name'], 'upper')?>">
                                                        <img src="<?=$val['formed_image']?>" alt="">
                                                    </a>
                                                </div>

                                                <div class="col-xs-8 col-ms-3 col-sm-4 borderRightN bottomxs bottommd">
                                                    <h4><?=$val['brand_name']?> </h4>
                                                    <p><span class="verde"><?=$val['name']?></span></p>
                                                    <?php if ($val['size']): echo "<b>Talla</b>: ".$val['size']."<br>"; endif; ?>
                                                    <?php if ($val['color']): echo "<b>Color</b>: ".$val['color']; endif; ?>
                                                    <div class="imgMarca">
                                                        <?=$val['category_name']?>
                                                    </div>
                                                </div>

                                                <div  class="col-xs-3 col-ms-2 col-sm-2 borderRightN cantidadCart">
                                                    <div class="form-group ">
                                                        <label for="cantidad">Cantidad</label>
                                                        <p><?=$val['quantity']?></p>
                                                    </div>
                                                </div>

                                                <div  class="col-xs-9 col-ms-5 col-sm-4 ">
                                                    <div class="borderRightN" style="width:100%; float:left;">
                                                        <?php if ($val['new_price'] != ""): ?>
                                                            <?php if ($val['new_price'] != $val['price_iva']): ?>
                                                                <small class="css-line-through"><?=$val['price_iva']?> €</small>
                                                            <?php endif; ?>

                                                            <h4><?=$val['new_price']?> €</h4>
                                                        <?php else: ?>
                                                            <h4><?=$val['price_iva']?> €</h4>
                                                        <?php endif; ?>

                                                        <?php if ($val['calculated_iva'] == 0): ?>
                                                            <span>PROMOCIÓN</span><br>
                                                        <?php else: ?>
                                                            <!-- <span>IVA (<?=$val['product_iva']?>%): <?=$val['calculated_iva']?> €</span><br> -->
                                                        <?php endif; ?>
                                                        <!--<p> <span class="negro"><strong>2 UNIDADES 120,90 €</strong></span></p>-->
                                                        <small>Sub total: <b><?=$val['sub_total']?> &euro;</b></small>
                                                        <div id="js-coupon-<?=$val['product_id']?>"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--END ITEM CART -->
                                    <?php endforeach; ?>

                                    <!--CALCULO PORTES-->
                                    <div class="col-12  col-sm-10 offset-sm-2 col-md-8 offset-sm-4 calculoPortes float-right">
                                        <i class="fa fa-truck fa-1x css-color-grisIco absoluteIco"></i>
                                        <!-- <div class="txtPortes">
                                            <p><br>Elije tu provincia<br>para calcular los portes</p>
                                        </div> -->
                                        <div class="ProvinciaPortes" style="width:54%; float:right;">
                                            <div class="form-group">
                                                <label for="provincia">País: </label>
                                                <span class="css-bold"><?=$country_name?></span>
                                            </div>

                                            <?php if ($province_name): ?>
                                                <div class="form-group">
                                                    <label for="provincia">Provincia: </label>
                                                    <span class="css-bold"><?=$province_name?></span>
                                                </div>
                                            <?php endif; ?>
                                            <input type="hidden" name="country" id="country" value="<?=@$country_id?>">
                                            <input type="hidden" name="province" id="province" value="<?=@$province_id?>">
                                        </div>
                                    </div>
                                    <!--END CALCULO PORTES-->

                                    <!--FORMA DE PAGO-->
                                    <?php if ($validate_address): ?>
                                        <div class="col-12 col-sm-10 offset-sm-2 col-md-8 offset-md-4 calculoPortes">
                                            <h2><i class="fa fa-money fa-1x verde"></i> Forma de pago</h2><br>

                                            <?php foreach ($arr_method as $key => $val): $check = ($key == 0) ? "checked" : ""; ?>
                                                <div class="icosF_Pago">
                                                    <i class="<?=$val['icon']?> fa-3x css-color-grisIco"></i>
                                                    <div class="radio">
                                                        <label style="font-size: 1.5em">
                                                            <input type="radio" name="method" value="<?=$val['id']?>" <?=$check?>>
                                                            <span class="cr"><i class="cr-icon fa fa-2x fa-check"></i></span><br>
                                                            <p><?=$val['method']?></p>
                                                        </label>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                            <div class="text-danger" id="method_validate"></div>
                                        </div>
                                    <?php endif; ?>
                                    <!--END.FORMA DE PAGO-->
                                </div>
                            </div>

                            <div class="col-lg-4 offset-lg-1 col-md-4 offset-md-1">
                                <div class="row">
                                    <!--CALCULO CART-->
                                    <div class="col-12 ResumenCart">
                                        <h3><i class="fa fa-calculator fa-1x css-color-grisIco"></i> Cálculo cesta</h3><br>
                                        <div class="camposCart">
                                            <p>Subtotal:</p>
                                            <p>Portes:</p>
                                            <p id="discount-percent">Descuento:</p>
                                            <p id="charge-percent" class="css-fontSize11">Recargo por gestión (0%):</p>
                                        </div>
                                        <div class="importesCart">
                                            <p><?=$sub_total?> €</p>
                                            <p><?=$shipping?> €</p>
                                            <p id="discount">0.00 €</p>
                                            <p id="charge">0.00 €</p>
                                        </div>
                                        <div  class="hr"></div>
                                        <div class="totalCart">
                                            <p>Total:</p>
                                        </div>
                                        <div class="pvpCart">
                                            <h2 class="ajx-total" data-total="<?=$total?>"><?=$total?> €</h2>
                                        </div>

                                        <div  class="hr"></div>

                                        <div class="descuentos_bono css-fondoGris" style="width:100%; float:left; padding:10px;">
                                            <div class="campoLabel" style="width:100%; float:left; font-size:16px">
                                                <label for=" " class="css-fonsize12"> <i class="fa fa-tag css-color-grisIco css-fonsize18" aria-hidden="true"></i> Tienes un Vale descuento</label>
                                            </div>
                                            <div class="col-8 float-left" style="margin: 0px; padding: 0px;">
                                                <input type="text" id="valeDto" name="valeDto" class="form-control" style="width: 100%; border-radius: 0px" value="" placeholder="introduce código">
                                            </div>
                                            <div class="col-4 float-right"  style="margin: 0px; padding: 0px;">
                                                <a id="aplicarVale" class="btn btn-warning" style="width: 100%;border-radius: 0px" role="button">Aplicar</a>
                                            </div>
                                        </div>

                                        <div class="hr"></div>

                                        <div class="descuentos_bono css-fondoGris" style="width:100%; float:left; padding:10px;">
                                            <div class="col-12 float-left" style="margin: 0px; padding: 0px;">
                                                <textarea name="note" id="note" class="form-control" rows="5" placeholder="Nota adicional" style="width: 100%; border-radius: 0px"></textarea>
                                                <div class="text-danger" id="note_validate"></div>
                                            </div>
                                        </div>

                                        <div class="hr"></div>

                                        <div class="">
                                            <p><i class="fa fa-gift fa-1x css-color-grisIco float-left" aria-hidden="true"></i>
                                                <a href="#regalo" class="regalo float-left" data-toggle="collapse">  Si es un regalo y desea decirnos la persona y el mensaje, haga clic aquí >. </a>
                                                <div id="regalo" class="collapse">
                                                    <div class="order-notes">
                                                        <textarea id="order_note" name="order_note" placeholder="Escribe aquí el nombre y dedicatoria " row="2" ></textarea>
                                                        <div class="text-danger" id="order_note_validate"></div>
                                                    </div>
                                                </div>
                                            </p>
                                        </div>

                                        <div  class="hr"></div>

                                        <div class="checkout_form" style="width: 100%; float: left;">
                                            <div class="col-12">
                                                <input id="terms" name="terms" type="checkbox" data-target="createp_account" value="1">
                                                <label class="css-fonsize12" for="account">Acepto las <a href="http://www.cosmeceuticalcenter.com/condiciones-venta.php" class="css-link-black" target="_blank">condiciones de venta</a></label>
                                                <div class="text-danger" id="terms_validate"></div>
                                            </div>
                                        </div>

                                        <div  class="hr"></div>

                                        <div class="buttonSeguir">
                                            <!-- <a href="javascript: test()">Test</a> -->
                                            <a class="btn btn-warning" href="PRODUCTOS" role="button">seguir comprando</a>
                                        </div>
                                        <div class="buttonPago">
                                            <?php if ($validate_address): ?>
                                                <?php $disable = ($this->cart['total_products'] > 0) ? "" : "disabled" ?>
                                                <div id="paypal-button">
                                                    <button type="submit" class="btn btn-lg customButtonCar btn-submit">Realizar Pago</button>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <!--END.CALCULO CART-->
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!--END.CONTENIDO-->

            <?php include("html/overall/footer.php"); ?>
        </div>

        <!-- JS
        ============================================ -->
        <?php include("html/overall/js.php"); ?>
        <!-- PayPal -->
        <script src="https://www.paypal.com/sdk/js?locale=es_ES&currency=EUR&client-id=AXo9yBxPY5-EcC4lUC2q6p1FeD1_SgpaoZj_DFnJjAjBuicDx5HefPp7u4fsSecbwOemNA4f8eXz4IiD"></script>
        <!-- <script src="https://www.paypal.com/sdk/js?locale=es_ES&currency=EUR&client-id=Afit9YjKmuF4VvhtnoDHKKMZx-5h-g7aWdtkKAsOSkRSP5it2_COD7CJvJ_-m0qygDw-FdIO4My6EjVr"></script> -->
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-cart.js"></script>
    </body>
</html>
