<!doctype html>
<html class="no-js" lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <title> Paso 01 | <?=APP_TITLE?> </title>
        <meta name="Description" CONTENT=" " />
        <meta name="Keywords" CONTENT="" />
        <?php include('html/overall/header.php'); ?>
        <link rel="stylesheet" href="<?=APP_ASSETS?>css/css-cart.css">
    </head>

    <body>
        <div class="home_three_body_wrapper">
            <?php include("html/overall/navbar.php"); ?>

            <!--breadcrumbs-->
            <div class="breadcrumbs_area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb_content">
                                <h3>Cesta de la compra</h3>
                                <ul>
                                    <li><a href="HOME">Home</a></li>
                                    <li>></li>
                                    <li>Cesta Compra</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end.breadcrumbs-->

            <!--PASOS CESTA ================================================== -->
            <div class="contenidos">
                <div class="container">
                    <div class="row">
                        <div class=" col-12 col-sm-8 offset-sm-2">
                            <div class="process">
                                <div class="process-row">
                                    <div class="process-step">
                                        <button id="iconBasicos" type="button" class="btn   btn-success btn-circle" disabled="disabled"><i class="fa fa-user fa-3x "></i></button>
                                        <h2>PASO 01</h2><p>ACCESO / REGISTRO</p>
                                    </div>
                                    <div class="process-step">
                                        <button id="iconUbicacion" type="button" class="btn  btn-default  btn-circle" disabled="disabled"><i class="fa fa-shopping-cart fa-3x "></i></button>
                                        <h2>PASO 02</h2><p>REALIZAR PAGO</p>
                                    </div>
                                    <div class="process-step">
                                        <button type="button" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-thumbs-up fa-3x "></i></button>
                                        <h2>PASO 03</h2><p>VALIDACIÓN PAGO</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.END PASO CESTA -->

            <!--CONTENIDO-->
            <div class="Checkout_section textura_Gris">
                <div class="container">
                    <div class="checkout_form">
                        <div class="account_form">
                            <h2>Acceso a tu cuenta</h2>

                            <form name="frm-register" id="frm-register" method="post" action="">
                                <div class="js-alert"></div>
                                <div class="row">
                                    <!--datos user-->
                                    <div class="col-lg-6 col-md-6">
                                        <h3><i class="fa fa-user" aria-hidden="true"></i> CREA TU CUENTA</h3>
                                        <div class="row">
                                            <div class="col-6">
                                                <input id="particular" name="type_account" type="radio" data-target="createp_account" checked value="1" />
                                                <label for="particular">Particular</label>
                                            </div>

                                            <div class="col-6">
                                                <input id="company" name="type_account" type="radio" data-target="createp_account" value="2" />
                                                <label for="company">Empresa</label>
                                            </div>

                                            <div class="col-12">
                                                <p><hr></p>
                                            </div>

                                            <div class="col-lg-6 mb-20">
                                                <label>NIF/CIF/NIE <span>*</span></label>
                                                <input type="text" name="identification" id="identification">
                                                <span class="text-danger" id="identification_validate"></span>
                                            </div>

                                            <div class="col-lg-6 mb-20">
                                                <label>Nombre <span>*</span></label>
                                                <input type="text" name="name" id="name">
                                                <span class="text-danger" id="name_validate"></span>
                                            </div>

                                            <div class="col-lg-6 mb-20">
                                                <label>Apellido 1 <span>*</span></label>
                                                <input type="text" name="last_name_1" id="last_name_1">
                                                <span class="text-danger" id="last_name_1_validate"></span>
                                            </div>
                                            <div class="col-lg-6 mb-20">
                                                <label>Apellido 2 <span>*</span></label>
                                                <input type="text" name="last_name_2" id="last_name_2">
                                                <span class="text-danger" id="last_name_2_validate"></span>
                                            </div>

                                            <div class="col-12 mb-20">
                                                <label>Empresa</label>
                                                <input type="text" name="company" id="company">
                                                <span class="text-danger" id="company_validate"></span>
                                            </div>

                                            <div class="col-lg-6 mb-20">
                                                <label>Móvil<span>*</span></label>
                                                <input type="text" name="phone" id="phone" class="only-phone">
                                                <span class="text-danger" id="phone_validate"></span>
                                            </div>

                                            <div class="col-lg-6 mb-20">
                                                <label>Email <span>*</span></label>
                                                <input type="text" name="email" id="email" value="<?=$email?>">
                                                <span class="text-danger" id="email_validate"></span>
                                            </div>

                                            <div class="col-lg-6 mb-20">
                                                <label>Contraseña <span>*</span></label>
                                                <input type="password" name="pass" id="pass" value="<?=$pass?>">
                                                <span class="text-danger" id="pass_validate"></span>
                                            </div>

                                            <div class="col-lg-6 mb-20">
                                                <label>Confirmar contraseña <span>*</span></label>
                                                <input type="password" name="repeat_pass" id="repeat_pass">
                                                <span class="text-danger" id="repeat_pass_validate"></span>
                                            </div>

                                            <div class="col-12">
                                                <p><hr></p>
                                            </div>

                                            <div class="col-lg-12 mb-20">
                                                <label>Fecha de nacimiento:<span>*</span></label>
                                            </div>

                                            <div class="col-lg-2 mb-20">
                                                <label for="country">Dia <span>*</span></label>
                                                <select class="niceselect_option" name="day" id="day">
                                                    <?php for ($i = 1; $i <= 31; $i++): ?>
                                                        <option value="<?=$i?>"><?=$i?></option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>

                                            <div class="col-lg-5 mb-20">
                                                <label for="country">Mes <span>*</span></label>
                                                <select class="niceselect_option" name="month" id="month">
                                                    <?php foreach ($arr_monts as $key => $val): ?>
                                                        <option value="<?=$key?>"><?=$val?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>

                                            <div class="col-lg-5 mb-20">
                                                <label for="country">Año <span>*</span></label>
                                                <select class="niceselect_option" name="year" id="year">
                                                    <?php for ($i = date('Y'); $i > (date('Y') - 100); $i--): ?>
                                                        <option value="<?=$i?>" <?=isSelected($i, (date('Y') - 20))?>><?=$i?></option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>

                                            <div class="col-12">
                                                <p><hr></p>
                                            </div>

                                            <div class="col-6 mb-20 js-country">
                                                <label for="country">País <span>*</span></label>
                                                <select class="niceselect_option" name="country" id="country">
                                                    <option value="">Seleccione un país</option>
                                                    <?php foreach ($arr_country as $val): ?>
                                                        <option value="<?=$val['id']?>" <?=isSelected($val['id'], $country)?>><?=$val['country']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="text-danger" id="country_validate"></span>
                                            </div>

                                            <div class="col-6 mb-20 js-province">
                                                <label for="province">Provincia <span>*</span></label>
                                                <select class="niceselect_option" name="province" id="province">
                                                    <?php foreach ($arr_province as $val): ?>
                                                        <option value="<?=$val['id']?>"><?=$val['name']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="text-danger" id="province_validate"></span>
                                            </div>

                                            <div class="col-12 mb-20">
                                                <label>Dirección  <span>*</span></label>
                                                <input name="location_1" id="location_1" type="text" placeholder="Calle, código postal, nombre de empresa, c / o">
                                                <span class="text-danger" id="location_1_validate"></span>
                                            </div>

                                            <div class="col-12 mb-20">
                                                <input type="text" name="location_2" id="location_2" placeholder="Apartamento, suite, unidad, edificio, piso, etc">
                                                <span class="text-danger" id="location_2_validate"></span>
                                            </div>

                                            <div class="col-6 mb-20">
                                                <label>Ciudad <span>*</span></label>
                                                <input  type="text" name="city" id="city">
                                                <span class="text-danger" id="city_validate"></span>
                                            </div>
                                            <div class="col-6 mb-20">
                                                <label>Código postal <span>*</span></label>
                                                <input type="text" name="postal_code" id="postal_code">
                                                <span class="text-danger" id="postal_code_validate"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end datos user (spanglish jajaj)-->

                                    <!-- direccion-->
                                    <div class="col-lg-6 col-md-6">
                                        <h3><i class="fa fa-truck" aria-hidden="true"></i> DATOS DE ENVÍO</h3>
                                        <div class="row">
                                            <div class="col-12">
                                                <input id="location_info" type="checkbox" data-target="createp_account" />
                                                <label for="location_info">Utilizar los mismo datos que los de registro</label>
                                            </div>

                                            <div class="col-12">
                                                <p><hr></p>
                                            </div>

                                            <div class="col-12 mb-20">
                                                <label>Nombre dirección</label>
                                                <input name="location_name" id="location_name" type="text" placeholder="indica un nombre para recorda esta dirección." >
                                                <span class="text-danger" id="location_name_validate"></span>
                                            </div>

                                            <div class="col-6 mb-20 js-shipping-country">
                                                <label for="country">País <span>*</span></label>
                                                <select class="niceselect_option" name="shipping_country" id="shipping_country">
                                                    <option value="">Seleccione un país</option>
                                                    <?php foreach ($arr_country as $val): ?>
                                                        <option value="<?=$val['id']?>" <?=isSelected($val['id'], $country)?>><?=$val['country']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="text-danger" id="shipping_country_validate"></span>
                                            </div>

                                            <div class="col-6 mb-20 js-shipping-province">
                                                <label for="country">Provincia <span>*</span></label>
                                                <select class="niceselect_option" name="shipping_province" id="shipping_province">
                                                    <?php foreach ($arr_province as $val): ?>
                                                        <option value="<?=$val['id']?>"><?=$val['name']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="text-danger" id="shipping_province_validate"></span>
                                            </div>

                                            <div class="col-12 mb-20">
                                                <label>Dirección  <span>*</span></label>
                                                <input name="shipping_location_1" id="shipping_location_1" placeholder="Calle, código postal, nombre de empresa, c / o" type="text">
                                                <span class="text-danger" id="shipping_location_1_validate"></span>
                                            </div>

                                            <div class="col-12 mb-20">
                                                <input name="shipping_location_2" id="shipping_location_2" placeholder="Apartamento, suite, unidad, edificio, piso, etc" type="text">
                                                <span class="text-danger" id="shipping_location_2_validate"></span>
                                            </div>

                                            <div class="col-6 mb-20">
                                                <label>Ciudad <span>*</span></label>
                                                <input name="shipping_city" id="shipping_city" type="text">
                                                <span class="text-danger" id="shipping_city_validate"></span>
                                            </div>

                                            <div class="col-6 mb-20">
                                                <label>Código postal <span>*</span></label>
                                                <input name="shipping_postal_code" id="shipping_postal_code" type="text">
                                                <span class="text-danger" id="shipping_postal_code_validate"></span>
                                            </div>

                                            <div class="col-12">
                                                <div class="order-notes">
                                                    <label for="order_note">Información adicional </label>
                                                    <textarea id="order_note" name="order_note" placeholder=" " rows="4" ></textarea>
                                                    <span class="text-danger" id="order_note_validate"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-8 offset-2 css-marginT20">
                                        <div class="col-12">
                                            <input id="newsletter_subs" name="newsletter_subs" type="checkbox" data-target="createp_account" />
                                            <label for="newsletter_subs">Suscripción al newsletter</label>
                                        </div>

                                        <div class="col-12">
                                            <div class="cuenta_button">
                                                <button type="submit" class="btn-submit">Registrarme ></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
				</div>
            </div>
            <!--END.CONTENIDO-->

            <?php include("html/overall/footer.php"); ?>
            <?php include("html/overall/modal.php"); ?>
            <!-- Modal olvide mi contraseña -->
            <div class="modal fade" id="mod-forgot" tabindex="-1" role="dialog"  aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document" style="min-width: 500px;">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="modal_body">
                            <div class="form-group css-paddingL10 css-paddingR10">
                                <form id="frm-forgot" name="frm-forgot" method="post" action="">
                                    <label for="forgot_email">Ingresa tu email</label>
                                    <input type="text" class="form-control" id="forgot_email" name="forgot_email" aria-describedby="emailHelp">
                                    <small id="forgot_email_validate" class="form-text text-muted">Se enviará un email a tu cuenta con una contraseña temporal</small>

                                    <button type="submit" class="btn btn-black btn-submit">Envíar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="key" data-act="2"></div>

        <!-- Registro Modal -->
        <div class="modal fade" tabindex="-1" role="dialog" id="mod-register">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="display: block;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                        <h4 class="modal-title css-text-black">Valida tu suscripción</h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            
                        </div>
                        <div class="text-center">
                            <div class="js-modal-container">
                                <i class="fa fa-spinner fa-spin css-fontSize24"></i> Procesando. No cierre ni recargue esta ventana.
                                <!-- <i class="fa fa-address-card css-fontSize40 css-color-crema" aria-hidden="true"></i>
                                <h4 class="css-text-black css-marginT10">Valida tu registro</h4>
                                <p>Para validar tu registro sólo tendrás que pulsar el vinculo que recibirás en la cuenta de correo con la que te has registrado, podras realizar la compra pero no podrás ingresar al área de clientes hasta que valides tu cuenta.</p>
                                <i class="fa fa-exclamation-circle css-fontSize24 css-icon-yellow" aria-hidden="true"></i>
                                <p>Recuerda revisar tu bandeja de correo no deseado. Añade <span class="css-icon-yellow">enviosweb@cosmeceuticalcenter.com</span> a tus contactos y llegaremos siempre a tu bandeja de entrada</p> -->
                            </div>
                            <img src="<?=APP_LOGO?>logo.jpg" alt=""/><br><br>
                        </div>
                    </div>
                    <div class="modal-footer js-modal-footer-container">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Continuar</button>
                        <a href="PASO-2" class="btn btn-default">Continuar</a> -->
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
        <!-- JS
        ============================================ -->
        <?php include("html/overall/js.php"); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-cart-register.js"></script>
    </body>
</html>
