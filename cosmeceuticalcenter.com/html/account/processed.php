<!DOCTYPE html>
<html class="no-js" lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <title> Procesado | <?=APP_TITLE?> </title>
        <meta name="Description" CONTENT=" " />
        <meta name="Keywords" CONTENT="" />
        <?php include('html/overall/header.php'); ?>
        <link rel="stylesheet" href="<?=APP_ASSETS?>css/css-cart.css">
    </head>

    <body>
        <div class="home_three_body_wrapper">
            <!--header area start-->
            <?php include("html/overall/navbar.php"); ?>
            <!--header area end-->

            <!--breadcrumbs-->
            <div class="breadcrumbs_area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb_content">
                                <h3>Cesta de la compra</h3>
                                <ul>
                                    <li><a href="HOME">Home</a></li>
                                    <li>></li>
                                    <li><a href="CARRITO">Carrito</a></li>
                                    <li>></li>
                                    <li>Procesado</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end.breadcrumbs-->

            <!--PASOS CESTA ================================================== -->
            <div class="contenidos">
                <div class="container">
                    <div class="row">
                        <div class=" col-12 col-sm-8 offset-sm-2">
                            <div class="process">
                                <div class="process-row">
                                    <div class="process-step">
                                        <button id="iconBasicos" type="button" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-user fa-3x "></i></button>
                                        <h2>PASO 01</h2><p>ACCESO / REGISTRO</p>
                                    </div>

                                    <div class="process-step">
                                        <button id="iconUbicacion" type="button" class="btn  btn-default  btn-circle" disabled="disabled"><i class="fa fa-shopping-cart fa-3x "></i></button>
                                        <h2>PASO 02</h2><p>REALIZAR PAGO</p>
                                    </div>

                                    <div class="process-step">
                                        <button type="button" class="btn btn-success btn-circle" disabled="disabled"><i class="fa fa-thumbs-up fa-3x "></i></button>
                                        <h2>PASO 03</h2><p>VALIDACIÓN PAGO</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.END PASO CESTA -->

            <!-- CONTENIDO PASO 3 -->
            <div class="contenidos textura_Gris  paddingTop paddingBottom marginTopX">
                <div class="container">
                    <div class="row">
                        <?=$text?>

                        <div class="col-12 col-md-8 offset-md-2 paso3">
                            <h2 class="css-color-verde">Tu pedido ha sido confirmado <i class="fa fa-1xB fa-check css-grisIco"> </i></h2>
                            <p><span class="css-color-rojo">Hemos confirmado el pago de tu pedido</span></p>

                            <h4>En breve recibirás un mail con el detalle de tu pedido y factura.</h4><br>
                            Puedes acceder a tu área de cliente también dispones de los datos de envio y facturación.<br><br>
                            <a class="btn btn-warning" href="DASHBOARD" role="button"> <i class="fa fa-arrow-circle-left fa-lg"></i> ir al área del cliente</a>
                            <br>
                            <br>
                            <hr>

                            <br>
                            Gracias por confiar en COSMECEUTICAL CENTER.
                            <br><br>
                            <a class="btn btn-warning" href="HOME" role="button"> <i class="fa fa-arrow-circle-left fa-lg"></i> ir a la home</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.END paso 3 -->

            <?php include("html/overall/footer.php"); ?>
        </div>

        <!-- JS
        ============================================ -->
        <?php include("html/overall/js.php"); ?>
    </body>
</html>
