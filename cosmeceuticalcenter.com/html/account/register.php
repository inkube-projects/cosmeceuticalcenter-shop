<!DOCTYPE html>
<html class="no-js" lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <title> HOME | <?=APP_TITLE?> </title>
        <meta name="Description" CONTENT="<?=@$arr_seo[0]['description']?>" />
        <meta name="Keywords" CONTENT="<?=@$arr_seo[0]['keywords']?>" />
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="home_three_body_wrapper">
            <!--header area start-->
            <?php include("html/overall/navbar.php"); ?>
            <!--header area end-->

            <!--breadcrumbs-->
            <div class="breadcrumbs_area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb_content">
                                <h3>Mi cuenta</h3>
                                <ul>
                                    <li><a href="HOME">home</a></li>
                                    <li>></li>
                                    <li>Mi cuenta</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end.breadcrumbs-->

            <!--CREAR CUENTA-->
            <div class="Checkout_section bgGray">
                <div class="container">
                    <div class="checkout_form">
                        <form name="frm-register" id="frm-register" method="post" action="">
                            <div class="js-alert"></div>
                            <div class="row">
                                <!--datos user-->
                                <div class="col-lg-6 col-md-6">
                                    <h3><i class="fa fa-user" aria-hidden="true"></i> CREA TU CUENTA</h3>
                                    <div class="row">
                                        <div class="col-6">
                                            <input id="particular" name="type_account" type="radio" data-target="createp_account" checked value="1" />
                                            <label for="particular">Particular</label>
                                        </div>

                                        <div class="col-6">
                                            <input id="company" name="type_account" type="radio" data-target="createp_account" value="2" />
                                            <label for="company">Empresa</label>
                                        </div>

                                        <div class="col-12">
                                            <p><hr></p>
                                        </div>

                                        <div class="col-lg-6 mb-20">
                                            <label>NIF/CIF/NIE <span>*</span></label>
                                            <input type="text" name="identification" id="identification">
                                            <span class="text-danger" id="identification_validate"></span>
                                        </div>

                                        <div class="col-lg-6 mb-20">
                                            <label>Nombre <span>*</span></label>
                                            <input type="text" name="name" id="name">
                                            <span class="text-danger" id="name_validate"></span>
                                        </div>

                                        <div class="col-lg-6 mb-20">
                                            <label>Apellido 1 <span>*</span></label>
                                            <input type="text" name="last_name_1" id="last_name_1">
                                            <span class="text-danger" id="last_name_1_validate"></span>
                                        </div>
                                        <div class="col-lg-6 mb-20">
                                            <label>Apellido 2 <span>*</span></label>
                                            <input type="text" name="last_name_2" id="last_name_2">
                                            <span class="text-danger" id="last_name_2_validate"></span>
                                        </div>

                                        <div class="col-12 mb-20">
                                            <label>Empresa</label>
                                            <input type="text" name="company" id="company">
                                            <span class="text-danger" id="company_validate"></span>
                                        </div>

                                        <div class="col-lg-6 mb-20">
                                            <label>Móvil<span>*</span></label>
                                            <input type="text" name="phone" id="phone" class="only-phone">
                                            <span class="text-danger" id="phone_validate"></span>
                                        </div>

                                        <div class="col-lg-6 mb-20">
                                            <label>Email <span>*</span></label>
                                            <input type="text" name="email" id="email">
                                            <span class="text-danger" id="email_validate"></span>
                                        </div>

                                        <div class="col-lg-6 mb-20">
                                            <label>Contraseña <span>*</span></label>
                                            <input type="password" name="pass" id="pass">
                                            <span class="text-danger" id="pass_validate"></span>
                                        </div>

                                        <div class="col-lg-6 mb-20">
                                            <label>Confirmar contraseña <span>*</span></label>
                                            <input type="password" name="repeat_pass" id="repeat_pass">
                                            <span class="text-danger" id="repeat_pass_validate"></span>
                                        </div>

                                        <div class="col-12">
                                            <p><hr></p>
                                        </div>

                                        <div class="col-lg-12 mb-20">
                                            <label>Fecha de nacimiento:<span>*</span></label>
                                        </div>

                                        <div class="col-lg-2 mb-20">
                                            <label for="country">Dia <span>*</span></label>
                                            <select class="niceselect_option" name="day" id="day">
                                                <?php for ($i = 1; $i <= 31; $i++): ?>
                                                    <option value="<?=$i?>"><?=$i?></option>
                                                <?php endfor; ?>
                                            </select>
                                        </div>

                                        <div class="col-lg-5 mb-20">
                                            <label for="country">Mes <span>*</span></label>
                                            <select class="niceselect_option" name="month" id="month">
                                                <?php foreach ($arr_monts as $key => $val): ?>
                                                    <option value="<?=$key?>"><?=$val?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="col-lg-5 mb-20">
                                            <label for="country">Año <span>*</span></label>
                                            <select class="niceselect_option" name="year" id="year">
                                                <?php for ($i = date('Y'); $i > (date('Y') - 100); $i--): ?>
                                                    <option value="<?=$i?>" <?=isSelected($i, (date('Y') - 20))?>><?=$i?></option>
                                                <?php endfor; ?>
                                            </select>
                                        </div>

                                        <div class="col-12">
                                            <p><hr></p>
                                        </div>

                                        <div class="col-6 mb-20 js-country">
                                            <label for="country">País <span>*</span></label>
                                            <select class="niceselect_option" name="country" id="country">
                                                <option value="">Seleccione un país</option>
                                                <?php foreach ($arr_country as $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelected($val['id'], $country)?>><?=$val['country']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span class="text-danger" id="country_validate"></span>
                                        </div>

                                        <div class="col-6 mb-20 js-province">
                                            <label for="province">Provincia <span>*</span></label>
                                            <select class="niceselect_option" name="province" id="province">
                                                <?php foreach ($arr_province as $val): ?>
                                                    <option value="<?=$val['id']?>"><?=$val['name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span class="text-danger" id="province_validate"></span>
                                        </div>

                                        <div class="col-12 mb-20">
                                            <label>Dirección  <span>*</span></label>
                                            <input name="location_1" id="location_1" type="text" placeholder="Calle, código postal, nombre de empresa, c / o">
                                            <span class="text-danger" id="location_1_validate"></span>
                                        </div>

                                        <div class="col-12 mb-20">
                                            <input type="text" name="location_2" id="location_2" placeholder="Apartamento, suite, unidad, edificio, piso, etc">
                                            <span class="text-danger" id="location_2_validate"></span>
                                        </div>

                                        <div class="col-6 mb-20">
                                            <label>Ciudad <span>*</span></label>
                                            <input  type="text" name="city" id="city">
                                            <span class="text-danger" id="city_validate"></span>
                                        </div>
                                        <div class="col-6 mb-20">
                                            <label>Código postal <span>*</span></label>
                                            <input type="text" name="postal_code" id="postal_code">
                                            <span class="text-danger" id="postal_code_validate"></span>
                                        </div>
                                    </div>
                                </div>
                                <!--end datos user (spanglish jajaj)-->

                                <!-- direccion-->
                                <div class="col-lg-6 col-md-6">
                                    <h3><i class="fa fa-truck" aria-hidden="true"></i> DATOS DE ENVÍO</h3>
                                    <div class="row">
                                        <div class="col-12">
                                            <input id="location_info" type="checkbox" data-target="createp_account" />
                                            <label for="location_info">Utilizar los mismo datos que los de registro</label>
                                        </div>

                                        <div class="col-12">
                                            <p><hr></p>
                                        </div>

                                        <div class="col-12 mb-20">
                                            <label>Nombre dirección</label>
                                            <input name="location_name" id="location_name" type="text" placeholder="indica un nombre para recorda esta dirección." >
                                            <span class="text-danger" id="location_name_validate"></span>
                                        </div>

                                        <div class="col-6 mb-20 js-shipping-country">
                                            <label for="country">País <span>*</span></label>
                                            <select class="niceselect_option" name="shipping_country" id="shipping_country">
                                                <option value="">Seleccione un país</option>
                                                <?php foreach ($arr_country as $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelected($val['id'], $country)?>><?=$val['country']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span class="text-danger" id="shipping_country_validate"></span>
                                        </div>

                                        <div class="col-6 mb-20 js-shipping-province">
                                            <label for="country">Provincia <span>*</span></label>
                                            <select class="niceselect_option" name="shipping_province" id="shipping_province">
                                                <?php foreach ($arr_province as $val): ?>
                                                    <option value="<?=$val['id']?>"><?=$val['name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span class="text-danger" id="shipping_province_validate"></span>
                                        </div>

                                        <div class="col-12 mb-20">
                                            <label>Dirección  <span>*</span></label>
                                            <input name="shipping_location_1" id="shipping_location_1" placeholder="Calle, código postal, nombre de empresa, c / o" type="text">
                                            <span class="text-danger" id="shipping_location_1_validate"></span>
                                        </div>

                                        <div class="col-12 mb-20">
                                            <input name="shipping_location_2" id="shipping_location_2" placeholder="Apartamento, suite, unidad, edificio, piso, etc" type="text">
                                            <span class="text-danger" id="shipping_location_2_validate"></span>
                                        </div>

                                        <div class="col-6 mb-20">
                                            <label>Ciudad <span>*</span></label>
                                            <input name="shipping_city" id="shipping_city" type="text">
                                            <span class="text-danger" id="shipping_city_validate"></span>
                                        </div>

                                        <div class="col-6 mb-20">
                                            <label>Código postal <span>*</span></label>
                                            <input name="shipping_postal_code" id="shipping_postal_code" type="text">
                                            <span class="text-danger" id="shipping_postal_code_validate"></span>
                                        </div>

                                        <div class="col-12">
                                            <div class="order-notes">
                                                <label for="order_note">Información adicional </label>
                                                <textarea id="order_note" name="order_note" placeholder=" " rows="4" ></textarea>
                                                <span class="text-danger" id="order_note_validate"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-8 offset-2 css-marginT20">
                                    <div class="col-12">
                                        <input id="newsletter_subs" name="newsletter_subs" type="checkbox" data-target="createp_account" />
                                        <label for="newsletter_subs">Suscripción al newsletter</label>
                                    </div>

                                    <div class="col-12">
                                        <div class="cuenta_button">
                                            <button type="submit" class="btn-submit">Registrarme ></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--END.CREAR CUENTA-->
            <div id="key" data-act="1"></div>

            <?php include("html/overall/footer.php"); ?>
        </div>

        <!-- Registro Modal -->
        <div class="modal fade" tabindex="-1" role="dialog" id="mod-register">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="display: block;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                        <h4 class="modal-title css-text-black">Valida tu suscripción</h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <i class="fa fa-address-card css-fontSize40 css-color-crema" aria-hidden="true"></i>
                            <h4 class="css-text-black css-marginT10">Valida tu registro</h4>
                            <p>Para validar tu registro sólo tendrás que pulsar el vinculo que recibirás en la cuenta de correo con la que te has registrado</p>
                            <i class="fa fa-exclamation-circle css-fontSize24 css-icon-yellow" aria-hidden="true"></i>
                            <p>Recuerda revisar tu bandeja de correo no deseado. Añade <span class="css-icon-yellow">enviosweb@cosmeceuticalcenter.com</span> a tus contactos y llegaremos siempre a tu bandeja de entrada</p>
                            <img src="<?=APP_LOGO?>logo.jpg" alt=""/><br><br>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> -->
                        <a href="LOGIN" class="btn btn-default">Continuar</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!--!FINAL home_three_body_wrapper ---->
        <?php include("html/overall/modal.php"); ?>
        <!-- JS
        ============================================ -->
        <?php include("html/overall/js.php"); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-register.js"></script>
    </body>
</html>
