<!DOCTYPE html>
<html class="no-js" lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <title> HOME | <?=APP_TITLE?> </title>
        <meta name="Description" CONTENT=" " />
        <meta name="Keywords" CONTENT="" />
        <?php include('html/overall/header.php'); ?>
        <style>
    		.tab-content {
                padding-top: 20px;
                padding-bottom: 30px;
                padding-right: 30px;
                padding-left: 30px;
            }

            .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
                color: #495057;
                background-color: #f8f9f9;
                border-color: #dee2e6 #dee2e6 #f8f9f9;
            }
            .tab-content {
                padding-top: 20px;
                padding-bottom: 30px;
                padding-right: 30px;
                padding-left: 30px;
                border-bottom: solid 1px #dee2e6;
                border-left: solid 1px #dee2e6;
                border-right: solid 1px #dee2e6;
            }

            .table {
                background-color: #fff;
            }
    		.table-responsive table tbody tr td {
                text-align: left;
            }

    		.css-color-verde{ color: #8dc716;}
    		.css-color-naranja{ color: #ff9c01;}
    		.css-color-azul{ color:#0096ff;}
    		.css-color-rojo{ color:#AB0002;}

            .table-responsive table thead {
                background-color: #1B1B1B;
            }

    		.table-responsive table thead tr th {
                text-align: left;
                color: #FFF;
                font-family: "Rubik", sans-serif;
            }
        </style>
    </head>

    <body>
        <div class="home_three_body_wrapper">
            <!--header area start-->
            <?php include("html/overall/navbar.php"); ?>
            <!--header area end-->

            <!--breadcrumbs-->
            <div class="breadcrumbs_area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb_content">
                                <h3>Área Privada</h3>
                                <ul>
                                    <li><a href="HOME">home</a></li>
                                    <li>></li>
                                    <li>Área privada</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end.breadcrumbs-->

            <!--CONTENIDO-->
            <!--CREAR CUENTA-->
            <div class="container">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="perfil-tab" data-toggle="tab" href="#perfil" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-user css-fonsize16"></i> Mi perfil <i class="fa fa-angle-down"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="factura-tab" data-toggle="tab" href="#factura" role="tab" aria-controls="profile" aria-selected="false"><i class="fa fa-folder-open css-fonsize16"></i> Datos facturación <i class="fa fa-angle-down"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pedidos-tab" data-toggle="tab" href="#pedidos" role="tab" aria-controls="profile" aria-selected="false"><i class="fa fa-folder-open css-fonsize16"></i> Mis pedidos <i class="fa fa-angle-down"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="direcciones-tab" data-toggle="tab" href="#direcciones" role="tab" aria-controls="contact" aria-selected="false"><i class="fa fa-truck css-fonsize16"></i> Datos de envío <i class="fa fa-angle-down"></i></a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" id="cupones-tab" data-toggle="tab" href="#cupones" role="tab" aria-controls="contact" aria-selected="false"><i class="fa fa-tag css-fonsize16"></i> Vales descuento <i class="fa fa-angle-down"></i></a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" id="favorites-tab" data-toggle="tab" href="#favorites" role="tab" aria-controls="contact" aria-selected="false"><i class="fa fa-bookmark css-fonsize16" aria-hidden="true"></i> Favoritos <i class="fa fa-angle-down"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="favorites-tab" data-toggle="tab" href="#opinions" role="tab" aria-controls="contact" aria-selected="false"><i class="fa fa-star css-fonsize16" aria-hidden="true"></i> Opiniones <i class="fa fa-angle-down"></i></a>
                    </li>
                </ul>

                <div class="tab-content bgGray" id="myTabContent">
                    <!--TAB DE PERFIL-->
                    <div class="tab-pane fade show active" id="perfil" role="tabpanel" aria-labelledby="perfil-tab">
                        <div class="col-xs-12 col-md-8 offset-md-2 checkout_form">
                            <form name="frm-profile" id="frm-profile" class="css-frm" method="post" action="">
                                <div class="js-alert"></div>

                                <div class="row">
                                    <div class="col-6 mb-20">
                                        <label>Nombre(s)</label>
                                        <input type="text" name="name" id="name" value="<?=$user_name?>">
                                        <span class="text-danger" id="name_validate"></span>
                                    </div>

                                    <div class="col-6 mb-20">
                                        <label>Apellido(s)</label>
                                        <input type="text" name="last_name" id="last_name" value="<?=$user_lastname?>">
                                        <span class="text-danger" id="last_name_validate"></span>
                                    </div>

                                    <div class="col-lg-6 mb-20">
                                        <label>Móvil<span>*</span></label>
                                        <input type="text" name="phone" id="phone" class="only-phone" value="<?=$mobile?>">
                                        <span class="text-danger" id="phone_validate"></span>
                                    </div>

                                    <div class="col-lg-6 mb-20">
                                        <label>Email <span>*</span></label>
                                        <input type="text" name="email" id="email" value="<?=$email?>">
                                        <span class="text-danger" id="email_validate"></span>
                                    </div>

                                    <!--fech anacimienot-->
                                    <div class="col-12">
                                        <p><hr></p>
                                    </div>

                                    <div class="col-lg-12 mb-20">
                                        <label>Fecha de nacimiento:<span>*</span></label>
                                    </div>

                                    <div class="col-lg-2 mb-20">
                                        <label for="country">Dia <span>*</span></label>
                                        <select class="niceselect_option" name="day" id="day">
                                            <?php for ($i = 1; $i <= 31; $i++): ?>
                                                <option value="<?=$i?>" <?=isSelected($i, $day)?>><?=$i?></option>
                                            <?php endfor; ?>
                                        </select>
                                    </div>

                                    <div class="col-lg-5 mb-20">
                                        <label for="country">Mes <span>*</span></label>
                                        <select class="niceselect_option" name="month" id="month">
                                            <?php foreach ($arr_monts as $key => $val): ?>
                                                <option value="<?=$key?>" <?=isSelected($key, $month)?>><?=$val?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="col-lg-5 mb-20">
                                        <label for="country">Año <span>*</span></label>
                                        <select class="niceselect_option" name="year" id="year">
                                            <?php for ($i = date('Y'); $i > (date('Y') - 100); $i--): ?>
                                                <option value="<?=$i?>" <?=isSelected($i, $year)?>><?=$i?></option>
                                            <?php endfor; ?>
                                        </select>
                                    </div>

                                    <!--end fecha nacimiento-->
                                    <h4 class="css-marginT20 css-no-bold">Si llena estos campos su contraseña sera cambiada</h4>

                                    <div class="col-lg-6 mb-20">
                                        <label>Contraseña<span>*</span></label>
                                        <input type="password" name="pass" id="pass">
                                    </div>

                                    <div class="col-lg-6 mb-20">
                                        <label>Confirmar contraseña<span>*</span></label>
                                        <input type="password" name="pass_repeat" id="pass_repeat">
                                    </div>

                                    <div class="col-12">
                                        <div class="order-notes">
                                            <label for="order_note">Información adicional </label>
                                            <textarea id="note" name="note" placeholder=" " rows="4" ><?=$note?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-8 offset-2 css-marginT20">
                                        <div class="col-12">
                                            <input id="account" type="checkbox" data-target="createp_account" />
                                            <label for="account">Suscripción al newsletter</label>
                                        </div>

                                        <div class="col-12">
                                            <div class="cuenta_button">
                                                <button type="submit" class="btn-submit">guardar datos ></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--END.TAB DE PERFIL-->

                    <!--TAB DE DATOS FACTURA-->
                    <div class="tab-pane fade" id="factura" role="tabpanel" aria-labelledby="factura-tab">
                        <div class="col-xs-12 col-md-8 offset-md-2 checkout_form">
                            <form name="frm-bill" id="frm-bill" method="post" class="css-frm" action="">
                                <div class="js-alert"></div>

                                <div class="row">
                                    <div class="col-6">
                                        <input id="particular" name="type_account" type="radio" data-target="createp_account" <?=isOption(1, $account_type)?> value="1" />
                                        <label for="particular">Particular</label>
                                    </div>

                                    <div class="col-6">
                                        <input id="company" name="type_account" type="radio" data-target="createp_account" value="2" <?=isOption(2, $account_type)?> />
                                        <label for="company">Empresa</label>
                                    </div>

                                    <div class="col-12">
                                        <p></p><hr><p></p>
                                    </div>

                                    <div class="col-lg-6 mb-20">
                                        <label>NIF/CIF/NIE <span>*</span></label>
                                        <input type="text" name="identification" id="identification" value="<?=$identification?>">
                                        <span class="text-danger" id="identification_validate"></span>
                                    </div>

                                    <div class="col-lg-6 mb-20">
                                        <label>Nombre(s) <span>*</span></label>
                                        <input type="text" name="bill_name" id="bill_name" value="<?=$bill_name?>">
                                        <span class="text-danger" id="bill_name_validate"></span>
                                    </div>

                                    <div class="col-lg-6 mb-20">
                                        <label>Apellido(s) <span>*</span></label>
                                        <input type="text" name="bill_last_name" id="bill_last_name" value="<?=$bill_last_name?>">
                                        <span class="text-danger" id="bill_last_name_validate"></span>
                                    </div>
                                    <!-- <div class="col-lg-6 mb-20">
                                        <label>Apellido 2 <span>*</span></label>
                                        <input type="text" name="last_name_2" id="last_name_2">
                                        <span class="text-danger" id="last_name_2_validate"></span>
                                    </div> -->

                                    <div class="col-6 mb-20">
                                        <label>Empresa</label>
                                        <input type="text" name="company" id="company" value="<?=$company?>">
                                        <span class="text-danger" id="company_validate"></span>
                                    </div>

                                    <div class="col-12">
                                        <p></p><hr><p></p>
                                    </div>

                                    <div class="col-6 mb-20 js-country">
                                        <label for="country">País <span>*</span></label>
                                        <select class="niceselect_option" name="bill_country" id="bill_country">
                                            <option value="">Seleccione un país</option>
                                            <?php foreach ($arr_country as $val): ?>
                                                <option value="<?=$val['id']?>" <?=isSelected($val['id'], $bill_country)?>><?=$val['country']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class="text-danger" id="bill_country_validate"></span>
                                    </div>

                                    <div class="col-6 mb-20 js-province">
                                        <label for="province">Provincia <span>*</span></label>
                                        <select class="niceselect_option" name="bill_province" id="bill_province">
                                            <?php foreach ($arr_province as $val): ?>
                                                <option value="<?=$val['id']?>" <?=isSelected($val['id'], $bill_province)?>><?=$val['name']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class="text-danger" id="bill_province_validate"></span>
                                    </div>

                                    <div class="col-12 mb-20">
                                        <label>Dirección  <span>*</span></label>
                                        <input name="bill_location_1" id="bill_location_1" type="text" placeholder="Calle, código postal, nombre de empresa, c / o" value="<?=$bill_address_1?>">
                                        <span class="text-danger" id="bill_location_1_validate"></span>
                                    </div>

                                    <div class="col-12 mb-20">
                                        <input type="text" name="bill_location_2" id="bill_location_2" placeholder="Apartamento, suite, unidad, edificio, piso, etc" value="<?=$bill_address_2?>">
                                        <span class="text-danger" id="bill_location_2_validate"></span>
                                    </div>

                                    <div class="col-6 mb-20">
                                        <label>Ciudad <span>*</span></label>
                                        <input  type="text" name="bill_city" id="bill_city" value="<?=$bill_city?>">
                                        <span class="text-danger" id="bill_city_validate"></span>
                                    </div>
                                    <div class="col-6 mb-20">
                                        <label>Código postal <span>*</span></label>
                                        <input type="text" name="bill_postal_code" id="bill_postal_code" value="<?=$bill_postal_code?>">
                                        <span class="text-danger" id="bill_postal_code_validate"></span>
                                    </div>

                                    <div class="col-8 offset-2 css-marginT20">
                                        <div class="col-12">
                                            <div class="cuenta_button">
                                                <button type="submit" class="btn-submit">guardar datos ></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--END.TAB DE DATOS FACTURA-->

                    <!--TAB DE PEDIDOS-->
                    <div class="tab-pane fade" id="pedidos" role="tabpanel" aria-labelledby="pedidos-tab">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="tableUser">Cod. Pedido</th>
                                        <th class="tableUser">Fecha</th>
                                        <th class="tableUser">Estado</th>
                                        <th class="tableUser">Importe total</th>
                                        <th class="tableUser">Factura</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($arr_order as $val): ?>
                                        <tr>
                                            <td><?=$val['code']?></td>
                                            <td><?$val['formed_date']?></td>
                                            <td><?=$val['formed_status']?></td>
                                            <td><?=$val['total']?> € </td>
                                            <td><a href="#" class="view"><i class="fa fa-cloud-download css-fonsize16 css-color-azul"></i> Factura</a></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--END. TAB DE PEDIDOS-->

                    <!--TAB DE DIRECCIONES -->
                    <div class="tab-pane fade" id="direcciones" role="tabpanel" aria-labelledby="direcciones-tab">
                        <div class="col-xs-12 col-md-8 offset-md-2 checkout_form">
                            <form name="frm-address" id="frm-address" method="post" class="css-frm" action="">
                                <div class="js-alert"></div>

                                <div class="row">
                                    <div class="row">
                                        <div class="col-12 mb-20">
                                            <label>Nombre dirección</label>
                                            <input name="location_name" id="location_name" type="text" placeholder="indica un nombre para recordar esta dirección." value="<?=$location_name?>">
                                            <span class="text-danger" id="location_name_validate"></span>
                                        </div>

                                        <div class="col-6 mb-20 js-country">
                                            <label for="country">País <span>*</span></label>
                                            <select class="niceselect_option" name="addr_country" id="addr_country">
                                                <option value="">Seleccione un país</option>
                                                <?php foreach ($arr_country as $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelected($val['id'], $addr_country)?>><?=$val['country']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span class="text-danger" id="addr_country_validate"></span>
                                        </div>

                                        <div class="col-6 mb-20 js-province">
                                            <label for="province">Provincia <span>*</span></label>
                                            <select class="niceselect_option" name="addr_province" id="addr_province">
                                                <?php foreach ($arr_province as $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelected($val['id'], $addr_province)?>><?=$val['name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span class="text-danger" id="addr_province_validate"></span>
                                        </div>

                                        <div class="col-12 mb-20">
                                            <label>Dirección  <span>*</span></label>
                                            <input name="addr_location_1" id="addr_location_1" type="text" placeholder="Calle, código postal, nombre de empresa, c / o" value="<?=$addr_address_1?>">
                                            <span class="text-danger" id="addr_location_1_validate"></span>
                                        </div>

                                        <div class="col-12 mb-20">
                                            <input type="text" name="addr_location_2" id="addr_location_2" placeholder="Apartamento, suite, unidad, edificio, piso, etc" value="<?=$addr_address_2?>">
                                            <span class="text-danger" id="addr_location_2_validate"></span>
                                        </div>

                                        <div class="col-6 mb-20">
                                            <label>Ciudad <span>*</span></label>
                                            <input  type="text" name="addr_city" id="addr_city" value="<?=$addr_city?>">
                                            <span class="text-danger" id="addr_city_validate"></span>
                                        </div>
                                        <div class="col-6 mb-20">
                                            <label>Código postal <span>*</span></label>
                                            <input type="text" name="addr_postal_code" id="addr_postal_code" value="<?=$addr_postal_code?>">
                                            <span class="text-danger" id="addr_postal_code_validate"></span>
                                        </div>

                                        <div class="col-12">
                                            <div class="order-notes">
                                                <label for="order_note">Información adicional </label>
                                                <textarea id="addr_order_note" name="addr_order_note" rows="4"><?=$addr_order_note?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-8 offset-2 css-marginT20">
                                        <div class="col-12">
                                            <div class="cuenta_button">
                                                <button type="submit" class="btn-submit">guardar datos ></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--END.TAB DE DIRECCIONES-->

                    <!--TAB DE DATOS CUPONES-->
                    <div class="tab-pane fade" id="cupones" role="tabpanel" aria-labelledby="cupones-tab">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="tableUser">Nº Vale</th>
                                        <th class="tableUser">Fecha incio</th>
                                        <th class="tableUser">Fecha de fin</th>
                                        <th class="tableUser">Descuento</th>
                                        <th class="tableUser">Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>BLAKCFRIDAY2019</td>
                                        <td>28-03-2019</td>
                                        <td>15-04-2019</td>
                                        <td><span class="success"><strong>10%</strong></span></td>
                                        <td><i class="fa fa-thumbs-up css-fonsize16 css-color-verde"></i> sin utlizar</td>
                                    </tr>
                                    <tr>
                                        <td>DIADELPADRE2019</td>
                                        <td>12-03-2019</td>
                                        <td>120-03-2019</td>
                                        <td><span class="success"><strong>10%</strong></span></td>
                                        <td><i class="fa fa-times-circle css-fonsize16 css-color-naranja"></i> caducado</td>
                                    </tr>
                                    <tr>
                                        <td>REYES2019</td>
                                        <td>201-12-2018</td>
                                        <td>04-01-2019</td>
                                        <td><span class="success"><strong>10%</strong></span></td>
                                        <td><i class="fa fa-thumbs-down css-fonsize16 css-color-rojo"></i> ya ha sido utlizado</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--END.TABLE RESPONSIVE-->
                    </div>
                    <!--END.TAB DE CUPONES-->

                    <!-- Tab favoritos -->
                    <div class="tab-pane fade" id="favorites" role="tabpanel" aria-labelledby="favorites-tab">
                        <div class="css-overlay-load" id="js-overlay"><i class="fa fa-spinner fa-spin"></i></div>

                        <div class="table-responsive js-favorites">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="tableUser">Ref</th>
                                        <th class="tableUser">Producto</th>
                                        <th class="tableUser">Marca</th>
                                        <th class="tableUser">Categoría</th>
                                        <th class="tableUser">Precio</th>
                                        <th class="tableUser">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($arr_favorites as $key => $fav): ?>
                                        <tr>
                                            <td><?=($key + 1)?></td>
                                            <td><?=$fav['product_name']?></td>
                                            <td><?=$fav['brand_name']?></td>
                                            <td><?=$fav['category_name']?></td>
                                            <td><?=$fav['price']?> &euro;</td>
                                            <td class="text-center">
                                                <a href="PRODUCTO/<?=$fav['product_id']?>/<?=friendlyURL($fav['product_name'], 'upper')?>" class="btn btn-info css-text-white"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                <a href="#" class="btn btn-danger css-text-white remove-favorite" data-fav="<?=$fav['id']?>"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                    <?php if (count($arr_favorites) == 0): ?>
                                        <tr>
                                            <td colspan="6" style="text-transform: none;">No tienes ningún favorito agregado</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- tab opiniones -->
                    <div class="tab-pane fade" id="opinions" role="tabpanel" aria-labelledby="favorites-tab">
                        <div class="css-overlay-load" id="js-overlay"><i class="fa fa-spinner fa-spin"></i></div>

                        <div class="table-responsive js-favorites">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="tableUser">Orden</th>
                                        <th class="tableUser">Producto</th>
                                        <th class="tableUser">Categoría</th>
                                        <th class="tableUser">Marca</th>
                                        <th class="tableUser">Puntuación</th>
                                        <th class="tableUser">Fecha</th>
                                        <th class="tableUser">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($arr_features as $key => $val): ?>
                                        <tr>
                                            <td><?=$val['order_code']?></td>
                                            <td><?=$val['product_name']?></td>
                                            <td><?=$val['category_name']?></td>
                                            <td><?=$val['brand_name']?></td>
                                            <td>
                                                <?php if ($val['status'] == 0): ?>
                                                    <span class="css-icon-yellow">Pendiente</span>
                                                <?php else: ?>
                                                    <?=$val['score']?> <i class="fa fa-star css-icon-yellow"></i>
                                                <?php endif; ?>

                                            </td>
                                            <td><?=$val['formed_date']?></td>
                                            <td class="text-center">
                                                <a href="OPINION/<?=$val['id']?>" class="btn btn-info css-text-white"><i class="fa fa-search" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                    <?php if (count($arr_features) == 0): ?>
                                        <tr>
                                            <td colspan="6" style="text-transform: none;">No tienes opiniones</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php include("html/overall/footer.php"); ?>
        </div>
        <!--!FINAL home_three_body_wrapper ---->
        <?php include("html/overall/modal.php"); ?>

        <!-- JS
        ============================================ -->
        <?php include("html/overall/js.php"); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-dashboard.js"></script>
    </body>
</html>
