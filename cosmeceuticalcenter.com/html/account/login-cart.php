<!doctype html>
<html class="no-js" lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <title> Paso 01 | <?=APP_TITLE?> </title>
        <meta name="Description" CONTENT=" " />
        <meta name="Keywords" CONTENT="" />
        <?php include('html/overall/header.php'); ?>
        <link rel="stylesheet" href="<?=APP_ASSETS?>css/css-cart.css">
    </head>

    <body>
        <div class="home_three_body_wrapper">
            <?php include("html/overall/navbar.php"); ?>

            <!--breadcrumbs-->
            <div class="breadcrumbs_area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb_content">
                                <h3>Cesta de la compra</h3>
                                <ul>
                                    <li><a href="HOME">Home</a></li>
                                    <li>></li>
                                    <li>Cesta Compra</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end.breadcrumbs-->

            <!--PASOS CESTA ================================================== -->
            <div class="contenidos">
                <div class="container">
                    <div class="row">
                        <div class=" col-12 col-sm-8 offset-sm-2">
                            <div class="process">
                                <div class="process-row">
                                    <div class="process-step">
                                        <button id="iconBasicos" type="button" class="btn   btn-success btn-circle" disabled="disabled"><i class="fa fa-user fa-3x "></i></button>
                                        <h2>PASO 01</h2><p>ACCESO / REGISTRO</p>
                                    </div>
                                    <div class="process-step">
                                        <button id="iconUbicacion" type="button" class="btn  btn-default  btn-circle" disabled="disabled"><i class="fa fa-shopping-cart fa-3x "></i></button>
                                        <h2>PASO 02</h2><p>REALIZAR PAGO</p>
                                    </div>
                                    <div class="process-step">
                                        <button type="button" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-thumbs-up fa-3x "></i></button>
                                        <h2>PASO 03</h2><p>VALIDACIÓN PAGO</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.END PASO CESTA -->

            <!--CONTENIDO-->
            <div class="Checkout_section textura_Gris">
                <div class="container">
                    <div class="row">
						
						
						
                        <div class="col-lg-6 col-md-6">
                            <div class="account_form">
                                <h2>Acceso a tu cuenta</h2>

                                <form name="frm-login" id="frm-login" method="post" action="">
                                    <div class="js-alert"></div>

                                    <p>
                                        <label>Email<span>*</span></label>
                                        <input type="text" name="email" id="email">
                                        <span class="text-danger" id="email_validate"></span>
                                    </p>

                                    <p>
                                        <label>Passwords <span>*</span></label>
                                        <input type="password" name="pass" id="pass">
                                        <span class="text-danger" id="pass_validate"></span>
                                    </p>
													
													<p class="bg-warning" style="padding: 15px; color: #000; background-color:#F4E7C2 !important;">Si ya eres cliente, solo tendrás que recuperar tu contraseña introduciendo tu mail, para poder acceder a la nueva área de clientes.</p>

                                    <div class="login_submit">
                                        <a href="#" class="js-forgot">¿Has olvida tú contraseña?</a>
                                        
                                        <button type="submit" class="btn-submit">Acceder</button>
                                    </div>
                                </form>
                            </div>
                        </div>
						
						
						
                        <!--register area start-->
                        <div class="col-lg-6 col-md-6">
                            <div class="account_form register">
                                <h2>Crear una cuenta</h2>
                                <!-- <form action="#">
						            <a style="margin-top: 20px; margin-left: 20px;" class="button" href="REGISTRO">Crear cuenta &gt; </a>
                                </form> -->
                                <form name="frm-register" id="frm-register" method="post" action="">
                                    <div class="js-alert-r"></div>

                                    <p>
                                        <label>Email<span>*</span></label>
                                        <input type="text" name="r_email" id="r_email">
                                        <span class="text-danger" id="r_email_validate"></span>
                                    </p>

                                    <p>
                                        <label>Passwords <span>*</span></label>
                                        <input type="password" name="r_pass" id="r_pass">
                                        <span class="text-danger" id="r_pass_validate"></span>
                                    </p>
                                    
                                    <div class="login_submit">
                                        <button type="submit" class="btn-submit">Crear cuenta</button>
                                    </div>
                                </form>
                            </div>    
                        </div>
                        <!--register area end-->
					</div>
                </div>
            </div>
            <!--END.CONTENIDO-->

            <?php include("html/overall/footer.php"); ?>
            <?php include("html/overall/modal.php"); ?>
            <!-- Modal olvide mi contraseña -->
            <div class="modal fade" id="mod-forgot" tabindex="-1" role="dialog"  aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document" style="min-width: 500px;">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="modal_body">
                            <div class="form-group css-paddingL10 css-paddingR10">
                                <form id="frm-forgot" name="frm-forgot" method="post" action="">
                                    <label for="forgot_email">Ingresa tu email</label>
                                    <input type="text" class="form-control" id="forgot_email" name="forgot_email" aria-describedby="emailHelp">
                                    <small id="forgot_email_validate" class="form-text text-muted">Se enviará un email a tu cuenta con una contraseña temporal</small>

                                    <button type="submit" class="btn btn-black btn-submit">Envíar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="key" data-act="2"></div>
        <!-- JS
        ============================================ -->
        <?php include("html/overall/js.php"); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-login.js"></script>
    </body>
</html>
