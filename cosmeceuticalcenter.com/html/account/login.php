<!DOCTYPE html>
<html class="no-js" lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <title> HOME | <?=APP_TITLE?> </title>
        <meta name="Description" CONTENT="<?=@$arr_seo[0]['description']?>" />
        <meta name="Keywords" CONTENT="<?=@$arr_seo[0]['keywords']?>" />
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="home_three_body_wrapper">
            <!--header area start-->
            <?php include("html/overall/navbar.php"); ?>
            <!--header area end-->

            <!--breadcrumbs-->
            <div class="breadcrumbs_area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb_content">
                                <h3>Mi cuenta</h3>
                                <ul>
                                    <li><a href="HOME">home</a></li>
                                    <li>></li>
                                    <li>Mi cuenta</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end.breadcrumbs-->

            <!-- customer login start -->
            <div class="customer_login">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="account_form">
								
											
								
                                <h2>Acceso a tu cuenta</h2>

                                <form name="frm-login" id="frm-login" method="post" action="">
                                    <div class="js-alert"></div>

                                    <p>
                                        <label>Email<span>*</span></label>
                                        <input type="text" name="email" id="email">
                                        <span class="text-danger" id="email_validate"></span>
                                    </p>

                                    <p>
                                        <label>Passwords <span>*</span></label>
                                        <input type="password" name="pass" id="pass">
                                        <span class="text-danger" id="pass_validate"></span>
                                    </p>
									
													<p class="bg-warning" style="padding: 15px; color: #000; background-color:#F4E7C2 !important;">Si ya eres cliente, solo tendrás que recuperar tu contraseña introduciendo tu mail, para poder acceder a la nueva área de clientes.</p>

                                    <div class="login_submit">
                                        <a href="#" class="js-forgot"> <i class="fa fa-lock" aria-hidden="true"></i> ¿Has olvida tú contraseña?</a>

                                        <label for="remember">
                                            <input id="remember" type="checkbox">
                                            Recordar contraseña
                                        </label>
                                        <button type="submit" class="btn-submit">Acceder</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- customer login end -->

            <?php include("html/overall/brands-nicho.php"); ?>
            <?php include("html/overall/newsletter-footer.php"); ?>
            <?php include("html/overall/footer.php"); ?>
        </div>
        <?php include("html/overall/modal.php"); ?>

        <!-- Modal olvide mi contraseña -->
        <div class="modal fade" id="mod-forgot" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document" style="min-width: 500px;">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="modal_body">
                        <div class="form-group css-paddingL10 css-paddingR10">
                            <form id="frm-forgot" name="frm-forgot" method="post" action="">
                                <label for="forgot_email">Ingresa tu email</label>
                                <input type="text" class="form-control" id="forgot_email" name="forgot_email" aria-describedby="emailHelp">
                                <small id="forgot_email_validate" class="form-text text-muted">Se enviará un email a tu cuenta con una contraseña temporal</small>

                                <button type="submit" class="btn btn-black btn-submit">Envíar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="key" data-act="1"></div>
        <!-- JS
        ============================================ -->
        <?php include("html/overall/js.php"); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-login.js"></script>
    </body>
</html>
