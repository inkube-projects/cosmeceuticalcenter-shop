<!DOCTYPE html>
<html class="no-js" lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <title> Notificaciones | <?=APP_TITLE?> </title>
        <meta name="Description" CONTENT=" " />
        <meta name="Keywords" CONTENT="" />
        <?php include('html/overall/header.php'); ?>
        <link rel="stylesheet" href="<?=APP_ASSETS?>css/css-cart.css">
        <style media="screen">
            .borrarCart {
                font-size: 18px;
                color: #FF0004;
                margin-top: 7px;
            }
            .btn-actions {
                height: 64px;
            }
        </style>
    </head>

    <body>
        <div class="css-overlay-load" id="js-overlay"><i class="fa fa-spinner fa-spin"></i></div>
        <div class="home_three_body_wrapper">
            <!--header area start-->
            <?php include("html/overall/navbar.php"); ?>
            <!--header area end-->

            <!--breadcrumbs-->
            <div class="breadcrumbs_area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb_content">
                                <h3>Notificaciones</h3>
                                <ul>
                                    <li><a href="HOME">home</a></li>
                                    <li>></li>
                                    <li>Notificaciones</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end.breadcrumbs-->

            <!--CONTENIDO-->
            <div class="Checkout_section textura_Gris">
                <div class="container">
                    <div class="row">
                        <!--datos user-->
                        <div class="col-lg-7 col-md-7">
                            <form name="frm-cart" id="frm-cart" class="css-frm" method="post" action="">
                                <div class="js-alert"></div>

                                <div class="row">
                                    <?php foreach ($arr_notifications as $not): ?>
                                        <!--ITEM NOTIFICATION -->
                                        <div class="col-12 itemCart">
                                            <div class="row">
                                                <div class="col-xs-8 col-ms-3 col-sm-10 borderRightN bottomxs bottommd">
                                                    <p><span class="verde"><?=$not['title']?></span></p>
                                                    <div class="imgMarca">
                                                        <?=$not['formed_status']?>
                                                    </div>
                                                </div>

                                                <div class="col-xs-9 col-ms-5 col-sm-2">
                                                    <div style="width:100%; float:left; position:relative; height:100%; text-align:center;">
                                                        <a href="#" class="btn btn-info btn-actions btn-show" data-not="<?=$not['id']?>"><i class="fa fa-search borrarCart css-text-white" style="margin-top: 18px;"></i></a>
                                                        <button type="button" class="btn btn-danger btn-actions js-remove" data-not="<?=$not['id']?>"><i class="fa fa-trash-o borrarCart css-text-white"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--END ITEM NOTIFICATION -->
                                    <?php endforeach; ?>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--END.CONTENIDO-->

            <?php include("html/overall/footer.php"); ?>
            <?php include("html/overall/modal.php"); ?>
        </div>

        <!-- JS
        ============================================ -->
        <?php include("html/overall/js.php"); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-notifications.js"></script>
    </body>
</html>
