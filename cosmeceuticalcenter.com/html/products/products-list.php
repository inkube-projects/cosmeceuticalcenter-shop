<!DOCTYPE html>
<html class="no-js" lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <title> Productos | <?=APP_TITLE?> </title>
        <meta name="Description" CONTENT="<?=@$arr_seo[0]['description']?>" />
        <meta name="Keywords" CONTENT="<?=@$arr_seo[0]['keywords']?>" />
        <?php include('html/overall/header.php'); ?>
        <?php include("html/overall/analytics.php"); ?>
        <style>
            .list-group-horizontal {
                -ms-flex-direction: row;
                flex-direction: row;
            }

            .list-group-item {
                position: relative;
                display: block;
                padding: 8px 14px;
                margin-bottom: -1px;
                background-color: #fff;
                border: 1px solid rgba(0,0,0,.125);
                font-size: 12px;
                line-height: 14px;
                color:#A5A5A5;
            }

            .list-group-horizontal a:hover {
                text-decoration: none;
                color: #000;
            }
        </style>
    </head>

    <body>
    	<?php include("html/overall/cookies.php"); ?>
        <div class="home_three_body_wrapper">

            <?php include("html/overall/navbar.php"); ?>

            <!--breadcrumbs area start-->
            <div class="breadcrumbs_area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb_content">
                                <!--<h4>Marca</h4>-->
                                <ul>
                                    <li><a href="HOME">Home</a></li>
                                    <li>></li>
                                    <li>Productos</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--breadcrumbs area end-->

            <?php if (isset($cat_info)): ?>
                <div class="unlimited_services">
                    <div class="container">
                        <div class="row align-items-center">

                            <div class=" col-XS-12">
                                <h1 style="font-size: 32px; font-weight: 400;">
                                    <a class="css-link-gray" href="PRODUCTOS/CATEGORIA/<?=$cat_info['id']?>/<?=friendlyURL($cat_info['name'], 'upper')?>"><?=$cat_info['name']?> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                </h1>
                                <!-- <h5>Titulo de categoria o frase destacada</h5> -->
                                <p style="font-size: 12px;">
                                    <?=$cat_info['description']?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container" style="padding-top: 40px;">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="list-group list-group-horizontal">
                                <?php foreach ($arr_subcategories as $key => $val): ?>
                                    <li class="list-group-item"><a href="SUBCATEGORIA/<?=$val['id']?>/<?=$cat_info['name_canonical']?>"><?=$val['name']?></a> </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($brand != "" && $brand != 0): ?>
                <div class="unlimited_services">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-3 col-md-12 ">
                                <div class="services_section_thumb">
                                    <img src="<?=$arr_brand['formed_image']?>" alt="">
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-12  offset-1">
                                <h2 style="font-size: 25px; font-weight: 400;"><?=$arr_brand['name']?></h2>
                                <?=$arr_brand['description']?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>


            <!--shop  area start-->
            <div class="shop_area shop_fullwidth shop_reverse">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="shop_toolbar">
                                <div class="list_button">
                                    <!--<ul class="nav" role="tablist">
                                        <li>
                                            <a class="active" data-toggle="tab" href="shop-fullwidth.html#large" role="tab" aria-controls="large" aria-selected="true"><i class="ion-grid"></i></a>
                                        </li>
                                        <li>
                                            <a  data-toggle="tab" href="shop-fullwidth.html#list" role="tab" aria-controls="list" aria-selected="false"><i class="ion-ios-list-outline"></i> </a>
                                        </li>
                                    </ul>-->
                                </div>
                                <div class="orderby_wrapper">
                                    <h3>Ordenar por : </h3>
                                    <div class=" niceselect_option">
                                        <form class="select_option" action="#">
                                            <select name="sort" id="sort">
                                                <?php foreach (self::$allowed_sort as $key => $val): ?>
                                                    <option value="<?=$key?>" <?=isSelected($key, $select_sort)?>><?=$val?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </form>
                                    </div>

                                    <div class="page_amount">
                                        <p>Total <?=$rangue['min']?>–<?=$rangue['max']?> de <?=$total_results?> resultados</p>
                                    </div>
                                </div>
                            </div>
                            <!--shop toolbar end-->

                            <!--shop tab product start-->
                            <div class="tab-content">
                                <div class="tab-pane grid_view fade show active" id="large" role="tabpanel">
                                    <div class="js-alert"></div>

                                    <div class="row">
                                        <?php foreach ($arr_products as $val): ?>
                                            <div class="col-lg-3 col-md-4 col-sm-6">
                                                <div class="single_product">
                                                    <div class="product_thumb">
                                                        <!-- <a class="primary_img" href="#"><img src="<?=$val['formed_image']?>" alt=""></a>
                                                        <a class="secondary_img" href="#"><img src="<?=$val['formed_image']?>" alt=""></a> -->
                                                        <a class="primary_img" href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['formed_id']?>">
                                                            <div class="css-product-list-img" style="background-image: url('<?=$val['formed_image']?>')"></div>
                                                        </a>
                                                        <a class="secondary_img" href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['formed_id']?>">
                                                            <div class="css-product-list-img" style="background-image: url('<?=$val['formed_image']?>')"></div>
                                                        </a>
                                                        <div class="quick_button">
                                                            <a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['formed_id']?>" data-product="<?=$val['formed_id']?>" class="js-product-view" data-placement="top" data-original-title="<?=$val['formed_price']?> €"> Vista previa</a>
                                                        </div>
                                                    </div>

                                                    <div class="product_content">
                                                        <div class="tag_cate">
                                                            <a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['formed_id']?>"><?=$val['brand_name']?></a>
                                                        </div>
                                                        <h3>
                                                            <a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['formed_id']?>"><?=$val['product_name']?></a>
                                                        </h3>
                                                        <p class="css-height60 css-fontSize12"><?=sanitize($val['description_short'])?></p>
                                                        <div class="price_box">
                                                            <?php if ($val['formed_old_price']): ?>
                                                                <span class="old_price"><?=$val['formed_old_price']?> €</span>
                                                            <?php endif; ?>
                                                            <span class="current_price"><?=$val['formed_price']?> €</span>

                                                            <!-- <span class="current_price"><?=$val['calculed_price']?> €</span> -->
                                                        </div>
                                                        <div class="product_hover">
                                                            <div class="product_desc">
                                                                <p><?=substr(sanitize($val['product_description']),0,150)?>...</p>
                                                            </div>
                                                            <div class="action_links">
                                                                <ul>
                                                                    <li class="add_to_cart"><a href="#" class="js-add-cart" data-product="<?=$val['formed_id']?>" data-view="l"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>

                                <div class="tab-pane list_view fade " id="list" role="tabpanel">

                                </div>
                            </div>

                            <!-- Paginador -->
                            <div class="shop_toolbar t_bottom">
                                <?=$index_pages?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--shop  area end-->
            <?php include("html/overall/brands-nicho.php"); ?>
            <?php include("html/overall/newsletter-footer.php"); ?>
            <?php include("html/overall/footer.php"); ?>
        </div>
        <!--!FINAL home_three_body_wrapper ---->
        <?php include("html/overall/modal.php"); ?>

        <!-- JS
        ============================================ -->
        <?php include("html/overall/js.php"); ?>
    </body>

    <script type="text/javascript">
        $(function(e){
            $('ul.list .option').on('click', function(){
                window.location = '<?=BASE_URL?>PRODUCTOS/' + $(this).data('value');
            });
        });
    </script>
</html>
