<!DOCTYPE html>
<html class="no-js" lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <title> Productos | <?=@$arr_seo[0]['title']?> </title>
        <meta name="Description" CONTENT="<?=@$arr_seo[0]['description']?>" />
        <meta name="Keywords" CONTENT="<?=@$arr_seo[0]['keywords']?>" />
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/bootstrap/css/bootstrap.css">
        <?php include('html/overall/header.php'); ?>
        <?php include("html/overall/analytics.php"); ?>
        <style media="screen">
            .product_d_meta span { margin-bottom: 2px; }
            .wrap-drop {
                background: #ededed;
                /* box-shadow: 3px 3px 3px rgba(0,0,0,.2); */
                cursor: pointer;
                margin: 0 auto;
                /* max-width: 225px; */
                padding: 16px 10px 16px 20px;
                position: relative;
                width: 100%;
                z-index: 3;
                font-size: 16px;
            }

            .wrap-drop::after {
                border-color: #000 transparent;
                border-style: solid;
                border-width: 6px 6px 0;
                content: "";
                height: 0;
                margin-top: -4px;
                position: absolute;
                right: 1rem;
                top: 50%;
                width: 0;
            }

            .wrap-drop .drop {
                background: #fff;
                /* box-shadow: 3px 3px 3px rgba(0,0,0,.2); */
                display: none;
                left: 0;
                list-style: none;
                margin-top: 0;
                opacity: 0;
                padding-left: 0;
                pointer-events: none;
                position: absolute;
                right: 0;
                top: 100%;
                z-index: 2;
            }

            .wrap-drop .drop li a {
                color: #747474;
                display: block;
                padding: 16px 10px 16px 30px;
                text-decoration: none;
                border-top: solid 1px #FFF;
                background: #ededed;
            }

            .wrap-drop span {
            color:#000;
            }

            .wrap-drop .drop li:hover a {
                background-color: #ededed;
                color: #000;
            }

            .wrap-drop.active::after {
                border-width:0  6px 6px;
            }

            .wrap-drop.active .drop {
                display:block;
                opacity:1;
                pointer-events:auto;
            }
        </style>
    </head>

    <body>
        <?php include("html/overall/cookies.php"); ?>
        <div class="home_three_body_wrapper">
            <?php include("html/overall/navbar.php"); ?>

            <!--breadcrumbs area start-->
            <div class="breadcrumbs_area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb_content">
                                <!--<h4>Marca</h4>-->
                                <ul>
                                    <li><a href="HOME">Home</a></li>
                                    <li>></li>
                                    <li><a href="PRODUCTOS">Productos</a></li>
                                    <li>></li>
                                    <li><?=$name?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--breadcrumbs area end-->

            <!--product details start-->
            <div class="product_details variable_product">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="product-details-tab">
                                <div id="img-1" class="zoomWrapper single-zoom">
                                    <a href="javascript:;">
                                        <img id="zoom1" src="<?=$arr_gallery[0]['formed_image']?>" class="img-responsive" data-zoom-image="<?=$arr_gallery[0]['formed_image']?>" alt="big-1">
                                    </a>
                                </div>

                                <?php if (count($arr_gallery) > 1): ?>
                                    <div class="single-zoom-thumb">
                                        <ul class="s-tab-zoom owl-carousel single-product-active" id="gallery_01">
                                            <?php foreach ($arr_gallery as $key => $val): $active = ($key == 0) ? 'active' : '' ; ?>
                                                <li>
                                                    <a href="javascript:;" class="elevatezoom-gallery <?=$active?>" data-update="" data-image="<?=$val['formed_image']?>" data-zoom-image="<?=$val['formed_image']?>">
                                                        <img src="<?=$val['formed_image']?>" alt="zo-th-1"/>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="product_d_right">
                                <form action="#">
                                    <h4><a href="MARCA/<?=$brand_name_canonical?>"><?=$brand_name?></a></h4>
                                    <h1><?=$name?></h1>

                                    <div class="product_nav">
                                        <ul>
                                            <?php if (isset($arr_prev[0]['id'])): ?>
                                                <li class="next"><a href="PRODUCTO/<?=$arr_prev[0]['brand_name_canonical']?>/<?=friendlyURL($arr_prev[0]['product_name'], 'upper')?>/<?=$arr_prev[0]['id']?>"><i class="fa fa-angle-left"></i></a></li>
                                            <?php endif; ?>

                                            <?php if (isset($arr_next[0]['id'])): ?>
                                                <li class="next"><a href="PRODUCTO/<?=$arr_next[0]['brand_name_canonical']?>/<?=friendlyURL($arr_next[0]['product_name'], 'upper')?>/<?=$arr_next[0]['id']?>"><i class="fa fa-angle-right"></i></a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>

                                    <div class=" product_ratting">
                                        <ul>
                                            <?php for ($i = 1; $i <= 5; $i++): $class = ($i <= $media) ? "fa-star css-icon-yellow" : "fa-star-o" ?>
                                                <li><i class="fa <?=$class?>"></i></li>
                                            <?php endfor; ?>
                                            <!-- <li> <a href="#OpninoneS"> <strong>30</strong> opniones de clientes <i class="fa fa-angle-down"></i></a></li> -->
                                        </ul>
                                    </div>

                                    <div class="product_price">
                                        <?php if ($formed_old_price): ?>
                                            <span class="old_price"><?=$formed_old_price?> €</span>
                                        <?php endif; ?>
                                        <span class="current_price"><?=$formed_price?> €</span>
                                    </div>
                                    <div class="product_desc">
                                        <h4>Presentación</h4>
                                        <div class="js-presentation">
                                            <?=$presentation?>
                                        </div>
                                    </div>

                                    <?php if (count($arr_products_sizes) > 0): ?>
                                        <div class="col-md-4">
                                            <h2 class="css-fontSize23">Tallas:</h2>
                                            <select class="form-control" name="product_size" id="product_size">
                                                <option value="">Seleccione una talla</option>
                                                <?php foreach ($arr_products_sizes as $key => $val): ?>
                                                    <option value="<?=$val['size_id']?>"><?=$val['size_info']['name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    <?php endif; ?>

                                    <div class="clearfix css-marginB20"></div>

                                    <?php if (count($arr_products_color) > 0): ?>
                                        <div class="col-md-4">
                                            <h2 class="css-fontSize23">Colores:</h2>

                                            <select class="form-control" name="product_color" id="product_color">
                                                <option value="">Seleccione un color</option>
                                                <?php foreach ($arr_products_color as $key => $val): ?>
                                                    <option value="<?=$val['color_id']?>"><?=$val['color_info'][0]['name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="css-colors css-show-desktop">
                                                <?php foreach ($arr_products_color as $key => $val): ?>
                                                    <div class="pull-left text-center">
                                                        <div class="js-btn-color css-color center-block" data-color="<?=$val['color_id']?>" style="background-color: #<?=$val['color_info'][0]['color']?>"></div>
                                                        <?=$val['color_info'][0]['name']?>
                                                    </div>
                                                <?php endforeach; ?>

                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <div class="clearfix css-marginT50"></div>
                                    <div class="js-alert"></div>

                                    <?php if ($inventory > 0 || count($arr_products_color) > 0 || count($arr_products_sizes) > 0): ?>
                                        <div class="product_variant quantity">
                                            <label>Cantidad</label>
                                            <input min="0" max="100" value="1" type="number" name="quantity" id="quantity" class="only-number">
                                            <button class="button js-add-cart-detail" type="button" data-product="<?=$id?>" data-view="s"> añadir a la cesta </button>
                                            <a href="CARRITO" class="css-btn css-btn-my-cart css-btn-gray text-center"><i class="fa fa-shopping-cart" aria-hidden="true"></i> ver cesta</a>
                                        </div>
                                    <?php endif; ?>

                                    <div class="product_d_action">
                                        <ul>
                                            <li><a href="#" title="Mis favoritos" class="js-add-favorite" data-p="<?=$id?>"> <i class="fa fa-heart css-fonsize18" aria-hidden="true"></i> + añadir a mis favoritos</a></li>
                                        </ul>
                                    </div>
                                </form>

                                <div class="product_d_meta">
                                    <span>STOCK: <?=$icon?></span>
                                    <span class="css-color-crema css-marginT20"><?=$icon_message?></span>

                                    <span>Contenido:</span>
                                    <span class="css-color-crema"><?=$content?></span>

                                    <?php if ($inventory == 0 && count($arr_products_color) == 0 && count($arr_products_sizes) == 0): ?>
                                        <?php if (isset($_SESSION['USER_SESSION_COSME']['id'])): ?>
                                            <div class="js-alert"></div>

                                            <div class="css-marginT10">
                                                AVISO: <i class="fa fa-exclamation-triangle css-icon-yellow" aria-hidden="true"></i>
                                                <p class="css-fontSize11">Te avisamos cuando haya stock <a href="#" onclick="notifyUser(event, <?=$id?>)" class="css-fontSize12 css-link-black">AVISAME > </a></p>
                                                <div class="text-danger css-marginB10" id="notification_email_validate"></div>
                                            </div>
                                        <?php else: ?>
                                            <div class="js-alert"></div>

                                            <div class="css-marginT10">
                                                AVISO: <i class="fa fa-exclamation-triangle css-icon-yellow" aria-hidden="true"></i>
                                                <p class="css-fontSize11">Te avisamos cuando haya stock <a href="#" class="css-fontSize12 css-link-black let-me-know">AVISAME > </a></p>
                                                <div class="text-danger css-marginB10" id="notification_email_validate"></div>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <span class="css-marginT20">CATEGORÍA:
                                        <?php foreach ($arr_categories as $key => $val): echo ($key == 0) ? "" : ","; ?>
                                            <a href="CATEGORIA/<?=$val['category_name_canonical']?>" class="css-color-crema"><?=$val['name']?></a>
                                        <?php endforeach; ?>
                                    </span>
                                    <span class="css-marginT20">TAGS: <a href="#" class="css-color-crema no-action"><?=$tags?></a></span>
                                    <!-- <span>Tags:
                                        <a href="#">Crema</a>
                                        <a href="#">Antieging</a>
                                    </span> -->
                                </div>
                                <div class="priduct_social">
                                    <span>COMPÁRTELO:</span>
									<!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox"></div>
									<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5cf15f056e3ebbf8"></script>
                                    <!--<ul>
                                        <li><a href="#" title="facebook"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#" title="twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href=""javascript:;" title="pinterest"><i class="fa fa-pinterest"></i></a></li>
                                        <li><a href="#" title="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--product details end-->

            <!--product info start-->
            <div class="product_d_info">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="product_d_inner">
                                <!--TABS ESCRITORIO-->
                                <div class="product_info_button d-none d-sm-none d-md-block">
                                    <ul class="nav" role="tablist" id="js-tabs">
                                        <li >
                                            <a class="active" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="false">Descripción</a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#active_principles" role="tab" aria-controls="active_principles" aria-selected="false">Principios activos</a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#prescription" role="tab" aria-controls="prescription" aria-selected="false">Prescripción</a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#use_recommendations" role="tab" aria-controls="use_recommendations" aria-selected="false">Recomendaciones</a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#results_benefits" role="tab" aria-controls="results_benefits" aria-selected="false">Resultados & Beneficios </a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#features" role="tab" aria-controls="features" aria-selected="false">Comentarios </a>
                                        </li>
                                    </ul>
                                </div>
                                <!--END TABs ESCRITORIO-->

                                <!-- <div class="wrap-drop d-block d-sm-block d-md-none" id="select01">
                                    <span>Descripción</span>
                                    <ul class="nav drop" role="tablist">
                                        <li  class="selected">
                                            <a class="active" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="false">Descripción</a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab"  onclick="window.location.href('#active_principles');" role="tab" aria-controls="active_principles" aria-selected="false">Principios activos</a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#prescription" role="tab" aria-controls="prescription" aria-selected="false">Prescripción</a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#use_recommendations" role="tab" aria-controls="use_recommendations" aria-selected="false">Recomendaciones</a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#results_benefits" role="tab" aria-controls="results_benefits" aria-selected="false">Resultados & Beneficios </a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#features" role="tab" aria-controls="features" aria-selected="false">Comentarios </a>
                                        </li>
                                    </ul>
                                </div> -->

                                <div class="wrap-drop d-block d-sm-block d-md-none" id="select01">
                                    <select name="tabs" id="tabs" class="form-control">
                                        <option value="0">Descripción</option>
                                        <option value="1">Principios activos</option>
                                        <option value="2">Prescripción</option>
                                        <option value="3">Recomendaciones</option>
                                        <option value="4">Resultados & Beneficios</option>
                                        <option value="5">Comentarios</option>
                                    </select>
                                </div>

                                <div class="tab-content">
                                    <!-- Descripción -->
                                    <div class="tab-pane fade show active" id="description" role="tabpanel">
                                        <div class="product_info_content">
                                            <?=$description?>
                                        </div>
                                    </div>

                                    <!-- Principios activos -->
                                    <div class="tab-pane fade" id="active_principles" role="tabpanel" >
                                        <?=$active_principles?>
                                    </div>

                                    <!-- Prescripción -->
                                    <div class="tab-pane fade" id="prescription" role="tabpanel" >
                                        <div class="product_info_content">
                                            <?=$prescription?>
                                        </div>
                                    </div>

                                    <!-- Recomendaciones -->
                                    <div class="tab-pane fade" id="use_recommendations" role="tabpanel" >
                                        <div class="product_info_content">
                                            <?=$use_recommendations?>
                                        </div>
                                    </div>

                                    <!-- Resultados y beneficios -->
                                    <div class="tab-pane fade" id="results_benefits" role="tabpanel" >
                                        <div class="product_info_content">
                                            <?=$results_benefits?>
                                        </div>
                                    </div>

                                    <!-- Opiniones -->
                                    <div class="tab-pane fade" id="features" role="tabpanel" >
                                        <div class="product_info_content">
                                            <?php foreach ($arr_features as $val): ?>
                                                <div class="media css-marginB20 css-border-bottom">
                                                    <div class="media-left media-middle css-marginR20">
                                                        <a href="#">
                                                            <img class="media-object" src="<?=APP_NO_IMAGES?>profile.jpg" alt="..." width="64">
                                                        </a>
                                                    </div>
                                                    <div class="media-body css-marginB20">
                                                        <h4 class="media-heading">
                                                            <?php for ($i = 0; $i < $val['score']; $i++): ?>
                                                                <i class="fa fa-star css-icon-yellow"></i>
                                                            <?php endfor; ?>
                                                        </h4>
                                                        <?=$val['review']?>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--product info end-->

            <!--OPNIONES CLIENTES -->
            <!-- <div id="OpinioneS" class="container" style="margin-bottom: 50px;">
            <div class="row">

            <div class="col-lg-12">
            <hr>
            </div>


            <div class="col-lg-8">

            <h4 style="margin-bottom: 30PX;">OPNINONES DE CLIENTES</h4>


            <div class="media mb-4">
            <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
            <div class="media-body">
            <p class="css-color-crema css-weight700">08-05-2019</p>
            <h5 class="mt-0 css-text-black">Commenter Name</h5>
            <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
            </div>
            </div>



            <div class="media mb-4">
            <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
            <div class="media-body">
            <p class="css-color-crema css-weight700">08-05-2019</p>
            <h5 class="mt-0 css-text-black">Commenter Name</h5>
            <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
            </div>
            </div>

            </div>

            <div class="col-lg-12">
            <hr>
            </div>


            </div>
            </div> -->
            <!--OPNIONES CLIENTES -->

            <!--product section area start-->
            <section class="product_section p_section1 related_product">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="section_title">
                                <h4>Productos relacionados</h4>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="product_area ">
                                <div class="product_container bottom">
                                    <?php $slick_class = (count($arr_related) >= 6) ? "product_row1" : "css-product-row";?>
                                    <div class="custom-row <?=$slick_class?>">
                                        <?php foreach ($arr_related as $key => $val): ?>
                                            <div class="custom-col-5">
                                                <div class="single_product">
                                                    <div class="product_thumb">
                                                        <a class="primary_img" href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['id']?>">
                                                            <div class="css-product-list-img" style="background-image: url('<?=$val['formed_image']?>')"></div>
                                                        </a>
                                                        <a class="secondary_img" href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['id']?>">
                                                            <div class="css-product-list-img" style="background-image: url('<?=$val['formed_image']?>')"></div>
                                                        </a>

                                                        <div class="quick_button">
                                                            <a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['id']?>" data-placement="top" data-original-title="<?=$val['calculated_price']?> €"> Ver más</a>
                                                        </div>
                                                    </div>
                                                    <div class="product_content">
                                                        <div class="tag_cate">
                                                            <a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['id']?>"><?=$val['brand_name']?></a>
                                                        </div>
                                                        <h3><a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['id']?>"><?=$val['product_name']?></a></h3>
                                                        <div class="price_box">
                                                            <!-- <span class="old_price">86.00 €</span> -->
                                                            <!-- <span class="current_price"><?=$val['calculated_price']?> €</span> -->
                                                            <?php if ($val['formed_old_price']): ?>
                                                                <span class="old_price"><?=$val['formed_old_price']?> €</span>
                                                            <?php endif; ?>
                                                            <span class="current_price"><?=$val['formed_price']?> €</span>
                                                        </div>
                                                        <div class="product_hover">
                                                            <div class="product_desc">
                                                                <p><?=substr(sanitize($val['product_description']),0,70)?>...</p>
                                                            </div>
                                                            <div class="action_links">
                                                                <ul>
                                                                    <li class="add_to_cart"><a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['id']?>"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--product section area end-->

            <?php include("html/overall/banner-prduct-election.php"); ?>
            <?php include("html/overall/newsletter-footer.php"); ?>
            <?php include("html/overall/footer.php"); ?>
            <div id="key" data-value="<?=$id?>"></div>
        </div>
    	<?php include("html/overall/modal.php"); ?>
        <div class="css-hide js-full-info"><?=$presentation?></div>
    </body>

    <!-- Modal para avisame -->
    <div class="modal fade" tabindex="-1" role="dialog" id="mod-colors">
        <div class="modal-dialog" role="document" style="max-width: 900px;">
            <div class="modal-content">
                <div class="modal-header text-left">
                    <h4 class="modal-title">Colores</h4>
                </div>

                <div class="modal-body" id="ajx-colors">

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- JS
    ============================================ -->
    <?php include("html/overall/js.php"); ?>
    <!-- jQuery validation -->
    <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
    <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
    <!-- Checkbox -->
    <script type="text/javascript" src="<?=APP_ASSETS?>js/js-checkbox.js"></script>
    <!-- Products JS -->
    <script type="text/javascript" src="<?=APP_ASSETS?>js/js-products.js"></script>
    <script type="text/javascript">
        var colorOption = [
            <?php foreach ($arr_color_image as $key => $val): ?>
                {
                    "color": "<?=$val['color_id']?>",
                    "image": "<?=$val['image']?>"
                },
            <?php endforeach; ?>
        ];

        var color_path = '<?=APP_IMG_PRODUCTS.'product_'.$id.'/gallery/'?>';
    </script>
</html>
