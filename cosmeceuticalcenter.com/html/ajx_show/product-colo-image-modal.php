<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="modal_tab">
                <div class="tab-content product-details-large">
                    <?php foreach ($arr_color_images as $key => $val): $active = ($key == 0) ? 'active' : '' ; ?>
                        <div class="tab-pane fade show <?=$active?>" id="tab<?=$key?>" role="tabpanel" >
                            <div class="modal_tab_img">
                                <img src="<?=$path.$val['image']?>" alt="">
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <div class="modal_tab_button">
                    <ul class="nav product_navactive owl-carousel" role="tablist">
                        <?php foreach ($arr_color_images as $key => $val): $active = ($key == 0) ? 'active' : '' ; ?>
                            <li>
                                <a class="nav-link <?=$active?>" data-toggle="tab" href="#tab<?=$key?>" role="tab" aria-controls="tab<?=$key?>" aria-selected="false">
                                    <img src="<?=$path.$val['image']?>" alt="">
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    /*---product navactive activation---*/
    $('.product_navactive').owlCarousel({
        autoplay: true,
        loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 4,
        dots:false,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            250:{
                items:2,
            },
            480:{
                items:3,
            },
            768:{
                items:4,
            },
        }
    });

    $('.product_navactive a').on('click',function(e){
        e.preventDefault();
        var $href = $(this).attr('href');
        $('.product_navactive a').removeClass('active');
        $(this).addClass('active');

        $('.product-details-large .tab-pane').removeClass('active show');
        $('.product-details-large '+ $href ).addClass('active show');
    });
</script>
