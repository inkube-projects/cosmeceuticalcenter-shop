<table class="table">
    <thead>
        <tr>
            <th class="tableUser">Ref</th>
            <th class="tableUser">Producto</th>
            <th class="tableUser">Marca</th>
            <th class="tableUser">Categoría</th>
            <th class="tableUser">Precio</th>
            <th class="tableUser">Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($arr_favorites as $key => $fav): ?>
            <tr>
                <td><?=($key + 1)?></td>
                <td><?=$fav['product_name']?></td>
                <td><?=$fav['brand_name']?></td>
                <td><?=$fav['category_name']?></td>
                <td><?=$fav['price']?> &euro;</td>
                <td class="text-center">
                    <a href="PRODUCTO/<?=$fav['product_id']?>/<?=friendlyURL($fav['product_name'], 'upper')?>" class="btn btn-info css-text-white"><i class="fa fa-search" aria-hidden="true"></i></a>
                    <a href="#" class="btn btn-danger css-text-white remove-favorite" data-fav="<?=$fav['id']?>"><i class="fa fa-times" aria-hidden="true"></i></a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
