<div class="container">
    <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-12">
            <div class="modal_tab">
                <div class="tab-content product-details-large">
                    <?php foreach ($arr_gallery as $key => $val): $active = ($key == 0) ? 'active' : '' ; ?>
                        <div class="tab-pane fade show <?=$active?>" id="tab<?=$key?>" role="tabpanel" >
                            <div class="modal_tab_img">
                                <img src="<?=$val['formed_image']?>" alt="">
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                
                <div class="modal_tab_button">
                    <ul class="nav product_navactive owl-carousel" role="tablist">
                        <?php foreach ($arr_gallery as $key => $val): $active = ($key == 0) ? 'active' : '' ; ?>
                            <li>
                                <a class="nav-link <?=$active?>" data-toggle="tab" href="#tab<?=$key?>" role="tab" aria-controls="tab<?=$key?>" aria-selected="false">
                                    <img src="<?=$val['formed_image']?>" alt="">
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-lg-7 col-md-7 col-sm-12">
            <div class="modal_right">
                <div class="tag_cate">
                    <a href="#" tabindex="-1"><?=$product['brand_name']?></a>
                </div>
                <div class="modal_title mb-10">
                    <h2><?=$product['product_name']?></h2>
                </div>
                <div class="modal_price mb-10 css-marginB0">
                    <span class="new_price"><?=$formed_price?> €</span>
                    <?php if ($formed_old_price): ?>
                        <span class="old_price"><?=$formed_old_price?> €</span>
                    <?php endif; ?>
                </div>

                <div class="css-marginB20">Contenido: <?=$content?></div>

                <div class="see_all">
                    <a href="PRODUCTO/<?=friendlyURL($product['brand_name'], 'upper')?>/<?=friendlyURL($product['product_name'], 'upper')?>/<?=$product['id']?>"> <i class="fa fa-info-circle css-fonsize20"></i> conoce los detalles del producto > </a>
                </div>
                <div class="modal_add_to_cart mb-15">
                    <form action="#">
                        <input min="0" max="100" step="2" value="1" type="number">
                        <!-- <button type="submit">añadir al carro</button> -->
                        <button class="button" type="button" onclick="addCart(<?=$product['id']?>, 's')"> añadir a la cesta </button>
                        <a href="CARRITO" class="css-btn css-btn-my-cart css-btn-gray text-center"><i class="fa fa-shopping-cart" aria-hidden="true"></i> cesta</a>
                    </form>
                </div>
                <div class="modal_description mb-15">
                    <p><?=substr(sanitize($product['presentation']),0,150)?>... <a href="PRODUCTO/<?=friendlyURL($product['brand_name'], 'upper')?>/<?=friendlyURL($product['product_name'], 'upper')?>/<?=$product['id']?>" class="css-more-link">Ver más</a></p>
                </div>

                <div>
                    <span>STOCK: <?=$icon?></span><br>
                    <span class="css-color-crema"><?=$icon_message?></span>
                    <?php if ($inventory == 0 && isset($_SESSION['USER_SESSION_COSME']['id'])): ?>
                        <div class="js-alert"></div>

                        <div class="css-marginT10">
                            AVISO: <i class="fa fa-exclamation-triangle css-icon-yellow" aria-hidden="true"></i>
                            <p class="css-fontSize11">Te avisamos cuando haya stock <a href="#" onclick="notifyUser(event, <?=$product['id']?>)" class="css-fontSize12 css-link-black">AVISAME > </a></p>
                            <div class="text-danger css-marginB10" id="notification_email_validate"></div>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="modal_social css-marginT20">
                    <h2>Compártelo:</h2>
					<!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox"></div>
									<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5cf15f056e3ebbf8"></script>
                    <!--<ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>-->
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    /*---product navactive activation---*/
    $('.product_navactive').owlCarousel({
        autoplay: true,
        loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 4,
        dots:false,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            250:{
                items:2,
            },
            480:{
                items:3,
            },
            768:{
                items:4,
            },
        }
    });

    // $('.modal').on('shown.bs.modal', function (e) {
    //     $('.product_navactive').resize();
    // });

    $('.product_navactive a').on('click',function(e){
        e.preventDefault();
        var $href = $(this).attr('href');
        $('.product_navactive a').removeClass('active');
        $(this).addClass('active');

        $('.product-details-large .tab-pane').removeClass('active show');
        $('.product-details-large '+ $href ).addClass('active show');
    });
</script>
