<div class="mini_cart_inner">
    <?php foreach ($arr_cart as $key => $val): ?>
        <div class="cart_item">
            <div class="cart_img">
                <a href="PRODUCTO/<?=$val['brand_name']?>/<?=friendlyURL($val['name'], 'upper')?>/<?=$val['product_id']?>">
                    <img src="<?=$val['formed_image']?>" alt="<?=$val['name']?>">
                </a>
            </div>

            <div class="cart_info">
                <a href="PRODUCTO/<?=$val['brand_name']?>/<?=friendlyURL($val['name'], 'upper')?>/<?=$val['product_id']?>">
                    <?=$val['name']?> <br>
                    <?php if (@$val['size']): echo "<b>Talla</b>: ".@$val['size']."<br>"; endif; ?>
                    <?php if (@$val['color']): echo "<b>Color</b>: ".@$val['color']; endif; ?>
                </a>
                <span class="quantity">Cant: <?=$val['quantity']?> x <?=$val['formed_price']?> €</span>
                <?php if ($val['calculated_iva'] == 0): ?>
                    <span>PROMOCIÓN</span>
                <?php else: ?>
                    <span>IVA (<?=$val['product_iva']?>%): <?=$val['calculated_iva']?> €</span>
                <?php endif; ?>
                <span class="price_cart"><?=$val['sub_total']?> €</span>
            </div>

            <div class="cart_remove">
                <a href="#" class="js-cart-remove" data-value="<?=$val['product_id']?>" data-color="<?=@$val['color_id']?>" data-size="<?=@$val['size_id']?>"><i class="ion-android-close"></i></a>
            </div>
        </div>
    <?php endforeach; ?>

    <div class="cart_total">
        <span>Subtotal:</span>
        <span><?=$sub_total?> €</span>
    </div>
</div>

<div class="mini_cart_footer">
    <div class="cart_button view_cart">
        <a href="CARRITO">Ver cesta</a>
    </div>
</div>
