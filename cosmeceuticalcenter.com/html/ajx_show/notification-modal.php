<h2><?=$arr_notification[0]['title']?></h2>
<div class="mod-container">
    <?=$arr_notification[0]['description']?>

    <hr>

    <?php if ($arr_notification[0]['link']): ?>
        <div class="row">
            <div class="col-4 mx-auto">
                <a href="<?=$arr_notification[0]['link']?>" class="btn btn-black css-100w">Ir</a>
            </div>
        </div>
    <?php endif; ?>
</div>
