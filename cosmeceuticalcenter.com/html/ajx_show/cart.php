<div class="container">
    <div class="row">
        <!--datos user-->
        <div class="col-lg-7 col-md-7">
            <form name="frm-cart" id="frm-cart" class="css-frm" method="post" action="">
                <div class="js-alert"></div>
                <div class="row">
                    <?php foreach ($arr_cart as $val): ?>
                        <input type="hidden" name="c[]" value="<?=$val['cart_id']?>">
                        <!--ITEM CART -->
                        <div class="col-12 itemCart">
                            <div class="row">
                                <div  class="col-xs-4 col-ms-2 col-sm-2">
                                    <a href="PRODUCTO/<?=$val['product_id']?>/<?=friendlyURL($val['name'], 'upper')?>">
                                        <img src="<?=$val['formed_image']?>" alt="">
                                    </a>
                                </div>

                                <div class="col-xs-8 col-ms-3 col-sm-4 borderRightN bottomxs bottommd">
                                    <h4><?=$val['brand_name']?> </h4>
                                    <p><span class="verde"><?=$val['name']?></span></p>
                                    <?php if ($val['size']): echo "<b>Talla</b>: ".$val['size']."<br>"; endif; ?>
                                    <?php if ($val['color']): echo "<b>Color</b>: ".$val['color']; endif; ?>
                                    <div class="imgMarca">
                                        <?=$val['category_name']?>
                                    </div>
                                </div>

                                <div  class="col-xs-3 col-ms-2 col-sm-2 borderRightN cantidadCart">
                                    <div class="form-group ">
                                        <label for="cantidad">Cantidad</label>
                                        <input type="number" name="qty[]" id="qty_<?=$val['cart_id']?>" class="css-ipt-quantity only-number" min="1" max="100" value="<?=$val['quantity']?>" data-value="<?=$val['cart_id']?>" data-color="<?=$val['color_id']?>" data-size="<?=$val['size_id']?>">
                                    </div>
                                </div>

                                <div  class="col-xs-9 col-ms-5 col-sm-4 ">
                                    <div class="borderRightN" style="width:75%; float:left;">
                                        <?php if ($val['new_price'] != ""): ?>
                                            <small class="css-line-through"><?=$val['price']?> €</small>
                                            <h4><?=$val['new_price']?> €</h4>
                                        <?php else: ?>
                                            <h4><?=$val['price']?> €</h4>
                                        <?php endif; ?>

                                        <?php if ($val['calculated_iva'] == 0): ?>
                                            <span>PROMOCIÓN</span>
                                        <?php else: ?>
                                            <span>IVA (<?=$val['product_iva']?>%): <?=$val['calculated_iva']?> €</span>
                                        <?php endif; ?>
                                        <!--<p> <span class="negro"><strong>2 UNIDADES 120,90 €</strong></span></p>-->
                                        <br><small>Sub total: <?=$val['sub_total']?></small>
                                    </div>

                                    <div style="width:25%; float:left; position:relative; height:100%; text-align:center;">
                                        <button type="button" class="btn btn-link btn-xs btn-remove" data-value="<?=$val['cart_id']?>" data-size="<?=$val['size_id']?>" data-color="<?=$val['color_id']?>">
                                            <i class="fa fa-trash-o borrarCart"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--END ITEM CART -->
                    <?php endforeach; ?>

                    <!--CALCULO PORTES-->
                    <div class="col-12 col-ms-8 col-ms-offset-4  col-sm-10 col-sm-offset-2 col-md-8 col-md-offset-4 calculoPortes">
                        <i class="fa fa-truck fa-1x css-color-grisIco absoluteIco"></i>
                        <div class="txtPortes">
                            <p><br>Elije tu provincia<br>para calcular los portes</p>
                        </div>
                        <div class="ProvinciaPortes" style="width:54%; float:right;">
                            <div class="form-group">
                                <label for="country">País</label>
                                <select class="form-control shipping" id="country" name="country">
                                    <?php foreach ($arr_country as $val): ?>
                                        <option value="<?=$val['id']?>" <?=isSelected($val['id'], $country)?>><?=$val['country']?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="ProvinciaPortes" style="width:54%; float:right;">
                            <div class="form-group">
                                <label for="provincia">Provincia</label>
                                <select class="form-control shipping" id="province" name="province">
                                    <option>Elige Provincia</option>
                                    <?php foreach ($arr_province as $val): ?>
                                        <option value="<?=$val['id']?>" <?=isSelected($val['id'], $province_id)?>><?=$val['name']?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--END CALCULO PORTES-->
                </div>
            </form>
        </div>

        <div class="col-lg-4 offset-lg-1 col-md-4 offset-md-1">
            <div class="row">
                <!--CALCULO CART-->
                <div class="col-12 ResumenCart">
                    <h3><i class="fa fa-calculator fa-1x css-color-grisIco"></i> Cálculo cesta</h3><br>
                    <div class="camposCart">
                        <p>Subtotal:</p>
                        <p>Portes:</p>
                    </div>
                    <div class="importesCart">
                        <p><?=$sub_total?> €</p>
                        <p id="ajx-shipping"><?=$shipping?> €</p>
                    </div>
                    <div  class="hr"></div>
                    <div class="totalCart">
                        <p>Total:</p>
                    </div>
                    <div class="pvpCart">
                        <h2 class="ajx-total" data-total="<?=$total?>"><?=$total?> €</h2>
                    </div>

                    <div class="checkout_form" style="width: 100%; float: left;">
                        <!-- <div class="col-12">
                            <input id="account" type="checkbox" data-target="createp_account">
                            <label class="css-fonsize12" for="account">Acepto las <a href="#" target="_blank">condiciones de venta</a></label>
                        </div> -->
                    </div>

                    <div  class="hr"></div>

                    <div class="buttonSeguir">
                        <a class="btn btn-warning" href="PRODUCTOS" role="button">seguir comprando</a>
                    </div>
                    <div class="buttonPago">
                        <?php
                        $disable = ($this->cart['total_products'] > 0) ? "" : "disabled";
                        $url = ($this->cart['total_products'] > 0) ? "PASO-2" : "";
                        ?>
                        <a href="<?=$url?>" class="btn btn-lg customButtonCar">Continuar</a>
                    </div>
                </div>
                <!--END.CALCULO CART-->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?=APP_ASSETS?>js/js-cart.js"></script>
