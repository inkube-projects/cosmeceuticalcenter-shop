<!DOCTYPE html>
<html class="no-js" lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <title> Opinión de <?=$product_name?> | <?=APP_TITLE?> </title>
        <meta name="Description" CONTENT=" " />
        <meta name="Keywords" CONTENT="" />
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="home_three_body_wrapper">
            <!--header area start-->
            <?php include("html/overall/navbar.php"); ?>
            <!--header area end-->

            <!--breadcrumbs-->
            <div class="breadcrumbs_area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb_content">
                                <h3>Orden <?=$order_code?></h3>
                                <ul>
                                    <li><a href="HOME">Compras</a></li>
                                    <li>></li>
                                    <li>Mi opinión</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end.breadcrumbs-->

            <!--CREAR CUENTA-->
            <div class="Checkout_section bgGray">
                <div class="container">
                    <div class="checkout_form">
                        <form name="frm-opinion" id="frm-opinion" class="css-frm" method="post" action="">
                            <input type="hidden" name="score" id="score" value="">

                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <h3><i class="fa fa-user" aria-hidden="true"></i> Qué opinas de: <?=$product_name?></h3>
                                    <div class="js-alert"><?=$flash_message?></div>
                                    <div class="row">
                                        <div class="col-6 css-stars-container">
                                            <label>Estrellas</label><br>
                                            <?php if ($act == 1): ?>
                                                <a href="#" class="js-score js-star" data-score="1"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                <a href="#" class="js-score js-star" data-score="2"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                <a href="#" class="js-score js-star" data-score="3"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                <a href="#" class="js-score js-star" data-score="4"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                <a href="#" class="js-score js-star" data-score="5"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                            <?php else: ?>
                                                <?php for($i = 1; $i <= 5; $i++): $class = ($i <= $arr_features[0]['score']) ? "fa-star" : "fa-star-o"; ?>
                                                    <i class="fa <?=$class?> css-icon-yellow" aria-hidden="true"></i>
                                                <?php endfor; ?>
                                            <?php endif; ?>
                                        </div>

                                        <div class="col-12">
                                            <p><hr></p>
                                        </div>

                                        <div class="col-12">
                                            <?php if ($act == 1): ?>
                                                <div class="order-notes">
                                                    <label for="opinion">Información adicional </label>
                                                    <textarea id="opinion" name="opinion" placeholder="Cuentanos que piensas" rows="4"></textarea>
                                                    <span class="text-danger" id="opinion_validate"></span>
                                                </div>
                                            <?php else: ?>
                                                <div class="css-opinion-container">
                                                    <?=$arr_features[0]['review']?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <?php if ($act == 1): ?>
                                        <button type="submit" class="btn btn-black btn-submit">Enviar</button>
                                    <?php endif; ?>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <h3><i class="fa fa-truck" aria-hidden="true"></i> DATOS DEL PRODUCTO</h3>
                                    <div class="row">
                                        <div class="col-7">
                                            <div class="css-image-product" style="background-image: url('<?=$cover_image?>')"></div>
                                        </div>

                                        <div class="col-5">
                                            <h4><?=$product_name?></h4>
                                            <p><?=$brand_name?></p>
                                            <p class="css-justify"><?=$product_description?>... <a href="PRODUCTO/<?=$product_id?>/<?=friendlyURL($product_name, 'upper')?>" class="css-text-black">Ver más</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <?php include("html/overall/footer.php"); ?>
        </div>
        <div id="key" data-id="<?=$id?>"></div>
        <!--!FINAL home_three_body_wrapper ---->
        <?php include("html/overall/modal.php"); ?>
        <!-- JS
        ============================================ -->
        <?php include("html/overall/js.php"); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-opinion.js"></script>
    </body>
</html>
