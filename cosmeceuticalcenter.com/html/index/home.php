<!DOCTYPE html>
<html class="no-js" lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <title> HOME | <?=APP_TITLE?> </title>
        <meta name="Description" CONTENT="<?=@$arr_seo[0]['description']?>" />
        <meta name="Keywords" CONTENT="<?=@$arr_seo[0]['keywords']?>" />
        <?php include('html/overall/header.php'); ?>
    </head>
	<style>
		/*CARUSEL* MOVIL*/
@media only screen and (max-width: 479px) {

.slider_content {
    max-width: 420px;
    height: 280px;
    /* border: none; */
    /* padding: 0px; */
    top: 60px;
    position: absolute;
    width: 94%;
    left: 0;
    margin: 3%;
}

	.owl-item{
    height: 480px;
	}

	.single_slider {
    background-position: 70%;
}

	.slider_content_info {
    width: 100%;
    height: 100%;
    padding: 15px;
    background: #eaeaea;
}

	.banner_fullwidth {
    margin-bottom: 155px;
}

}
	</style>

    <body>
        <div class="home_three_body_wrapper">
            <!--header area start-->
            <?php include("html/overall/navbar.php"); ?>
            <!--header area end-->
			


       
            <div class="slider_area owl-carousel">
				
			
			
				
				
<?php foreach ($arr_carousel as $key => $val): ?>
                    <div class="single_slider" data-bgimg="<?=$val['formed_image']?>">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-12">
                                    <div class="slider_content">
                                        <?php if ($val['description'] && $val['color']): ?>
                                            <div class="<?=$val['color']?>">
                                                <?=$val['description']?>

                                                <?php if ($val['url']): ?>
                                                    <a class="button" href="<?=$val['url']?>">+ info</a>
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


              <?php endforeach; ?>
            </div>
	
            <!--slider area end-->

            <!--shipping area start-->
            <div class="shipping_area shipping_two">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="single-shipping">
                                <a href="" class="js-featured" data-value="4" data-placement="top" data-original-title="quick view">
                                    <!--<div class="shipping_icone"><i class="fa fa-users"></i></div>-->
                                    <div class="shipping_icone"><i class="fa fa-cart-plus"></i></div>
                                </a>
                                <div class="shipping_content">
                                    <!--<h3>Tu cuenta</h3>
                                    <p>Siempre informado de todo, descuento, novedas, pedidos, mis favoritos y mucho más...</p>-->
                                </div>
                                <h3>Ventajas</h3>
                                <p>¿Qué diferencia a Cosmeceutical Center del resto de tiendas de cosmética online?</p>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="single-shipping">
                                <a href="HOME" class="js-featured" data-value="1" data-placement="top" data-original-title="quick view">
                                    <div class="shipping_icone"><i class="fa fa-phone"></i></div>
                                </a>
                                <div class="shipping_content">
                                    <h3>Pedidos Telefónicos</h3>
                                    <p>Atención personalizada, te asesoramos en tus compras <br>
													  <a href="tel:+34954378643">+34 954 378 643 </a> - 
													 <a href="tel:+34673375357"> +34 673 375 357</a></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="single-shipping">
                                <a href="" class="js-featured" data-value="2" data-placement="top" data-original-title="quick view">
                                    <div class="shipping_icone"><i class="fa fa-gift"></i></div>
                                </a>
                                <div class="shipping_content">
                                    <h3>Muestras Gratuitas</h3>
                                    <p>Muestras adaptadas a tus necesidades en cada pedido. ¡No experiementes con tu piel!</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="single-shipping">
                                <a href="" class="js-featured" data-value="3" data-placement="top" data-original-title="quick view">
                                    <div class="shipping_icone"><i class="fa fa-truck"></i></div>
                                </a>
                                <div class="shipping_content">
                                    <h3>Envíos Gratuitos</h3>
                                    <p>En tus compras online o teléfonicas portes gratuitos en pedidos superiores a 80 euros</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--shipping area end-->

            <!--banner area start-->
            <section class="banner_section home_banner_two">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-6 col-md-6">
                            <div class="single_banner">
                                <div class="banner_thumb">
                                    <a href="http://www.cosmeceuticalcenter.com/NEW-TRATAMIENTOS" target="_blank">
                                        <img src="<?=APP_IMG_BANNERS?>banner5.jpg" alt="tratamientos de belleza y salud">
                                    </a>
                                    <div class="banner_content">
                                        <p><i class="fa fa-map-marker css-fonsize20 css-colorB"></i> Solo en clínica</p>
                                        <h2>TRATAMIENTOS</h2>
                                        <span>Servicio integral de salud y belleza</span><br>
                                        <a class="button btn-sm css-marginT20" href="http://www.cosmeceuticalcenter.com/NEW-TRATAMIENTOS" target="_blank">+ nuestros tratamientos</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="single_banner">
                                <div class="banner_thumb">
                                    <a href="http://www.cosmeceuticalcenter.com/ASESORIA-COSMECEUTICA" target="_blank">
                                        <img src="<?=APP_IMG_BANNERS?>banner6.jpg" alt="asesoria antiaging">
                                    </a>
                                    <div class="banner_content ">
                                        <p><i class="fa fa-map-marker css-fonsize20 css-fontcolorFFF"></i> <span class="css-fontcolorFFFB">En clinica y </span> <i class="fa fa-television css-fonsize20 css-fontcolorFFF"></i> <span class="css-fontcolorFFFB">Online</span></p>
                                        <h2 class="css-fontcolorFFF">ANTIAGING</h2>
                                        <span class="css-fontcolorFFF">Consultoría y Asesoramiento</span><br>
                                        <a class="button btn-sm css-marginT20" href="http://www.cosmeceuticalcenter.com/ASESORIA-COSMECEUTICA" target="_blank">+ asesoria antiaging</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--banner area end-->

            <!--product section area start-->
            <section class="product_section  p_section1">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
							
									<div class="tax_free"><a href="https://www.cosmeceuticalcenter.com/SHOP-TAX-FREE" target="_blank"><img src="https://www.cosmeceuticalcenter.com/images/taxfree2.png" alt="" width="60" height="75" border="0"/></a>
									</div>
                            <p class="css-marginB0">Productos antiaging y suplementos nutricionales recomendados de la semana</p>
                            <div class="section_title">
                                <h2>Recomendación semanal</h2>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="product_area">
                                <div class="product_container bottom">
                                    <?php //$slick_class = (count($arr_weekly) >= 6) ? "product_row1" : "css-product-row";?>
                                    <div class="product_row1 custom-row <?//=$slick_class?>">
                                        <?php foreach ($arr_weekly as $val): ?>
                                            <div class="custom-col-5">
                                                <div class="single_product">
                                                    <div class="product_thumb">
                                                        <a class="primary_img" href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>">
                                                            <div class="css-product-list-img" style="background-image: url('<?=$val['formed_image']?>')"></div>
                                                        </a>
                                                        <a class="secondary_img" href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>">
                                                            <div class="css-product-list-img" style="background-image: url('<?=$val['formed_image']?>')"></div>
                                                        </a>

                                                        <div class="quick_button d-none d-sm-none d-md-block">
                                                            <a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>" data-product="<?=$val['product_id']?>" class="js-product-view" data-placement="top" data-original-title="<?=$val['formed_price']?> €"> vista rápida</a>
                                                        </div>
                                                    </div>

                                                    <div class="product_content">
                                                        <div class="tag_cate">
                                                            <a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>"><?=$val['brand_name']?></a>
                                                        </div>
                                                        <h3><a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>"><?=$val['product_name']?></a></h3>
                                                        <p class="css-fontSize12 css-height60"><?=sanitize($val['description_short'])?></p>

                                                        <div class="price_box">
                                                            <?php if ($val['formed_old_price']): ?>
                                                                <span class="old_price"><?=$val['formed_old_price']?> €</span>
                                                            <?php endif; ?>
                                                            <span class="current_price"><?=$val['formed_price']?> €</span>
                                                        </div>

                                                        <div class="product_hover">
                                                            <div class="product_desc">
                                                                <p style="css-text-lowe css-fontSize12"><?=substr(sanitize($val['product_description']), 0, 70)?>...</p>
                                                            </div>
                                                            <div class="action_links">
                                                                <ul>
                                                                    <li class="add_to_cart">
                                                                        <a href="HOME" class="js-add-cart" data-product="<?=$val['product_id']?>" data-view="l"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--product section area end-->

            <!--shipping area start-->
            <div class="shipping_area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="single-shipping">
                                <h3>Marcas de primera calidad</h3>
                                <p>Estudiamos las formulaciones y calidad de cada<br> producto antes de recomendarlo</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="single-shipping">
                                <h3>Productos testados</h3>
                                <p>Muchos de los productos que recomendamos han <br>sido probados en nuestra clínica</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--shipping area end-->

            <section class="product_section p_section1 product_s_three">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="product_area">
                                <div class="product_tab_button">
                                    <ul class="nav" role="tablist">
                                        <li>
                                            <a class="active" data-toggle="tab" href="#featured" role="tab" aria-controls="featured" aria-selected="true">Novedades</a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#arrivals" role="tab" aria-controls="arrivals" aria-selected="false">Con descuento</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="tab-content css-relative">
                                    <div class="tab-pane fade show active" id="featured" role="tabpanel">
                                        <div class="product_container">
                                            <?php $slick_class = (count($arr_novelty) >= 6) ? "product_column3" : "css-product-row";?>
                                            <div class="custom-row <?=$slick_class?>">
                                                <?php foreach ($arr_novelty as $val): ?>
                                                    <div class="custom-col-5">
                                                        <div class="single_product">
                                                            <div class="product_thumb">
                                                                <!-- <a class="primary_img" href="#"><img src="<?=$val['formed_image']?>" alt=""></a>
                                                                <a class="secondary_img" href="#"><img src="<?=$val['formed_image']?>" alt=""></a> -->
                                                                <a class="primary_img" href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>">
                                                                    <div class="css-product-list-img" style="background-image: url('<?=$val['formed_image']?>')"></div>
                                                                </a>
                                                                <a class="secondary_img" href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>">
                                                                    <div class="css-product-list-img" style="background-image: url('<?=$val['formed_image']?>')"></div>
                                                                </a>
                                                                <div class="quick_button d-none d-sm-none d-md-block">
                                                                    <a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>" data-product="<?=$val['product_id']?>" class="js-product-view" data-placement="top" data-original-title="<?=$val['price_iva']?> €"> vista rápida</a>
                                                                </div>
                                                            </div>
                                                            <div class="product_content">
                                                                <div class="tag_cate">
                                                                    <a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>"><?=$val['brand_name']?></a>
                                                                </div>
                                                                <h3><a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>"><?=$val['product_name']?></a></h3>
                                                                <p class="css-height60 css-fontSize12"><?=sanitize($val['description_short'])?></p>
                                                                <div class="price_box">
                                                                    <!-- <span class="old_price"><?=$val['price']?> €</span> -->
                                                                    <span class="current_price"><?=$val['price_iva']?> €</span>
                                                                </div>
                                                                <div class="product_hover">
                                                                    <div class="product_desc">
                                                                        <p><?=substr(sanitize($val['product_description']), 0, 70)?>...</p>
                                                                    </div>
                                                                    <div class="action_links">
                                                                        <ul>
                                                                            <li class="add_to_cart">
                                                                                <a href="HOME" class="js-add-cart" data-product="<?=$val['product_id']?>" data-view="l"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="arrivals" role="tabpanel">
                                        <div class="product_container">
                                            <?php $slick_class = (count($arr_discount) >= 6) ? "product_column3" : "css-product-row";?>
                                            <div class="custom-row <?=$slick_class?>">
                                                <?php foreach ($arr_discount as $val): ?>
                                                    <div class="custom-col-5">
                                                        <div class="single_product">
                                                            <div class="product_thumb">
                                                                <a class="primary_img" href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>">
                                                                    <div class="css-product-list-img" style="background-image: url('<?=$val['formed_image']?>')"></div>
                                                                </a>
                                                                <a class="secondary_img" href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>">
                                                                    <div class="css-product-list-img" style="background-image: url('<?=$val['formed_image']?>')"></div>
                                                                </a>
                                                                <div class="quick_button d-none d-sm-none d-md-block">
                                                                    <a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>" data-product="<?=$val['product_id']?>" class="js-product-view" data-placement="top" data-original-title="<?=$val['discount_price']?> €"> vista rápida</a>
                                                                </div>
                                                            </div>
                                                            <div class="product_content">
                                                                <div class="tag_cate">
                                                                    <a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>"><?=$val['brand_name']?></a>
                                                                </div>
                                                                <h3><a href="PRODUCTO/<?=friendlyURL($val['brand_name'], 'upper')?>/<?=friendlyURL($val['product_name'], 'upper')?>/<?=$val['product_id']?>"><?=$val['product_name']?></a></h3>
                                                                <p  class="css-height60 css-fontSize12"><?=sanitize($val['description_short'])?></p>
                                                                <div class="price_box">
                                                                    <span class="old_price"><?=$val['product_price']?> €</span>
                                                                    <span class="current_price"><?=$val['discount_price']?> €</span>
                                                                </div>
                                                                <div class="product_hover">
                                                                    <div class="product_desc">
                                                                        <p><?=substr(sanitize($val['product_description']), 0, 70)?>...</p>
                                                                    </div>
                                                                    <div class="action_links">
                                                                        <ul>
                                                                            <li class="add_to_cart">
                                                                                <a href="HOME" class="js-add-cart" data-product="<?=$val['product_id']?>" data-view="l"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--product section area end-->
            <?php include("html/overall/brands-nicho.php"); ?>
            <?php include("html/overall/newsletter-footer.php"); ?>
            <?php include("html/overall/footer.php"); ?>

        </div>
        <!--!FINAL home_three_body_wrapper ---->
        <?php include("html/overall/modal.php"); ?>

        <!-- JS
        ============================================ -->
        <?php include("html/overall/js.php"); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-newsletter.js"></script>
    </body>
</html>
