'use strict'

$(function(){
    $('.js-country ul.list .option').on('click', function(){
        var value = $(this).data('value');
        if (value != 73) {
            $("#province").attr('disabled', true);
            $(".js-province .niceselect_option").addClass('css-hide');
        } else {
            $(".js-province .niceselect_option").removeClass('css-hide');
        }

        $("#country").val(value);
    });

    $('.js-shipping-country ul.list .option').on('click', function(){
        var value = $(this).data('value');
        if (value != 73) {
            $("#shipping_province").attr('disabled', true);
            $(".js-shipping-province .niceselect_option").addClass('css-hide');
        } else {
            $(".js-shipping-province .niceselect_option").removeClass('css-hide');
        }

        $("#shipping_country").val(value);
    });

    // Elimina favoritos
    $('.js-favorites').on('click', '.remove-favorite', function(e){
        e.preventDefault();

        if (confirm('¿Estas seguro de eliminar este favorito?')) {
            $.ajax({
                type: 'POST',
                url: base_url + 'ajx/favorites/remove',
                data: {
                    product: $(this).data('fav')
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#js-overlay').css('display', 'block');
                },
                success: function(r){
                    $('#js-overlay').css('display', 'none');

                    if (r.status == "OK") {
                        $('.js-favorites').html(r.data);
                    } else {
                        alert(r.message);
                    }
                }
            });
        }
    })

    $("#frm-profile").validate({
        rules:{
            name: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            last_name: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            phone: {
                required: true,
                noSpecialCharacters: /([*?^${}><\[\]\/\\])/g
            },
            email: {
                required: true,
                email: true
            },
            pass_repeat: {
                equalTo: "#pass"
            },
            note: {
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            }
        },
        messages:{
            name: {
                required: "Debes ingresar tu Nombre",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            last_name: {
                required: "Debes ingresar tu Apellido",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            phone: {
                required: "Debes agregar un Número telefónico",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            email: {
                required: "Debes ingresar un email",
                email: "Debes ingresar un email válido"
            },
            pass_repeat: {
                equalTo: "#pass"
            },
            note: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist("frm-profile", 1);
            return false;
        }
    });

    $("#frm-bill").validate({
        rules:{
            identification: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            bill_name: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            bill_last_name: {
                required: true,
                noSpecialCharacters: /([*?^${}><\[\]\/\\])/g
            },
            company: {
                noSpecialCharacters: /([*?^${}><\[\]\/\\])/g
            },
            bill_country: {
                required: true,
            },
            bill_location_1: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            bill_location_2: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            bill_city: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            bill_postal_code: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            }
        },
        messages:{
            identification: {
                required: "Debes agregar NIF/CIF/NIE",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            bill_name: {
                required: "Debes agregar un Nombre para facturar",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            bill_last_name: {
                required: "Debes agregar al menos un Apellido",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            company: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            country: {
                required: "Debes seleccionar un país",
            },
            bill_location_1: {
                required: "Debes agregar una dirección completa",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            bill_location_2: {
                required: "Debes agregar una dirección completa",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            bill_city: {
                required: "Debes agregar una ciudad",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            bill_postal_code: {
                required: "Debes agregar el código postal",
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist("frm-bill", 2);
            return false;
        }
    });

    $("#frm-address").validate({
        rules:{
            location_name: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            addr_country: {
                required: true,
            },
            addr_location_1: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            addr_location_2: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            addr_city: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            addr_postal_code: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            addr_order_note: {
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            }
        },
        messages:{
            location_name: {
                required: "Debea agregar un Nombre a la Dirección de envío",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            addr_country: {
                required: "Debes seleccionar un país",
            },
            addr_location_1: {
                required: "Debes agregar una dirección completa",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            addr_location_2: {
                required: "Debes agregar una dirección completa",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            addr_city: {
                required: "Debes agregar una ciudad",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            addr_postal_code: {
                required: "Debes agregar un código postal",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            addr_order_note: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist("frm-address", 3);
            return false;
        }
    });
});


function persist(form_name, act) {
    var i = $("#" + form_name).serialize();
    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/account/' + act,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                location.reload(true);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('guardar datos >');
                $(document).scrollTop(0);
            }
        }
    });
}
