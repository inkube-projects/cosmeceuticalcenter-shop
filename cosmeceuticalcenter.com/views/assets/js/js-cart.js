'use strict'

$(function(e){
    var methods = $('input[name=method]');

    $('.css-ipt-quantity').on('change', function(e){
        var color = $(this).data('color');
        var size = $(this).data('size');

        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/cart/2',
            dataType: 'json',
            data: {
                cart: $(this).data('value'),
                quantity: $(this).val(),
                country: $('#country').val(),
                province: $('#province').val(),
                size: size,
                color: color
            },
            beforeSend: function(){
                $('#js-overlay').css('display', 'block');
            },
            success: function(r) {
                $('#js-overlay').css('display', 'none');

                if (r.status == "OK") {
                    $("#ajx-cart").html(r.data);
                } else {
                    $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                    $(document).scrollTop(0);
                }
            }
        });
    });

    $('#aplicarVale').on('click', function(e){
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/cart/7',
            dataType: 'json',
            // data: {
            //     coupon: $('#valeDto').val()
            // },
            data: $('#frm-cart').serialize(),
            beforeSend: function(){
                $('#js-overlay').css('display', 'block');
            },
            success: function(r) {
                $('#js-overlay').css('display', 'none');

                if (r.status == "OK") {
                    $('#discount-percent').html('Descuento (' + r.data.percent + '%):')
                    $('#discount').html('-' + r.data.discount + ' €')
                    $('.ajx-total').html(r.data.total);
                    $('.ajx-total').data('total', r.data.total);
                    $('#charge').html(r.data.mp_calculate_charge + ' €')

                    $("input[name=method]").each(function(e) {
                        if($(this).is(':checked')){
                            if ($(this).val() == "2") {
                                getPaypalButton();
                            }
                        }
                    });

                    r.data.products_info.forEach(function(val) {
                        if (val['valid_discount_product']) {
                            $('#js-coupon-' + val['product_id']).html('<small>Descuento por vale (' +r.data.percent + '%): ' + val['p_total_discount'] + ' €</small> <br> <small>Total producto: ' + val['p_total'] + '€</small>');
                        }
                    });

                } else {
                    $('#valeDto').val('');
                    $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                    $(document).scrollTop(0);
                }
            }
        });
    });

    $('#ajx-cart').on('change', '#country', function(){
        var country = $(this).val();

        if (country != 73) {
            $('#province').attr('disabled', true);
        } else {
            $('#province').attr('disabled', false);
        }
    })

    $('#ajx-cart').on('change', '.shipping', function(){
        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/cart/6',
            dataType: 'json',
            data: {
                province: $('#province').val(),
                country: $('#country').val()
            },
            beforeSend: function(){
                $('#js-overlay').css('display', 'block');
            },
            success: function(r) {
                $('#js-overlay').css('display', 'none');

                if (r.status == "OK") {
                    $("#ajx-shipping").html(r.data.shipping + '&euro;');
                    $("#ajx-total").html(r.data.total + '&euro;');
                } else {
                    $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                    $(document).scrollTop(0);
                }
            }
        });
    });

    $(".btn-remove").on('click', function(e){
        if (confirm('¿Estás seguro de eliminar este producto de tu cesta?')) {
            $.ajax({
                type: 'POST',
                url: base_url + 'ajx/cart/remove',
                data: {
                    id: $(this).data('value'),
                    color: $(this).data('color'),
                    size: $(this).data('size')
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#js-overlay').css('display', 'block');
                },
                success: function(r) {
                    $('#js-overlay').css('display', 'none');

                    if (r.status == "OK" && !r.reload) {
                        $("#ajx-cart").html(r.data);
                    } else if (r.status == "OK" && r.reload) {
                        location.reload();
                    } else {
                        $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                        $(document).scrollTop(0);
                    }
                }
            });
        }
    });

    $("#frm-cart").validate({
        rules:{
            terms: {
                required: true,
            },
            "method": {
                required: true,
            },
            order_note: {
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            note: {
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            valeDto: {
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            }
        },
        messages:{
            terms: {
                required: "Debes aceptar los términos de venta",
            },
            "method": {
                required: "Debes seleccionar un método de pago",
            },
            order_note: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            note: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            valeDto: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            var method = $('input[name=method]').val();
            if (method == "1") {
                persistRedsysButton();
            }

            return false;
        }
    });

    $('input[name=method]').on('click', function(e){
        var method = $(this).val();
        setPaypalCharge();

        if (method == "1") {
            getRedsysButton();
        } else if (method == "2") {
            getPaypalButton();
        }
    });
});

/**
 * Genera el boton de paypal
 * @return {void}
 */
function __getPaypalButton()
{
    $('#paypal-button').html('');
    paypal.Button.render({
        // Configure environment
        env: paypalENV,
        client: {
            sandbox: clientID,
            production: clientID
        },
        // Customize button (optional)
        locale: 'es_ES',
        style: {
            size: 'small',
            color: 'gold',
            shape: 'pill',
        },
        // Set up a payment
        payment: function (data, actions) {
            return actions.payment.create({
                transactions: [{
                    amount: {
                        total: $('.ajx-total').data('total'),
                        currency: 'EUR'
                    }
                }]
          });
        },
        // Execute the payment
        onAuthorize: function (data, actions) {
            return actions.payment.execute()
            .then(function () {
                $('#o').val(data.paymentID);

                $.ajax({
                    type: 'POST',
                    url: base_url + 'ajx/cart/4',
                    data: $('#frm-cart').serialize(),
                    dataType: 'json',
                    beforeSend: function(){
                        $('#js-overlay').css('display', 'block');
                        $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
                    },
                    success: function(r) {
                        $('#js-overlay').css('display', 'none');

                        if (r.status == "OK") {
                            window.location.href = base_url + 'PROCESADO/' + r.id;
                        } else {
                            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                            $(document).scrollTop(0);
                            $(".btn-submit").attr("disabled", false).html('Registrarme');
                        }
                    }
                });
            });
        }
    }, '#paypal-button');
}

function getPaypalButton()
{
    $('#paypal-button').html('');
    paypal.Buttons({
        createOrder: function(data, actions) {
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: $('.ajx-total').data('total')
                    }
                }]
            });
        },
        onApprove: function(data, actions) {
            return actions.order.capture().then(function(details) {
                $('#o').val(data.orderID);

                $.ajax({
                    type: 'POST',
                    url: base_url + 'ajx/cart/4',
                    data: $('#frm-cart').serialize(),
                    dataType: 'json',
                    beforeSend: function(){
                        $('#js-overlay').css('display', 'block');
                        $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
                    },
                    success: function(r) {
                        $('#js-overlay').css('display', 'none');

                        if (r.status == "OK") {
                            window.location.href = base_url + 'PROCESADO/' + r.id;
                        } else {
                            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                            $(document).scrollTop(0);
                            $(".btn-submit").attr("disabled", false).html('Registrarme');
                        }
                    }
                });
            });
        }
    }).render('#paypal-button');
}

function getRedsysButton()
{
    $('#paypal-button').html('');
    $('#paypal-button').html('<button type="submit" class="btn btn-lg customButtonCar btn-submit">Realizar Pago</button>');
}

function persistRedsysButton() {
    $('#paypal-button').html('');
    $("#amount").val($('.ajx-total').data('total'));

    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/cart/8',
        data: $('#frm-cart').serialize(),
        dataType: 'json',
        beforeSend: function(){
            $('#js-overlay').css('display', 'block');
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r) {
            $('#js-overlay').css('display', 'none');

            if (r.status == "OK") {
                $('#paypal-button').html(r.data);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(document).scrollTop(0);
                $(".btn-submit").attr("disabled", false).html('Registrarme');
            }
        }
    });
}

function setPaypalCharge() {
    var method = "";
    $("input[name=method]").each(function(e) {
        if($(this).is(':checked')){
            method = $(this).val();
        }
    });

    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/cart/10',
        dataType: 'json',
        data: {
            method: method,
            valeDto: $('#valeDto').val()
        },
        beforeSend: function(){
            // $('#js-overlay').css('display', 'block');
        },
        success: function(r) {
            $('#js-overlay').css('display', 'none');

            if (r.status == "OK") {
                $('#charge-percent').html('Recargo por gestión (' + r.data.percent + '%):')
                $('#charge').html(r.data.charge + ' €')
                $('.ajx-total').html(r.data.total);
                $('.ajx-total').data('total', r.data.total);

                if (typeof r.data.coupon_percent !== "undefined" && r.data.coupon_percent) {
                    $('#discount-percent').html('Descuento (' + r.data.coupon_percent + '%):')
                }
                if (typeof r.data.coupon_percent !== "undefined" && r.data.coupon_percent) {
                    $('#discount').html('-' + r.data.coupon_discount + ' €')
                }

                $("input[name=method]").each(function(e) {
                    if($(this).is(':checked')){
                        if ($(this).val() == "2") {
                            getPaypalButton();
                        }
                    }
                });
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(document).scrollTop(0);
            }
        }
    });
}
