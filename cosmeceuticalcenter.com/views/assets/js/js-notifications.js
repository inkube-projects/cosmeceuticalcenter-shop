$(function(e){
    $('.btn-show').on('click', function(e){
        e.preventDefault();
        var parent = $(this).parent().parent().parent().children().find('div.imgMarca');

        $.ajax({
            type: 'GET',
            url: base_url + 'ajx/notifications/show/' + $(this).data('not'),
            beforeSend: function(){
                $('#mod-notifications').modal();
            },
            success: function(r){
                if (r.status == "OK") {
                    $('#ajx-notification').html(r.data.html);
                    $('.css-notifications').html(r.data.t_notifications);
                    parent.html('Visto');
                } else {
                    $('#ajx-notification').html(r.message);
                }
            }
        });
    });

    $('.js-remove').on('click', function(e){
        if (confirm('¿Deseas eliminar esta notificación?')) {
            $.ajax({
                type: 'POST',
                url: base_url + 'ajx/notifications/remove',
                data: {
                    notification: $(this).data('not')
                },
                success: function(r){
                    if (r.status == "OK") {
                        location.reload();
                    } else {
                        alert(r.message);
                    }
                }
            });
        }
    });
});
