$(function(){
    // Validación de newsletter
    $("#frm-newsletter").validate({
        rules:{
            newsletter_email: {
                required: true,
                email: true
            },
            "newsletter_terms[]": {
                required: true,
            }
        },
        messages:{
            newsletter_email: {
                required: "Debes ingresar tu E-mail",
                email: "Debes ingresar un E-mail válido"
            },
            "newsletter_terms[]": {
                required: "Debes aceptar los términos y condiciones",
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("id");
            $("#" + name + "_validate").html(error[0]);
            console.log(error[0]);
        },
        submitHandler: function() {
            persist();
            return false;
        }
    });
})

function persist() {
    var i = $("#frm-newsletter").serialize();

    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/newsletter',
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $("#ajx-newsletter").html('<div class="icoNews"></div><h4 class="css-text-black css-marginT10">Valida tu registro al newsletter</h4><p>Para validar tu registro sólo tendrás que pulsar el vinculo que recibirás en la cuenta de correo con la que te has suscrito</p><i class="fa fa-exclamation-circle css-fontSize20 css-icon-yellow" style="color: #887346" aria-hidden="true"></i><p>Recuerda revisar tu bandeja de correo no deseado. Añade <span style="color: #887346">enviosweb@cosmeceuticalcenter.com</span> a tus contactos y llegaremos siempre a tu bandeja de entrada</p>');
            } else {
                $("#ajx-newsletter").html('<i class="fa fa-address-card css-fontSize40 css-color-crema" aria-hidden="true"></i><h4 class="css-text-black css-marginT10">' + r.message + '</h4>');
            }

            $("#mod-newsletter").modal();
            $(".btn-submit").attr("disabled", false).html('subscribe');
        }
    });
}
