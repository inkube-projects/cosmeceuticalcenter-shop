'use strict'

$(function(){
    $('.js-country ul.list .option').on('click', function(){
        var value = $(this).data('value');
        if (value != 73) {
            $("#province").attr('disabled', true);
            $(".js-province .niceselect_option").addClass('css-hide');
        } else {
            $(".js-province .niceselect_option").removeClass('css-hide');
        }

        $("#country").val(value);
    });

    $('.js-shipping-country ul.list .option').on('click', function(){
        var value = $(this).data('value');
        if (value != 73) {
            $("#shipping_province").attr('disabled', true);
            $(".js-shipping-province .niceselect_option").addClass('css-hide');
        } else {
            $(".js-shipping-province .niceselect_option").removeClass('css-hide');
        }

        $("#shipping_country").val(value);
    });

    $("#location_info").on('click', function(){
        if ($(this).prop('checked')) {
            shippingInfo(true);
        } else {
            shippingInfo(false);
        }
    });

    $("#frm-register").validate({
        rules:{
            "type_account": {
                required: true,
            },
            identification: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            name: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            last_name_1: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            last_name_2: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            company: {
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            phone: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            email: {
                required: true,
                email: true
            },
            pass: {
                required: true,
            },
            repeat_pass: {
                required: true,
                equalTo: "#pass"
            },
            country: {
                required: true,
            },
            location_1: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            location_2: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            city: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            postal_code: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            location_name: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            shipping_country: {
                required: true,
            },
            shipping_location_1: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            shipping_location_2: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            shipping_city: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            shipping_postal_code: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
            order_note: {
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            }
        },
        messages:{
            "type_account": {
                required: "Debes seleccionar al menos un tipo de cuenta",
            },
            identification: {
                required: "Debes ingresar NIF/CIF/NIE",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            name: {
                required: "Debes ingresar tu nombre",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            last_name_1: {
                required: "Debes ingresar tu apellido",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            last_name_2: {
                required: "Debes ingresar tu apellido",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            company: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            phone: {
                required: "Debes ingresra tú número telefónico",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            email: {
                required: "Debes ingresar tu E-mail",
                email: "Debes ingresar un E-mail válido"
            },
            pass: {
                required: "Debes ingresar una contraseña",
            },
            repeat_pass: {
                required: "Debes repetir tu contraseña",
                equalTo: "Las contraseñas deben ser iguales"
            },
            country: {
                required: "Debes seleccionar un país",
            },
            location_1: {
                required: "Debes ingresar tu dirección completa",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            location_2: {
                required: "Debes ingresar tu dirección completa",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            city: {
                required: "Debes ingresar tu ciudad",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            postal_code: {
                required: "Debes colocar tu código postal",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            location_name: {
                required: "Debes agregar un nombre a la dirección para recordar",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            shipping_country: {
                required: "Debes seleccionar el país de envío",
            },
            shipping_location_1: {
                required: "Debes ingresar la dirección de envío completa",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            shipping_location_2: {
                required: "Debes ingresar la dirección de envío completa",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            shipping_city: {
                required: "Debes colocar la ciudad de envío",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            shipping_postal_code: {
                required: "Debes indicar el código postal",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            order_note: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            register();
            return false;
        }
    });
});

function register()
{
    var i = $("#frm-register").serialize();
    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/register',
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
            $("#mod-register").modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        success: function(r){
            if (r.status == 'OK') {
                $('.js-modal-container').html('<i class="fa fa-address-card css-fontSize40 css-color-crema" aria-hidden="true"></i><h4 class="css-text-black css-marginT10">Valida tu registro</h4><p>Para validar tu registro sólo tendrás que pulsar el vinculo que recibirás en la cuenta de correo con la que te has registrado, podras realizar la compra pero no podrás ingresar al área de clientes hasta que valides tu cuenta.</p><i class="fa fa-exclamation-circle css-fontSize24 css-icon-yellow" aria-hidden="true"></i><p>Recuerda revisar tu bandeja de correo no deseado. Añade <span class="css-icon-yellow">enviosweb@cosmeceuticalcenter.com</span> a tus contactos y llegaremos siempre a tu bandeja de entrada</p>');
                $('.js-modal-footer-container').html('<button type="button" class="btn btn-default" onclick="login()">Continuar con la compra</button>');
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Registrarme');
                $(document).scrollTop(0);
            }

            $(".btn-submit").attr("disabled", false).html('Registrarme >');
        }
    });
}

function login() {
    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/login',
        data: {
            email: $('#email').val(),
            pass: $('#pass').val()
        },
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                window.location.replace(base_url + "PASO-2");
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Acceder');
                $(document).scrollTop(0);
            }
        }
    });
}

function shippingInfo(type)
{
    if (type) {
        var country = $("#country").val();
        var province = (country == 73) ? $("#province").val() : null;
        var location_1 = $("#location_1").val();
        var location_2 = $("#location_2").val();
        var city = $("#city").val();
        var postal_code = $("#postal_code").val();

        $("#shipping_country").val(country);
        $("#shipping_location_1").val(location_1);
        $("#shipping_location_2").val(location_2);
        $("#shipping_city").val(city);
        $("#shipping_postal_code").val(postal_code);

        if (province) {
            $("#shipping_province").val(province);
            $(".js-shipping-province .niceselect_option").removeClass('css-hide');
        } else {
            $("#shipping_province").attr('disabled', true);
            $(".js-shipping-province .niceselect_option").addClass('css-hide');
        }

        $('select').niceSelect('update');
    } else {
        $("#shipping_country").val('');
        $("#shipping_location_1").val('');
        $("#shipping_location_2").val('');
        $("#shipping_city").val('');
        $("#shipping_postal_code").val('');
    }
}
