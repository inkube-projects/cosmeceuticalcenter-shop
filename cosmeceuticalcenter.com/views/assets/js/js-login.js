'use strict'

$(function(){
    $(".js-forgot").on('click', function(e){
        e.preventDefault();
        $("#mod-forgot").modal('show');
    });

    $("#frm-forgot").validate({
        rules:{
            forgot_email: {
                required: true,
                email: true
            },
        },
        messages:{
            forgot_email: {
                required: "Debes ingresar tu E-mail",
                email: "Debes ingresar un E-mail válido"
            },
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            var i = $("#frm-forgot").serialize();
            $.ajax({
                type: 'POST',
                url: base_url + 'ajx/forgot-pass',
                data: i,
                dataType: 'json',
                beforeSend: function(){
                    $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
                },
                success: function(r){
                    if (r.status == 'OK') {
                        $("#forgot_email_validate").html('<b class="text-success">se ha enviado un email a tu cuenta<b>');
                    } else {
                        $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                        $(".btn-submit").attr("disabled", false).html('Envíar');
                        $(document).scrollTop(0);
                        $("#mod-forgot").modal('hide');
                    }

                    $(".btn-submit").attr("disabled", false).html('Enviar');
                }
            });

            return false;
        }
    });

    $("#frm-login").validate({
        rules:{
            email: {
                required: true,
                email: true
            },
            pass: {
                required: true,
            }
        },
        messages:{
            email: {
                required: "Debes ingresar tu E-mail",
                email: "Debes ingresar un E-mail válido"
            },
            pass: {
                required: "Debes ingresar tu Contraseña",
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist("frm-login");
            return false;
        }
    });

    $("#frm-register").validate({
        rules:{
            r_email: {
                required: true,
                email: true
            },
            r_pass: {
                required: true,
            }
        },
        messages:{
            r_email: {
                required: "Debes ingresar tu E-mail",
                email: "Debes ingresar un E-mail válido"
            },
            r_pass: {
                required: "Debes ingresar un Contraseña",
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            register();
            return false;
        }
    });
});


function persist(form_name) {
    var i = $("#" + form_name).serialize(), act = $("#key").data('act'),
    redirect = (act == 1) ? base_url + "DASHBOARD" : base_url + "PASO-2";


    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/login',
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", redirect);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Acceder');
                $(document).scrollTop(0);
            }
        }
    });
}

function register()
{
    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/cart-register/5',
        data: $('#frm-register').serialize(),
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", base_url + "R-PASO-1");
            } else {
                $('.js-alert-r').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('CREAR CUENTA');
            }
        }
    });
}
