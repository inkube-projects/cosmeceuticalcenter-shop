$(function(){
    $('.elevatezoom-gallery').on('click', function(){
        var image = new Image();
        image.src = $(this).data('image');

        var container_width = $('.zoomContainer').css('width');
        container_width = container_width.replace('px', '');
        var new_height = getNewSize(image.width, image.height, container_width);
        // alert(new_height);
        setTimeout(function(){
            $('.zoomContainer').css('height', new_height + 'px');
            $('.zoomWindow').css('height', new_height + 'px');
        }, 100);

        // console.log(new_height);
    });

    $('#tabs').on('change', function(){
        $('#js-tabs li a').eq($(this).val()).tab('show');
    });

    $('.js-btn-color').on('click', function(){
        var selected_color = $(this).data('color');
        $('#mod-colors').modal('show');

        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/products/getColors',
            data: {
                product_id: $('#key').data('value'),
                color_id: selected_color
            },
            dataType: 'json',
            beforeSend: function(){
                $("#ajx-colors").html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
            },
            success: function(r){
                if (r.status == 'OK') {
                    $("#ajx-colors").html(r.data);
                } else {
                    $("#ajx-colors").html('Error');
                }
            }
        });
    });

    $('.product_navactive a').on('click',function(e){
        e.preventDefault();
        var $href = $(this).attr('href');
        $('.product_navactive a').removeClass('active');
        $(this).addClass('active');

        $('.product-details-large .tab-pane').removeClass('active show');
        $('.product-details-large '+ $href ).addClass('active show');
    });

    var presentation = $(".js-presentation").html();
    // alert(presentation.substring(1, 50));
    $(".js-presentation").html('');
    $(".js-presentation").html(presentation.substring(0, 350) + '... <br><a href="javascript: getFullInfo()" class="css-more-link"><b>ampliar info <i class="fa fa-arrow-circle-down"></i></b></a>');

    $('.js-notify').on('click', function(e){
        $('#notification_email_validate').html('')
        var n_email = $('#notification_email').val();

        if (!isEmail(n_email) || n_email == "") {
            $('#notification_email_validate').removeClass('text-success').addClass('text-danger').html('Debes ingresar un email válido'); return false
        }

        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/product/notify',
            data: {
                product: $('#key').data('value'),
                email: n_email
            },
            dataType: 'json',
            beforeSend: function(){
                $(this).attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
            },
            success: function(r){
                if (r.status == 'OK') {
                    $('.notification_email_validate').removeClass('text-danger').addClass('text-success').html('Te notificaremos en la mayor brevedad posible');
                } else {
                    $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                    $(this).attr("disabled", false).html('Enviar');
                }
            }
        });

        return false;
    });

    // Agregar los articulos de lista al carrito
    $('.js-add-cart-detail').on('click', function(e){
        e.preventDefault();
        var d_product = $(this).data('product');
        var d_view = $(this).data('view');
        var product_size = $('#product_size').val();
        var product_color = $('#product_color').val();

        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/cart',
            data: {
                product: d_product,
                quantity: (d_view == "s") ? $("#quantity").val() : 1,
                size: product_size,
                color: product_color
            },
            dataType: 'json',
            beforeSend: function(){
                $(this).attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
            },
            success: function(r){
                if (r.status == 'OK') {
                    $('.ajx-add-cart-message').html('<i class="fa fa-check-circle-o css-fontSize100 text-success css-marginB10" aria-hidden="true"></i><br>El producto ha sido agregado correctamente a tu cesta');
                    $(".mini_cart").html(r.data.html);
                    $(".js-icon-cart").html('<i class="ion-android-cart"></i>' + r.data.total + ' €<i class="fa fa-angle-down"></i>');
                    $(".cart_quantity").html(r.data.total_products);
                } else {
                    $('.ajx-add-cart-message').html('<i class="fa fa-frown-o css-fontSize100 text-danger css-marginB10" aria-hidden="true"></i><br>' + r.message);
                }
                $('#mod-cart-add').modal();
            }
        });
    });
});

function getNewSize(width, height, thumb_width) {
    return Math.ceil(height * (thumb_width / width));
}

function getFullInfo() {
    $(".js-presentation").html($('.js-full-info').html());
}
