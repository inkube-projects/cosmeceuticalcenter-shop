(function ($) {
    "use strict";

    new WOW().init();

    /*---background image---*/
	function dataBackgroundImage() {
		$('[data-bgimg]').each(function () {
			var bgImgUrl = $(this).data('bgimg');
			$(this).css({
				'background-image': 'url(' + bgImgUrl + ')', // + meaning concat
			});
		});
    }

    $(window).on('load', function () {
        dataBackgroundImage();
    });

    /*---stickey menu---*/
    $(window).on('scroll',function() {
           var scroll = $(window).scrollTop();
           if (scroll < 100) {
            $(".sticky-header").removeClass("sticky");
           }else{
            $(".sticky-header").addClass("sticky");
           }
    });

    /*---jQuery MeanMenu---*/

    $('.mobile-menu nav').meanmenu({
        meanScreenWidth: "9901",
        meanMenuContainer: ".mobile-menu",
        onePage: true,
    });

    /*---slider activation---*/
    $('.slider_area').owlCarousel({
        animateOut: 'fadeOut',
        autoplay: true,
		loop: true,
        nav: false,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 1,
        dots:true,
    });

    /*---product_column3 activation---*/
    $('.product_column3').slick({
		autoplay: true,
        centerMode: true,
        centerPadding: '0',
        slidesToShow: 5,
        arrows:true,
        rows: 1,
        prevArrow:'<button class="prev_arrow"><i class="fa fa-angle-left"></i></button>',
        nextArrow:'<button class="next_arrow"><i class="fa fa-angle-right"></i></button>',
        responsive:[
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                  slidesToScroll: 1,
              }
            },
            {
              breakpoint: 768,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
              }
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 3,
                  slidesToScroll: 3,
              }
            },
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4,
                  slidesToScroll: 4,
              }
            },
        ]
    });

    /*---product row activation---*/
    $('.product_row1').slick({
        centerMode: true,
        centerPadding: '0',
        slidesToShow: 5,
        slidesToScroll: 5,
        arrows:true,
        prevArrow:'<button class="prev_arrow"><i class="fa fa-angle-left"></i></button>',
        nextArrow:'<button class="next_arrow"><i class="fa fa-angle-right"></i></button>',
        responsive:[
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                  slidesToScroll: 1,
              }
            },
            {
              breakpoint: 768,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
              }
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 3,
                  slidesToScroll: 3,
              }
            },
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4,
                  slidesToScroll: 4,
              }
            },

        ]
    });

    /*---product row 2 activation---*/
    $('.product_row2').slick({
        centerMode: true,
        centerPadding: '0',
        slidesToShow: 4,
        arrows:true,
        prevArrow:'<button class="prev_arrow"><i class="fa fa-angle-left"></i></button>',
        nextArrow:'<button class="next_arrow"><i class="fa fa-angle-right"></i></button>',
        responsive:[
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                  slidesToScroll: 1,
              }
            },
            {
              breakpoint: 768,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
              }
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 2,
                  slidesToScroll: 3,
              }
            },
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 3,
                  slidesToScroll: 4,
              }
            },

        ]
    });

    /*---blog column3 activation---*/
    $('.blog_column3').owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 3,
        dots:false,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            768:{
				items:2,
			},
            992:{
				items:3,
			},

        }
    });

    /*---blog active activation---*/
    $('.blog_thumb_active').owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 1,
        dots:true,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
    });

    /*---single product activation---*/
    $('.single-product-active').owlCarousel({
        autoplay: true,
		loop: false,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 4,
        margin:15,
        dots:false,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            320:{
				items:2,
			},
            992:{
				items:3,
			},
            1200:{
				items:4,
			},


		  }
    });

    /*---product navactive activation---*/
    $('.product_navactive').owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 4,
        dots:false,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            250:{
				items:2,
			},
            480:{
				items:3,
			},
            768:{
				items:4,
			},

        }
    });

    // $('.modal').on('shown.bs.modal', function (e) {
    //     $('.product_navactive').resize();
    // });

    $('.product_navactive a').on('click',function(e){
      e.preventDefault();

      var $href = $(this).attr('href');

      $('.product_navactive a').removeClass('active');
      $(this).addClass('active');

      $('.product-details-large .tab-pane').removeClass('active show');
      $('.product-details-large '+ $href ).addClass('active show');

    })

    /*---testimonial active activation---*/
    $('.testimonial_active').owlCarousel({
        autoplay: true,
		loop: true,
        nav: false,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 1,
        dots:true,
    });

    /*--- Magnific Popup---*/
    $('.instagram_pupop').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });

    /*--- Magnific Popup Video---*/
    $('.video_popup').magnificPopup({
        type: 'iframe',
        removalDelay: 300,
        mainClass: 'mfp-fade'
    });

    /*--- Magnific Popup Video---*/
    $('.port_popup').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });

    /*--- niceSelect---*/
     $('.select_option').niceSelect();

    /*---  Accordion---*/
    $(".faequently-accordion").collapse({
        accordion:true,
        open: function() {
        this.slideDown(300);
      },
      close: function() {
        this.slideUp(300);
      }
    });

    /*--- counterup activation ---*/
    $('.counter_number').counterUp({
        delay: 10,
        time: 1000
    });

    /*---  ScrollUp Active ---*/
    $.scrollUp({
        scrollText: '<i class="fa fa-angle-double-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    });

    /*---countdown activation---*/

	 $('[data-countdown]').each(function() {
		var $this = $(this), finalDate = $(this).data('countdown');
		$this.countdown(finalDate, function(event) {
		$this.html(event.strftime('<div class="countdown_area"><div class="single_countdown"><div class="countdown_number">%D</div><div class="countdown_title">days</div></div><div class="single_countdown"><div class="countdown_number">%H</div><div class="countdown_title">hrs</div></div><div class="single_countdown"><div class="countdown_number">%M</div><div class="countdown_title">mins</div></div><div class="single_countdown"><div class="countdown_number">%S</div><div class="countdown_title">secs</div></div></div>'));

       });
	});

    /*---slider-range here---*/
    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 0, 500 ],
        slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
       }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
       " - $" + $( "#slider-range" ).slider( "values", 1 ) );

    /*niceSelect*/
     $('.niceselect_option').niceSelect();

    /*---elevateZoom---*/
    $("#zoom1").elevateZoom({
        // responsive : true,
        // cursor: 'crosshair',
        // zoomType : 'inner'
        gallery:'gallery_01',
        zoomType : false,
        lensShape : "round",
        lensSize    : 200
    });

    /*---portfolio Isotope activation---*/
      $('.portfolio_gallery').imagesLoaded( function() {

        var $grid = $('.portfolio_gallery').isotope({
           itemSelector: '.gird_item',
            percentPosition: true,
            masonry: {
                columnWidth: '.gird_item'
            }
        });

          /*---ilter items on button click---*/
        $('.portfolio_button').on( 'click', 'button', function() {
           var filterValue = $(this).attr('data-filter');
           $grid.isotope({ filter: filterValue });

           $(this).siblings('.active').removeClass('active');
           $(this).addClass('active');
        });

    });

    /*---tooltip---*/
    $('[data-toggle="tooltip"]').tooltip();



    /*---Tooltip Active---*/
   $('.action_links ul li a,.quick_button a,.social_sharing ul li a,.product_d_action a,.priduct_social a').tooltip({
        animated: 'fade',
        placement: 'top',
        container: 'body'
    });


    /*---Newsletter Popup---*/

        setTimeout(function() {
            if($.cookie('shownewsletter')==1) $('.newletter-popup').hide();
            $('#subscribe_pemail').keypress(function(e) {
                if(e.which == 13) {
                    e.preventDefault();
                    email_subscribepopup();
                }
                var name= $(this).val();
                  $('#subscribe_pname').val(name);
            });
            $('#subscribe_pemail').change(function() {
             var name= $(this).val();
                      $('#subscribe_pname').val(name);
            });
            //transition effect
            if($.cookie("shownewsletter") != 1){
                // $('.newletter-popup').bPopup();
            }
            $('#newsletter_popup_dont_show_again').on('change', function(){
                if($.cookie("shownewsletter") != 1){
                    $.cookie("shownewsletter",'1')
                }else{
                    $.cookie("shownewsletter",'0')
                }
            });
        }, 2500);


    /*---slide toggle---*/
   $('.cart_link > a').on('click', function(event){
        if($(window).width() < 991){
            $('.mini_cart').slideToggle('medium');
        }
    });

    // Agregar los articulos de lista al carrito
    $('.js-add-cart').on('click', function(e){
        e.preventDefault();
        var d_product = $(this).data('product');
        var d_view = $(this).data('view');

        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/cart',
            data: {
                product: d_product,
                quantity: (d_view == "s") ? $("#quantity").val() : 1,
            },
            dataType: 'json',
            beforeSend: function(){
                $(this).attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
            },
            success: function(r){
                if (r.status == 'OK') {
                    $('.ajx-add-cart-message').html('<i class="fa fa-check-circle-o css-fontSize100 text-success css-marginB10" aria-hidden="true"></i><br>El producto ha sido agregado correctamente a tu cesta');
                    $(".mini_cart").html(r.data.html);
                    $(".js-icon-cart").html('<i class="ion-android-cart"></i>' + r.data.total + ' €<i class="fa fa-angle-down"></i>');
                    $(".cart_quantity").html(r.data.total_products);
                } else {
                    $('.ajx-add-cart-message').html('<i class="fa fa-frown-o css-fontSize100 text-danger css-marginB10" aria-hidden="true"></i><br>' + r.message);
                }
                $('#mod-cart-add').modal();
            }
        });
    });

    $('#frm-search').on('submit', function(e){
        e.preventDefault();
        var encodedURI = encodeURIComponent($("#src_search").val());
        document.location.href = base_url + 'PRODUCTOS/RESULTADOS/' + encodedURI;
    });

    $('#frm-search-mobile').on('submit', function(e){
        e.preventDefault();
        var encodedURI = encodeURIComponent($("#src_search_mobile").val());
        document.location.href = base_url + 'PRODUCTOS/RESULTADOS/' + encodedURI;
    });

    $(".js-featured").on('click', function(e){
        e.preventDefault();
        var value = $(this).data('value');
        var content = '';

        if (value == 1) {
            content = '<div class="text-center css-modal-text"> <div class="shipping_icone"><i class="fa fa-phone"></i></div> <div class="shipping_content"> <h3 class="css-modal-title">PEDIDOS TELEFÓNICOS</h3> <p>Atención personalizada, te asesoramos en tus compras <br><a href="tel:+34954378643">+34 954 378 643 </a> - <a href="tel:+34673375357"> +34 673 375 357</a> -  <i class="fa fa-whatsapp css-fonsize18"></i> Atendemos WhatsApp </p> <h3 class="css-modal-title"><i class="fa fa-clock-o"></i> HORARIOS DE ATENCIÓN</h3> <p>De lunes a viernes 10:00 a 21:00</p> <p>En Cosmeceutical Center cada detalle es importante, pero lo más importante eres tú. Por ese motivo te ofrecemos nuestro trato personalizado en cada pedido</p> <hr> <img src="' + base_admin + 'views/images/modal/modal01.jpg" alt=""> </div> </div>';
        } else if (value == 2) {
            content = '<div class="text-center css-modal-text"> <div class="shipping_icone"><i class="fa fa-gift"></i></div> <div class="shipping_content"> <h3 class="css-modal-title">MUESTRAS GRATUITAS</h3> <p>Muestras adaptadas a tus necesidades en cada pedido.<br></p><h3>¡No experimentes con tu piel!</h3><p>Deja que escojamos para ti las dosis de prueba que mejor se adapten a tu prescripción. ¡Probar por probar no es la solución!</p> <hr> <img src="' + base_admin + 'views/images/modal/modal02.jpg" alt=""> </div> </div>';
        } else if (value == 3) {
            content = '<div class="text-center css-modal-text"> <div class="shipping_icone"><i class="fa fa-truck"></i></div> <div class="shipping_content"> <h3 class="css-modal-title">ENVÍOS GRATUITOS</h3> <p>En tus compras online o telefónicas portes gratuitos<br><strong> en pedidos superiores a 80 euros</strong><br><br>Dedicamos el máximo esmero en la preparación y protección de los paquetes. La conservación de los productos cosmecéuticos es nuestra máxima prioridad. Pioneros en España en enviar tus paquetes en envases isotérmicos cuando las temperaturas son elevadas. <strong>Cuidamos la cadena de frio desde el principio, garantizando las máximas cualidades de los productos.</strong>.</p> <hr> <img src="' + base_admin + 'views/images/modal/modal03.jpg" alt=""> </div> </div>';
        } else if (value == 4) {
            content = '<div class="css-modal-text text-center"> <div class="shipping_icone"><i class="fa fa-cart-plus"></i></div> <div class="shipping_content"> <h3 class="css-modal-title text-center">VENTAJAS</h3> <p><ul class="float-left text-left"><li><img src="https://shop.cosmeceuticalcenter.com/cosmeceuticalcenter.com/views/images/logos/icoCosme.png" alt=""> Somos una clínica donde planteamos el equilibrio entre salud y belleza.</li><br><br><li><img src="https://shop.cosmeceuticalcenter.com/cosmeceuticalcenter.com/views/images/logos/icoCosme.png" alt=""> No seleccionamos cosmética sensorial, bonita o de moda. Solo cosmética medica funcional, eficaz o cosmecéutica.</li><br><br><li><img src="https://shop.cosmeceuticalcenter.com/cosmeceuticalcenter.com/views/images/logos/icoCosme.png" alt=""> Revisamos y seleccionamos para realmente ofrecer productos eficaces, con ingredientes activos en buenas proporciones y no creer en milagros y lanzamientos cosméticos sensacionalistas.</li><br><br><li><img src="https://shop.cosmeceuticalcenter.com/cosmeceuticalcenter.com/views/images/logos/icoCosme.png" alt=""> Puedes consultarnos pues somos Especialistas en el abordaje inicial y planificación del tratamiento. Te damos las pautas personalizadas de la mejor cosmética médica, medicina estética o  cirugía.</li><br><br><li><img src="https://shop.cosmeceuticalcenter.com/cosmeceuticalcenter.com/views/images/logos/icoCosme.png" alt=""> Es una forma diferente de comprar/consumir belleza y antiaging.  La piel es nuestra diana terapéutica, y hay que tratarla con criterio y conocimiento facultativo y científico.</li><br><br></ul><a class="button text-center align-items-center" href="http://www.cosmeceuticalcenter.com/NEW-NUTRICION-DIETETICA-DERMOESTETICA-MEDICINA-ANTIEDAD">+ info</a></p> <hr> <img src="' + base_admin + 'views/images/modal/modal04.jpg" alt=""> </div></div>';
        }

        $("#js-featured-content").html(content);
        $("#mod-featured").modal('show');
    });

    $('.js-product-view').on('click', function(e){
        $.ajax({
            type: 'GET',
            url: base_url + 'ajx/product/view/' + $(this).data('product'),
            dataType: 'json',
            beforeSend: function(){
                $("#modal_box").modal();
                $('#ajx-product-mod').html('<i class="fa fa-spinner fa-spin"></i> Cargando...')
            },
            success: function(r){
                $('#ajx-product-mod').html(r.data);
            }
        });
        e.preventDefault();
    });

    $(".cart_link").on('click', '.js-cart-remove', function(e){
        e.preventDefault();
        var parent = $(this).parent().parent();

        if (confirm('¿Estás seguro de eliminar este producto de tu cesta?')) {
            $.ajax({
                type: 'POST',
                url: base_url + 'ajx/cart-nav/remove',
                data: {
                    product: $(this).data('value'),
                    color: $(this).data('color'),
                    size: $(this).data('size')
                },
                dataType: 'json',
                success: function(r) {
                    if (r.status == "OK") {
                        $(".mini_cart").html(r.data.html);
                        $(".js-icon-cart").html('<i class="ion-android-cart"></i>' + r.data.total + ' €<i class="fa fa-angle-down"></i>');
                        $(".cart_quantity").html(r.data.total_products);
                    } else {
                        alert(r.message);
                    }
                }
            });
        }
    });

    // Quita el click de los enlaces
    $(".js-icon-cart").on('click', function(e){
        e.preventDefault();
    });

    $('.no-click').on('click', function(e){
        e.preventDefault();
    });

    // Agregar a favoritos
    $('.js-add-favorite').on('click', function(e){
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: base_url + 'ajx/favorites/add',
            dataType: 'json',
            data: {
                product: $(this).data('p')
            },
            success: function(r) {
                if (r.status == 'OK') {
                    alert('Se ha agregado a favoritos correctamente');
                } else {
                    alert(r.message);
                }
            }
        });
    });

    $(".let-me-know").on('click', function(e){
        e.preventDefault();
        $('#mod-let-me-know').modal();
    })
})(jQuery);

function addCart(d_product, d_view) {
    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/cart',
        data: {
            product: d_product,
            quantity: (d_view == "s") ? $("#quantity").val() : 1
        },
        dataType: 'json',
        beforeSend: function(){
            $(this).attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                alert('El producto ha sido agregado correctamente');
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(this).attr("disabled", false).html('<i class="fa fa-cart-plus css-fonsize18"></i> añadir carro');
                $(document).scrollTop(0);
            }
        }
    });

    return false;
}

/**
 * Registra para la notificación del usuario
 * @param  {integer} product_id ID del producto
 * @return {boolean}
 */
function notifyUser(e, product_id)
{
    e.preventDefault();

    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/product/notify',
        data: {
            product: product_id
        },
        dataType: 'json',
        beforeSend: function(){
            $(this).attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $('#notification_email_validate').removeClass('text-danger').addClass('text-success').html('Te notificaremos en la brevedad posible');
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(this).attr("disabled", false).html('Enviar');
            }
        }
    });
}
