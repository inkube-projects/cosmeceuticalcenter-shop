$(function(e){
    $('.js-star').on('mouseenter', function(e){
        if ($(this).attr('class') == "js-score js-star") {
            var score = $(this).data('score');

            for (var i = 1; i <= parseInt(score); i++) {
                $('a.js-star[data-score=' + i + '] > i').removeClass('fa-star-o').addClass('fa-star');
            }
        }
    }).on('mouseleave', function(e){
        if ($(this).attr('class') == "js-score js-star") {
            var score = $(this).data('score');

            for (var i = 1; i <= parseInt(score); i++) {
                $('a.js-star[data-score=' + i + '] > i').removeClass('fa-star').addClass('fa-star-o');
            }
        }
    });

    $('.js-score').on('click', function(e){
        e.preventDefault();
        var score = $(this).data('score');
        $('.js-score').removeClass('js-star-fixed').addClass('js-star');
        $('a.js-score > i').removeClass('fa-star').addClass('fa-star-o');

        $('#score').val(score);
        for (var i = 1; i <= parseInt(score); i++) {
            $('a[data-score=' + i + '] > i').removeClass('fa-star-o').addClass('fa-star');
            $('a[data-score=' + i + ']').removeClass('js-star').addClass('js-star-fixed');
        }
    });

    $("#frm-opinion").validate({
        rules:{
            opinion: {
                required: true,
                noSpecialCharacters: /([*+?^${}><\[\]\/\\])/g
            },
        },
        messages:{
            opinion: {
                required: "Debes ingresar tu opinión",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function() {
            persist('frm-opinion');
            return false;
        }
    });
});

function persist(form_name) {
    var i = $("#" + form_name).serialize();
    
    $.ajax({
        type: 'POST',
        url: base_url + 'ajx/opinion/add/' + $('#key').data('id'),
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", base_url + "OPINION/" + $('#key').data('id') + '/OK');
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Registrarme');
                $(document).scrollTop(0);
            }
        }
    });
}
