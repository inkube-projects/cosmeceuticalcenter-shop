<?php
require('core/core.php');
require('core/methods/controllers.php');

// if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
//     $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
//     header('HTTP/1.1 301 Moved Permanently');
//     header('Location: ' . $redirect);
//     exit();
// }

if (isset($_GET['view']) && isset($_GET['meth'])) {
    if (file_exists("core/Controller/".$_GET['view']."Controller.php") && in_array($_GET['meth'], $controllers[$_GET['view']])) {
        include("core/Controller/".$_GET['view']."Controller.php");
        $class_name = $_GET['view']."Controller";
        $action_method = $_GET['meth']."Action";
        $controller = new $class_name;
        $controller->{$action_method}();
    } else {
        include("core/Controller/ErrorController.php");
        $controller = new ErrorController;
        $controller->notFoundAction();
    }
} else {
    include("core/Controller/IndexController.php");
    $controller = new IndexController;
    $controller->indexAction();
}

?>
