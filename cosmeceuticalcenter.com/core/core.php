<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// nucleo del sitio web
@session_start();

// ---- Local
define('DB_USER', 'root');
define('DB_PASS', '123456');
define('DB_DSN', 'mysql:dbname=local_cosmeceuticalcenter_db;host=localhost;charset=utf8');
define('BASE_DOMAIN', 'http://localhost/aaa/');
define('BASE_PATH', 'cosmeceuticalcenter-shop/');

// ---- dev
// define('DB_USER', 'nas_cosme_dev');
// define('DB_PASS', 'wA4oht0sEt');
// define('DB_DSN', 'mysql:dbname=dev_cosmeceutical_shop;host=localhost;charset=utf8');
// define('BASE_DOMAIN', 'http://cosmeceutical.newappsystem.com/');
// define('BASE_PATH', '');

// ---- Producción
// define('DB_USER', 'shop_user');
// define('DB_PASS', '5zF2rz0@');
// define('DB_DSN', 'mysql:dbname=cosmeceutical_shop;host=localhost;charset=utf8');
// define('BASE_DOMAIN', 'https://shop.cosmeceuticalcenter.com/');
// define('BASE_PATH', '');

define('BASE_URL', BASE_DOMAIN.BASE_PATH);
define('BASE_URL_PUBLIC', BASE_URL.'cosmeceuticalcenter.com/');
define('APP_ASSETS', BASE_URL_PUBLIC.'views/assets/');
define('APP_PLUGINS', BASE_URL_PUBLIC.'views/assets/plugins/');
define('APP_PUBLIC_PATH_IMAGES', 'views/images/');
define('APP_TITLE', 'Cosmeceutical Center');


define('APP_IMG', BASE_URL_PUBLIC.'views/images/');
define('APP_IMG_PUBLIC', BASE_URL_PUBLIC.'views/images/');
define('APP_NO_IMAGES', BASE_URL_PUBLIC.'views/images/no_images/');
define('APP_LOGO', BASE_URL_PUBLIC.'views/images/logos/');

// Usuarios - backend
define('APP_IMG_ADMIN', 'views/images/users/');
define('APP_IMG_ADMIN_USER', BASE_URL_PUBLIC.'views/images/users/');

// Usuarios
define('APP_SYSTEM_USERS', APP_PUBLIC_PATH_IMAGES.'users/');
define('APP_IMG_USERS', APP_IMG_PUBLIC.'users/');
// Productos
define('APP_IMG_PRODUCTS', APP_IMG_PUBLIC.'products/');
// Imagenes del carrusel
define('APP_IMG_CAROUSEL', APP_IMG_PUBLIC.'slider/');
// Marcas
define('APP_IMG_BRANDS', APP_IMG_PUBLIC.'brands/');
// Banners
define('APP_IMG_BANNERS', APP_IMG_PUBLIC.'banners/');
// Icons
define('APP_IMG_ICONS', APP_IMG_PUBLIC.'icons/');
// Imagenes del categoría
define('APP_IMG_CATEGORY', APP_IMG_PUBLIC.'category/');

// IVA
define('APP_IVA', 21);
// Datos de email
define('EMAIL_HOST', 'mail.cosmeceuticalcenter.com');
define('EMAIL_SENDER', 'enviosweb@cosmeceuticalcenter.com');
define('EMAIL_PASSWORD', 'oopXe2A587Lmo123z');
// PayPal test
// define('PAYPAL_ENV', 'sandbox');
// define('PAYPAL_URL', 'https://api.sandbox.paypal.com/v1/');
// define('PAYPAL_CLIENT_ID', 'Afit9YjKmuF4VvhtnoDHKKMZx-5h-g7aWdtkKAsOSkRSP5it2_COD7CJvJ_-m0qygDw-FdIO4My6EjVr');
// define('PAYPAL_SECRET', 'EBf3ntYzMgCtTvCz1IWxGlAVQ4J4tvOEx8vTmd7xD--Zy6SfPt4rlCgqNZg4py6jyTOn3N5iG5e5uG1h');

define('PAYPAL_ENV', 'production');
define('PAYPAL_URL', 'https://api.paypal.com/v1/');
define('PAYPAL_CLIENT_ID', 'AXo9yBxPY5-EcC4lUC2q6p1FeD1_SgpaoZj_DFnJjAjBuicDx5HefPp7u4fsSecbwOemNA4f8eXz4IiD');
define('PAYPAL_SECRET', 'EMNrFKwkVIYGvZ77Ls_L2KOxBprrhm-KVVxIy2n0ps44McxM-_3gnPsw8yma5A4KNZe0Ne_8bPUyeXdT');

setlocale(LC_TIME, 'es_VE');
date_default_timezone_set('Europe/Madrid');

require('core/Model/classConnection.php');
require('core/bin/functions/main-lib.php');
require('core/paginator/Paginator.php');
require('vendor/autoload.php');

?>
