<?php

/**
 * Clase para paginador
 */
class Paginator
{
    public $db;

    public $arr;

    function __construct($db, array $arr)
    {
        $this->db = $db;
        $this->columns = $arr['columns'];
        $this->table = $arr['table'];
        $this->where = $arr['where'];
        $this->results_page = $arr['results_pages'];
        $this->page = $arr['actually_page'];
        $this->url = $arr['url'];
        $this->additional = $arr['additional'];
    }

    /**
     * Retorna un arreglo con los resultados por página
     * @param  string $sort Forma de ordenar los resultados
     * @return array
     */
    public function getResults($group, $sort = "")
    {
        // Se obtiene el número total de resultados
        $total = $this->getTotal($group);

        // Se verifica la página actual
        $act_page = $this->page;
        if ($act_page > ceil($total / $this->results_page)) {
            $act_page = 1;
        } elseif ($act_page == 0){
            $act_page = 1;
        }

        // Se obtiene el total de páginas
        $page_total = ceil($total / $this->results_page);

        // Resultados por pagina
        $limit  = "LIMIT ".($act_page - 1) * $this->results_page.", ".$this->results_page;

        // Se arma y ejecuta el query final
        $query = "SELECT ".$this->columns." FROM ".$this->table." WHERE ".$this->where." ".$group." ".$sort." ".$limit;
        // showArr($query); exit;
        $arr_result = $this->db->fetchSQL($query);

        return $arr_result;
    }


    /**
     * Realiza los calculos y retorna el paginador
     * @return object
     */
    public function getPaginator($group)
    {
        $total = $this->getTotal($group);
        $act_page = $this->page;
        if ($act_page > ceil($total / $this->results_page)) { $act_page = 1; } elseif ($act_page == 0){ $act_page = 1;}
        $page_total = ceil($total / $this->results_page);

        if ($act_page > 2){
            $pager_it_lo = $act_page - 2;

            if (($act_page + 2) >$page_total) {
                $pager_it_hi = $page_total;
            } else {
                $pager_it_hi = $act_page + 2;
            }
        } else {
            $pager_it_lo = 1;

            if($page_total > 5){
                $pager_it_hi = 5;
            } else {
                $pager_it_hi = $page_total;
            }
        }

        return $this->getHtmlPaginator($act_page, $page_total, $total);
    }

    /**
     * Retorna el HTML del paginador
     * @param  integer $act_page   Página actual
     * @param  integer $page_total Cantidad de páginas
     * @param  string  $url        URL base
     * @param  integer $total      Total de registros
     * @return string
     */
    public function getHtmlPaginator($act_page, $page_total, $total)
    {
        $url = $this->url;
        $additional = $this->additional;

        if($act_page == ""){ $act_page = 1; }

        if ($act_page > 2){
            $pager_it_lo = $act_page - 2;

            if (($act_page + 2) > $page_total){ $pager_it_hi = $page_total; } else { $pager_it_hi = $act_page + 2; }
        } else {
            $pager_it_lo = 1;
            if ($page_total > 5){ $pager_it_hi = 5; } else { $pager_it_hi = $page_total; }
        }

        $html = '
            <div class="pagination"><ul>
        ';

        if ($act_page > 1) {
            $html .= '<li><a href="'.$url.'/PAGINA/1'.$additional.'" title="Inicio"> << </a></li>';
        }

        if ($act_page != 1) {
            $html .= '<li class="previous"><a href="'.$url.'/PAGINA/'.($act_page - 1).$additional.'" title="Anterior" aria-label="Previous">Anterior</li>';
        } else {
            //$html .= '<li><a href="javascript: location.reload();" title="Anterior" class="css-disabled" aria-label="Previous"><i class="ti-arrow-left"></i></a></li>';
        }

        for($j = $pager_it_lo; $j <= $act_page; $j++){
            $ext = ''; $lnk = ''; $ext_fond='';
            if ($j == $act_page){ $ext = 'class="current"'; $lnk = "#"; $ext_fond = ''; }
            if ($total !=0 ) {
                $html .='<li '.$ext.'><a href="'.$url.'/PAGINA/'.$j.$additional.'" '.$ext.'>'.$j.'</a></li>';
            }
        }
        for($j = ($act_page + 1); $j <= $pager_it_hi; $j++){
            $html .= '<li ><a class="css-no-active" href="'.$url.'/PAGINA/'.$j.$additional.'">'.$j.'</a></li>';
        }

        if (($act_page != $page_total) && ($total != 0)){
            $html .= '<li class="next"><a href="'.$url.'/PAGINA/'.($act_page + 1).$additional.'" title="Siguiente" aria-label="Next">Siguiente</a></li>';
        } else {
            // $html .= '<li><a href="javascript: location.reload();" title="Siguiente" aria-label="Next" class="css-disabled"><i class="ti-arrow-right"></i></a></li>';
        }
        if (($j-1) <= $page_total) {
            $html .= '<li><a href="'.$url.'/PAGINA/'.$page_total.$additional.'" title="Final"> >> </a></li>';
        }

        $html .= '</ul></div>';

        return $html;
    }

    /**
     * Total de entradas que tiene la tabla
     * @return integer
     */
    public function getTotal($group = "")
    {
        $s = "SELECT * FROM ".$this->table." WHERE ".$this->where." ".$group;
        return $this->db->query($s)->rowCount();
    }

    /**
     * Obtiene ek rango superior he inferior
     * @param  integer $total Entradas por página
     * @return array
     */
    public function getRange($total = 0)
    {
        $act_page = ($this->page == 0) ? 1 : $this->page ;
        $result_page = $this->results_page;

        $arr_result = array(
            'min' => (($act_page - 1) * $result_page) + 1,
            'max' => (($act_page - 1) * $result_page) + $total
        );

        return $arr_result;
    }
}


?>
