<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');

/**
 *
 */
class DashboardController extends ControllerAware
{
    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Área privada
     * @return void
     */
    public function dashAction()
    {
        $db = $this->db;
        // datos de la cuenta
        $user_name = mb_convert_case($this->user_name, MB_CASE_TITLE, "UTF-8");
        $user_lastname = mb_convert_case($this->user_last_name, MB_CASE_TITLE, "UTF-8");
        $mobile = $this->user_phone_1;
        $email = $this->user_email;
        $note = $this->user_note;
        $birthdate = explode('-', $this->user_birth);
        $day = $birthdate[2];
        $month = $birthdate[1];
        $year = $birthdate[0];
        $arr_monts = array(
            '1' => 'Enero',
            '2' => 'Febrero',
            '3' => 'Marzo',
            '4' => 'Abril',
            '5' => 'Mayo',
            '6' => 'Junio',
            '7' => 'Julio',
            '8' => 'Agosto',
            '9' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre',
        );


        // Datos de facturación
        $s = "SELECT * FROM bill_information WHERE user_id='".$this->user_id."'";
        $arr_bill = $db->fetchSQL($s);

        $identification = @$arr_bill[0]['identification'];
        $bill_name = @mb_convert_case($arr_bill[0]['name'], MB_CASE_TITLE, "UTF-8");
        $bill_last_name = @mb_convert_case($arr_bill[0]['last_name'], MB_CASE_TITLE, "UTF-8");
        $company = @$arr_bill[0]['company'];
        $bill_country = @$arr_bill[0]['country_id'];
        $bill_province = @$arr_bill[0]['province_id'];
        $bill_address_1 = @$arr_bill[0]['address_1'];
        $bill_address_2 = @$arr_bill[0]['address_2'];
        $bill_city = @$arr_bill[0]['city'];
        $bill_postal_code = @$arr_bill[0]['postal_code'];
        $account_type = $this->user_account_type;

        // Dirección de envíos
        $s = "SELECT * FROM shipping_address WHERE user_id='".$this->user_id."'";
        $arr_ship = $db->fetchSQL($s);

        $location_name = @$arr_ship[0]['name_location'];
        $addr_country = @$arr_ship[0]['country_id'];
        $addr_province = @$arr_ship[0]['province_id'];
        $addr_address_1 = @$arr_ship[0]['address_1'];
        $addr_address_2 = @$arr_ship[0]['address_2'];
        $addr_city = @$arr_ship[0]['city'];
        $addr_postal_code = @$arr_ship[0]['postal_code'];
        $addr_order_note = @$arr_ship[0]['note'];

        $s = "SELECT * FROM view_orders WHERE user_id='".$this->user_id."'";
        $arr_order = $db->fetchSQL($s);

        foreach ($arr_order as $key => $val) {
            $create = datetime_format($val['create_at']);
            $arr_order[$key]['formed_date'] = to_date($create['date']);

            if ($val['status_id'] == 1) {
                $arr_order[$key]['formed_status'] = '<i class="fa fa-hourglass-start css-fonsize16 css-color-naranja"></i> '.$val['status_name'];
            } elseif ($val['status_id'] == 2) {
                $arr_order[$key]['formed_status'] = '<i class="fa fa-hourglass-start css-fonsize16 css-color-naranja"></i> '.$val['status_name'];
            } elseif ($val['status_id'] == 3) {
                $arr_order[$key]['formed_status'] = '<i class="fa fa-times css-fonsize16 css-color-naranja"></i> '.$val['status_name'];
            } elseif ($val['status_id'] == 4) {
                $arr_order[$key]['formed_status'] = '<i class="fa fa-truck css-fonsize16 css-color-naranja"></i> '.$val['status_name'];
            } elseif ($val['status_id'] == 5) {
                $arr_order[$key]['formed_status'] = '<span class="success"><i class="fa fa-check-square css-fonsize16 css-color-verde"></i> '.$val['status_name'].'</span>';
            } elseif ($val['status_id'] == 6) {
                $arr_order[$key]['formed_status'] = '<span class="success"><i class="fa fa-check-square css-fonsize16 css-color-verde"></i> '.$val['status_name'].'</span>';
            } elseif ($val['status_id'] == 7) {
                $arr_order[$key]['formed_status'] = '<i class="fa fa-hourglass-start css-fonsize16 css-color-naranja"></i> '.$val['status_name'];
            }
        }

        // Paises
        $s = "SELECT * FROM country";
        $arr_country = $db->fetchSQL($s);
        $country = 73;

        // Provincia
        $s = "SELECT * FROM province";
        $arr_province = $db->fetchSQL($s);

        // Favoritos
        $s = "SELECT * FROM view_favorites WHERE user_id='".$this->user_id."'";
        $arr_favorites = $db->fetchSQL($s);

        // Opiniones
        $s = "SELECT * FROM view_features WHERE user_id='".$this->user_id."'";
        $arr_features = $db->fetchSQL($s);

        foreach ($arr_features as $key => $val) {
            $create = datetime_format($val['feature_create']);
            $q = "SELECT * FROM view_products WHERE id='".$val['product_id']."'";
            $arr_porduct = $db->fetchSQL($q);

            $arr_features[$key]['product_name'] = $arr_porduct[0]['product_name'];
            $arr_features[$key]['category_name'] = $arr_porduct[0]['category_name'];
            $arr_features[$key]['brand_name'] = $arr_porduct[0]['brand_name'];
            $arr_features[$key]['formed_date'] = $create['date'];
        }

        require_once('html/account/dashboard.php');
    }

    /**
     * Carrito
     * @param  string $view Ruta de la vista
     * @return void
     */
    public function cartAction($view = 'html/account/cart.php', $session = true)
    {
        $db = $this->db;
        if ($view == 'html/account/cart.php') {
            $province_id = 1;
            if (isset($_SESSION['USER_SESSION_COSME']['id'])) {
                $province_id = $db->getValue("province_id", "shipping_address", "user_id='".$this->user_id."'");
            }

            $_SESSION['country'] = 73;
            $_SESSION['province'] = ($province_id == "" || !$province_id) ? "1" : $province_id;
        }

        if (!$session) {
            $arr_cart = $this->getCartFromCookies();
        } else {
            $s = "SELECT * FROM view_cart WHERE user_id='".$this->user_id."'";
            $arr_cart = $db->fetchSQL($s);
        }
        
        $sub_total = 0;
        $shipping = "0.00";

        foreach ($arr_cart as $key => $val) {
            $product_iva = $val['iva'];
            $product_iva = ($product_iva) ? $product_iva : APP_IVA;
            $arr_cart[$key]['product_iva'] = $product_iva;
            $arr_cart[$key]['formed_image'] = ($val['cover_image']) ? APP_IMG_PRODUCTS."product_".$val['product_id']."/cover/".$val['cover_image'] : APP_NO_IMAGES."no_image.jpg";
            $arr_cart[$key]['category_name'] = $db->getValue('name', 'products_category', "id='".$val['category_id']."'");
            $arr_cart[$key]['brand_name'] = $db->getValue('name', 'brands', "id='".$val['brand_id']."'");
            $arr_cart[$key]['size_id'] = $val['size_id'];
            $arr_cart[$key]['color_id'] = $val['color_id'];
            $arr_cart[$key]['size'] = $db->getValue("name", "size", "id='".$val['size_id']."'");
            $arr_cart[$key]['color'] = $db->getValue("name", "colors", "id='".$val['color_id']."'");

            $cnt_discount = $db->getCount('discount', "product_id='".$val['product_id']."'");
            $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$val['product_id']."'");
            $new_price = "";
            $type_price = "";

            if ($cnt_discount == 1) {
                $new_price = $db->getValue("new_price", "discount", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
                $type_price = "PROMOCIÓN";
            } elseif ($cnt_weekly == 1) {
                $new_price = $db->getValue("new_price", "weekly_recommendation", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
                $type_price = "R. SEMANAL";
            }

            $arr_cart[$key]['new_price'] = $new_price;
            $arr_cart[$key]['value'] = ($new_price != "") ? $new_price : $val['price_iva'];

            if ($new_price != "") {
                $calculated_iva = 0.00;
                $arr_cart[$key]['sub_total'] = number_format(($new_price * $val['quantity']),2,".","");
            } else {
                // $arr_cart[$key]['sub_total'] = number_format(($val['price'] * $val['quantity']),2,".","");
                $arr_cart[$key]['sub_total'] = number_format(($val['price_iva'] * $val['quantity']),2,".","");
                $calculated_iva = number_format((($product_iva * ($val['price'] * $val['quantity'])) / 100),2,".","");
            }

            $arr_cart[$key]['calculated_iva'] = $calculated_iva;
            // $arr_cart[$key]['sub_total'] = number_format(($calculated_iva + $arr_cart[$key]['sub_total']),2,".","");
            $arr_cart[$key]['sub_total'] = number_format($arr_cart[$key]['sub_total'],2,".","");
            $arr_cart[$key]['type_price'] = $type_price;

            $sub_total += $arr_cart[$key]['sub_total'];
        }

        $sub_total = number_format($sub_total,2,".","");

        // Se obtiene el valor del shipping
        if (isset($_SESSION['country'])) {
            $country_id = $_SESSION['country'];

            if ($country_id == 73) {
                if (isset($_SESSION['province'])) {
                    $province_id = $_SESSION['province'];
                } else {
                    $province_id = $this->user_province;
                }

                $shipping = $db->getValue('value', 'view_zones', "province_id='".$province_id."'");
            } else {
                $shipping = $db->getValue('value', 'view_zones_country', "country_id='".$_SESSION['country']."'");
            }
        } else {
            $country_id = 73;
            $province_id = 1;

            if ($session) {
                $country_id = $this->user_country;
                $province_id = $this->user_province;
            }

            $shipping = $db->getValue('value', 'view_zones', "province_id='".$province_id."'");
        }

        if (!$shipping || $shipping == "" || $sub_total > 80) {
            $shipping = "0.00";
        }

        $total = number_format($sub_total + $shipping,2,".","");

        $s = "SELECT * FROM province";
        $arr_province = $db->fetchSQL($s);
        $country_name = $db->getValue("country", "country", "id='".$country_id."'");
        if (isset($province_id)) {
            $province_name = $db->getValue("name", "province", "id='".$province_id."'");
        } else {
            $province_name = NULL;
        }

        $s = "SELECT * FROM method_payment";
        $arr_method = $db->fetchSQL($s);

        $s = "SELECT * FROM country";
        $arr_country = $db->fetchSQL($s);

        $alert = ""; $validate_address = true;
        if ($view == "html/account/pay.php") {
            $_SESSION['COSME_TOTAL'] = $total;
            $_SESSION['COSME_CHARGE'] = 0.00;

            // Se valida de que el usuario tenga una dirección
            // if (!$this->validateAddress()) {
            //     $alert = '<div class="alert alt-alert alert-danger" role="alert">Debes rellenar tu información en el <b>Área de Clientes</b> de envío antes de realizar una compra.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            //     $validate_address = false;
            // }
        }

        $province_id = (isset($province_id)) ? $province_id : "";
        $shipping = number_format($shipping,2,".","");

        require_once($view);
    }

    /**
     * Pago
     * @return void
     */
    public function payAction()
    {
        unset($_SESSION['COSME_ORDER_PARAMS']);
        if ($this->cart['total_products'] == 0) { header('location: '.BASE_URL.'HOME'); exit; }
        $db = $this->db;
        $this->cartAction('html/account/pay.php');
    }

    /**
     * Pantalla de Orden procesada
     * @return void
     */
    public function processedAction()
    {
        $db = $this->db;
        $code = secure_mysql($_GET['id']);

        if (isset($_SESSION['COSME_ORDER_PARAMS'])) {
            $this->validRecordCustom("redsys_reference='".$code."' AND user_id='".$this->user_id."'", "orders", BASE_URL."HOME");
            $text = '
            <div class="col-12 col-md-8 offset-md-2 marginBottomX">
                <h3><i class="fa fa-1x fa-credit-card"> </i> Pago con tarjeta.</h3>
            </div>';
        } else {
            $this->validRecordCustom("code='".$code."' AND user_id='".$this->user_id."'", "orders", BASE_URL."HOME");
            $text = '
            <div class="col-12 col-md-8 offset-md-2 marginBottomX">
                <h3><i class="fa fa-1x fa-cc-paypal"> </i> Pago con Paypal.</h3>
            </div>';
        }

        require_once('html/account/processed.php');
    }

    /**
     * Paso 01
     * @return void
     */
    public function loginAction()
    {
        $db = $this->db;

        require_once('html/account/login-cart.php');
    }

    public function cartRegisterAction()
    {
        $db = $this->db;
        $email = $_SESSION['CART_REGISTER_EMAIL'];
        $pass = $_SESSION['CART_REGISTER_PASS'];

        $arr_monts = array(
            '1' => 'Enero',
            '2' => 'Febrero',
            '3' => 'Marzo',
            '4' => 'Abril',
            '5' => 'Mayo',
            '6' => 'Junio',
            '7' => 'Julio',
            '8' => 'Agosto',
            '9' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre',
        );

        // Paises
        $s = "SELECT * FROM country";
        $arr_country = $db->fetchSQL($s);
        $country = 73;

        // Provincia
        $s = "SELECT * FROM province";
        $arr_province = $db->fetchSQL($s);

        require_once('html/account/register-cart.php');
    }
}


?>
