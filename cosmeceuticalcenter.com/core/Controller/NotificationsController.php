<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');

/**
 * Controlador de las notificaciones
 */
class NotificationsController extends ControllerAware
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Listado de notificaciones
     * @return void
     */
    public function listAction()
    {
        $db = $this->db;

        $s = "SELECT * FROM notifications WHERE user_id='".$this->user_id."' ORDER BY id DESC";
        $arr_notifications = $db->fetchSQL($s);

        foreach ($arr_notifications as $key => $not) {
            $arr_notifications[$key]['formed_status'] = ($not['status'] == 0) ? '<b>No visto</b>' : 'Visto';
        }

        require_once('html/notifications/notifications-list.php');
    }
}


?>
