<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');

/**
 * Controlador para las cuentas
 */
class AccountController extends ControllerAware
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Registro
     * @return void
     */
    public function registerAction()
    {
        $db = $this->db;

        $arr_monts = array(
            '1' => 'Enero',
            '2' => 'Febrero',
            '3' => 'Marzo',
            '4' => 'Abril',
            '5' => 'Mayo',
            '6' => 'Junio',
            '7' => 'Julio',
            '8' => 'Agosto',
            '9' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre',
        );

        // Paises
        $s = "SELECT * FROM country";
        $arr_country = $db->fetchSQL($s);
        $country = 73;

        // Provincia
        $s = "SELECT * FROM province";
        $arr_province = $db->fetchSQL($s);

        // SEO
        $s = "SELECT * FROM seo WHERE section_id='3'";
        $arr_seo = $db->fetchSQL($s);

        require_once('html/account/register.php');
    }

    /**
     * Inicio de sesión
     * @return void
     */
    public function loginAction()
    {
        $db = $this->db;
        
        // SEO
        $s = "SELECT * FROM seo WHERE section_id='4'";
        $arr_seo = $db->fetchSQL($s);

        require_once('html/account/login.php');
    }

    /**
     * Verifica la cuenta o el newsletter
     * @return void
     */
    public function verifyAction()
    {
        $db = $this->db;
        $token = sanitize(secure_mysql($_GET['token']));
        $cnt_val = $db->getCount("user_token", "token='".$token."'");

        if ($cnt_val == 1) {
            $s = "SELECT * FROM user_token WHERE token='".$token."'";
            $arr_token = $db->fetchSQL($s);

            $type = $arr_token[0]['type'];

            switch ($type) {
                case 1:
                    $user_id = $arr_token[0]['user_id'];
                    $s = "SELECT * FROM view_user WHERE id='".$user_id."'";
                    $arr_user = $db->fetchSQL($s);

                    if ($arr_user[0]['status_id'] == 1) {
                        $db->updateAction("user", array('status_id' => '2'), "id='".$user_id."'");
                    }

                    $text = '<h2 class="css-color-verde">Se ha verificado tu cuenta correctamente <i class="fa fa-1xB fa-check css-grisIco"> </i></h2>
                    Ahora puedes iniciar sesión y disfrutar de nuestros servicios.<br><br>
                    <a class="btn btn-warning" href="LOGIN" role="button"> <i class="fa fa-arrow-circle-left fa-lg"></i> Iniciar Sesión</a>';
                    break;

                case 2:
                    $email = $arr_token[0]['email'];
                    $s = "SELECT * FROM newsletter WHERE email='".$email."'";
                    $arr_newsletter = $db->fetchSQL($s);

                    if ($arr_newsletter[0]['status'] == 0) {
                        $db->updateAction("newsletter", array('status' => '1', 'update_at' => date('Y-m-d H:i:s')), "email='".$email."'");
                    }

                    $text = '<h2 class="css-color-verde">Se ha verificado tu email correctamente <i class="fa fa-1xB fa-check css-grisIco"> </i></h2>';
                    break;
            }

        } else {
            $text = '<h2 class="text-danger">Token inválido <i class="fa fa-1xB fa-times css-grisIco"> </i></h2>
            Si crees que es un error ponte en contacto con nosotros.<br><br>';
        }

        require_once('html/account/verify.php');
    }
}


?>
