<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');

/**
 * Controlador del index y pantallas sueltas
 */
class IndexController extends ControllerAware
{
    /**
     * Index
     * @return void
     */
    public function homeAction()
    {
        $db = new Connection;

        $s = "SELECT * FROM carousel WHERE status='1' ORDER BY order_image ASC";
        $arr_carousel = $db->fetchSQL($s);

        foreach ($arr_carousel as $key => $val) {
            $arr_carousel[$key]['formed_image'] = APP_IMG_CAROUSEL.$val['image'];
        }

        $s = "SELECT * FROM view_weekly WHERE status='1' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'";
        $arr_weekly = $db->fetchSQL($s);

        foreach ($arr_weekly as $key => $val) {
            // if ($val['iva'] && $val['iva'] != 0) {
            //     $product_iva_percent = $val['iva'];
            //     $product_iva = ($product_iva_percent * $val['price']) / 100;
            //     $price = number_format(($product_iva + $val['price']),2,".","");
            // } else {
            //     $price = $val['price'];
            // }
            $price = $val['price_iva'];
            $new_price = $val['new_price'];

            if ($price == $new_price) {
                $arr_weekly[$key]['formed_old_price'] = NULL;
                $arr_weekly[$key]['formed_price'] = $price;
            } else {
                $arr_weekly[$key]['formed_old_price'] = $price;
                $arr_weekly[$key]['formed_price'] = $new_price;
            }


            $arr_weekly[$key]['calculated_price'] = $price;
            $arr_weekly[$key]['formed_image'] = ($val['cover_image']) ? APP_IMG_PRODUCTS."product_".$val['product_id']."/cover/".$val['cover_image'] : APP_NO_IMAGES."no_image.jpg";
        }

        $s = "SELECT * FROM view_novelty WHERE status='1' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'";
        $arr_novelty = $db->fetchSQL($s);

        foreach ($arr_novelty as $key => $val) {
            $arr_novelty[$key]['formed_image'] = ($val['cover_image']) ? APP_IMG_PRODUCTS."product_".$val['product_id']."/cover/".$val['cover_image'] : APP_NO_IMAGES."no_image.jpg";
        }

        $s = "SELECT * FROM view_discount WHERE status='1' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'";
        $arr_discount = $db->fetchSQL($s);

        foreach ($arr_discount as $key => $val) {
            // if ($val['iva'] && $val['iva'] != 0) {
            //     $product_iva_percent = $val['iva'];
            //     $product_iva = ($product_iva_percent * $val['price']) / 100;
            //     $price = number_format(($product_iva + $val['price']),2,".","");
            // } else {
            //     $price = $val['price'];
            // }
            $arr_discount[$key]['formed_image'] = ($val['cover_image']) ? APP_IMG_PRODUCTS."product_".$val['product_id']."/cover/".$val['cover_image'] : APP_NO_IMAGES."no_image.jpg";
            $price = $val['price_iva'];

            $arr_discount[$key]['product_price'] = $val['price_iva'];
            $arr_discount[$key]['discount_price'] = $val['new_price'];

            // $arr_discount[$key]['calculated_price'] = $price;
            
        }

        // SEO
        $s = "SELECT * FROM seo WHERE section_id='1'";
        $arr_seo = $db->fetchSQL($s);

        require_once('html/index/home.php');
    }

    /**
     * Cerrar sesión
     * @return void
     */
    public function logOutAction()
    {
        $db = new Connection;
        // Se sincroniza el carrito
        $s = "SELECT * FROM cart_detail WHERE user_id='".$this->user_id."'";
        $arr_cart = $db->fetchSQL($s);
        $arr_cookie_cart = array();

        foreach ($arr_cart as $key => $val) {
            $arr_cookie_cart[] = array(
                'product_id' => $val['product_id'],
                'quantity' => $val['quantity'],
                'size_id' => $val['size_id'],
                'color_id' => $val['color_id']
            );
        }

        setcookie('cookie_cart_cosmeceutical','',time() - 3600, "/");
        if (count($arr_cookie_cart) > 0) {
            setcookie('cookie_cart_cosmeceutical', json_encode($arr_cookie_cart), time() + (86400 * 30), "/");
            $_SESSION['COSME_CART'] = json_encode($arr_cookie_cart);
        }

        // Se destruye la sesión
        $_SESSION['USER_SESSION_COSME'] = "";
        session_unset(); //para eliminar las variables de sesion
        session_destroy(); //con esto destruyes la sesion
        header('location: '.BASE_URL.'HOME');
    }
}


?>
