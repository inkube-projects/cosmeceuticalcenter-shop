<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');
include('core/Model/GeneralMethods.php');

/**
 * Controlador para los productos
 */
class ProductsController extends ControllerAware
{
    private static $allowed_sort = array(
        'ASC' => 'De la A a la Z',
        'ULTIMOS' => 'Últimos productos',
        'POPULAR' => 'Más populares',
        'PRECIO/ASC' => 'Precio ascendente',
        'PRECIO/DESC' => 'Precio descendente',
        'NOVEDADES' => 'Novedades',
        'DESC' => 'De la Z a la A',
        'DESCUENTOS' => 'Con descuento'
    );

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Listado de productos
     * @return void
     */
    public function listAction()
    {
        $db = $this->db;

        $src_search = (isset($_GET['src_search']) && (!empty($_GET['src_search']))) ? @secure_mysql(sanitize($_GET['src_search'])) : "" ;
        $select_sort = (isset($_GET['sort']) && (!@empty($_GET['sort']))) ? @secure_mysql(sanitize($_GET['sort'])) : NULL ;
        $category = (isset($_GET['category']) && (!empty($_GET['category']))) ? @secure_mysql(sanitize($_GET['category'])) : "" ;
        $brand = (isset($_GET['brand']) && (!@empty($_GET['brand']))) ? @secure_mysql(sanitize($_GET['brand'])) : "" ;
        $subcategory = (isset($_GET['sub']) && (!@empty($_GET['sub']))) ? @number_format($_GET['sub'],0,'','') : "" ;
        $additional = "";
        $uri_ext = "";
        $w = "NOT product_name = '' AND status='1' AND brand_status='1'";

        if ($category != "") {
            $cat_id = $db->getValue("id", "products_category", "name_canonical='".$category."'");
            $w .= " AND id IN (SELECT product_id FROM products_categories WHERE category_id='".$cat_id."')";
            $additional = "/CATEGORIA/".$category;
            // Informacion de la categoría
            $cat_info = $this->getCategoryInfo($cat_id);

            // Se verifica si tiene subcategorías
            $s = "SELECT * FROM products_sub_category WHERE category_id='".$cat_info['id']."'";
            $arr_subcategories = $db->fetchSQL($s);
        }

        if ($subcategory != "" && $subcategory != 0) {
            $w .= " AND id IN (SELECT product_id FROM products_subcategories WHERE subcategory_id='".$subcategory."')";
        }

        if ($brand != "") {
            $brand_id = $db->getValue("id", "brands", "name_canonical='".$brand."'");
            $w .= " AND brand_name_canonical='".$brand."'";
            $additional = "/MARCA/".$brand;

            $arr_brand = $this->getBrandById($brand_id);
        }

        if ($src_search != "") {
            $w .= " AND (product_name LIKE '%".$src_search."%' OR brand_name='".$src_search."')";
            $additional = "/".$src_search;
            $uri_ext = "/RESULTADOS";
        }

        if (!is_null($select_sort) && !$this->validSort($select_sort)) { header('location: '.BASE_URL.'PRODUCTOS'); exit; }
        $from = "";
        if ($select_sort == "ULTIMOS") {
            $sort = 'ORDER BY id DESC, product_name ASC';
        } elseif ($select_sort == "POPULAR") {
            $from = "view_weekly";
            $sort = 'ORDER BY product_name ASC';
        } elseif ($select_sort == "PRECIO/ASC") {
            $sort = 'ORDER BY price ASC, product_name ASC';
        } elseif ($select_sort == "PRECIO/DESC") {
            $sort = 'ORDER BY price DESC, product_name ASC';
        } elseif ($select_sort == "NOVEDADES") {
            $from = "view_novelty";
            $sort = 'ORDER BY product_name ASC';
            $additional = "/NOVEDADES";
        } elseif ($select_sort == "ASC") {
            $sort = 'ORDER BY product_name ASC';
        } elseif ($select_sort == "DESC") {
            $sort = 'ORDER BY product_name DESC';
        } elseif ($select_sort == "DESCUENTOS") {
            $from = "view_discount";
            $sort = 'ORDER BY product_name ASC';
            $additional = "/DESCUENTOS";
        } else {
            $sort = 'ORDER BY product_name ASC';
            $from = "view_products";
        }

        if ($from == "") {
            $from = "view_products";
        }

        if ($from == "view_novelty" || $from == "view_weekly" || $from == "view_discount") {
            $w .= " AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'";
        }

        $arr_pag_products = array(
            'columns' => "*",
            'table' => $from,
            'where' => $w,
            'results_pages' => 12,
            'actually_page' => @number_format($_GET['page'],0,"",""),
            'url' => BASE_URL."PRODUCTOS".$uri_ext,
            'additional' => $additional // Debe comenzar con /
        );
        $paginator = new Paginator($db, $arr_pag_products);
        $arr_products = $paginator->getResults('GROUP BY id', $sort);
        $index_pages = $paginator->getPaginator('GROUP BY id');
        $total_results = $paginator->getTotal();
        $rangue = $paginator->getRange(count($arr_products));

        foreach ($arr_products as $key => $val) {
            $arr_products[$key]['formed_id'] = ($from == "view_products") ? $arr_products[$key]['formed_id'] = $val['id'] : $arr_products[$key]['formed_id'] = $val['product_id'];
            $price = $val['price_iva'];

            // Se verifica si esta en descuento
            $cnt_discount = $db->getCount('discount', "product_id='".$arr_products[$key]['formed_id']."'");
            $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$arr_products[$key]['formed_id']."'");
            $new_price = "";
            if ($cnt_discount == 1) {
                $new_price = $db->getValue('new_price', 'discount', "product_id='".$arr_products[$key]['formed_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            } elseif ($cnt_weekly == 1) {
                $new_price = $db->getValue('new_price', 'weekly_recommendation', "product_id='".$arr_products[$key]['formed_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            }

            $arr_products[$key]['formed_image'] = ($val['cover_image']) ? APP_IMG_PRODUCTS."product_".$arr_products[$key]['formed_id']."/cover/".$val['cover_image'] : APP_NO_IMAGES."no_image.jpg";
            $arr_products[$key]['new_price'] = $new_price;
            $arr_products[$key]['calculed_price'] = $price;
            // $arr_products[$key]['formed_price'] = ($new_price != "") ? $new_price : $price;

            if ($price == $new_price || $new_price == "") {
                $arr_products[$key]['formed_old_price'] = NULL;
                $arr_products[$key]['formed_price'] = $price;
            } else {
                $arr_products[$key]['formed_old_price'] = $price;
                $arr_products[$key]['formed_price'] = $new_price;
            }
        }

        // SEO
        $s = "SELECT * FROM seo WHERE section_id='2'";
        $arr_seo = $db->fetchSQL($s);

        require_once('html/products/products-list.php');
    }

    /**
     * Detalle del producto
     * @return void
     */
    public function showAction()
    {
        $db = $this->db;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."' AND status='1'", "products", BASE_URL."404");

        $s = "SELECT * FROM view_products WHERE id='".$id."' AND status='1' AND brand_status='1'";
        $arr_product = $db->fetchSQL($s);

        $name = $arr_product[0]['product_name'];
        $brand_id = $arr_product[0]['brand_id'];
        $brand_name = $arr_product[0]['brand_name'];
        $brand_name_canonical = $arr_product[0]['brand_name_canonical'];
        $presentation = $arr_product[0]['presentation'];
        $category_id = $arr_product[0]['category_id'];
        $category_name = mb_convert_case($arr_product[0]['category_name'], MB_CASE_UPPER, "UTF-8");
        $description = $arr_product[0]['product_description'];
        $active_principles = $arr_product[0]['active_principles'];
        $prescription = $arr_product[0]['prescription'];
        $content = $arr_product[0]['content'];
        $use_recommendations = $arr_product[0]['use_recommendations'];
        $results_benefits = $arr_product[0]['results_benefits'];
        $iva = $arr_product[0]['iva'];
        $tags = $arr_product[0]['tags'];

        if ($iva && $iva != 0) {
            $product_iva_percent = $iva;
            $product_iva = ($product_iva_percent * $arr_product[0]['price']) / 100;
        }

        $calculated_price = $arr_product[0]['price_iva'];
        $cnt_discount = $db->getCount('discount', "product_id='".$id."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
        $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$id."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");

        $price = $arr_product[0]['price_iva'];
        $new_price = "";
        if ($cnt_discount == 1) {
            $new_price = $db->getValue('new_price', 'discount', "product_id='".$id."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
        } elseif ($cnt_weekly == 1) {
            $new_price = $db->getValue('new_price', 'weekly_recommendation', "product_id='".$id."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
        }

        if ($price == $new_price || $new_price == "") {
            $formed_old_price = NULL;
            $formed_price = $price;
        } else {
            $formed_old_price = $price;
            $formed_price = $new_price;
        }

        // Galería de imagenes
        $s = "SELECT * FROM products_gallery WHERE product_id='".$id."'";
        $arr_gallery = $db->fetchSQL($s);

        foreach ($arr_gallery as $key => $val) {
            $arr_gallery[$key]['formed_image'] = APP_IMG_PRODUCTS.'product_'.$id.'/gallery/'.$val['image'];
        }

        $s = "SELECT * FROM view_category WHERE product_id='".$id."'";
        $arr_categories = $db->fetchSQL($s);
        $arr_categories_id = array();

        foreach ($arr_categories as $key => $val) {
            $arr_categories_id[] = $val['category_id'];
        }

        $categories_id = (count($arr_categories_id) > 0) ? implode(",", $arr_categories_id) : NULL;

        // Relacionados
        if (!$categories_id) {
            $s = "SELECT * FROM view_products WHERE brand_id='".$brand_id."' AND NOT id='".$id."' AND status='1' LIMIT 0, 12";
        } else {
            $cnt_verify = $db->getCount("view_products", "id IN (SELECT product_id FROM products_categories WHERE category_id IN ($categories_id)) AND brand_id='".$brand_id."' AND NOT id='".$id."' AND status='1'");
            if ($cnt_verify == 0) {
                $s = "SELECT * FROM view_products WHERE id IN (SELECT product_id FROM products_categories WHERE category_id IN ($categories_id)) AND NOT id='".$id."' AND status='1' LIMIT 0, 12";
            } else {
                $s = "SELECT * FROM view_products WHERE id IN (SELECT product_id FROM products_categories WHERE category_id IN ($categories_id)) AND brand_id='".$brand_id."' AND NOT id='".$id."' AND status='1' LIMIT 0, 12";
            }
        }

        $arr_related = $db->fetchSQL($s);

        foreach ($arr_related as $key => $val) {
            $arr_related[$key]['formed_image'] = ($val['cover_image']) ? APP_IMG_PRODUCTS.'product_'.$val['id'].'/cover/'.$val['cover_image'] : APP_NO_IMAGES."no_image.jpg" ;

            // if ($val['iva'] && $val['iva'] != 0) {
            //     $product_iva_percent = $val['iva'];
            //     $product_iva = ($product_iva_percent * $val['price']) / 100;
            //     $price_related = number_format(($product_iva + $val['price']),2,".","");
            // } else {
            //     $price_related = $val['price'];
            // }

            // $arr_related[$key]['calculated_price'] = $price_related;
            // $arr_related[$key]['calculated_price'] = $val['price_iva'];

            $price = $val['price_iva'];

            // Se verifica si esta en descuento
            $cnt_discount = $db->getCount('discount', "product_id='".$val['id']."'");
            $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$val['id']."'");
            $new_price = "";
            if ($cnt_discount == 1) {
                $new_price = $db->getValue('new_price', 'discount', "product_id='".$val['id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            } elseif ($cnt_weekly == 1) {
                $new_price = $db->getValue('new_price', 'weekly_recommendation', "product_id='".$val['id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            }

            $arr_related[$key]['new_price'] = $new_price;
            $arr_related[$key]['calculed_price'] = $price;

            if ($price == $new_price || $new_price == "") {
                $arr_related[$key]['formed_old_price'] = NULL;
                $arr_related[$key]['formed_price'] = $price;
            } else {
                $arr_related[$key]['formed_old_price'] = $price;
                $arr_related[$key]['formed_price'] = $new_price;
            }
        }

        $s = "SELECT * FROM view_products WHERE id < '".$id."' AND status='1' ORDER BY id DESC LIMIT 0,1";
        $arr_prev = $db->fetchSQL($s);

        $s = "SELECT * FROM view_products WHERE id > '".$id."' AND status='1' ORDER BY id ASC LIMIT 0,1";
        $arr_next = $db->fetchSQL($s);

        // Inventario
        // $inventory = $db->getValue("quantity", "inventory", "product_id='".$id."'");
        // if ($inventory >= 1) {
        //     $icon = '<i class="fa fa-check-circle css-icon-check"></i>';
        //     $icon_message = "DISPONIBLE";
        // } else {
        //     $icon = '<i class="fa fa-ban css-icon-red"></i>';
        //     $icon_message = "AGOTADO";
        // }

        // Comentarios
        $s = "SELECT * FROM view_features WHERE product_id='".$id."'";
        $arr_features = $db->fetchSQL($s);

        // SEO
        $s = "SELECT * FROM products_seo WHERE product_id='".$id."' LIMIT 0, 1";
        $arr_seo = $db->fetchSQL($s);

        // Colores
        $s = "SELECT * FROM products_colors WHERE product_id='".$id."'";
        $arr_products_color = $db->fetchSQL($s);

        foreach ($arr_products_color as $key => $p_color) {
            $arr_products_color[$key]['color_info'] = $db->fetchSQL("SELECT * FROM colors WHERE id='".$p_color['color_id']."'");
        }

        $s = "SELECT * FROM color_image WHERE product_id='".$id."'";
        $arr_color_image = $db->fetchSQL($s);

        // Tallas
        $s = "SELECT * FROM products_sizes WHERE product_id='".$id."'";
        $arr_products_sizes = $db->fetchSQL($s);

        $inventory = 0;
        $icon = '<i class="fa fa-ban css-icon-red"></i>';
        $icon_message = "AGOTADO";

        // Inventario
        if (isset($arr_products_color[0]['id']) || isset($arr_products_sizes[0]['id'])) {
            if (isset($arr_products_color[0]['stock'])) {
                if ($arr_products_color[0]['stock'] > 0) {
                    $icon = '<i class="fa fa-check-circle css-icon-check"></i>';
                    $icon_message = "DISPONIBLE";
                }
            }

            if (isset($arr_products_sizes[0]['stock'])) {
                if ($arr_products_sizes[0]['stock'] > 0) {
                    $icon = '<i class="fa fa-check-circle css-icon-check"></i>';
                    $icon_message = "DISPONIBLE";
                }
            }
        } else {
            $inventory = $db->getValue("quantity", "inventory", "product_id='".$id."'");
            if ($inventory >= 1) {
                $icon = '<i class="fa fa-check-circle css-icon-check"></i>';
                $icon_message = "DISPONIBLE";
            } else {
                $icon = '<i class="fa fa-ban css-icon-red"></i>';
                $icon_message = "AGOTADO";
            }
        }


        // foreach ($arr_products_sizes as $key => $val) {
        //     $q = "SELECT * FROM size WHERE id='".$val['size_id']."'";
        //     $arr_size = $db->fetchSQL($q);
        //
        //     $arr_products_sizes[$key]['size_info'] = $arr_size[0];
        // }

        $media = $this->getMedia($id);
        require_once('html/products/product-show.php');
    }

    /**
     * Valida si el SORT es válido
     * @param  array  $arr_allowed Opciones permitidas
     * @param  string $sort       Ordenamiento
     * @return boolean
     */
    public function validSort($sort)
    {
        $r = false;
        foreach (self::$allowed_sort as $key => $val) {
            if ($sort === $key) {
                $r = true;
            }
        }

        return $r;
    }

    /**
     * Retorna la media
     * @param  integer $product_id ID del producto
     * @return integer
     */
    public function getMedia($product_id)
    {
        $db = $this->db;
        $total_score = $db->getSum("score", "features", "product_id='".$product_id."' AND status='1'");
        $total_review = $db->getCount("features", "product_id='".$product_id."' AND status='1'");

        return @floor($total_score / $total_review);
    }

    /**
     * Retorna la información de la categoría
     * @param  integer $cat_id ID de la categoría
     * @return array
     */
    public function getCategoryInfo($cat_id)
    {
        $db = $this->db;
        $s = "SELECT * FROM products_category WHERE id='".$cat_id."'";
        $arr_category = $db->fetchSQL($s);

        foreach ($arr_category as $key => $val) {
            $arr_category[0]['formed_image'] = ($val['image']) ? APP_IMG_CATEGORY.$val['image'] : APP_NO_IMAGES."category_no_image.jpg";
        }

        return $arr_category[0];
    }
}


?>
