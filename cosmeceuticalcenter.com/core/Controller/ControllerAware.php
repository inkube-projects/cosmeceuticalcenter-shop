<?php

/**
 * Clase contenedora de metodos para los controladores
 */
class ControllerAware
{
    function __construct()
    {
        $this->db = new Connection;
        // Informacion del usuario
        $this->user_id = @$_SESSION['USER_SESSION_COSME']['id'];
        $this->user_code = @$_SESSION['USER_SESSION_COSME']['code'];
        $this->user_username = @$_SESSION['USER_SESSION_COSME']['username'];
        $this->user_email = @$_SESSION['USER_SESSION_COSME']['username'];
        $this->user_name = @$_SESSION['USER_SESSION_COSME']['name'];
        $this->user_last_name = @$_SESSION['USER_SESSION_COSME']['last_name'];
        $this->user_role = @$_SESSION['USER_SESSION_COSME']['role'];
        $this->user_role_name = @$_SESSION['USER_SESSION_COSME']['role_name'];
        $this->user_phone_1 = @$_SESSION['USER_SESSION_COSME']['phone_1'];
        $this->user_birth = @$_SESSION['USER_SESSION_COSME']['birthdate'];
        $this->user_note = @$_SESSION['USER_SESSION_COSME']['note'];
        $this->user_account_type = @$_SESSION['USER_SESSION_COSME']['account_type'];
        $this->user_dni = @$_SESSION['USER_SESSION_COSME']['dni'];
        $this->user_province = @$_SESSION['USER_SESSION_COSME']['province_id'];
        $this->user_country = @$_SESSION['USER_SESSION_COSME']['country_id'];
        $this->user_city = @$_SESSION['USER_SESSION_COSME']['city'];
        $this->user_direction = @$_SESSION['USER_SESSION_COSME']['direction'];
        $this->user_postal_code = @$_SESSION['USER_SESSION_COSME']['postal_code'];
        // Métodos
        $this->verifyCart = $this->verifyCart();
        $this->user_view = (isset($_GET['view']) && (!empty($_GET['view']))) ? $_GET['view'] : "index";
        $this->user_method = (isset($_GET['meth']) && (!empty($_GET['meth']))) ? $_GET['meth'] : "index";
        $this->categories = $this->getCategories();
        $this->brands = $this->getBrands();
        $this->cart = $this->getCartPreview();
        $this->notifications = $this->getNotifications();

    }

    /**
     * Retorna un mensaje flash
     * @param  string $class   clase para el alert
     * @param  string $message Mensjae a mostrar
     * @return string
     */
    public function flashMessage($class, $message, $path = "")
    {
        $flash_message = '<div class="alert '.$class.' alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        '.$message.'
        </div>';

        return $flash_message;
    }

    /**
     * Muestra una forma global de los mensajes flash
     * @param  array $get Variable $_GET
     * @return string
     */
    public function flashMessageGlobal($get)
    {
        $flash_message = "";

        if (isset($get['m'])) {
            $url = explode(BASE_PATH, $_SERVER["REQUEST_URI"]);
            $path = BASE_URL.str_replace('/'.$get['m'], "", $url[1]);

            if ($get['m'] == 'OK1') {
                $flash_message = $this->flashMessage('alert-success', 'La entrada ha sido <b>creada</b> correctamente.', $path);
            } elseif ($get['m'] == 'OK2') {
                $flash_message = $this->flashMessage('alert-info', 'La entrada ha sido <b>editada</b> correctamente.', $path);
            } elseif ($get['m'] == 'OK3') {
                $flash_message = $this->flashMessage('alert-warning', 'La entrada ha sido <b>eliminada</b> correctamente.', $path);
            }
        }

        return $flash_message;
    }

    /**
     * Verifica si existe un registro en la base de datos
     * @param  array  $param Parametros de busqueda [columna - valor]
     * @param  string $table Tabla donde se va a buscar
     * @param  string $url   URL destino en caso de que no exista
     * @return void
     */
    public function validRecord(array $param, $table, $url)
    {
        $db = $this->db;

        foreach ($param as $key => $val) {
            $cnt_val = $db->getCount($table, $key."='".$val."'");

            if ($cnt_val == 0) {
                header("location: ".$url); exit;
            }
        }
    }

    /**
     * Verifica si la entrada existe en una tabla
     * @param  string $where Condición
     * @param  string $table Tabla
     * @param  string $url   URL destino en caso de que no exista
     * @return void
     */
    public function validRecordCustom($where, $table, $url)
    {
        $db = $this->db;

        $cnt_val = $db->getCount($table, $where);

        if ($cnt_val == 0) {
            header("location: ".$url); exit;
        }
    }

    /**
     * retorna en formato los arreglos
     * @param  array  $arr Arreglo
     * @return array
     */
    public function formatedArray($arr, $m = 4)
    {
        $arr_result = array();
        $position = 0;

        for ($i = 0; $i < count($arr); $i++) {
            if ((($i + 0) % $m) == 0) {
                $position++;
        	}

            $arr_result[($position - 1)][] = $arr[$i];
        }

        return $arr_result;
    }

    /**
     * Obtiene las categorías
     * @return array
     */
    public function getCategories()
    {
        $db = $this->db;
        $s = "SELECT * FROM products_category ORDER BY name ASC";
        return $db->fetchSQL($s);
    }

    /**
     * Obtiene las marcas
     * @return array
     */
    public function getBrands()
    {
        $db = $this->db;
        $s = "SELECT * FROM brands WHERE status='1' ORDER BY name ASC";
        return $db->fetchSQL($s);
    }

    /**
     * Obtiene una marca por su ID
     * @param  integer $id ID de la marca
     * @return array
     */
    public function getBrandById($id)
    {
        $db = $this->db;
        $s = "SELECT * FROM brands WHERE id='".$id."' AND status='1'";
        $arr_brand = $db->fetchSQL($s);

        $arr_brand[0]['formed_image'] = ($arr_brand[0]['image']) ? APP_IMG_BRANDS.$arr_brand[0]['image'] : APP_NO_IMAGES."brand_no_image.jpg";

        return $arr_brand[0];
    }

    public function getCartPreview()
    {
        $db = $this->db;
        if (isset($_SESSION['USER_SESSION_COSME']['id'])) {
            $s = "SELECT * FROM view_cart WHERE user_id='".$this->user_id."' LIMIT 0,5";
            $arr_cart = $db->fetchSQL($s);
        } else {
            $arr_cart = $this->getCartFromCookies();
        }

        $sub_total = 0;
        $total_products = 0;

        foreach ($arr_cart as $key => $val) {
            $product_iva = $val['iva'];
            $product_iva = ($product_iva) ? $product_iva : APP_IVA;
            $arr_cart[$key]['product_iva'] = $product_iva;
            $arr_cart[$key]['formed_image'] = ($val['cover_image']) ? APP_IMG_PRODUCTS."product_".$val['product_id']."/cover/".$val['cover_image'] : APP_NO_IMAGES."no_image.jpg";
            $arr_cart[$key]['brand_name'] = $db->getValue("name_canonical", "brands", "id='".$val['brand_id']."'");
            $arr_cart[$key]['size_id'] = $val['size_id'];
            $arr_cart[$key]['color_id'] = $val['color_id'];
            $arr_cart[$key]['size'] = $db->getValue("name", "size", "id='".$val['size_id']."'");
            $arr_cart[$key]['color'] = $db->getValue("name", "colors", "id='".$val['color_id']."'");

            $cnt_discount = $db->getCount('discount', "product_id='".$val['product_id']."'");
            $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$val['product_id']."'");
            $new_price = "";

            if ($cnt_discount == 1) {
                $new_price = $db->getValue("new_price", "discount", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            } elseif ($cnt_weekly == 1) {
                $new_price = $db->getValue("new_price", "weekly_recommendation", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            }

            $arr_cart[$key]['new_price'] = $new_price;
            if ($new_price != "") {
                $calculated_iva = 0.00;
                $arr_cart[$key]['sub_total'] = number_format(($new_price * $val['quantity']),2,".","");
            } else {
                // $arr_cart[$key]['sub_total'] = number_format(($val['price'] * $val['quantity']),2,".","");
                $arr_cart[$key]['sub_total'] = number_format(($val['price_iva'] * $val['quantity']),2,".","");
                $calculated_iva = number_format((($product_iva * ($val['price'] * $val['quantity'])) / 100),2,".","");
            }


            $arr_cart[$key]['calculated_iva'] = $calculated_iva;
            $arr_cart[$key]['formed_price'] = ($new_price != "") ? $new_price : $val['price'];
            // $arr_cart[$key]['sub_total'] = number_format(($calculated_iva + $arr_cart[$key]['sub_total']),2,".","");
            $arr_cart[$key]['sub_total'] = number_format($arr_cart[$key]['sub_total'],2,".","");
            $sub_total += $arr_cart[$key]['sub_total'];
            $total_products += $val['quantity'];
        }

        $sub_total = number_format($sub_total,2,".","");
        return array('cart' => $arr_cart, 'sub_total' => number_format($sub_total,2,".",""), 'total_products' => $total_products);
    }

    public function getCartFromCookies()
    {
        $db = $this->db;
        $arr_products = array();

        if (isset($_COOKIE['cookie_cart_cosmeceutical'])) {
            $arr_cart_cookie = json_decode($_COOKIE['cookie_cart_cosmeceutical'], true);
            $arr_products_cookies = array();
            // showArr($arr_cart_cookie); exit;
            foreach ($arr_cart_cookie as $key => $val) {
                $arr_products_cookies[] = $val['product_id'];
                $arr_quantity[$val['product_id']] = (isset($val['quantity'])) ? $val['quantity'] : 1;
            }
            $products_id = implode(",", $arr_products_cookies);

            foreach ($arr_cart_cookie as $key => $val) {
                $s = "SELECT *, id AS product_id, product_name AS name FROM view_products WHERE id='".$val['product_id']."'";
                $arr_product = $db->fetchSQL($s);
                $arr_products[$key] = $arr_product[0];
                $arr_products[$key]['quantity'] = $val['quantity'];
                $arr_products[$key]['cart_id'] = $val['product_id'];
                $arr_products[$key]['size_id'] = (isset($val['size_id'])) ? $val['size_id'] : NULL;
                $arr_products[$key]['color_id'] = (isset($val['color_id'])) ? $val['color_id'] : NULL;
                $arr_products[$key]['size'] = $db->getValue("name", "size", "id='".@$val['size_id']."'");
                $arr_products[$key]['color'] = $db->getValue("name", "colors", "id='".@$val['color_id']."'");
            }

            // $s = "SELECT *, id AS product_id, product_name AS name FROM view_products WHERE id IN (".$products_id.")";
            // $arr_products = (count($arr_products_cookies) > 0) ? $db->fetchSQL($s) : array();
            //
            // foreach ($arr_products as $key => $val) {
            //     $arr_products[$key]['quantity'] = $arr_quantity[$val['product_id']];
            //     $arr_products[$key]['cart_id'] = $val['product_id'];
            // }
        }

        return $arr_products;
    }

    public function getCartPreviewFromCookie()
    {
        $db = $this->db;
        if (isset($_COOKIE['cookie_cart_cosmeceutical'])) {
            $arr_cart_cookie = json_decode($_COOKIE['cookie_cart_cosmeceutical'], true);
            $arr_cart = array();
            $sub_total = 0;
            $total_products = 0;

            foreach ($arr_cart_cookie as $key => $val) {
                if (isset($val['product_id'])) {
                    $product_iva = $db->getValue("iva", "products", "id='".$val['product_id']."'");
                    $product_iva = ($product_iva) ? $product_iva : APP_IVA;
                    $arr_cart[$key]['product_iva'] = $product_iva;

                    $s = "SELECT * FROM view_products WHERE id='".$val['product_id']."'";
                    $arr_product = $db->fetchSQL($s);
                    $arr_product[0]['name'] = $arr_product[0]['product_name'];
                    $arr_product[0]['formed_image'] = ($arr_product[0]['cover_image']) ? APP_IMG_PRODUCTS."product_".$val['product_id']."/cover/".$arr_product[0]['cover_image'] : APP_NO_IMAGES."no_image.jpg";
                    $arr_product[0]['quantity'] = (isset($val['quantity'])) ? $val['quantity'] : 1;
                    $arr_product[0]['product_id'] = $val['product_id'];

                    $cnt_discount = $db->getCount('discount', "product_id='".$val['product_id']."'");
                    $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$val['product_id']."'");
                    $new_price = "";

                    if ($cnt_discount == 1) {
                        $new_price = $db->getValue("new_price", "discount", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
                    } elseif ($cnt_weekly == 1) {
                        $new_price = $db->getValue("new_price", "weekly_recommendation", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
                    }

                    if ($new_price != "") {
                        $arr_product[0]['price'] = $new_price;
                    }

                    $arr_product[0]['new_price'] = $new_price;
                    if ($new_price != "") {
                        $cart_sub_total = number_format(($new_price * $arr_product[0]['quantity']),2,".","");
                    } else {
                        $cart_sub_total = number_format(($arr_product[0]['price'] * $arr_product[0]['quantity']),2,".","");
                    }

                    $calculated_iva = number_format((($product_iva * $arr_cart[$key]['sub_total']) / 100),2,".","");
                    $arr_cart[$key]['calculated_iva'] = $calculated_iva;
                    $arr_cart[$key]['formed_price'] = ($new_price != "") ? $new_price : $val['price'];
                    $arr_product[0]['sub_total'] = $cart_sub_total;
                    $sub_total += $cart_sub_total;

                    $arr_cart[] = $arr_product[0];
                }
            }

            // showArr(($_COOKIE['cookie_cart_cosmeceutical'])); exit;
            $total_products = count($arr_cart_cookie);
            return array('cart' => $arr_cart, 'sub_total' => number_format($sub_total,2,".",""), 'total_products' => $total_products);
        }

        return array('cart' => array(), 'sub_total' => 0.00, 'total_products' => 0);
    }

    /**
     * Obtiene el numero de notificaciones
     * @return integer
     */
    public function getNotifications()
    {
        $db = $this->db;
        return $db->getCount("notifications", "user_id='".$this->user_id."' AND status='0'");
    }

    /**
     * Valida que el usuario tenga una dirección de envío
     * @return boolean
     */
    public function validateAddress()
    {
        $db = $this->db;
        $cnt_val = $db->getCount("shipping_address", "user_id='".$this->user_id."'");
        if ($cnt_val == 0) {
            return false;
        }
        return true;
    }

    /**
     * Verifica la disponibilidad de los productos
     * @return void
     */
    public function verifyCart()
    {
        $db = $this->db;

        if (isset($_SESSION['USER_SESSION_COSME']['id'])) {
            $user_id = $_SESSION['USER_SESSION_COSME']['id'];

            $s = "SELECT * FROM cart_detail WHERE user_id='".$user_id."'";
            $arr_cart = $db->fetchSQL($s);

            foreach ($arr_cart as $key => $val) {
                if (!$val['size_id'] && !$val['color_id']) {
                    $cart_quantity = $val['quantity'];
                    $qty_available = $db->getValue("quantity", "inventory", "product_id='".$val['product_id']."'");
                    $total = $qty_available - $cart_quantity;

                    if ($total < 0) {
                        $db->deleteAction("cart_detail", "id='".$val['id']."'");
                    }
                } else {
                    $cnt_colors = ($val['color_id'] && $val['color_id'] != "") ? $db->getCount("products_colors", "product_id='".$val['product_id']."' AND color_id='".$val['color_id']."'") : "0";
                    $cnt_size = ($val['size_id'] && $val['size_id'] != "") ? $db->getCount("products_sizes", "product_id='".$val['product_id']."' AND size_id='".$val['size_id']."'") : "0";

                    if ($cnt_colors > 0) {
                        $current_stock = $db->getValue("stock", "products_colors", "product_id='".$val['product_id']."' AND color_id='".$val['color_id']."'");
                        $new_quantity = $current_stock - $val['quantity'];
            
                        if ($new_quantity < 0) {
                            $db->deleteAction("cart_detail", "id='".$val['id']."'");
                        }
                    }

                    if ($cnt_size > 0) {
                        $current_stock = $db->getValue("stock", "products_sizes", "product_id='".$val['product_id']."' AND size_id='".$val['size_id']."'");
                        $new_quantity = $current_stock - $val['quantity'];
            
                        if ($new_quantity < 0) {
                            $db->deleteAction("cart_detail", "id='".$val['id']."'");
                        }
                    }
                }
            }
        }
    }
}


?>
