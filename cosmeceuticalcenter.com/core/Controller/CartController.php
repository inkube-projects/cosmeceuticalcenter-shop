<?php

include('core/Controller/DashboardController.php');
include('core/handler/session-handler.php');


/**
 * Clase invocadora para la cesta de productos
 */
class CartController
{
    /**
     * Constructor
     */
    function __construct()
    {
        $this->dashboardController = new DashboardController();
    }

    /**
     * Pantalla principal del carrito
     * @return void
     */
    public function cartAction()
    {
        $session = isset($_SESSION['USER_SESSION_COSME']['id']);
        $this->dashboardController->cartAction("html/account/cart.php", $session);
    }

    /**
     * Paso 01
     * @return void
     */
    public function loginAction()
    {
        $this->dashboardController->loginAction();
    }

    /**
     * Registro Paso 01
     *
     * @return void
     */
    public function cartRegisterAction()
    {
        $this->dashboardController->cartRegisterAction();
        // $db = $this->db;
        // $email = $_SESSION['CART_REGISTER_EMAIL'];
        // $pass = $_SESSION['CART_REGISTER_PASS'];
        // require_once('html/account/register-cart.php');
    }
}


?>
