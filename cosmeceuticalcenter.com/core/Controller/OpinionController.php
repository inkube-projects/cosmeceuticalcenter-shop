<?php

include('core/handler/session-handler.php');
include('core/Controller/ControllerAware.php');

/**
 * Controlador para las opiniones
 */
class OpinionController extends ControllerAware
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Opinion del producto
     * @return void
     */
    public function showAction()
    {
        $db = $this->db;
        $id = @number_format($_GET['id'],0,"","");
        $this->validRecordCustom("id='".$id."' AND user_id='".$this->user_id."'", "features", BASE_URL."404");

        $s = "SELECT * FROM features WHERE id='".$id."'";
        $arr_features = $db->fetchSQL($s);

        $review = $arr_features[0]['review'];
        $score = $arr_features[0]['score'];
        $act = ($arr_features[0]['status'] == 0) ? 1 : 2;

        // Datos del producto
        $s = "SELECT * FROM view_products WHERE id='".$arr_features[0]['product_id']."'";
        $arr_product = $db->fetchSQL($s);

        $product_id = $arr_features[0]['product_id'];
        $product_name = $arr_product[0]['product_name'];
        $cover_image = ($arr_product[0]['cover_image']) ? APP_IMG_PRODUCTS."product_".$arr_product[0]['id']."/cover/".$arr_product[0]['cover_image'] : APP_NO_IMAGES."no_image.jpg";
        $brand_name = $arr_product[0]['brand_name'];
        $product_description = substr(sanitize($arr_product[0]['product_description']),0,300);

        // Datos de la orden
        $s = "SELECT * FROM view_orders WHERE id='".$arr_features[0]['order_id']."'";
        $arr_order = $db->fetchSQL($s);

        $order_code = $arr_order[0]['code'];
        $flash_message = "";
        if (isset($_GET['m'])) {
            if ($_GET['m'] == "OK") {
                $flash_message = $this->flashMessage("alert-success", "Gracias por tu opinion");
            }
        }

        require_once('html/opinion/opinion-form.php');
    }
}


?>
