<?php

$controllers = array(
    'Index' => array('index','home','logOut','logOut'),
    'Error' => array('notFoundAction','serverErrorAction'),
    'Products' => array('list', 'show'),
    'Account' => array('register', 'login', 'verify'),
    'Dashboard' => array('dash', 'cart', 'pay', 'processed'),
    'Notifications' => array('list', 'show'),
    'Opinion' => array('list', 'show', 'edit'),
    'Cart' => array('cart', 'login', 'cartRegister')
);

?>
