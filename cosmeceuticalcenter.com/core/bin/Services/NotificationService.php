<?php

/**
 * Servicio para las notificaciones
 */
class NotificationService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Obtiene la información de una notificació
     * @param  integer $id ID de la notificación
     * @return string
     */
    public function getNotification($id)
    {
        $db = $this->db;
        $s = "SELECT * FROM notifications WHERE id='".$id."'";
        $arr_notification = $db->fetchSQL($s);

        // Se actualiza el estado de la notificación
        $arr_values = array(
            'status' => '1',
            'update_at' => date('Y-m-d H:i:s')
        );
        $db->updateAction("notifications", $arr_values, "id='".$id."'");

        ob_start();
        include('html/ajx_show/notification-modal.php');

        return ob_get_clean();
    }

    /**
     * Elimina una notificación
     * @param  integer $id ID de la notificación
     * @return void
     */
    public function removeNotificaction($id)
    {
        $db = $this->db;
        $db->deleteAction("notifications", "id='".$id."'");
    }
}


?>
