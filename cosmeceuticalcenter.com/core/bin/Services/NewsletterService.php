<?php

/**
 * Servicios para el login
 */
class NewsletterService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Ingresa un email
     * @return void
     */
    public function persist()
    {
        $db = $this->db;

        $arr_values = array(
            'email' => mb_convert_case($_POST['newsletter_email'], MB_CASE_LOWER, "UTF-8"),
            'status' => '0',
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s')
        );

        $newsletter = $db->insertAction("newsletter", $arr_values);

        // Se genera el token de verificación de la cuenta
        $token = genPass(uniqid($newsletter[0]['id']), 64);
        $arr_values = array(
            'email' => $newsletter[0]['email'],
            'token' => $token,
            'type' => 2,
            'create_at' => date('Y-m-d H:i:s')
        );
        $db->insertAction("user_token", $arr_values);

        // Se hace el envio del email
        $arr_data = array(
            'email' => $newsletter[0]['email'],
            'token' => $token
        );
        $this->sendEmailTemplate("html/email/newsletter.php", $arr_data, $newsletter[0]['email'], "", "Suscripcion a newsletter");

        // Envia la notificacion al administrador
        $arr_data = array(
            'title' => 'Nueva suscripci&oacute;n a newsletter',
            'message' => 'Nueva suscripción al newsletter: '.$newsletter[0]['email']
        );
        $arr_send = array('info@cosmeceuticalcenter.com', 'rufi@inkube.net');
        $this->sendEmailTemplate("html/email/notifications.php", $arr_data, $newsletter[0]['email'], "", "Nueva suscripcion a newsletter", true, $arr_send);
    }
}


?>
