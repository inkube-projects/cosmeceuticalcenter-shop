<?php

/**
 * Clase para los productos
 */
class ProductService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Obtiene la información del producto
     * @param  integer $id ID del producto
     * @return string
     */
    public function getProduct($id)
    {
        $db = $this->db;
        $s = "SELECT * FROM view_products WHERE id='".$id."'";
        $arr_product = $db->fetchSQL($s);

        return $this->getModalHTML($arr_product[0]);
    }

    /**
     * Obtiene el html del modal
     * @param  array $product Arreglo del producto
     * @return string
     */
    public function getModalHTML($product)
    {
        $db = $this->db;
        $s = "SELECT * FROM products_gallery WHERE product_id='".$product['id']."'";
        $arr_gallery = $db->fetchSQL($s);

        foreach ($arr_gallery as $key => $val) {
            $arr_gallery[$key]['formed_image'] = APP_IMG_PRODUCTS."product_".$product['id']."/gallery/".$val['image'];
        }

        $content = $product['content'];
        // if ($product['iva'] && $product['iva'] != 0) {
        //     $product_iva_percent = $product['iva'];
        //     $product_iva = ($product_iva_percent * $product['price']) / 100;
        //     $calculated_price = number_format(($product_iva + $product['price']),2,".","");
        // } else {
        //     $calculated_price = $product['price'];
        // }

        $cnt_discount = $db->getCount('discount', "product_id='".$product['id']."'");
        $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$product['id']."'");
        $new_price = "";
        
        $price = $product['price_iva'];
        if ($cnt_discount == 1) {
            $new_price = $db->getValue('new_price', 'discount', "product_id='".$product['id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
        } elseif ($cnt_weekly == 1) {
            $new_price = $db->getValue('new_price', 'weekly_recommendation', "product_id='".$product['id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
        }


        if ($price == $new_price || $new_price == "") {
            $formed_old_price = NULL;
            $formed_price = $price;
        } else {
            $formed_old_price = $price;
            $formed_price = $new_price;
        }

        // if (isset($old_price)) {
        //     if ($old_price == $price) {
        //         unset($old_price);
        //     }
        // }

        // Inventario
        $inventory = $db->getValue("quantity", "inventory", "product_id='".$product['id']."'");
        if ($inventory >= 1) {
            $icon = '<i class="fa fa-check-circle css-icon-check"></i>';
            $icon_message = "DISPONIBLE";
        } else {
            $icon = '<i class="fa fa-ban css-icon-red"></i>';
            $icon_message = "AGOTADO";
        }

        ob_start();
        include('html/ajx_show/product-modal.php');

        return ob_get_clean();
    }

    /**
     * Guarda el emial del usuario para ser notificado
     * @param  integer $product_id ID del producto
     * @param  string $email       Email del usuario
     * @return void
     */
    public function saveEmail($product_id, $email)
    {
        $db = $this->db;

        $arr_values = array(
            'product_id' => $product_id,
            'email' => mb_convert_case($email, MB_CASE_LOWER, "UTF-8"),
            'create_at' => date('Y-m-d H:i:s')
        );

        $db->insertAction('notify_products', $arr_values);
    }

    /**
     * Retorna las imagenes de los colores de un producto
     * @param  integer $color_id   ID del color
     * @param  integer $product_id ID del producto
     * @return string
     */
    public function getImageColors($color_id, $product_id)
    {
        $db = $this->db;

        $s = "SELECT * FROM color_image WHERE product_id='".$product_id."' AND color_id='".$color_id."'";
        $arr_color_images = $db->fetchSQL($s);
        $path = APP_IMG_PRODUCTS.'product_'.$product_id.'/gallery/';

        ob_start();
        include('html/ajx_show/product-colo-image-modal.php');

        return ob_get_clean();
    }
}


?>
