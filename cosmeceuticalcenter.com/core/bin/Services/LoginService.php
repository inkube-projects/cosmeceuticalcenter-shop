<?php

/**
 * Servicios para el login
 */
class LoginService
{
    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Actualiza la contraseña del usuario
     * @return void
     */
    public function forgotPass()
    {
        $db = $this->db;
        $pass = genPass(rand(00, 99), 8);

        $arr_values = array(
            'password' => hashPass($pass, 12),
            'password_requested_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s')
        );

        $user = $db->updateAction("user", $arr_values, "email='".$_POST['forgot_email']."'");

        // Se envía el email al usuario
        $this->sendEmail($user[0], $pass);
    }

    /**
     * Envía el email
     * @param  array $user  Objeto del usuario
     * @param  string $pass Nueva contraseña
     * @return void
     */
    public function sendEmail($user, $pass)
    {
        $db = $this->db;
        $mail = new PHPMailer(true);
        $email = $user['email'];

        $s = "SELECT * FROM user_information WHERE user_id='".$user['id']."'";
        $arr_user = $db->fetchSQL($s);
        $names = $arr_user[0]['name']." ".$arr_user[0]['last_name'];

        ob_start();
        include('html/email/forgot-pass.php');
        $template = ob_get_clean();

        //Server settings
        // $mail->SMTPDebug = 2;
        $mail->isSMTP();
        $mail->Host = EMAIL_HOST;
        $mail->SMTPAuth = true;
        $mail->Username = EMAIL_SENDER;
        $mail->Password = EMAIL_PASSWORD;
        $mail->SMTPSecure = 'tls';
        // $mail->Port = EMAIL_PORT;
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        //Recipients
        $mail->setFrom(EMAIL_SENDER, 'Cosmeceutical center');
        $mail->addAddress($user['email'], $names);

        //Content
        $mail->isHTML(true);
        $mail->Subject = 'Olvide mi clave | Cosmeceuticalcenter';
        $mail->Body    = $template;
        $mail->AltBody = $template;

        $mail->send();
    }
}


?>
