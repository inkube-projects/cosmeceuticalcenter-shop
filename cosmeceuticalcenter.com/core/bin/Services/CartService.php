<?php

/**
 * Servicios del carrito
 */
class CartService extends GeneralMethods
{
    public $db;
    public $paypal_charge = 2;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Agrega un producto al carrito
     * @param  integer  $product_id ID del producto
     * @param  integer $qty        Cantidad
     * @return void
     */
    public function persist($product_id, $qty = 1)
    {
        $db = $this->db;
        $arr_values = array(
            'user_id' => $this->user_id,
            'product_id' => $product_id,
            'quantity' => $qty,
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s')
        );

        if (isset($_POST['size'])) {
            if ($_POST['size'] && $_POST['size'] != "") {
                $size_id = @number_format($_POST['size'],0,"","");
                $arr_values['size_id'] = $size_id;
            }
        }

        if (isset($_POST['color'])) {
            if ($_POST['color'] && $_POST['color'] != "") {
                $color_id = @number_format($_POST['color'],0,"","");
                $arr_values['color_id'] = $color_id;
            }
        }

        $db->insertAction("cart_detail", $arr_values);
    }

    /**
     * Edita un producto dentro del carrito
     * @param  integer $product_id ID del producto
     * @param  integer $qty        Cantidad
     * @return void
     */
    public function update($product_id, $qty)
    {
        $db = $this->db;
        $w = "user_id='".$this->user_id."' AND product_id='".$product_id."'";

        if (isset($_POST['size'])) {
            if ($_POST['size'] && $_POST['size'] != "") {
                $size_id = @number_format($_POST['size'],0,"","");
                $w .= " AND size_id='".$size_id."'";
            }
        }

        if (isset($_POST['color'])) {
            if ($_POST['color'] && $_POST['color'] != "") {
                $color_id = @number_format($_POST['color'],0,"","");
                $w .= " AND color_id='".$color_id."'";
            }
        }

        $old_qty = $db->getValue("quantity", "cart_detail", $w);

        $arr_values = array(
            'quantity' => ($qty + $old_qty),
            'update_at' => date('Y-m-d H:i:s')
        );

        $db->updateAction("cart_detail", $arr_values, $w);
    }

    /**
     * Actualiza el carrito en la pantalla cart.php
     * @param  integer $cart_id  ID del carrito
     * @param  integer $quantity Cantidad
     * @return string
     */
    public function updateOrder($cart_id, $quantity)
    {
        $db = $this->db;
        $arr_values = array(
            'quantity' => $quantity,
            'update_at' => date('Y-m-d H:i:s')
        );

        $db->updateAction("cart_detail", $arr_values, "id='".$cart_id."'");

        return $this->getOrderHTML();
    }


    public function updateOrderCookie($arr_cookie_cart, $cart_id, $quantity)
    {
        $product_id = $cart_id;
        foreach ($arr_cookie_cart as $key => $val) {
            if (isset($val['product_id'])) {
                if ($val['product_id'] == $product_id) {
                    $arr_cookie_cart[$key]['quantity'] = $quantity;
                }
            } else {
                @unlink($arr_cookie_cart[$key]);
            }

        }

        setcookie('cookie_cart_cosmeceutical','',time() - 3600, "/");
        setcookie('cookie_cart_cosmeceutical', json_encode($arr_cookie_cart), time() + (86400 * 30), "/");
        $_SESSION['COSME_CART'] = json_encode($arr_cookie_cart);

        return $this->getOrderHTML();
    }

    /**
     * Agrega productos al carrito sin tener unsa sesión activa
     * @param  integer  $product_id ID del producto
     * @param  integer $qty         Cantidad
     * @return void
     */
    public function persistWithoutSession($product_id, $qty = 1, $size_id, $color_id)
    {
        if (isset($_COOKIE['cookie_cart_cosmeceutical'])) {
            $arr_cookie_cart = json_decode($_COOKIE['cookie_cart_cosmeceutical'], true);
            $pos = count($arr_cookie_cart);
            $arr_cookie_cart[$pos]['product_id'] = $product_id;
            $arr_cookie_cart[$pos]['quantity'] = $qty;
            $arr_cookie_cart[$pos]['size_id'] = $size_id;
            $arr_cookie_cart[$pos]['color_id'] = $color_id;
        } else {
            $arr_cookie_cart[] = array(
                'product_id' => $product_id,
                'quantity' => $qty,
                'size_id' => $size_id,
                'color_id' => $color_id
            );
        }
        setcookie('cookie_cart_cosmeceutical','',time() - 3600, "/");
        setcookie('cookie_cart_cosmeceutical', json_encode($arr_cookie_cart), time() + (86400 * 30), "/");
        $_SESSION['COSME_CART'] = json_encode($arr_cookie_cart);
    }

    /**
     * Actualiza el carrito sin una sesión
     * @param  integer  $product_id ID del producto
     * @param  integer $qty         Cantidad
     * @return void
     */
    public function updateWithoutSession($product_id, $qty = 1, $size_id, $color_id)
    {
        if (isset($_COOKIE['cookie_cart_cosmeceutical'])) {
            $arr_cookie_cart = json_decode($_COOKIE['cookie_cart_cosmeceutical'], true);
            $exist = false;

            foreach ($arr_cookie_cart as $key => $val) {
                if (isset($val['product_id'])) {
                    if ($val['product_id'] == $product_id && $val['size_id'] == $size_id && $val['color_id'] == $color_id) {
                        $old_qty = $val['quantity'];

                        $arr_cookie_cart[$key]['product_id'] = $product_id;
                        $arr_cookie_cart[$key]['quantity'] = ($old_qty + $qty);
                        $arr_cookie_cart[$key]['size_id'] = $size_id;
                        $arr_cookie_cart[$key]['color_id'] = $color_id;
                        $exist = true;
                    }
                } else {
                    unset($arr_cookie_cart[$key]);
                }
            }

            if (!$exist) {
                $arr_cookie_cart[] = array(
                    'product_id' => $product_id,
                    'quantity' => $qty,
                    'size_id' => $size_id,
                    'color_id' => $color_id
                );
            }
        } else {
            $arr_cookie_cart[] = array(
                'product_id' => $product_id,
                'quantity' => $qty,
                'size_id' => $size_id,
                'color_id' => $color_id
            );
        }
        setcookie('cookie_cart_cosmeceutical','',time() - 3600, "/");
        setcookie('cookie_cart_cosmeceutical', json_encode($arr_cookie_cart), time() + (86400 * 30), "/");
        $_SESSION['COSME_CART'] = json_encode($arr_cookie_cart);
    }

    /**
     * Elimina un producto del carrito
     * @param  integer $id ID del prodcuto
     * @return string
     */
    public function removeProduct($id)
    {
        $db = $this->db;
        $db->deleteAction("cart_detail", "id='".$id."' AND user_id='".$this->user_id."'");
        return $this->getOrderHTML();
    }

    /**
     * Obtiene el HTML del carrito
     * @return string
     */
    public function getOrderHTML()
    {
        $db = $this->db;
        $sub_total = 0;
        $shipping = "0.00";

        if (isset($_SESSION['USER_SESSION_COSME']['id'])) {
            $s = "SELECT * FROM view_cart WHERE user_id='".$this->user_id."'";
            $arr_cart = $db->fetchSQL($s);
        } else {
            $arr_cart = $this->getCartFromCookies();
        }

        foreach ($arr_cart as $key => $val) {
            $product_iva = $val['iva'];
            $product_iva = ($product_iva) ? $product_iva : APP_IVA;
            $arr_cart[$key]['product_iva'] = $product_iva;
            $arr_cart[$key]['formed_image'] = ($val['cover_image']) ? APP_IMG_PRODUCTS."product_".$val['product_id']."/cover/".$val['cover_image'] : APP_NO_IMAGES."no_image.jpg";
            $arr_cart[$key]['category_name'] = $db->getValue('name', 'products_category', "id='".$val['category_id']."'");
            $arr_cart[$key]['brand_name'] = $db->getValue('name', 'brands', "id='".$val['brand_id']."'");
            $arr_cart[$key]['size_id'] = $val['size_id'];
            $arr_cart[$key]['color_id'] = $val['color_id'];
            $arr_cart[$key]['size'] = $db->getValue("name", "size", "id='".$val['size_id']."'");
            $arr_cart[$key]['color'] = $db->getValue("name", "colors", "id='".$val['color_id']."'");

            $cnt_discount = $db->getCount('discount', "product_id='".$val['product_id']."'");
            $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$val['product_id']."'");
            $new_price = "";

            if ($cnt_discount == 1) {
                $new_price = $db->getValue("new_price", "discount", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            } elseif ($cnt_weekly == 1) {
                $new_price = $db->getValue("new_price", "weekly_recommendation", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            }
            $arr_cart[$key]['new_price'] = $new_price;
            $arr_cart[$key]['value'] = ($new_price != "") ? $new_price : $val['price_iva'];

            if ($new_price != "") {
                $calculated_iva = 0.00;
                $arr_cart[$key]['sub_total'] = number_format(($new_price * $val['quantity']),2,".","");
            } else {
                // $arr_cart[$key]['sub_total'] = number_format(($val['price'] * $val['quantity']),2,".","");
                // $calculated_iva = number_format((($product_iva * $arr_cart[$key]['sub_total']) / 100),2,".","");
                $arr_cart[$key]['sub_total'] = number_format(($val['price_iva'] * $val['quantity']),2,".","");
                $calculated_iva = number_format((($product_iva * ($val['price'] * $val['quantity'])) / 100),2,".","");
            }

            $arr_cart[$key]['calculated_iva'] = $calculated_iva;
            // $arr_cart[$key]['sub_total'] = number_format(($calculated_iva + $arr_cart[$key]['sub_total']),2,".","");
            $arr_cart[$key]['sub_total'] = number_format($arr_cart[$key]['sub_total'],2,".","");
            $sub_total += $arr_cart[$key]['sub_total'];
        }

        $sub_total = number_format($sub_total,2,".","");

        $province_id = "";
        if (isset($_POST['country'])) {
            $country = $_POST['country'];
            if ($country == 73) {
                $province_id = $_POST['province'];
                $shipping = $db->getValue('value', 'view_zones', "province_id='".$province_id."'");
            } else {
                $shipping = $db->getValue('value', 'view_zones_country', "country_id='".$country."'");
            }
        } else {
            if (isset($_SESSION['country'])) {
                $country = $_SESSION['country'];
                if ($_SESSION['country'] == 73) {
                    if (isset($_SESSION['province'])) {
                        $province_id = $_SESSION['province'];
                    } else {
                        $province_id = $this->user_province;
                    }

                    $shipping = $db->getValue('value', 'view_zones', "province_id='".$province_id."'");
                } else {
                    $shipping = $db->getValue('value', 'view_zones_country', "country_id='".$_SESSION['country']."'");
                }
            } else {
                $country = 73;
            }
        }

        if (!$shipping || $shipping == "" || $sub_total > 80) { $shipping = "0.00"; }

        $total = number_format($sub_total + $shipping,2,".","");

        $s = "SELECT * FROM country";
        $arr_country = $db->fetchSQL($s);

        $s = "SELECT * FROM province";
        $arr_province = $db->fetchSQL($s);
        $shipping = number_format($shipping,2,".","");

        ob_start();
        include('html/ajx_show/cart.php');

        return ob_get_clean();
    }

    /**
     * Se procesa la orden;
     * @return array
     */
    public function processOrderRedSys()
    {
        $db = $this->db;
        $user_id = $_REQUEST['user_id'];
        $country_id = $_REQUEST['country'];
        $province_id = $_REQUEST['province'];
        $aditional_note = $_REQUEST['aditional_note'];
        $order_note = $_REQUEST['order_note'];
        $order_id = $_REQUEST['order_id'];

        // Se genera el log
        $this->createOrderLog($user_id, "Tarjeta");

        // Datos del carrito
        $s = "SELECT * FROM view_cart WHERE user_id='".$user_id."'";
        $arr_cart = $db->fetchSQL($s);
        $sub_total = 0;
        $shipping = 0.00;

        foreach ($arr_cart as $key => $val) {
            $product_iva = $val['iva'];
            $product_iva = ($product_iva) ? $product_iva : APP_IVA;
            $cnt_discount = $db->getCount('discount', "product_id='".$val['product_id']."'");
            $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$val['product_id']."'");
            $new_price = "";

            if ($cnt_discount == 1) {
                $new_price = $db->getValue("new_price", "discount", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            } elseif ($cnt_weekly == 1) {
                $new_price = $db->getValue("new_price", "weekly_recommendation", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            }

            $arr_cart[$key]['new_price'] = $new_price;
            $arr_cart[$key]['value'] = ($new_price != "") ? $new_price : $val['price_iva'];

            if ($new_price != "") {
                $arr_cart[$key]['sub_total'] = number_format(($new_price * $val['quantity']),2,".","");
                $calculated_iva = 0.00;
            } else {
                $arr_cart[$key]['sub_total'] = number_format(($val['price_iva'] * $val['quantity']),2,".","");
                $calculated_iva = number_format((($product_iva * ($val['price'] * $val['quantity'])) / 100),2,".","");
            }

            $arr_cart[$key]['sub_total'] = number_format($arr_cart[$key]['sub_total'],2,".","");
            $sub_total += $arr_cart[$key]['sub_total'];
        }

        $sub_total = number_format($sub_total,2,".","");

        // Se obtiene el valor del shipping
        if ($country_id == 73) {
            $shipping = $db->getValue('value', 'view_zones', "province_id='".$province_id."'");
        } else {
            $province_id = "";
            $shipping = $db->getValue('value', 'view_zones_country', "country_id='".$country_id."'");
        }

        if (!$shipping || $shipping == "" || $sub_total > 80) {
            $shipping = "0.00";
        }

        $total = number_format(($sub_total + $shipping),2,".","");
        $percent = "";
        $discount = "";
        $coupon_id = "";
        if ($_REQUEST['valeDto'] != "") {
            $user_id = $_REQUEST['user_id'];
            $coupon = $this->applyCoupon($_REQUEST['valeDto'], $total, $user_id, $country_id, $province_id);
            $total = $coupon['total'];
            $percent = $coupon['percent'];
            $discount = $coupon['discount'];
            $coupon_id = $db->getValue("id", "coupon", "canonical_id='".$_REQUEST['valeDto']."'");
        }

        $order_status = 1;

        $placeHolders = array('__');
        $replacements = array(' ');
        $note = str_replace($placeHolders, $replacements, $aditional_note);
        $present_note = str_replace($placeHolders, $replacements, $order_note);

        // Se crea la Orden
        $arr_values = array(
            'sub_total' => $sub_total,
            'iva' => "",
            'iva_percentage' => "",
            'shipping' => $shipping,
            'coupon_code' => $_REQUEST['valeDto'],
            'coupon_id' => $coupon_id,
            'discount_percent' => $percent,
            'discount' => $discount,
            'total' => $total,
            'status_id' => $order_status,
            'user_id' => $user_id,
            'province_id' => $province_id,
            'method_payment_id' => 1,
            'note' => secure_mysql(sanitize($note)),
            'present' => ($present_note != "") ? "1" : "0",
            'present_note' => secure_mysql(sanitize($present_note)),
            'redsys_reference' => $order_id,
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s')
        );

        $order = $db->insertAction('orders', $arr_values);
        $arr_value = array('code' => 'OR'.$order[0]['id'].$user_id.'-'.$db->getCount('cart_detail', "user_id='".$user_id."'"));
        $order = $db->updateAction('orders', $arr_value, "id='".$order[0]['id']."'");

        // Se guarda el detalle de la orden y se descuenta del inventario
        foreach ($arr_cart as $val) {
            $product_iva = $val['iva'];
            $product_iva = ($product_iva) ? $product_iva : APP_IVA;

            if ($val['new_price'] != "") {
                $calculated_iva = "0.00";
                $sub_total = ($val['quantity'] * $val['new_price']) + $calculated_iva;
            } else {
                // $sub_total = ($val['quantity'] * $val['price']) + $calculated_iva;
                $sub_total = ($val['quantity'] * $val['price_iva']);
            }
            $calculated_iva = number_format(((($val['quantity'] * $val['price']) * $product_iva) / 100),2,".","");
            $arr_values = array(
                'product_id' => $val['product_id'],
                'order_id' => $order[0]['id'],
                'quantity' => $val['quantity'],
                'price' => $val['price_iva'],
                'new_price' => $val['new_price'],
                'product_iva' => $product_iva,
                'iva' => $calculated_iva,
                'sub_total' => number_format($sub_total,2,".",""),
                'user_id' => $user_id,
                'size_id' => $val['size_id'],
                'color_id' => $val['color_id'],
                'create_at' => date('Y-m-d H:i:s'),
                'update_at' => date('Y-m-d H:i:s')
            );
            $db->insertAction('order_detail', $arr_values);

            $cnt_colors = ($val['color_id'] && $val['color_id'] != "") ? $db->getCount("products_colors", "product_id='".$val['product_id']."' AND color_id='".$val['color_id']."'") : "0";
            $cnt_size = ($val['size_id'] && $val['size_id'] != "") ? $db->getCount("products_sizes", "product_id='".$val['product_id']."' AND size_id='".$val['size_id']."'") : "0";

            if ($cnt_colors == 0 && $cnt_size == 0) {
                // Se descuenta del inventario
                $current_quantity = $db->getValue("quantity", "inventory", "product_id='".$val['product_id']."'");
                $new_quantity = $current_quantity - $val['quantity'];
                if ($new_quantity < 0) {
                    // throw new \Exception("Error: La cantidad del pedido excede a la cantidad en existencia (".$db->getValue('name', 'products', "id='".$val['product_id']."'").")", 1);
                }

                $arr_values = array(
                    'quantity' => ($new_quantity <= 0) ? "0" : $new_quantity,
                    'update_at' => date('Y-m-d')
                );

                $db->updateAction("inventory", $arr_values, "product_id='".$val['product_id']."'");

                if ($new_quantity == 0) {
                    $this->sendEmail(1, $val['product_id']);
                }
            } else {
                // se resta colores
                if ($cnt_colors > 0) {
                    $current_stock = $db->getValue("stock", "products_colors", "product_id='".$val['product_id']."' AND color_id='".$val['color_id']."'");
                    $new_quantity = $current_stock - $val['quantity'];

                    if ($new_quantity == 0) {
                        $this->sendEmail(1, $val['product_id']);
                    }
                    $arr_values = array(
                        'stock' => ($new_quantity <= 0) ? "0" : $new_quantity,
                    );
                    $db->updateAction("products_colors", $arr_values, "product_id='".$val['product_id']."' AND color_id='".$val['color_id']."'");
                }
                
                // Se restan las tallas
                if ($cnt_size > 0) {
                    $current_stock = $db->getValue("stock", "products_sizes", "product_id='".$val['product_id']."' AND size_id='".$val['size_id']."'");
                    $new_quantity = $current_stock - $val['quantity'];

                    if ($new_quantity == 0) {
                        $this->sendEmail(1, $val['product_id']);
                    }
                    $arr_values = array(
                        'stock' => ($new_quantity <= 0) ? "0" : $new_quantity,
                    );
                    $db->updateAction("products_sizes", $arr_values, "product_id='".$val['product_id']."' AND size_id='".$val['size_id']."'");
                }
            }
        }

        // Si se usa un vale de descuento se guarda en el historial
        if ($_REQUEST['valeDto'] != "") {
            $arr_values = array(
                'user_id' => $_REQUEST['user_id'],
                'coupon_id' => $coupon_id,
                'create_at' => date('Y-m-d H:i:s')
            );
            $db->insertAction("coupon_user_history", $arr_values);
        }

        $db->deleteAction('cart_detail', "user_id='".$user_id."'");
        $this->sendEmail(2, 0, $order[0]);

        // Se envia el email al usuario
        $s = "SELECT * FROM view_user WHERE id='".$user_id."'";
        $arr_user = $db->fetchSQL($s);
        $order_date = datetime_format($order[0]['create_at']);

        $s = "SELECT * FROM view_orders_detail WHERE order_id='".$order[0]['id']."'";
        $arr_detail = $db->fetchSQL($s);

        $arr_data = array(
            'order' => $order[0],
            'order_date' => $order_date['date'],
            'method' => 'tarjeta',
            'detail' => $arr_detail,
            'total_products' => $db->getSum("quantity", "order_detail", "order_id='".$order[0]['id']."'")
        );

        $arr_target_email = array($arr_user[0]['email'], "info@cosmeceuticalcenter.com", "rufi@inkube.net", "ali.jose118@gmail.com");
        // $arr_target_email = array($arr_user[0]['email']);
        $this->sendEmailTemplate("html/email/order-info.php", $arr_data, $arr_user[0]['email'], $arr_user[0]['name']." ".$arr_user[0]['last_name'], "Confirmacion del pedido | Cosmeceutical Center", true, $arr_target_email);
    }

    /**
     * Genera la orden de compra
     * @return array
     */
    public function processOrderPaypal($present, $validate_amount)
    {
        $db = $this->db;

        // Se genera el LOG
        $this->createOrderLog($this->user_id, "PayPal");

        // Datos del carrito
        $s = "SELECT * FROM view_cart WHERE user_id='".$this->user_id."'";
        $arr_cart = $db->fetchSQL($s);
        $sub_total = 0;
        $shipping = "0.00";

        foreach ($arr_cart as $key => $val) {
            $product_iva = $val['iva'];
            $product_iva = ($product_iva) ? $product_iva : APP_IVA;
            $cnt_discount = $db->getCount('discount', "product_id='".$val['product_id']."'");
            $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$val['product_id']."'");
            $new_price = "";

            if ($cnt_discount == 1) {
                $new_price = $db->getValue("new_price", "discount", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            } elseif ($cnt_weekly == 1) {
                $new_price = $db->getValue("new_price", "weekly_recommendation", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            }
            $arr_cart[$key]['new_price'] = $new_price;
            $arr_cart[$key]['value'] = ($new_price != "") ? $new_price : $val['price_iva'];

            if ($new_price != "") {
                $arr_cart[$key]['sub_total'] = number_format(($new_price * $val['quantity']),2,".","");
                $calculated_iva = 0.00;
            } else {
                $arr_cart[$key]['sub_total'] = number_format(($val['price_iva'] * $val['quantity']),2,".","");
                $calculated_iva = number_format((($product_iva * ($val['price'] * $val['quantity'])) / 100),2,".","");
            }

            $arr_cart[$key]['sub_total'] = number_format($arr_cart[$key]['sub_total'],2,".","");
            $sub_total += $arr_cart[$key]['sub_total'];
        }

        $sub_total = number_format($sub_total,2,".","");

        // Se obtiene el valor del shipping
        if (isset($_SESSION['country'])) {
            if ($_SESSION['country'] == 73) {
                if (isset($_SESSION['province'])) {
                    $province_id = $_SESSION['province'];
                } else {
                    $province_id = $this->user_province;
                }

                $shipping = $db->getValue('value', 'view_zones', "province_id='".$province_id."'");
            } else {
                $province_id = "";
                $shipping = $db->getValue('value', 'view_zones_country', "country_id='".$_SESSION['country']."'");
            }
        } else {
            $country_id = $this->user_country;
            $province_id = $this->user_province;

            $shipping = $db->getValue('value', 'view_zones', "province_id='".$province_id."'");
        }

        if (!$shipping || $shipping == "" || $sub_total > 80) {
            $shipping = "0.00";
        }

        $total = number_format(($sub_total + $shipping),2,".","");
        $percent = "";
        $discount = "";
        $coupon_id = "";
        $validate_coupon = $this->validateCoupon($_POST['valeDto'], $this->user_id);
        if ($_POST['valeDto'] != "" && $validate_coupon) {
            $coupon = $this->applyCoupon($_POST['valeDto'], $total);
            $total = $coupon['total'];
            $percent = $coupon['percent'];
            $discount = $coupon['discount'];
            $shipping = $coupon['shipping'];
            $coupon_id = $db->getValue("id", "coupon", "canonical_id='".$_POST['valeDto']."'");
        } else {
            if ($_POST['method'] == 2) {
                $charge = number_format((($this->paypal_charge * $total) / 100),2,".","");
                $total = number_format(($total + $charge),2,".","");
            }
        }

        $order_status = ($total != $validate_amount) ? 4 : 1;

        // Se crea la Orden
        $arr_values = array(
            'sub_total' => $sub_total,
            'iva' => "",
            'iva_percentage' => "",
            'shipping' => $shipping,
            'coupon_code' => ($validate_coupon) ? $_POST['valeDto'] : "",
            'coupon_id' => $coupon_id,
            'discount_percent' => $percent,
            'discount' => $discount,
            'total' => $total,
            'status_id' => $order_status,
            'user_id' => $this->user_id,
            'province_id' => $province_id,
            'method_payment_id' => $_POST['method'],
            'note' => secure_mysql(sanitize($_POST['note'])),
            'present' => $present,
            'present_note' => secure_mysql(sanitize($_POST['order_note'])),
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s')
        );

        $order = $db->insertAction('orders', $arr_values);
        $arr_value = array('code' => 'OR'.$order[0]['id'].$this->user_id.'-'.$db->getCount('cart_detail', "user_id='".$this->user_id."'"));
        $order = $db->updateAction('orders', $arr_value, "id='".$order[0]['id']."'");

        // Se guarda el detalle de la orden y se descuenta del inventario
        foreach ($arr_cart as $val) {
            $product_iva = $db->getValue("iva", "products", "id='".$val['product_id']."'");
            $product_iva = ($product_iva) ? $product_iva : APP_IVA;

            if ($val['new_price'] != "") {
                $calculated_iva = "0.00";
                $sub_total = ($val['quantity'] * $val['new_price']) + $calculated_iva;
            } else {
                $calculated_iva = number_format(((($val['quantity'] * $val['price']) * $product_iva) / 100),2,".","");
                // $sub_total = ($val['quantity'] * $val['price']) + $calculated_iva;
                $sub_total = $val['quantity'] * $val['price_iva'];
            }

            $arr_values = array(
                'product_id' => $val['product_id'],
                'order_id' => $order[0]['id'],
                'quantity' => $val['quantity'],
                'price' => $val['price_iva'],
                'new_price' => $val['new_price'],
                'product_iva' => $product_iva,
                'iva' => $calculated_iva,
                'sub_total' => $sub_total,
                'user_id' => $this->user_id,
                'size_id' => $val['size_id'],
                'color_id' => $val['color_id'],
                'create_at' => date('Y-m-d H:i:s'),
                'update_at' => date('Y-m-d H:i:s')
            );
            $db->insertAction('order_detail', $arr_values);

            // Se descuenta del inventario
            // $current_quantity = $db->getValue("quantity", "inventory", "product_id='".$val['product_id']."'");
            // $new_quantity = $current_quantity - $val['quantity'];
            // if ($new_quantity < 0) {
            //     throw new \Exception("Error: La cantidad del pedido excede a la cantidad en existencia (".$db->getValue('name', 'products', "id='".$val['product_id']."'").")", 1);
            // }

            // $arr_values = array(
            //     'quantity' => ($new_quantity <= 0) ? "0" : $new_quantity,
            //     'update_at' => date('Y-m-d')
            // );

            // $db->updateAction("inventory", $arr_values, "product_id='".$val['product_id']."'");

            // if ($new_quantity == 0) {
            //     $this->sendEmail(1, $val['product_id']);
            // }

            $cnt_colors = ($val['color_id'] && $val['color_id'] != "") ? $db->getCount("products_colors", "product_id='".$val['product_id']."' AND color_id='".$val['color_id']."'") : "0";
            $cnt_size = ($val['size_id'] && $val['size_id'] != "") ? $db->getCount("products_sizes", "product_id='".$val['product_id']."' AND size_id='".$val['size_id']."'") : "0";

            if ($cnt_colors == 0 && $cnt_size == 0) {
                // Se descuenta del inventario
                $current_quantity = $db->getValue("quantity", "inventory", "product_id='".$val['product_id']."'");
                $new_quantity = $current_quantity - $val['quantity'];
                if ($new_quantity < 0) {
                    // throw new \Exception("Error: La cantidad del pedido excede a la cantidad en existencia (".$db->getValue('name', 'products', "id='".$val['product_id']."'").")", 1);
                }

                $arr_values = array(
                    'quantity' => ($new_quantity <= 0) ? "0" : $new_quantity,
                    'update_at' => date('Y-m-d')
                );

                $db->updateAction("inventory", $arr_values, "product_id='".$val['product_id']."'");

                if ($new_quantity == 0) {
                    $this->sendEmail(1, $val['product_id']);
                }
            } else {
                // se resta colores
                if ($cnt_colors > 0) {
                    $current_stock = $db->getValue("stock", "products_colors", "product_id='".$val['product_id']."' AND color_id='".$val['color_id']."'");
                    $new_quantity = $current_stock - $val['quantity'];

                    if ($new_quantity == 0) {
                        $this->sendEmail(1, $val['product_id']);
                    }
                    $arr_values = array(
                        'stock' => ($new_quantity <= 0) ? "0" : $new_quantity,
                    );
                    $db->updateAction("products_colors", $arr_values, "product_id='".$val['product_id']."' AND color_id='".$val['color_id']."'");
                }
                
                // Se restan las tallas
                if ($cnt_size > 0) {
                    $current_stock = $db->getValue("stock", "products_sizes", "product_id='".$val['product_id']."' AND size_id='".$val['size_id']."'");
                    $new_quantity = $current_stock - $val['quantity'];

                    if ($new_quantity == 0) {
                        $this->sendEmail(1, $val['product_id']);
                    }
                    $arr_values = array(
                        'stock' => ($new_quantity <= 0) ? "0" : $new_quantity,
                    );
                    $db->updateAction("products_sizes", $arr_values, "product_id='".$val['product_id']."' AND size_id='".$val['size_id']."'");
                }
            }
        }

        // Si se usa un vale de descuento se guarda en el historial
        if ($_POST['valeDto'] != "" && $validate_coupon) {
            $arr_values = array(
                'user_id' => $this->user_id,
                'coupon_id' => $coupon_id,
                'create_at' => date('Y-m-d H:i:s')
            );
            $db->insertAction("coupon_user_history", $arr_values);
        }

        $db->deleteAction('cart_detail', "user_id='".$this->user_id."'");
        $this->sendEmail(2, 0, $order[0]);

        // Se envia el email al usuario
        $s = "SELECT * FROM view_user WHERE id='".$this->user_id."'";
        $arr_user = $db->fetchSQL($s);
        $order_date = datetime_format($order[0]['create_at']);

        $s = "SELECT * FROM view_orders_detail WHERE order_id='".$order[0]['id']."'";
        $arr_detail = $db->fetchSQL($s);

        $arr_data = array(
            'order' => $order[0],
            'order_date' => $order_date['date'],
            'method' => 'PayPal',
            'detail' => $arr_detail,
            'total_products' => $db->getSum("quantity", "order_detail", "order_id='".$order[0]['id']."'")
        );

        $arr_target_email = array($arr_user[0]['email'], "info@cosmeceuticalcenter.com", "rufi@inkube.net", "ali.jose118@gmail.com");
        $this->sendEmailTemplate("html/email/order-info.php", $arr_data, "", $arr_user[0]['name']." ".$arr_user[0]['last_name'], "Confirmacion del pedido | Cosmeceutical Center", true, $arr_target_email);

        return $order[0];
    }

    /**
     * ELimina un producto del carrito desde la barra de navegación
     * @param  integer $product_id ID del producto
     * @return void
     */
    public function removeCartSession($product_id, $size_id, $color_id)
    {
        $db = $this->db;
        $db->deleteAction('cart_detail', "user_id='".$this->user_id."' AND product_id='".$product_id."' AND size_id='".$size_id."' AND color_id='".$color_id."'");
    }

    /**
     * Elimina una cookie del carrito desde la barra de navegación
     * @param  integer $product_id ID del producto
     * @return void
     */
    public function removeCartCookie($product_id, $size_id, $color_id)
    {
        if (isset($_COOKIE['cookie_cart_cosmeceutical'])) {
            $arr_cookie_cart = json_decode($_COOKIE['cookie_cart_cosmeceutical'], true);

            foreach ($arr_cookie_cart as $key => $val) {
                if ($val['product_id'] == $product_id && @$val['size_id'] == $size_id && @$val['color_id'] == $color_id) {
                    unset($arr_cookie_cart[$key]);
                }
            }

            setcookie('cookie_cart_cosmeceutical', json_encode($arr_cookie_cart), time() + (86400 * 30), "/");

            if (count($arr_cookie_cart) == 0) {
                unset($_SESSION['COSME_CART']);
            } else {
                $_SESSION['COSME_CART'] = json_encode($arr_cookie_cart);
            }
        }
    }

    /**
     * Edita la orden con la provincia
     * @param  integer $province_id ID de la provincia
     * @return array
     */
    public function getShippingPrices($province_id, $country_id)
    {
        $db = $this->db;

        $_SESSION['province'] = $province_id;
        $_SESSION['country'] = $country_id;
        $sub_total = 0;

        if (isset($_SESSION['USER_SESSION_COSME']['id'])) {
            $s = "SELECT * FROM view_cart WHERE user_id='".$this->user_id."'";
            $arr_cart = $db->fetchSQL($s);
        } else {
            $arr_cart = $this->getCartFromCookies();
        }

        if ($country_id && $country_id != "") {
            if ($country_id == 73) {
                $shipping = $db->getValue('value', 'view_zones', "province_id='".$province_id."'");
            } else {
                $shipping = $db->getValue('value', 'view_zones_country', "country_id='".$country_id."'");
            }
        }

        foreach ($arr_cart as $key => $val) {
            $product_iva = $val['iva'];
            $product_iva = ($product_iva) ? $product_iva : APP_IVA;

            $cnt_discount = $db->getCount('discount', "product_id='".$val['product_id']."'");
            $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$val['product_id']."'");
            $new_price = "";

            if ($cnt_discount == 1) {
                $new_price = $db->getValue("new_price", "discount", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            } elseif ($cnt_weekly == 1) {
                $new_price = $db->getValue("new_price", "weekly_recommendation", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            }
            $arr_cart[$key]['new_price'] = $new_price;
            $arr_cart[$key]['value'] = ($new_price != "") ? $new_price : $val['price_iva'];

            if ($new_price != "") {
                $calculated_iva = 0.00;
                $arr_cart[$key]['sub_total'] = number_format(($new_price * $val['quantity']),2,".","");
            } else {
                $arr_cart[$key]['sub_total'] = number_format(($val['price_iva'] * $val['quantity']),2,".","");
                $calculated_iva = number_format((($product_iva * ($val['price'] * $val['quantity'])) / 100),2,".","");
            }

            $arr_cart[$key]['sub_total'] = number_format($arr_cart[$key]['sub_total'],2,".","");
            $sub_total += $arr_cart[$key]['sub_total'];
        }

        $sub_total = number_format($sub_total,2,".","");
        $shipping = ($shipping == "" || $sub_total > 80) ? 0.00 : $shipping;
        $total = number_format(($sub_total + $shipping),2,".","");

        return array(
            'shipping' => number_format($shipping,2,".",""),
            'total' => $total
        );
    }

    /**
     * Envía los emails
     * @param  array $order Arreglo con la información de la orden
     * @return void
     */
    public function sendEmail($act = 1, $product_id = 0, $order = array())
    {
        $db = $this->db;
        $mail = new PHPMailer(true);

        if ($act == 1) { // Alerta de inventario
            // Producto
            $s = "SELECT * FROM products WHERE id='".$product_id."'";
            $arr_product = $db->fetchSQL($s);

            $product_name = $arr_product[0]['name'];
            $template = "html/email/inventory-alert.php";
            $subject = 'Alerta de inventario | Cosmeceuticalcenter';
        } elseif ($act == 2) {
            $template = "html/email/new-order.php";
            $subject = 'Nueva Orden | Cosmeceuticalcenter';
            $code = $order['code'];
        }

        ob_start();
        include($template);
        $template = ob_get_clean();

        //Server settings
        // $mail->SMTPDebug = 2;
        $mail->isSMTP();
        $mail->Host = EMAIL_HOST;
        $mail->SMTPAuth = true;
        $mail->Username = EMAIL_SENDER;
        $mail->Password = EMAIL_PASSWORD;
        $mail->SMTPSecure = 'tls';
        // $mail->Port = EMAIL_PORT;
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        //Recipients
        $mail->setFrom(EMAIL_SENDER, 'Cosmeceutical center');
        $mail->addAddress("info@cosmeceuticalcenter.com", "Cosmeceutical");
        $mail->addAddress("rufi@inkube.net", "Cosmeceutical");

        //Content
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body    = $template;
        $mail->AltBody = $template;

        $mail->send();
    }

    /**
     * Aplica un cupón
     * @param  string $coupon codigo del cupón
     * @return double
     */
    public function applyCoupon($coupon, $total = NULL, $user_id = NULL, $country_id = NULL, $province_id = NULL)
    {
        $db = $this->db;
        $o_total = number_format(0,2,".","");
        $p_total_discount = number_format(0,2,".","");
        $mp_calculate_charge = number_format(0,2,".",""); // Para metodo de pago
        $shipping = number_format(0,2,".","");
        $user = ($user_id) ? $user_id : $this->user_id;

        $s = "SELECT * FROM coupon WHERE canonical_id='".$coupon."'";
        $arr_coupon = $db->fetchSQL($s);

        $discount_percent = $arr_coupon[0]['discount'];
        $discount = number_format((($discount_percent * $total) / 100),2,".","");
        $new_total = number_format(($total - $discount),2,".","");
        if ($new_total < 0) {
            $new_total = "0.00";
        }

        $s = "SELECT * FROM view_cart WHERE user_id='".$user."'";
        $arr_cart = $db->fetchSQL($s);

        foreach ($arr_cart as $key => $val) {
            $new_price = "";
            $p_discount = 0;
            $valid_discount_product = false;
            $product_iva = ($val['iva']) ? $val['iva'] : 0;
            $cnt_discount = $db->getCount('discount', "product_id='".$val['product_id']."'");
            $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$val['product_id']."'");

            if ($cnt_discount == 1) {
                $new_price = $db->getValue("new_price", "discount", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            } elseif ($cnt_weekly == 1) {
                $new_price = $db->getValue("new_price", "weekly_recommendation", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            }

            if ($new_price != "") {
                $sub_total = number_format(($new_price * $val['quantity']),2,".","");
            } else {
                $sub_total = number_format(($val['price_iva'] * $val['quantity']),2,".","");
                $p_discount = number_format((($discount_percent * $sub_total) / 100),2,".","");
                $sub_total = number_format(($sub_total - $p_discount),2,".","");
                $valid_discount_product = true;
            }

            $p_total_discount += $p_discount;
            $o_total += $sub_total;

            $arr_cart[$key]['p_total_discount'] = number_format($p_discount,2,".","");
            $arr_cart[$key]['p_total'] = number_format($sub_total,2,".","");
            $arr_cart[$key]['product_iva'] = number_format($product_iva,2,".","");
            $arr_cart[$key]['valid_discount_product'] = $valid_discount_product;
        }


        if ($country_id) { // redsys
            if ($country_id == 73) {
                $shipping = $db->getValue('value', 'view_zones', "province_id='".$province_id."'");
            } else {
                $province_id = "";
                $shipping = $db->getValue('value', 'view_zones_country', "country_id='".$country_id."'");
            }
        } else { // paypal
            if (isset($_SESSION['country'])) {
                $country_id = $_SESSION['country'];
                if ($_SESSION['country'] == 73) {
                    if (isset($_SESSION['province'])) {
                        $province_id = $_SESSION['province'];
                    } else {
                        $province_id = $this->user_province;
                    }

                    $shipping = $db->getValue('value', 'view_zones', "province_id='".$province_id."'");
                } else {
                    $shipping = $db->getValue('value', 'view_zones_country', "country_id='".$_SESSION['country']."'");
                }
            } else {
                $country_id = 73;
                $province_id = 1;

                if ($session) {
                    $country_id = $this->user_country;
                    $province_id = $this->user_province;
                }

                $shipping = $db->getValue('value', 'view_zones', "province_id='".$province_id."'");
            }
        }


        if (!$shipping || $shipping == "" || $o_total > 80) {
            $shipping = number_format(0,2,".","");
        }

        if (isset($_POST['method'])) {
            if ($_POST['method'] == 2) { // pago con paypal - 2%
                $mp_calculate_charge = number_format((($this->paypal_charge * $o_total) / 100),2,".","");
            }
        }

        $arr_response = array(
            'total' => number_format(($o_total + $mp_calculate_charge + $shipping),2,".",""),
            'percent' => $discount_percent,
            'discount' => number_format($p_total_discount,2,".",""),
            'products_info' => $arr_cart,
            'mp_calculate_charge' => number_format($mp_calculate_charge,2,".",""),
            'shipping' => $shipping
        );

        return $arr_response;
    }

    /**
     * Obtiene el carrito desde las Cookies
     * @return array
     */
    public function getCartFromCookies()
    {
        $db = $this->db;
        $arr_products = array();

        if (isset($_SESSION['COSME_CART'])) {
            $arr_cart_cookie = json_decode($_SESSION['COSME_CART'], true);
            $arr_products_cookies = array();

            foreach ($arr_cart_cookie as $key => $val) {
                $arr_products_cookies[] = $val['product_id'];
                $arr_quantity[$val['product_id']] = (isset($val['quantity'])) ? $val['quantity'] : 1;
            }
            $products_id = implode(",", $arr_products_cookies);

            foreach ($arr_cart_cookie as $key => $val) {
                $s = "SELECT *, id AS product_id, product_name AS name FROM view_products WHERE id='".$val['product_id']."'";
                $arr_product = $db->fetchSQL($s);
                $arr_products[$key] = $arr_product[0];
                $arr_products[$key]['quantity'] = $val['quantity'];
                $arr_products[$key]['cart_id'] = $val['product_id'];
                $arr_products[$key]['size_id'] = @$val['size_id'];
                $arr_products[$key]['color_id'] = @$val['color_id'];
                $arr_products[$key]['size'] = $db->getValue("name", "size", "id='".@$val['size_id']."'");
                $arr_products[$key]['color'] = $db->getValue("name", "colors", "id='".@$val['color_id']."'");
            }

            // $s = "SELECT *, id AS product_id, product_name AS name FROM view_products WHERE id IN (".$products_id.")";
            // $arr_products = $db->fetchSQL($s);
            // foreach ($arr_products as $key => $val) {
            //     $arr_products[$key]['quantity'] = $arr_quantity[$val['product_id']];
            //     $arr_products[$key]['cart_id'] = $val['product_id'];
            // }
        }

        return $arr_products;
    }

    /**
     * Retorna el carrito del navbar
     * @return void
     */
    public function getNavCart()
    {
        $db = $this->db;
        $sub_total = 0;
        $total_products = 0;

        if (isset($_SESSION['USER_SESSION_COSME']['id'])) {
            $s = "SELECT * FROM view_cart WHERE user_id='".$this->user_id."'";
            $arr_cart = $db->fetchSQL($s);
        } else {
            $arr_cart = $this->getCartFromCookies();
        }

        foreach ($arr_cart as $key => $val) {
            $product_iva = $val['iva'];
            $product_iva = ($product_iva) ? $product_iva : APP_IVA;
            $arr_cart[$key]['product_iva'] = $product_iva;
            $arr_cart[$key]['formed_image'] = ($val['cover_image']) ? APP_IMG_PRODUCTS."product_".$val['product_id']."/cover/".$val['cover_image'] : APP_NO_IMAGES."no_image.jpg";
            $arr_cart[$key]['brand_name'] = $db->getValue("name_canonical", "brands", "id='".$val['brand_id']."'");
            $arr_cart[$key]['size_id'] = $val['size_id'];
            $arr_cart[$key]['color_id'] = $val['color_id'];
            $arr_cart[$key]['size'] = $db->getValue("name", "size", "id='".$val['size_id']."'");
            $arr_cart[$key]['color'] = $db->getValue("name", "colors", "id='".$val['color_id']."'");

            $cnt_discount = $db->getCount('discount', "product_id='".$val['product_id']."'");
            $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$val['product_id']."'");
            $new_price = "";

            if ($cnt_discount == 1) {
                $new_price = $db->getValue("new_price", "discount", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            } elseif ($cnt_weekly == 1) {
                $new_price = $db->getValue("new_price", "weekly_recommendation", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            }
            $arr_cart[$key]['new_price'] = $new_price;
            $arr_cart[$key]['value'] = ($new_price != "") ? $new_price : $val['price_iva'];

            if ($new_price != "") {
                $calculated_iva = 0.00;
                $arr_cart[$key]['sub_total'] = number_format(($new_price * $val['quantity']),2,".","");
            } else {
                // $arr_cart[$key]['sub_total'] = number_format(($val['price'] * $val['quantity']),2,".","");
                // $calculated_iva = number_format((($product_iva * $arr_cart[$key]['sub_total']) / 100),2,".","");
                $arr_cart[$key]['sub_total'] = number_format(($val['price_iva'] * $val['quantity']),2,".","");
                $calculated_iva = number_format((($product_iva * ($val['price'] * $val['quantity'])) / 100),2,".","");
            }

            $arr_cart[$key]['calculated_iva'] = $calculated_iva;
            $arr_cart[$key]['formed_price'] = ($new_price != "") ? $new_price : $val['price_iva'];
            // $arr_cart[$key]['sub_total'] = number_format(($calculated_iva + $arr_cart[$key]['sub_total']),2,".","");
            $arr_cart[$key]['sub_total'] = number_format($arr_cart[$key]['sub_total'],2,".","");
            $total_products += $val['quantity'];
            $sub_total += $arr_cart[$key]['sub_total'];
        }

        $sub_total = number_format($sub_total,2,".","");

        ob_start();
        include('html/ajx_show/cart-navbar.php');
        $arr_response = array(
            'html' => ob_get_clean(),
            'total' => $sub_total,
            'total_products' => $total_products
        );

        return $arr_response;
    }

    /**
     * Aplica carga por gestión
     * @param  integer $method Método de pago
     * @return array
     */
    public function applyMethodCharge($method, $coupon)
    {
        $db = $this->db;
        $current_amount = $_SESSION['COSME_TOTAL'];
        $total = $_SESSION['COSME_TOTAL'];
        $charge = $_SESSION['COSME_CHARGE'];
        $percent = ($method == 1) ? 0 : $this->paypal_charge;

        if ($_SESSION['COSME_CHARGE'] == 0 && $method == 2) {
            $charge = number_format((($this->paypal_charge * $current_amount) / 100),2,".","");
            $total = number_format(($current_amount + $charge),2,".","");
            $percent = $this->paypal_charge;

            $_SESSION['COSME_CHARGE'] = $charge;
            $_SESSION['COSME_TOTAL'] = $total;
        } elseif ($_SESSION['COSME_CHARGE'] > 0 && $method == 1) {
            $charge = $_SESSION['COSME_CHARGE'];
            $total = $current_amount - $charge;
            $charge = 0.00;
            $percent = 0;

            $_SESSION['COSME_CHARGE'] = 0.00;
            $_SESSION['COSME_TOTAL'] = $total;
        }

        // Se aplica el vale si no esta vacio
        if ($coupon != "") {
            $applyCoupon = $this->applyCoupon($coupon, $total);
            $total = $applyCoupon['total'];
            $c_percent = $applyCoupon['percent'];
            $c_discount = $applyCoupon['discount'];
            $charge = $applyCoupon['mp_calculate_charge'];
            $_SESSION['COSME_TOTAL'] = $total;
        }

        return array(
            'charge' => number_format($charge,2,".",""),
            'total' => number_format($total,2,".",""),
            'percent' => $percent,
            'coupon_percent' => @$c_percent,
            'coupon_discount' => @number_format($c_discount,2,".",""));
    }

    /**
     * Genera los logs de las ordenes
     *
     * @param integer $user_id  ID del usuario
     * @param string  $method   Método de pago
     * @return void
     */
    public function createOrderLog($user_id, $method)
    {
        $db = $this->db;
        $s = "SELECT * FROM view_user WHERE id='".$user_id."'";
        $arr_user = $db->fetchSQL($s);
        $user_code = $arr_user[0]['code'];

        $user_path = APP_SYSTEM_USERS."user_".$user_code."/logs/";
        $exist = fileExist($user_path);

        if (!$exist) {
            @mkdir(APP_SYSTEM_USERS."user_".$user_code."/");
            @mkdir(APP_SYSTEM_USERS."user_".$user_code."/profile/");
            @mkdir($user_path);
        }

        $log_name = "user_log".rand(0000,9999)."_".date("Y-m-d_H_i_s").".log";
        $log_content = "Log de respaldo para la orden del usuario: ".$user_id." ".$arr_user[0]["name"]." ".$arr_user[0]["last_name"]." ".$method." \n\n";

        // Información del carrito
        $s = "SELECT * FROM view_cart WHERE user_id='".$user_id."'";
        $arr_cart = $db->fetchSQL($s);

        foreach ($arr_cart as $key => $val) {
            $log_content.= "Producto ID: ".$val['product_id']." Nombre del producto: ".$val['name']." Cantidad: ".$val['quantity']." Precio: ".$val['price_iva']." IVA: ".$val['iva']."\n";
            $log_content.= "---------------------------------------------------------------------"."\n";
        }

        $log = fopen($user_path.$log_name, "w");
        fwrite($log, $log_content);
        fclose($log);

        $arr_values = array(
            'user_id' => $user_id,
            'log_name' => $log_name,
            'create_at' => date('Y-m-d H:i:s')
        );

        $db->insertAction("orders_logs", $arr_values);
    }

    /**
     * notificación de pedido fallido
     *
     * @return void
     */
    public function notifyErrorOrder()
    {
        $db = $this->db;
        // Datos del carrito
        $s = "SELECT * FROM view_cart WHERE user_id='".$_REQUEST['user_id']."'";
        $arr_cart = $db->fetchSQL($s);
        $sub_total = 0;
        $shipping = 0.00;
        $arr_detail = array();

        foreach ($arr_cart as $key => $val) {
            $product_iva = $val['iva'];
            $product_iva = ($product_iva) ? $product_iva : APP_IVA;
            $cnt_discount = $db->getCount('discount', "product_id='".$val['product_id']."'");
            $cnt_weekly = $db->getCount('weekly_recommendation', "product_id='".$val['product_id']."'");
            $new_price = "";

            if ($cnt_discount == 1) {
                $new_price = $db->getValue("new_price", "discount", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            } elseif ($cnt_weekly == 1) {
                $new_price = $db->getValue("new_price", "weekly_recommendation", "product_id='".$val['product_id']."' AND date_init <= '".date('Y-m-d')."' AND date_end >= '".date('Y-m-d')."'");
            }

            $arr_cart[$key]['new_price'] = $new_price;
            $arr_cart[$key]['value'] = ($new_price != "") ? $new_price : $val['price_iva'];

            if ($new_price != "") {
                $arr_cart[$key]['sub_total'] = number_format(($new_price * $val['quantity']),2,".","");
                $price_without_iva = (100 * $new_price) / (100 + $val['iva']);
                $calculated_iva = $new_price - $price_without_iva;
            } else {
                $arr_cart[$key]['sub_total'] = number_format(($val['price_iva'] * $val['quantity']),2,".","");
                $price_without_iva = $val['price'];
                $calculated_iva = number_format((($val['iva'] * $val['price']) / 100),2,".","");
            }

            $arr_cart[$key]['sub_total'] = number_format($arr_cart[$key]['sub_total'],2,".","");
            $sub_total += $arr_cart[$key]['sub_total'];

            $arr_detail[$key]['product_id'] = $val['product_id'];
            $arr_detail[$key]['product_name'] = $val['name'];
            $arr_detail[$key]['order_new_price'] = number_format($new_price,2,".","");
            $arr_detail[$key]['order_price'] = number_format($price_without_iva,2,".","");
            $arr_detail[$key]['quantity'] = $val['quantity'];
            $arr_detail[$key]['iva'] = $val['iva'];
            $arr_detail[$key]['product_iva'] = number_format($calculated_iva,2,".","");
            $arr_detail[$key]['sub_total'] = number_format($arr_cart[$key]['sub_total'],2,".","");
        }

        $sub_total = number_format($sub_total,2,".","");

        $country_id = $_REQUEST['country'];
        $province_id = $_REQUEST['province'];

        // Se obtiene el valor del shipping
        if ($country_id == 73) {
            $shipping = $db->getValue('value', 'view_zones', "province_id='".$province_id."'");
        } else {
            $province_id = "";
            $shipping = $db->getValue('value', 'view_zones_country', "country_id='".$country_id."'");
        }

        if (!$shipping || $shipping == "" || $sub_total > 80) {
            $shipping = "0.00";
        }

        $total = number_format(($sub_total + $shipping),2,".","");
        $percent = "0";
        $discount = "0.00";
        $coupon_id = "";
        if ($_REQUEST['valeDto'] != "") {
            $user_id = $_REQUEST['user_id'];
            $coupon = $this->applyCoupon($_REQUEST['valeDto'], $total, $user_id, $country_id, $province_id);
            $total = $coupon['total'];
            $percent = $coupon['percent'];
            $discount = $coupon['discount'];
            $coupon_id = $db->getValue("id", "coupon", "canonical_id='".$_REQUEST['valeDto']."'");
        }

        // Usuario
        $s = "SELECT * FROM view_user WHERE id='".$_REQUEST['user_id']."'";
        $arr_user = $db->fetchSQL($s);

        $arr_data = array(
            'order_date' => date('Y-m-d'),
            'method' => 'Tarjeta',
            'coupon_percent' => $percent,
            'coupon_code' => $_REQUEST['valeDto'],
            'coupon_discount' => $discount,
            'detail' => $arr_detail,
            'total_products' => $db->getSum("quantity", "cart_detail", "user_id='".$_REQUEST['user_id']."'"),
            'shipping' => $shipping,
            'total' => number_format($total,2,".",""),
        );

        $arr_target_email = array("info@cosmeceuticalcenter.com", "rufi@inkube.net", "ali.jose118@gmail.com");
        // $arr_target_email = array("rufi@inkube.net", "ali.jose118@gmail.com");
        $this->sendEmailTemplate(
            "html/email/order-notify-error.php",
            $arr_data,
            $arr_user[0]['email'],
            $arr_user[0]['name']." ".$arr_user[0]['last_name'],
            "Pedido fallido | Cosmeceutical Center",
            true,
            $arr_target_email
        );

        // Se guarda el registro en la BD
        $arr_values = array(
            'shipping' => $shipping,
            'coupon_code' => $_REQUEST['valeDto'],
            'discount_percent' => $percent,
            'discount_amount' => $discount,
            'total_value' => number_format($total,2,".",""),
            'user_id' => $_REQUEST['user_id'],
            'create_at' => date('Y-m-d H:i:s')
        );
        $f_order = $db->insertAction('order_failed', $arr_values);

        foreach ($arr_detail as $key => $val) {
            $arr_values = array(
                'f_order_id' => $f_order[0]['id'],
                'product_id' => $val['product_id'],
                'product_name' => $val['product_name'],
                'price' => $val['order_price'],
                'new_price' => $val['order_new_price'],
                'quantity' => $val['quantity'],
                'iva_percent' => $val['iva'],
                'iva_amount' => $val['product_iva'],
                'total_amount' => $val['sub_total']
            );
            $db->insertAction('order_failed_detail', $arr_values);
        }

        header('location: '.BASE_URL.'HOME'); exit;
    }

    /**
     * Valida el cupon
     * @param  string $code      Código del cupón
     * @param  integer $user_id  ID del usuario
     * @return boolean
     */
    public function validateCoupon($code, $user_id)
    {
        $db = $this->db;
        $r = false;
        $not_allowed = "#$%^*\|";
        $string_validate = true;

        if (!empty($code)) {
            for ($i = 0; $i < strlen($code); $i++){
                if (!strpos($not_allowed, substr($code, $i, 1)) === false){
                    $string_validate = false;
                }
            }

            if ($string_validate) {
                $cnt_val = $db->getCount("coupon", "canonical_id='".$code."' AND init_date <= '".date('Y-m-d')."' AND end_date >= '".date('Y-m-d')."'");
                if ($cnt_val == 1) {
                    $cnt_history = $db->getCount("view_coupon_history", "canonical_id='".$code."' AND user_id='".$user_id."'");

                    if ($cnt_history == 0) {
                        $r = true;
                    }
                }
            }
        }

        return $r;
    }
}


?>
