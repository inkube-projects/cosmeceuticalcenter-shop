<?php

/**
 * Servicio de las opiniones
 */
class OpinionService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Actualiza la opinion
     * @param  integer $id ID de la opinion
     * @return void
     */
    public function persist($id)
    {
        $db = $this->db;

        $arr_values = array(
            'review' => secure_mysql($_POST['opinion']),
            'score' => $_POST['score'],
            'status' => "1",
            'update_at' => date('Y-m-d H:i:s')
        );

        $db->updateAction("features", $arr_values, "id='".$id."'");
    }
}


?>
