<?php

/**
 * Servicios de cuenta
 */
class AccountService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Persiste datos de la cuenta
     * @param  string $date Fecha
     * @return boolean
     */
    public function persistProfile($date)
    {
        $db = $this->db;
        $arr_values = array(
            'username' => mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            'email' => mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            'username_canonical' => mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            'email_canonical' => mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            'update_at' => date('Y-m-d'),
        );
        $db->updateAction("user", $arr_values, "id='".$this->user_id."'");

        if ($_POST['pass'] != "") {
            $db->updateAction("user", array('password' => hashPass($_POST['pass'], 12)), "id='".$this->user_id."'");
        }

        $arr_values = array(
            'name' => secure_mysql($_POST['name']),
            'last_name' => secure_mysql($_POST['last_name']),
            'birthdate' => $date,
            'phone_1' => $_POST['phone'],
            'note' => secure_mysql($_POST['note'])
        );
        $db->updateAction("user_information", $arr_values, "user_id='".$this->user_id."'");

        $this->addLogs(sprintf("Editando datos del perfil"));

        $user = $db->fetchSQL("SELECT * FROM view_user WHERE id='".$this->user_id."'");
        $_SESSION['USER_SESSION_COSME'] = $user[0];

        return true;
    }

    /**
     * Datos de facturación
     * @param  integer $province ID de la provincia
     * @return boolean
     */
    public function persistBill($province)
    {
        $db = $this->db;
        $arr_values = array(
            'identification' => secure_mysql(addslashes($_POST['identification'])),
            'name' => secure_mysql(addslashes($_POST['bill_name'])),
            'last_name' => secure_mysql(addslashes($_POST['bill_last_name'])),
            'company' => secure_mysql(addslashes($_POST['company'])),
            'country_id' => $_POST['bill_country'],
            'province_id' => $province,
            'address_1' => secure_mysql(addslashes($_POST['bill_location_1'])),
            'address_2' => secure_mysql(addslashes($_POST['bill_location_2'])),
            'city' => secure_mysql(addslashes($_POST['bill_city'])),
            'postal_code' => secure_mysql(addslashes($_POST['bill_postal_code'])),
            'user_id' => $this->user_id,
            'update_at' => date('Y-m-d'),
        );
        $db->deleteAction("bill_information", "user_id='".$this->user_id."'");
        $db->insertAction("bill_information", $arr_values);

        $this->addLogs(sprintf("Editando datos de facturación"));

        return true;
    }

    /**
     * Dirección de envío
     * @param  integer $province ID de la provincia
     * @return boolean
     */
    public function persistShippingAddress($province)
    {
        $db = $this->db;
        $arr_values = array(
            'name_location' => secure_mysql(addslashes($_POST['location_name'])),
            'country_id' => secure_mysql(addslashes($_POST['addr_country'])),
            'province_id' => $province,
            'address_1' => secure_mysql(addslashes($_POST['addr_location_1'])),
            'address_2' => secure_mysql(($_POST['addr_location_2'])),
            'city' => secure_mysql(addslashes($_POST['addr_city'])),
            'postal_code' => secure_mysql(addslashes($_POST['addr_postal_code'])),
            'note' => secure_mysql(addslashes($_POST['addr_order_note'])),
            'update_at' => date('Y-m-d'),
        );
        $cnt_val = $db->getCount("shipping_address", "user_id='".$this->user_id."'");
        if ($cnt_val == 0) {
            $arr_values['user_id'] = $this->user_id;
            $db->insertAction("shipping_address", $arr_values);
        } else {
            $db->updateAction("shipping_address", $arr_values, "user_id='".$this->user_id."'");
        }

        $this->addLogs(sprintf("Editando dirección de envío"));
        return true;
    }
}


?>
