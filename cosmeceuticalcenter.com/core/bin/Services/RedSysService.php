<?php

include('core/bin/Services/RedSys/apiRedsys.php');

/**
 * Servicio para RedSys
 */
class RedSysService extends RedsysAPI
{
    // public $action_url = "https://sis-t.redsys.es:25443/sis/realizarPago"; // test
    // public $fuc = "999008881";
    // private $secret = "sq7HjrUOBfKmC576ILgskD5srU870gJ7";
    // public $terminal = "01";
    // public $currency = "978";
    // public $trans = "0";

    public $action_url = "https://sis.redsys.es/sis/realizarPago"; // Producción
    public $fuc = "327087201";
    private $secret = "abzBMTnuF+ZgX0Mgz7rkom3/KwbPHlBf";
    public $terminal = "1";
    public $currency = "978";
    public $trans = "0";

    public $version = "HMAC_SHA256_V1";

    public function getRedSysData($amount, $user_id)
    {
        $db = new Connection;
        $cnt_orders = $db->getCount("orders", "");
        $order_id = $user_id.($cnt_orders + rand(11, 99)).rand(0000, 9999);
        $this->setParamsOrder($order_id);
        $placeHolders = array(' ', '  ', '/');
        $replacements = array('__', '__', '__');
        $note = str_replace($placeHolders, $replacements, $_POST['note']);
        $order_note = str_replace($placeHolders, $replacements, $_POST['order_note']);

        $merchant_url = BASE_URL_PUBLIC."ajax.php?m=cart&act=9&aditional_note=".$note."&order_note=".$order_note."&order_id=".$order_id."&user_id=".$user_id;
		$merchant_url .= "&country=".$_POST['country']."&province=".$_POST['province']."&valeDto=".$_POST['valeDto'];

        $merchant_error_url = BASE_URL_PUBLIC."ajax.php?m=cart&act=11&user_id=".$user_id."&country=".$_POST['country']."&province=".$_POST['province']."&valeDto=".$_POST['valeDto'];
        // showArr($merchant_url); exit;

        // Se Rellenan los campos
        $this->setParameter("DS_MERCHANT_CONSUMERLANGUAGE", "001"); // 001 Español
		$this->setParameter("DS_MERCHANT_AMOUNT", $amount);
		$this->setParameter("DS_MERCHANT_ORDER", $order_id);
		$this->setParameter("DS_MERCHANT_MERCHANTCODE",$this->fuc);
		$this->setParameter("DS_MERCHANT_CURRENCY",$this->currency);
		$this->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $this->trans);
		$this->setParameter("DS_MERCHANT_TERMINAL", $this->terminal);
		$this->setParameter("DS_MERCHANT_MERCHANTURL", $merchant_url);
		$this->setParameter("DS_MERCHANT_URLOK", BASE_URL."PROCESADO/".$order_id);
		$this->setParameter("DS_MERCHANT_URLKO", $merchant_error_url);

        $params = $this->createMerchantParameters();
        $signature = $this->createMerchantSignature($this->secret);
        $version = $this->version;
        $url = $this->action_url;

        $form = '<html lang="es">
		<head>
		</head>
		<body>
		<form name="frm" action="'.$url.'" method="POST">
		<input type="hidden" name="Ds_SignatureVersion" value="'.$version.'"/></br>
		<input type="hidden" name="Ds_MerchantParameters" value="'.$params.'"/></br>
		<input type="hidden" name="Ds_Signature" value="'.$signature.'"/></br>
		<input type="hidden" value="Enviar" >
		<script language=javascript>document.frm.submit();</script>
		</form>

		</body>
		</html>';

        return $form;
    }

    /**
     * Crea una variable de sesion con los parametros necesarios para lla orden
     * @param void
     */
    public function setParamsOrder($order_id)
    {
        $_SESSION['COSME_ORDER_PARAMS']['order_id'] = $order_id;
        $_SESSION['COSME_ORDER_PARAMS']['order_note'] = $_POST['order_note'];
        $_SESSION['COSME_ORDER_PARAMS']['note'] = $_POST['note'];
        $_SESSION['COSME_ORDER_PARAMS']['method'] = 1;
        $_SESSION['COSME_ORDER_PARAMS']['valeDto'] = $_POST['valeDto'];
        $_SESSION['COSME_ORDER_PARAMS']['token'] = hashPass($order_id, 12);
    }

    /**
     * Verifica si el pago y la orden es válido
     * @return string|Exception
     */
    public function statusPayment()
    {
        $db = new Connection;
        if (!empty($_REQUEST)) {
            $version = $_REQUEST["Ds_SignatureVersion"];
            $datos = $_REQUEST["Ds_MerchantParameters"];
            $signatureRecibida = $_REQUEST["Ds_Signature"];

            $decodec = $this->decodeMerchantParameters($datos);
            $kc = $this->secret;
            $firma = $this->createMerchantSignatureNotif($kc, $datos);
            $codigoRespuesta = $this->getParameter('Ds_Response');
            $transactionId = $this->getParameter('Ds_AuthorisationCode');

            if ($firma === $signatureRecibida){
                $num_order = $this->getParameter('Ds_Order');
                $arrNumOrder = explode("-",$num_order);
                $numOrder = intval($arrNumOrder[0]);

                $cnt_val = $db->getCount("orders", "redsys_reference='".$numOrder."'");
                if ($cnt_val == 1) {
                    throw new Exception("Esta orden ya se encuentra cancelada", 1);
                }
                if(intval($codigoRespuesta) >= 100){
                    throw new Exception("Orden: ".$numOrder." DENEGADO POR EL BANCO", 1);
                }

                return $numOrder;
            } else {
                throw new Exception("FIRMA KO", 1);
            }
        }
    }
}


?>
