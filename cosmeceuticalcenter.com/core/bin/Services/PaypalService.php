<?php

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Core\ProductionEnvironment;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;

/**
 * Clase que se encarga de los servicios de PayPal
 */
class PaypalService
{
    // public $env = "sandbox";
    // public $clientID = "Afit9YjKmuF4VvhtnoDHKKMZx-5h-g7aWdtkKAsOSkRSP5it2_COD7CJvJ_-m0qygDw-FdIO4My6EjVr";
    // private $secretID = "EBf3ntYzMgCtTvCz1IWxGlAVQ4J4tvOEx8vTmd7xD--Zy6SfPt4rlCgqNZg4py6jyTOn3N5iG5e5uG1h";
    public $env = "production";
    public $clientID = "AXo9yBxPY5-EcC4lUC2q6p1FeD1_SgpaoZj_DFnJjAjBuicDx5HefPp7u4fsSecbwOemNA4f8eXz4IiD";
    private $secretID = "EMNrFKwkVIYGvZ77Ls_L2KOxBprrhm-KVVxIy2n0ps44McxM-_3gnPsw8yma5A4KNZe0Ne_8bPUyeXdT";


    public function getOrder($orderId)
    {
        $client = $this->client();
        $response = $client->execute(new OrdersGetRequest($orderId));

        // print "Gross Amount: {$response->result->purchase_units[0]->amount->currency_code} {$response->result->purchase_units[0]->amount->value}\n";

        $arr_response = array(
            'status_code' => $response->statusCode,
            'status' => $response->result->status,
            'order_id' => $response->result->id,
            'intent' => $response->result->intent,
            'amount' => $response->result->purchase_units[0]->amount->value,
            'links' => $response->result->links
        );

        return $arr_response;
    }

    public function client()
    {
        return new PayPalHttpClient($this->environment());
    }

    /**
     * Set up and return PayPal PHP SDK environment with PayPal access credentials.
     * This sample uses SandboxEnvironment. In production, use ProductionEnvironment.
     */
    public function environment()
    {
        if ($this->env == "sandbox") {
            return new SandboxEnvironment($this->clientID, $this->secretID);
        } elseif ($this->env == "production") {
            return new ProductionEnvironment($this->clientID, $this->secretID);
        }
    }
}


?>
