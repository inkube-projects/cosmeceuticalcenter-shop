<?php

/**
 * Serivio para los favoritos
 */
class FavoriteService extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Agrega los favoritos
     * @return void
     */
    public function persist()
    {
        $db = $this->db;

        $db->deleteAction('favorites', "user_id='".$this->user_id."' AND product_id='".$_POST['product']."'");
        $arr_value = array(
            'user_id' => $this->user_id,
            'product_id' => $_POST['product']
        );

        $db->insertAction("favorites", $arr_value);
    }

    /**
     * Elimina un favorito
     * @param  integer $id ID del favorito
     * @return string
     */
    public function remove($id)
    {
        $db = $this->db;
        $db->deleteAction("favorites", "id='".$id."'");

        return $this->getFavoritesHtml();
    }

    /**
     * Obtiene el HTML de la tabla de los favoritos
     * @return string
     */
    public function getFavoritesHtml()
    {
        $db = $this->db;

        $s = "SELECT * FROM view_favorites WHERE user_id='".$this->user_id."'";
        $arr_favorites = $db->fetchSQL($s);

        ob_start();
        include('html/ajx_show/favorites-table.php');

        return ob_get_clean();
    }
}


?>
