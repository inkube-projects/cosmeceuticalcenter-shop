<?php

include('core/Model/GeneralMethods.php');
include('core/bin/Services/FavoriteService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_POST) {
    $favoriteService = new FavoriteService($db);

    switch ($act) {
        case 1: // Vista previa del producto
            $db->beginTransaction();
            $product_id = @number_format($_POST['product'],0,"","");

            try {
                $db->existRecord("id='".$product_id."'", "products", "El producto no es válido");

                $result = $favoriteService->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha agregado correctamente");
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Elimina un favorito
            $db->beginTransaction();
            $product_id = @number_format($_POST['product'],0,"","");

            try {
                $db->existRecord("id='".$product_id."'", "products", "El producto no es válido");

                $result = $favoriteService->remove($product_id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha eliminado correctamente", 'data' => $result);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
