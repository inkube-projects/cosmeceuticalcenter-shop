<?php

include('core/Model/GeneralMethods.php');
include('core/bin/Services/NotificationService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $notificationService = new NotificationService($db);

    switch ($act) {
        case 1: // Muestra una notificación
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");

            try {
                $db->existRecord("id='".$id."'", "notifications", "La notificación no es válida");

                $result = $notificationService->getNotification($id);

                $arr_response = array(
                    'status' => 'OK',
                    'message' => "Se ha cargado correctamente",
                    'data' => array(
                        'html' => $result,
                        't_notifications' => $db->getCount("notifications", "user_id='".$admin_pp_id."' AND status='0'")
                    )
                );
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Elimina una notificación
            $db->beginTransaction();
            $id = @number_format($_POST['notification'],0,"","");

            try {
                $db->existRecord("id='".$id."'", "notifications", "La notificación no es válida");

                $result = $notificationService->removeNotificaction($id);
                $arr_response = array(
                    'status' => 'OK',
                    'message' => "Se ha eliminado correctamente",
                );
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
