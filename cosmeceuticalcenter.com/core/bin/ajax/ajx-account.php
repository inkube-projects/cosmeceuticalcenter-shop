<?php

include('core/Model/GeneralMethods.php');
include('core/bin/Services/AccountService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $accountService = new AccountService($db);

    switch ($act) {
        case 1: // Editan datos de la cuenta
            $db->beginTransaction();
            $date = $_POST['year']."-".$_POST['month']."-".$_POST['day'];
            $arr_required = array(
                'name',
                'last_name',
                'phone',
                'email'
            );

            try {
                isRequiredValuesPost($_POST, $arr_required);
                // Cuenta de usuario
                isValidString($_POST['name'], "Error Campo Nombre: No se permiten caracteres especiales");
                isValidString($_POST['last_name'], "Error Campo Apellido 1: No se permiten caracteres especiales");
                isValidPhoneNumber($_POST['phone']);
                isValidEmail($_POST['email']);
                isValidDate($date, "en");

                $db->duplicatedRecord("email='".$_POST['email']."' AND NOT id='".$admin_pp_id."'", "user", "El email ya se encuentra en uso");

                $result = $accountService->persistProfile($date);

                $arr_response = array('status' => 'OK', 'message' => "Se ha procesado correctamente");
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Datos de facturación
            $db->beginTransaction();
            $arr_required = array(
                'identification',
                'bill_name',
                'bill_last_name',
                'bill_country',
                'bill_location_1',
                'bill_location_2',
                'bill_city',
                'bill_postal_code'
            );

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidOption(1, 2, $_POST['type_account'], "El tipo de cuenta no es válido");
                isValidString($_POST['identification'], "Error Campo NIF/CIF/NIE: No se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['bill_name'], "Error Campo Nombre(s): No se permiten caracteres especiales");
                isValidString($_POST['bill_last_name'], "Error Campo Apellidos(s): No se permiten caracteres especiales");
                isValidString($_POST['company'], "Error Campo Empresa: No se permiten caracteres especiales");
                isValidString($_POST['bill_location_1'], "Error Campo Dirección: No se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['bill_location_2'], "Error Campo Dirección: No se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['bill_city'], "Error Campo Ciudad: No se permiten caracteres especiales");
                isValidString($_POST['bill_postal_code'], "Error Campo Código Postal: No se permiten caracteres especiales");

                $db->existRecord("id='".@number_format($_POST['bill_country'])."'", "country", "El país no es válido");
                if (isset($_POST['bill_province']) && $_POST['bill_country'] == 73) { // 73 es el ID de españa
                    $db->existRecord("id='".@number_format($_POST['bill_province'])."'", "province", "La provincia no es válida");
                    $province = $_POST['bill_province'];
                } else {
                    $province = "";
                }

                $result = $accountService->persistBill($province);

                $arr_response = array('status' => 'OK', 'message' => "Se ha procesado correctamente");
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3: // Datos de envío
            $db->beginTransaction();
            $arr_required = array(
                'location_name',
                'addr_country',
                'addr_location_1',
                'addr_location_2',
                'addr_city',
                'addr_postal_code'
            );

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['location_name'], "Error Campo Nombre dirección: No se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['addr_location_1'], "Error Campo dirección: No se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['addr_location_2'], "Error Campo dirección: No se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['addr_city'], "Error Campo ciudad: No se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['addr_postal_code'], "Error código postal: No se permiten caracteres especiales", "#$%^*\|");

                $db->existRecord("id='".@number_format($_POST['addr_country'])."'", "country", "El país no es válido");
                if (isset($_POST['addr_province']) && $_POST['addr_country'] == 73) { // 73 es el ID de españa
                    $db->existRecord("id='".@number_format($_POST['addr_province'])."'", "province", "La provincia no es válida");
                    $province = $_POST['addr_province'];
                } else {
                    $province = "";
                }

                $result = $accountService->persistShippingAddress($province);

                $arr_response = array('status' => 'OK', 'message' => "Se ha procesado correctamente");
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
