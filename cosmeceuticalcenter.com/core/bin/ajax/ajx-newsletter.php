<?php

include('core/Model/GeneralMethods.php');
include('core/bin/Services/NewsletterService.php');

$db = new Connection();
$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_POST) {
    $newsletterService = new NewsletterService($db);

    switch ($act) {
        case 1: // Agrega un email al newsletter
            try {
                isRequiredValuesPost($_POST, array('newsletter_email'));
                isValidEmail($_POST['newsletter_email']);
                $db->duplicatedRecord("email='".$_POST['newsletter_email']."'", "newsletter", "El email ya se encuentra registrado");

                $newsletterService->persist();

                $arr_response = array('status' => 'OK', 'message' => 'Inicio de sesión correcto');
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
