<?php

include('core/Model/GeneralMethods.php');
include('core/bin/Services/CartService.php');
include('core/bin/Services/PaypalService.php');
include('core/bin/Services/RedSysService.php');

$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $cartService = new CartService($db);

    switch ($act) {
        case 1: // Agrega un producto al carrito
            $db->beginTransaction();
            $product_id = @number_format($_POST['product'],0,"","");

            try {
                $db->existRecord("id='".$product_id."'", "products", "El producto no es válido");

                if (isset($_POST['quantity'])) {
                    isValidNumber($_POST['quantity']);
                    $qty = $_POST['quantity'];
                } else {
                    $qty = 1;
                }

                validateProductsAttr($product_id);
                verifyProductsQuantity($product_id, $qty);

                if (isset($_SESSION['USER_SESSION_COSME']['id'])) {
                    $w = "user_id='".$admin_pp_id."' AND product_id='".$product_id."'";

                    if (isset($_POST['size'])) {
                        if ($_POST['size'] && $_POST['size'] != "") {
                            $size_id = @number_format($_POST['size'],0,"","");
                            $w .= " AND size_id='".$size_id."'";
                        }
                    }

                    if (isset($_POST['color'])) {
                        if ($_POST['color'] && $_POST['color'] != "") {
                            $color_id = @number_format($_POST['color'],0,"","");
                            $w .= " AND color_id='".$color_id."'";
                        }
                    }

                    $cnt_cart = $db->getCount("cart_detail", $w);
                    if ($cnt_cart == 0) {
                        $cartService->persist($product_id, $qty);
                    } else {
                        $cartService->update($product_id, $qty);
                    }
                } else {
                    $cnt_cart = (isset($_COOKIE['cookie_cart_cosmeceutical'])) ? count(json_decode($_COOKIE['cookie_cart_cosmeceutical'], true)) : 0;
                    $size_id = NULL;
                    $color_id = NULL;

                    if (isset($_POST['size'])) {
                        if ($_POST['size'] && $_POST['size'] != "") {
                            $size_id = @number_format($_POST['size'],0,"","");
                        }
                    }

                    if (isset($_POST['color'])) {
                        if ($_POST['color'] && $_POST['color'] != "") {
                            $color_id = @number_format($_POST['color'],0,"","");
                        }
                    }

                    if ($cnt_cart == 0) {
                        $cartService->persistWithoutSession($product_id, $qty, $size_id, $color_id);
                    } else {
                        $cartService->updateWithoutSession($product_id, $qty, $size_id, $color_id);
                    }
                }

                $data = $cartService->getNavCart();

                $arr_response = array('status' => 'OK', 'message' => "Se ha agregado correctamente", "data" => $data);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se edita el carrito
            $db->beginTransaction();
            $cart_id = @number_format($_POST['cart'],0,"","");
            $quantity = @number_format($_POST['quantity'],0,"","");
            $country = @number_format($_POST['country'],0,"","");
            $province = @number_format($_POST['province'],0,"","");

            try {
                if (isset($admin_pp_id)) { // Si hay una sesión se verifica en la BD
                    $db->existRecord("id='".$cart_id."' AND user_id='".$admin_pp_id."'", "cart_detail", "El producto no es válido");
                    $db->existRecord("id='".$country."'", "country", "EL país no es válido");
                    if ($province && $province != 0) {
                        $db->existRecord("id='".$province."'", "province", "La provincia no es válida");
                    }
                    $product_id = $db->getValue("product_id", "cart_detail", "id='".$cart_id."' AND user_id='".$admin_pp_id."'");
                    isValidNumber($quantity);
                    isValidOption(1, 100, $quantity, "La cantidad no es válida");

                    verifyProductsQuantity($product_id, $quantity);
                    $result = $cartService->updateOrder($cart_id, $quantity);
                } else { // Si no hay sesión se verifica en la cookie
                    $arr_cookie_cart = json_decode($_COOKIE['cookie_cart_cosmeceutical'], true);
                    $db->existRecord("id='".$cart_id."'", "products", "El producto no es válido");
                    $db->existRecord("id='".$country."'", "country", "EL país no es válido");
                    if ($province && $province != 0) {
                        $db->existRecord("id='".$province."'", "province", "La provincia no es válida");
                    }
                    verifyProductsQuantity($cart_id, $quantity);
                    $result = $cartService->updateOrderCookie($arr_cookie_cart, $cart_id, $quantity);
                }

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente el carrito", 'data' => $result);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3: // Elimina un producto del carrito
            $db->beginTransaction();
            $cart_id = @number_format($_POST['id'],0,"","");
            $size_id = ($_POST['size'] != "") ? @number_format($_POST['size'],0,"","") : NULL;
            $color_id = ($_POST['color'] != "") ? @number_format($_POST['color'],0,"","") : NULL;
            $reload = false;

            try {
                if ($size_id) {
                    $db->existRecord("id='".$size_id."'", "size", "La talla no es válida");
                }
                if ($color_id) {
                    $db->existRecord("id='".$color_id."'", "colors", "El color no es válido");
                }

                if (isset($admin_pp_id)) { // Se elimina de la BD
                    $db->existRecord("id='".$cart_id."' AND user_id='".$admin_pp_id."'", "cart_detail", "El producto no es válido");
                    $result = $cartService->removeProduct($cart_id);
                } else { // Se elimina de la cookie
                    $result = $cart_id;
                    $reload = true;
                    if (isset($_COOKIE['cookie_cart_cosmeceutical'])) {
                        $arr_cookie_cart = json_decode($_COOKIE['cookie_cart_cosmeceutical'], true);

                        foreach ($arr_cookie_cart as $key => $val) {
                            if (isset($val['product_id'])) {
                                if ($val['product_id'] == $cart_id && $val['size_id'] == $size_id && $val['color_id'] == $color_id) {
                                    unset($arr_cookie_cart[$key]);
                                }
                            } else {
                                unset($arr_cookie_cart[$key]);
                            }
                        }
                        setcookie('cookie_cart_cosmeceutical','',time() - 3600, "/");
                        if (count($arr_cookie_cart) > 0) {
                            setcookie('cookie_cart_cosmeceutical', json_encode($arr_cookie_cart), time() + (86400 * 30), "/");
                        } else {
                            unset($_SESSION['COSME_CART']);
                        }
                    }
                }

                $arr_response = array('status' => 'OK', 'message' => "Se ha elimnado correctamente el producto", 'data' => $result, 'reload' => $reload);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 4: // realiza el checkout paypal
            $paypalService = new PaypalService;
            $db->beginTransaction();
            $present = "0";

            try {
                isRequiredValuesPost($_POST, array('method', 'o'));
                isValidString($_POST['order_note'], "No se permiten caracteres especiales", "#%^*\|");
                isValidString($_POST['note'], "Nota Adicional: No se permiten caracteres especiales", "#%^*\|");
                if ($_POST['order_note'] != "") { $present = "1"; }
                $cnt_val = $db->getCount("cart_detail", "user_id='".$admin_pp_id."'");
                if ($cnt_val == 0) { throw new \Exception("EL carrito no es válido", 1); }
                $db->existRecord("id='".@number_format($_POST['method'],0,"","")."'", "method_payment", "El método de pago no es válido");
                // se valida el cupón
                // isValidString($_POST['valeDto'], "Vale de descuento: No se permiten caracteres especiales", "#$%^*\|");
                // if ($_POST['valeDto'] != "") {
                //     validateCoupon($_POST['valeDto'], $admin_pp_id);
                // }

                $validate = $paypalService->getOrder($_POST['o']);
                $result = $cartService->processOrderPaypal($present, $validate['amount']);
                $arr_response = array('status' => 'OK', 'message' => "Se ha procesado correctamente la Orden", 'id' => $result['code']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 5: // elimina productos desde el navbar
            $db->beginTransaction();
            $product_id = @number_format($_POST['product'],0,"","");
            $size_id = ($_POST['size'] != "") ? @number_format($_POST['size'],0,"","") : NULL;
            $color_id = ($_POST['color'] != "") ? @number_format($_POST['color'],0,"","") : NULL;


            try {
                $db->existRecord("id='".$product_id."'", "products", "El producto no es válido");

                if ($size_id) {
                    $db->existRecord("id='".$size_id."'", "size", "La talla no es válida");
                }
                if ($color_id) {
                    $db->existRecord("id='".$color_id."'", "colors", "El color no es válido");
                }

                if (isset($_SESSION['USER_SESSION_COSME']['id'])) {
                    $cartService->removeCartSession($product_id, $size_id, $color_id);
                } else {
                    $cartService->removeCartCookie($product_id, $size_id, $color_id);
                }

                $data = $cartService->getNavCart();
                $arr_response = array('status' => 'OK', 'message' => "Se ha eliminado correctamente", 'data' => $data);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 6: // Calcula con la provincia
            $province_id = @number_format($_POST['province'],0,"","");
            $country_id = @number_format($_POST['country'],0,"","");

            try {
                $db->existRecord("id='".$province_id."'", "province", "La provincia no es válidas");

                $result = $cartService->getShippingPrices($province_id, $country_id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha calculado correctamente", 'data' => $result);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 7: // se aplica el cupón
            $db->beginTransaction();
            $coupon = $_POST['valeDto'];

            try {
                validateCoupon($_POST['valeDto'], $admin_pp_id);

                $data = $cartService->applyCoupon($coupon);

                $arr_response = array('status' => 'OK', 'message' => "Se ha procesado correctamente", 'data' => $data);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 8: // Se envía a la pasarela de pago
            $redSysService = new RedSysService;
            try {
                isRequiredValuesPost($_POST, array('method'));
                isValidString($_POST['order_note'], "No se permiten caracteres especiales", "#%^*\|");
                isValidString($_POST['note'], "Nota Adicional: No se permiten caracteres especiales", "#%^*\|");
                if ($_POST['order_note'] != "") { $present = "1"; }
                $cnt_val = $db->getCount("cart_detail", "user_id='".$admin_pp_id."'");
                if ($cnt_val == 0) { throw new \Exception("EL carrito no es válido", 1); }
                $db->existRecord("id='".@number_format($_POST['method'],0,"","")."'", "method_payment", "El método de pago no es válido");
                // se valida el cupón
                isValidString($_POST['valeDto'], "Vale de descuento: No se permiten caracteres especiales", "#$%^*\|");
                if ($_POST['valeDto'] != "") {
                    validateCoupon($_POST['valeDto'], $admin_pp_id);
                }
                $db->existRecord("id='".@number_format($_POST['country'],0,"","")."'", "country", "El país no es válido");
                if (isset($_POST['province'])) {
                    if ($_POST['province'] && $_POST['province'] != "") {
                        $db->existRecord("id='".@number_format($_POST['province'],0,"","")."'", "province", "La provincia no es válida");
                    }
                }

                $amount = $_POST['amount'] * 100;
                $amount = number_format($amount,0,"","");
                $data = $redSysService->getRedSysData($amount, $admin_pp_id);
                $arr_response = array('status' => 'OK', 'message' => "Se ha procesado correctamente", 'data' => $data);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 9: // Se procesa la orden
            $redSysService = new RedSysService;

            try {
                $data = $redSysService->statusPayment();
                $result = $cartService->processOrderRedSys();
                $arr_response = array('status' => 'OK', 'message' => "FIRMA OK");
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 10: // Se hace el recargo de paypal
            $method = @number_format($_POST['method'],0,"","");
            $coupon = @secure_mysql($_POST['valeDto']);

            try {
                $db->existRecord("id='".$method."'", "method_payment", "El método de pago no es válido");
                if ($coupon != "") {
                    $db->existRecord("canonical_id='".$coupon."'", "coupon", "El vale de descuento no es válido");
                }

                $data = $cartService->applyMethodCharge($method, $coupon);
                $arr_response = array('status' => 'OK', 'message' => "Se ha procesado correctamente", 'data' => $data);
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 11:
            try {
                $result = $cartService->notifyErrorOrder();
                $arr_response = array('status' => 'OK', 'message' => "FIRMA KO");
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 12:
            // paypal
            // try {
            //     $_POST['valeDto'] = "";
            //     $_POST['method'] = 2;
            //     $_POST['note'] = "";
            //     $_POST['order_note'] = "";
            //     $present = "0";

            //     $result = $cartService->processOrderPaypal($present, '389.13');
            //     $arr_response = array('status' => 'OK', 'message' => "Se ha procesado correctamente la Orden", 'id' => $result['code']);
            // } catch (\Exception $e) {
            //     $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            // }

            // Para tarjeta
            // try {
            //     $_REQUEST['user_id'] = 25;
            //     $_REQUEST['country'] = 73;
            //     $_REQUEST['province'] = 1;
            //     $_REQUEST['aditional_note'] = "";
            //     $_REQUEST['order_note'] = "";
            //     $_REQUEST['order_id'] = "test";
            //     $_REQUEST['valeDto'] = "";

            //     $cartService->processOrderRedSys();
            //     $arr_response = array('status' => 'OK', 'message' => "FIRMA KO");
            // } catch (Exception $e) {
            //     $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            // }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

/**
 * Verifica las cantidades en existencia
 * @param  integer $product_id ID del producto
 * @param  integer $qty        Cantidad de la orden
 * @return Exception|boolean
 */
function verifyProductsQuantity($product_id, $qty)
{
    $db = new Connection;
    $product_name = $db->getValue("name", "products", "id='".$product_id."'");

    if (isset($_POST['size']) || isset($_POST['color'])) {
        $cnt_colors = (isset($_POST['color']) && $_POST['color'] && $_POST['color'] != "") ? $db->getCount("products_colors", "product_id='".$product_id."' AND color_id='".$_POST['color']."'") : "0";
        $cnt_size = (isset($_POST['size']) && $_POST['size'] && $_POST['size'] != "") ? $db->getCount("products_sizes", "product_id='".$product_id."' AND size_id='".$_POST['size']."'") : "0";

        if ($cnt_colors > 0) {
            $current_stock = $db->getValue("stock", "products_colors", "product_id='".$product_id."' AND color_id='".$_POST['color']."'");
            $new_quantity = $current_stock - $qty;

            if ($new_quantity < 0) {
                throw new \Exception("La cantidad del pedido excede a la cantidad en existencia (producto: ".$product_name."), Color", 1);
            }
        }

        if ($cnt_size > 0) {
            $current_stock = $db->getValue("stock", "products_sizes", "product_id='".$product_id."' AND size_id='".$_POST['size']."'");
            $new_quantity = $current_stock - $qty;

            if ($new_quantity < 0) {
                throw new \Exception("La cantidad del pedido excede a la cantidad en existencia (producto: ".$product_name."), Talla", 1);
            }
        }
    } else {
        $current_quantity = $db->getValue("quantity", "inventory", "product_id='".$product_id."'");

        if (!$current_quantity) {
            throw new \Exception("La cantidad del pedido excede a la cantidad en existencia", 1);
        }
        $total_quantity = $current_quantity - $qty;

        if ($total_quantity < 0) {
            throw new \Exception("La cantidad del pedido excede a la cantidad en existencia (producto: ".$product_name.")", 1);
        }
    }



    return true;
}

/**
 * Validaciones de los cupones
 * @param  string $code     Código del cupón
 * @param  integer $user_id ID del usuario
 * @return boolean
 */
function validateCoupon($code, $user_id)
{
    $db = new Connection();
    // showArr("canonical_id='".$code."' AND init_date <= '".date('Y-m-d')."' AND end_date >= '".date('Y-m-d')."'"); exit;
    $db->existRecord("canonical_id='".$code."' AND init_date <= '".date('Y-m-d')."' AND end_date >= '".date('Y-m-d')."'", "coupon", "El cupón no es válido");
    $cnt_val = $db->getCount("view_coupon_history", "canonical_id='".$code."' AND user_id='".$user_id."'");
    if ($cnt_val > 0) { throw new \Exception("Ya has consumido este cupón", 1); }

    $s = "SELECT * FROM coupon WHERE canonical_id='".$code."'";
    $arr_coupon = $db->fetchSQL($s);

    if ($arr_coupon[0]['uses'] && $arr_coupon[0]['uses'] == 0) {
        throw new \Exception("Este vale ya se ha agotado", 1);
    }

    return true;
}

/**
 * Verifica si el producto posee colores o tallas y valida las mimas
 * @param  integer $product_id  ID del producto
 * @return void
 */
function validateProductsAttr($product_id)
{
    $db = new Connection();
    $cnt_size = $db->getCount("products_sizes", "product_id='".$product_id."'");
    $cnt_color = $db->getCount("products_colors", "product_id='".$product_id."'");

    // Se verifica las talla del producto
    if ($cnt_size > 0) {
        $size_id = @number_format($_POST['size'],0,"","");
        $db->existRecord("id='".$size_id."'", "size", "Debes seleccionar una talla");
    }

    // Se verifica el color
    if ($cnt_color > 0) {
        $color_id = @number_format($_POST['color'],0,"","");
        $db->existRecord("id='".$color_id."'", "colors", "Debes seleccionar un color");
    }
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
