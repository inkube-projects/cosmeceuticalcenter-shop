<?php

include('core/Model/GeneralMethods.php');
include('core/bin/Services/OpinionService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $opinionService = new OpinionService($db);

    switch ($act) {
        case 1: // Guarda la opinion
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");

            try {
                isRequiredValuesPost($_POST, array('score', 'opinion'));
                isValidString($_POST['opinion'], "No se permiten caracteres especiales (#$^*\|)", $not_allowed = "#$^*\|");
                isValidNumber($_POST['score']);
                isValidOption(1, 5, $_POST['score'], "Debes agregar un score válido");
                $db->existRecord("id='".$id."'", "features", "La opinion no es válida");

                $result = $opinionService->persist($id);

                $arr_response = array('status' => 'OK','message' => "Se ha guardado correctamente");
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
