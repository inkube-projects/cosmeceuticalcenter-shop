<?php

include('core/bin/Services/LoginService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_POST) {
    $loginService = new LoginService($db);
    switch ($act) {
        case 1: // Iniciar sesión
            try {
                isRequiredValuesPost($_POST, array('email', 'pass'));
                $user = isValidCredentials($_POST['email']);
                createSession($user);

                $arr_response = array('status' => 'OK', 'message' => 'Inicio de sesión correcto');
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Olvide mi contraseña
            try {
                isRequiredValuesPost($_POST, array('forgot_email'));
                isValidEmail($_POST['forgot_email']);
                $db->existRecord("email='".$_POST['forgot_email']."'", "user", "El E-mail no es esta registrado");

                $loginService->forgotPass();

                $arr_response = array('status' => 'OK', 'message' => 'Se ha procesado correctamente');
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }

        break;
    }

}

//------------------------------------------------------------------------------

/**
 * Verifica las credenciales
 * @param  string  $username Nombre de usuario o email
 * @return array
 */
function isValidCredentials($username)
{
    $db = new Connection();
    isValidString($username);

    if (filter_var($username, FILTER_VALIDATE_EMAIL) === false) { // se verifica si es un email o un nombre de usuario
        $cnt_val = $db->getCount("view_user", "username='".$username."' AND role_id ='3'");
        $username = mb_convert_case($username, MB_CASE_UPPER, "UTF-8");

        if ($cnt_val == 1) {
            $user = $db->fetchSQL("SELECT * FROM view_user WHERE username='".$username."' AND role_id ='3'");
            if (!password_verify($_POST['password'], $user[0]['password'])) {

                throw new Exception("Usuario o contraseña incorrecto(s)", 1);
            }
        } else {
            throw new Exception("Usuario o contraseña incorrecto(s)", 1);
        }
    } else {
        $cnt_val = $db->getCount("view_user", "email='".$username."' AND role_id ='3'");
        $username = mb_convert_case($username, MB_CASE_LOWER, "UTF-8");

        if ($cnt_val == 1) {
            $user = $db->fetchSQL("SELECT * FROM view_user WHERE email='".$username."' AND role_id ='3'");
            if (!password_verify($_POST['pass'], $user[0]['password'])) {
                throw new Exception("Usuario o contraseña incorrecto(s)", 1);
            }
        } else {
            throw new Exception("Usuario o contraseña incorrecto(s)", 1);
        }
    }

    return $user;
}

/**
 * Crea la sesión de usuario
 * @param  array $user Arreglo del usuario que se le creará la sesión
 * @return boolean
 */
function createSession($user)
{
    $db = new Connection();
    $ip_acces = @$_SERVER['REMOTE_ADDR'];
    $ip_proxy = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $ip_compa = @$_SERVER['HTTP_CLIENT_IP'];
    $arr_values = array(
        'user_id' => $user[0]['id'],
        'username' => $user[0]['username'],
        'action' => 'Inicio de sesión',
        'ip_access' => $ip_acces,
        'ip_proxy' => $ip_proxy,
        'ip_company' => $ip_compa,
        'create_at' => date('Y-m-d H:i:s')
    );

    $db->insertAction("user_logs", $arr_values);
    $db->updateAction("user", array('last_login' => date('Y-m-d H:i:s')), "id='".$user[0]['id']."'");
    $_SESSION['USER_SESSION_COSME'] = $user[0];
    // ini_set('session.cookie_lifetime', time() + (60*60*2));
    setcookie("session.cookie_lifetime", "session expired", time()+3600);
    addCart($user[0]['id'], $db);
    return true;
}

function addCart($user_id, $db)
{
    if (isset($_COOKIE['cookie_cart_cosmeceutical'])) {

        foreach (json_decode($_COOKIE['cookie_cart_cosmeceutical']) as $val) {
            $quantity = (!isset($val->quantity)) ? 1 : $val->quantity;
            $size_id = (isset($val->size_id)) ? $val->size_id : NULL;
            $color_id = (isset($val->color_id)) ? $val->color_id : NULL;

            $cnt_cart = $db->getCount('cart_detail', "user_id='".$user_id."' AND product_id='".$val->product_id."' AND size_id='".$size_id."' AND color_id='".$color_id."'");
            $cnt_product = $db->getCount("products", "id='".$val->product_id."'");

            if ($cnt_product > 0) {
                if ($cnt_cart == 1) {
                    $s = "SELECT * FROM cart_detail WHERE user_id='".$user_id."' AND product_id='".$val->product_id."' AND size_id='".$size_id."' AND color_id='".$color_id."'";
                    $arr_cart = $db->fetchSQL($s);

                    $arr_values = array(
                        // 'quantity' => ($arr_cart[0]['quantity'] + $quantity),
                        'quantity' => $quantity,
                        'update_at' => date('Y-m-d H:i:s')
                    );

                    $db->updateAction('cart_detail', $arr_values, "user_id='".$user_id."' AND product_id='".$val->product_id."'");
                } else {

                    $arr_values = array(
                        'user_id' => $user_id,
                        'product_id' => $val->product_id,
                        'quantity' => $quantity,
                        'size_id' => $size_id,
                        'color_id' => $color_id,
                        'update_at' => date('Y-m-d H:i:s')
                    );

                    $db->insertAction('cart_detail', $arr_values);
                }
            }
        }

        // unset($_COOKIE['cookie_cart_cosmeceutical']);
        // setcookie('cookie_cart_cosmeceutical', '', time() - 3600, "/");
    }
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
