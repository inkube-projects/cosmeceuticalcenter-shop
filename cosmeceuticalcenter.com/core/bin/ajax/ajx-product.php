<?php

include('core/Model/GeneralMethods.php');
include('core/bin/Services/ProductService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $productService = new ProductService($db);

    switch ($act) {
        case 1: // Vista previa del producto
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");

            try {
                $db->existRecord("id='".$id."'", "products", "El producto no es válido");

                $result = $productService->getProduct($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha agregado correctamente", 'data' => $result);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Guarda el email para notificaciones
            $db->beginTransaction();
            $product_id = @number_format($_POST['product'],0,"","");

            try {
                $db->existRecord("id='".$product_id."'", "products", "El producto no es válido");

                if (isset($_POST['email'])) {
                    isValidEmail($_POST['email']);
                    $email = $_POST['email'];
                } else {
                    $email = $admin_pp_email;
                }

                $cnt_val = $db->getCount("notify_products", "product_id='".$product_id."' AND email='".$email."'");
                if ($cnt_val == 0) {
                    $result = $productService->saveEmail($product_id, $email);
                }

                $arr_response = array('status' => 'OK', 'message' => "Se ha procesado correctamente");
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3:
            $product_id = @number_format($_POST['product_id'],0,"","");
            $color_id = @number_format($_POST['color_id'],0,"","");

            try {
                $db->existRecord("id='".$product_id."'", "products", "El producto no es válido");
                $db->existRecord("id='".$color_id."'", "colors", "El color no es válido");

                $data = $productService->getImageColors($color_id, $product_id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha procesado correctamente", "data" => $data);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
