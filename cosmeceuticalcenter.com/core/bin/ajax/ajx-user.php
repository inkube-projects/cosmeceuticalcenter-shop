<?php

include('core/Model/GeneralMethods.php');
include('core/bin/Services/UsersService.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$act = @number_format($_GET['act'],0,"","");

if ($_GET) {
    $usersHelper = new UsersHelper($db);

    switch ($act) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();
            $date = $_POST['year']."-".$_POST['month']."-".$_POST['day'];
            $arr_required = array(
                'identification',
                'name',
                'last_name_1',
                'last_name_2',
                'phone',
                'email',
                'pass',
                'repeat_pass',
                'day',
                'month',
                'year',
                'country',
                'location_1',
                'location_2',
                'city',
                'postal_code',
                'location_name',
                'shipping_country',
                'shipping_location_1',
                'shipping_location_2',
                'shipping_city',
                'shipping_postal_code'
            );

            try {
                isRequiredValuesPost($_POST, $arr_required);
                // Cuenta de usuario
                isValidOption(1, 2, $_POST['type_account'], "El tipo de cuenta no es válido");
                isValidString($_POST['identification'], "Error Campo NIF/CIF/NIE: No se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['name'], "Error Campo Nombre: No se permiten caracteres especiales");
                isValidString($_POST['last_name_1'], "Error Campo Apellido 1: No se permiten caracteres especiales");
                isValidString($_POST['last_name_2'], "Error Campo Apellido 2: No se permiten caracteres especiales");
                isValidString($_POST['company'], "Error Campo Empresa: No se permiten caracteres especiales");
                isValidPhoneNumber($_POST['phone']);
                isValidEmail($_POST['email']);
                isValidDate($date, "en");
                isValidString($_POST['location_1'], "Error Campo Dirección: No se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['location_2'], "Error Campo Dirección: No se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['city'], "Error Campo Ciudad: No se permiten caracteres especiales");
                isValidString($_POST['postal_code'], "Error Campo Ciudad: No se permiten caracteres especiales");

                $db->existRecord("id='".@number_format($_POST['country'])."'", "country", "El país no es válido");
                if (isset($_POST['province']) && $_POST['country'] == 73) { // 73 es el ID de españa
                    $db->existRecord("id='".@number_format($_POST['province'])."'", "province", "La provincia no es válida");
                    $province = $_POST['province'];
                } else {
                    $province = "";
                }
                
                $db->duplicatedRecord("email='".$_POST['email']."'", "user", "El email ya se encuentra en uso");

                // Direccion de envío
                isValidString($_POST['location_name'], "Error Campo Nombre dirección: No se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['shipping_location_1'], "Error Campo Dirección (Envío): No se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['shipping_location_2'], "Error Campo Dirección (Envío): No se permiten caracteres especiales", "#$%^*\|");
                isValidString($_POST['shipping_city'], "Error Campo Ciudad: No se permiten caracteres especiales");
                isValidString($_POST['shipping_postal_code'], "Error Campo Ciudad: No se permiten caracteres especiales");
                isValidString($_POST['order_note'], "Error Campo Ciudad: No se permiten caracteres especiales", "^*\|");

                $db->existRecord("id='".@number_format($_POST['shipping_country'])."'", "country", "El país no es válido");
                if (isset($_POST['province']) && $_POST['shipping_country'] == 73) { // 73 es el ID de españa
                    $db->existRecord("id='".@number_format($_POST['shipping_province'])."'", "province", "La provincia no es válida");
                    $shipping_province = $_POST['shipping_province'];
                } else {
                    $shipping_province = "";
                }

                $result = $usersHelper->persist($date, $province, $shipping_province);

                $arr_response = array('status' => 'OK', 'message' => "Se ha registrado correctamente el usuario");
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se guarda una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['u'],0,"","");
            $arr_required = array('name', 'last_name', 'country', 'city', 'province', 'email', 'username', 'status', 'role', 'location_1', 'postal_code');

            try {
                $db->existRecord("id='".$id."'", "view_user", "El usuario no existe");

                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name'], "Error Campo Nombre: No se permiten caracteres especiales");
                isValidString($_POST['last_name'], "Error Campo Apellido: No se permiten caracteres especiales");
                isValidString($_POST['city'], "Error Campo Ciudad: No se permiten caracteres especiales");
                isValidEmail($_POST['email']);
                isValidPhoneNumber($_POST['phone_1']);
                isValidPhoneNumber($_POST['phone_2']);
                isValidString($_POST['username']);
                isValidString($_POST['company'], "Error Campo Empresa: No se permiten caracteres especiales");
                isValidString($_POST['location_1'], "Error Campo Dirección: No se permiten caracteres especiales");
                isValidString($_POST['location_2'], "Error Campo Dirección 2: No se permiten caracteres especiales");
                isValidString($_POST['postal_code'], "Error Campo Código postal: No se permiten caracteres especiales");
                isValidString($_POST['note'], "Error Campo Comentarios: No se permiten caracteres especiales");
                isValidString($_POST['dni'], "Error Campo DNI: No se permiten caracteres especiales", "#$%^*\|/");
                isValidDate($_POST['birthdate']);

                $db->duplicatedRecord("username='".$_POST['username']."' AND NOT id='".$id."'", "user", "El nombre de usuario ya se encuentra en uso");
                $db->duplicatedRecord("email='".$_POST['email']."' AND NOT id='".$id."'", "user", "El E-mail ya se encuentra en uso");
                $db->existRecord("id='".@number_format($_POST['status'],0,"","")."'", "user_status", "El estado de usuario no es válido");
                $db->existRecord("id='".@number_format($_POST['role'],0,"","")."'", "role", "El rol no es válido");
                $db->existRecord("id='".@number_format($_POST['country'],0,"","")."'", "country", "El país no es válido");
                $db->existRecord("id='".@number_format($_POST['province'],0,"","")."'", "province", "La provincia no es válida");

                $result = $usersHelper->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente el usuario", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3: // Se guarda la imagen de portada
            try {
                isValidImage(1, 'cover_image', 200, 200);
                $result = $usersHelper->prepareCoverImage(1);

                $arr_response = array('status' => 'OK', 'data' => $result);
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 4: // se recorta la imagen
            try {
                isRequiredValuesPost($_POST, array('x','y','w','h'));
                $result = $usersHelper->prepareCoverImage(2);

                $arr_response = array('status' => 'OK', 'data' => $result);
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 5: //  Se verifica el email
            $email = $_POST['r_email'];

            try {
                isValidEmail($_POST['r_email']);
                $db->duplicatedRecord("email='".$email."'", "user", "El E-email ya se encuentra en uso");
                $_SESSION['CART_REGISTER_EMAIL'] = $email;
                $_SESSION['CART_REGISTER_PASS'] = $_POST['r_pass'];
                $arr_response = array('status' => 'OK');
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
