<?php
/**
 * Encriptado basico de contraseña
 * @param  String $input Contraseña a encriptar
 * @return String
 */
function encriptar($input){
	$basura = "cla_tu";
	$basura_2 = "ve_ner";
	$clave = sha1($basura.$input.$basura_2);
	return $clave;
}

/**
 * Encriptado con bcrypt
 * @param  String $value Entrada a encriptar
 * @param  Integer $salt  Salt de la contraseña (1 al 12)
 * @return Sttring
 */
function hashPass($value, $salt)
{
   $opt = array('cost' => $salt);
   $encoded = password_hash($value, PASSWORD_BCRYPT, $opt);
   return $encoded;
}

/**
 * Retorna el atributo html selected si los valores son iguales
 * @param  String  $cmp1 Valor 1
 * @param  String  $cmp2 Valor 2
 * @return String
 */
function isSelected($cmp1,$cmp2)
{
   if ($cmp1==$cmp2){
      return ' selected="selected"';
   } else {
      return '';
   }
}

function isRequiredParameters($request, array $arr_required)
{
	for ($i = 0; $i < sizeof($arr_required); $i++) {
 		if (isset($request[$arr_required[$i]])) {
 			if (trim($request[$arr_required[$i]]) == "") {
 				throw new exception("The request is not valid", 400);
 			}
 		} else {
 			throw new exception("The request is not valid", 400);
 		}
    }
}

/**
 * Retorna el atributo html checked si los valores son iguales
 * @param  String  $cmp1 Valor 1
 * @param  String  $cmp2 Valor 2
 * @return String
 */
function isOption($cmp1,$cmp2)
{
   if ($cmp1==$cmp2) {
      return ' checked="checked"';
   } else {
      return '';
   }
}

/**
 * Retorna el atributo html checked si el valor se encuentra dentro del arreglo
 * @param  Array  $arreglo      Arreglo a verificar
 * @param  String $valor_actual Valor a Buecar en el arreglo
 * @return String
 */
function isChecked($arreglo,$valor_actual)
{
   if (in_array($valor_actual, $arreglo)) {
      return ' checked="checked"';
   } else {
      return '';
   }
}

/**
 * Valida contra caracteres especiales
 * @param  String  $value Valor a verificar
 * @return String
 */
function isValidString($value, $message = "No se permiten caracteres especiales", $not_allowed = "#$%^*\|/-")
{
   for ($i  = 0; $i<strlen($value); $i++){
      if (!strpos($not_allowed, substr($value,$i,1))===false){
         throw new exception("No se permiten caracteres especiales", 400);
      }
   }
   return $value;
}

/**
 * Se valida que sea un numero entero válido
 * @param  Integer  $value Valor a validar
 * @return Integer
 */
function isValidNumber($value)
{
   $allowed = "1234567890";
   for ($i = 0; $i < strlen($value); $i++){
      if (strpos($allowed, substr($value,$i,1)) === false){
         throw new exception("Solo se permiten números", 400);
      }
   }
   return $value;
}

/**
 * Se valida si es número telefónico válido
 * @param  String  $value Valor a validar
 * @return boolean
 */
function isValidPhoneNumber($value)
{
	if (!empty($value)) {
		$allowed = "1234567890 ()-+";
		for ($i = 0; $i < strlen($value); $i++){
			if (strpos($allowed, substr($value,$i,1)) === false){
				throw new exception("Número telefónico inválido", 400);
			}
		}
	}

	return $value;
}

/**
 * Se valida si es un decimal válido
 * @param  Decimal  $value Valor a validar
 * @return Decimal
 */
function isValidDecimal($value, $message = "The decimal is invalid (format 0.00)")
{
	if (!empty($value)) {
		$regex = '/^\s*[+\-]?(?:\d+(?:\.\d*)?|\.\d+)\s*$/';
		if (!preg_match($regex, $value)) {
			throw new exception($message, 400);
		}
	}

	return $value;
}

/**
 * Se valida que sea una URL válida
 * @param  String  $url Valor a verificar
 * @return String
 */
function isValidURL($url)
{
   if (!empty($url)) {
      if (filter_var($url, FILTER_VALIDATE_URL) === false) {
         throw new exception("Debes ingresar un URL válido", 400);
      }
   }
   return $url;
}

/**
 * Se verifica si es un Email válido
 * @param  String  $email Valor a validar
 * @return String
 */
function isValidEmail($email)
{
   if (!empty($email)) {
		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
         throw new exception("Debes ingresar un email válido", 400);
      }
   }
   return $email;
}

/**
 * Se verifica que sea una fecha válida
 * @param  String  $date   Valor a verificar
 * @param  String  $format Formato de la fecha
 * @param  String  $sep    Separador
 * @return String
 */
function isValidDate($date, $format = "es", $sep = "-")
{
	if (!empty($date)) {
		$date = explode($sep, $date);
		if ($format == "es") {
			$day = $date[0]; $month = $date[1]; $year = $date[2];
		} elseif ($format == "en") {
			$day = $date[2]; $month = $date[1]; $year = $date[0];
		}

		if (!checkdate($month, $day, $year)) {
			throw new exception("Debes ingresar una fecha válida", 400);
		}
	}

   return $date;
}

/**
 * Valida si una una opción entre un rango de dos valores
 * @param  integer  $range_init Rango mínimo
 * @param  integer  $range_end  Rango máximo
 * @param  integer  $value      Valor que se esta ingresando
 * @param  string  $message    Mensaje de excepción
 * @return boolean
 */
function isValidOption($range_init, $range_end, $value, $message)
{
	$value = @number_format($value,0,"","");
	if ($value < $range_init && $value > $range_end) {
		throw new exception($message, 400);
	}

	return true;
}

/**
 * Valida los parametros obligatorios en POST
 * @param  object  $post         Objeto Post
 * @param  array  $arr_required  Arreglo con los parametros a verificar
 * @return boolean
 */
function isRequiredValuesPost($post, $arr_required)
{
   for ($i = 0; $i < sizeof($arr_required); $i++) {
		if (isset($post[$arr_required[$i]])) {
			if (trim($post[$arr_required[$i]]) == "") {
				throw new exception("El campo ".$arr_required[$i]." no puede estar vacío", 400);
			}
		} else {
			throw new exception("El campo ".$arr_required[$i]." no puede estar vacío", 400);
		}
   }

	return true;
}

/**
 * Muentra un arreglo o obejto
 * @param  object $arr Objeto a mostrar
 * @return object
 */
function showArr($arr)
{
	echo "<pre>";
	var_dump($arr);
	echo "</pre>";
}

/**
 * Limpia las entradas de etiquetas html
 * @param string $input Entrada a limpiar
 * @return [type]        [description]
 */
function sanitize($input)
{
   if (is_array($input)) {
      foreach($input as $var=>$val) {
         $output[$var] = cleanInput($val);
      }
   } else {
      if (get_magic_quotes_gpc()) {
         $input = stripslashes($input);
      }
      $input  = cleanInput($input);
		$output = $input;//$output = mysql_real_escape_string($input);
   }return addslashes($output);
}
//----------------------------------------------------------------------------------------------------------------
function cleanInput($input) {
  $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
	//'CREATE', 'DELETE','INSERT', 'UPDATE', 'DROP'
  );
    $output = preg_replace($search, '', $input);
    return $output;
}

/**
 * Invierte el formato de las fechas
 * @param  string $date_en fecha
 * @return string
 */
function to_date($date_en)
{
   $f = explode("-",$date_en);
   $f = array_reverse($f);
   return implode("-",$f);
}

/**
 * Cambia el formato de fechas
 * @param  string $datetime Fecha en formato datetime
 * @return array
 */
function datetime_format($datetime)
{
	if ($datetime != "") {
		$datetime = explode(" ", $datetime);
		$arr_result = array('date' => to_date($datetime[0]), 'time' => $datetime[1]);
	} else {
		$arr_result = array('date' => '', 'time' => '');
	}

	return $arr_result;
}

/**
 * Calcula los dias entre dos fechas
 * @param  string $fecha_i fecha inicial
 * @param  string $fecha_f Fecha final
 * @return string
 */
function dias_transcurridos($fecha_i,$fecha_f)
{
	$dias = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
	$dias = abs($dias); $dias = floor($dias);
	return $dias;
}

/**
 * Genera un token o contraseña
 * @param  string $pre Variable basura
 * @param  integer $num Tamaño del token
 * @return string
 */
function genPass($pre, $num) {
	$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
	$cad = "";
	for($i=0;$i<$num;$i++) {
	$cad.= substr($str,rand(0,62),1);
	}
	return $pre.$cad;
}

/**
 * Retorna la fecha con el nombre del mes
 * @param  string $date La fecha que se le cambiara el formato
 * @return string
 */
function dateName($date){
	$arrMes = array('01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Septiembre', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre');
	$mes = explode("-",$date); $mesPos = $mes[1]; $mesNB = $arrMes[$mesPos]; $year = $mes[2];
	return $mes[0]." ".$mesNB.". ".$year;
}

/**
 * Elimina un directorio
 * @param  string $carpeta ruta a elimnar
 * @return boolean
 */
function removeDir($folder)
{
	foreach(glob($folder . "/*") as $file){
		if (is_dir($file)){
			removeDir($file);
		} else {
			unlink($file);
		}
	}
	rmdir($folder);

	return true;
}

/**
 * Crea un token unico
 * @return string
 */
function UniqueToken() {
    $s = strtoupper(md5(uniqid(rand(),true)));
    $guidText =
        substr($s,0,8).
        substr($s,8,4).
        substr($s,12,4).
        substr($s,16,4).
        substr($s,20);
    return $guidText;
}

/**
 * Limpia un directorio
 * @param  string $path Directorio a limpiar
 * @return void
 */
function clearDirectory($path)
{
	$files = scandir($path);

	foreach ($files as $val) {
		@unlink($path.$val);
	}
}

/**
 * Obtiene el dominio y lo retorna con el formato deseado
 * @param  string $url RL a modificar
 * @return string
 */
function saca_dominio($url){
   $protocolos = array('www.');
   $url = explode('/', str_replace($protocolos, '', $url));
   return "http://".$url[0]."/cristobal";

}

/**
 * Verifica si el archivo existe
 * @param  string $filename Nombre y ruta del archivo
 * @return boolean
 */
function fileExist($filename)
{
	if ((file_exists($filename)) && (basename($filename) != "")){
		return true;
	} else {
		return false;
	}
}

function directoryExist($path)
{
	if (file_exists($path)) {
    	$r = true;
	} else {
    	$r = false;
	}

	return $r;
}

/**
 * Valida si el archivo es PDF
 * @param  string  $input Nombre del input
 * @return boolean
 */
function isValidPDF($input)
{
    if (!empty($_FILES[$input]['tmp_name'])) {
       $file_type = $_FILES[$input]['type'];

       if (strpos($file_type, "pdf")){
          return true;
       } else {
          throw new Exception("Debes ingresar un archivo PDF", 1);
       }
    }
}

/**
 * Verifica si una imagen es válida
 * @param  string  $param Nombre del parametro que contiene la imagen
 * @param  integer  $min_w Ancho mínima
 * @param  integer  $min_h Altura mínima
 * @return boolean|exception
 */
function isValidImage($param, $min_w, $min_h)
{
    if (!empty($_FILES[$param]['tmp_name'])) {
        $img_type = $_FILES[$param]['type'];

        if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
            list($width, $height) = getimagesize($_FILES[$param]['tmp_name']);

            if (($width < $min_w) || ($height < $min_h)) {
                throw new Exception(sprintf("La imagen debe tener por lo menos %d x %d px", $min_w, $min_h), 1);
            }
        } else {
            throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
        }
    } else {
        // throw new Exception("La imagen de portada no puede estar vacia", 1);
    }

    return true;
}

/**
 * Convierte un string en URL amigable
 * @param  string $toClean String a convertir
 * @return string
 */
function friendlyURL($toClean, $case = "lower")
{
	$chars = array(
        '?' => 'S', '?' => 's', 'Ð' => 'Dj','?' => 'Z', '?' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A',
        'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I',
        'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U',
        'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss','à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a',
        'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i',
        'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u',
        'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'ƒ' => 'f', ',' => '',  '.' => '',  ':' => '',
        ';' => '',  '_' => '',  '<' => '',  '>' => '',  '\\'=> '',  'ª' => '',  'º' => '',  '!' => '',  '|' => '',  '"' => '',
        '@' => '',  '·' => '',  '#' => '',  '$' => '',  '~' => '',  '%' => '',  '€' => '',  '&' => '',  '¬' => '',  '/' => '',
        '(' => '',  ')' => '',  '=' => '',  '?' => '',  '\''=> '',  '¿' => '',  '¡' => '',  '`' => '',  '+' => '',  '´' => '',
        'ç' => '',  '^' => '',  '*' => '',  '¨' => '',  'Ç' => '',  '[' => '',  ']' => '',  '{' => '',  '}' => '',  '? '=> '-',
    );
	$toClean = str_replace('&', '-and-', $toClean);
	$toClean = str_replace('.', '', $toClean);

	if ($case == "lower") {
		$toClean = strtolower(strtr($toClean, $chars));
	} else {
		$toClean = strtoupper(strtr($toClean, $chars));
	}

	$toClean = str_replace(' ', '-', $toClean);
	$toClean = str_replace('--', '-', $toClean);
	$toClean = str_replace('--', '-', $toClean);
	$toClean = preg_replace('/[^\w\d_ -]/si', '', $toClean);
	return trim($toClean);
}


function ImagenTemp($input, $newWidth, $adcName, $ruta) // sube una imagen con una redimension
{
$imgOK = false;
$img_type = $_FILES[$input]['type'];
if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
	$extens = str_replace("image/",".",$_FILES[$input]['type']);
	$imagen = $adcName;
	if(move_uploaded_file($_FILES[$input]['tmp_name'], $ruta.$imagen)){
		if(($extens==".jpeg")||($extens==".jpg")){
			$frame = imagecreatefromjpeg($ruta.$imagen);
		}
		if($extens==".png"){
			$frame = imagecreatefrompng($ruta.$imagen);
		}
		if($extens==".gif"){
			$frame = imagecreatefromgif($ruta.$imagen);
		}
		$width = imagesx($frame);
		$height = imagesy($frame);
		if($newWidth==''){ $new_width = $width;}else{ $new_width = $newWidth;}
		$new_height = floor($height * ($newWidth / $width));
		$img=imagecreatetruecolor($new_width, $new_height);
		imagealphablending($img, true);
		$transparent = imagecolorallocatealpha($img, 255, 255, 255, 1);
		imagefill($img, 0, 0, $transparent);
		imagecopyresampled($img,$frame,0,0,0,0,$new_width, $new_height,$width,$height);
		imagealphablending($img, false);
		imagesavealpha($img,true);
		imagepng($img,$ruta.$imagen);
		if(($extens==".jpeg")||($extens==".jpg")){
			if(imagejpeg($img,$ruta.$imagen,90)){ $imgOK = true;}
		}
		if($extens==".png"){
			if(imagepng($img,$ruta.$imagen)){ $imgOK = true;}
		}
		if($extens==".gif"){
			if(imagegif($img,$ruta.$imagen)){ $imgOK = true;}
		}
		imagedestroy($img);
	}
}
return $imgOK;
}

/**
 * Sube imagenes de un arrehlo
 * @param string $input     Nombre del input
 * @param integer $thumbWid Ancho de la miniatura
 * @param string $adcName   Nombre
 * @param string $ruta      Ruta de las imagenes
 * @param integer $i        Posición del arreglo
 */
function UpPictureArray($input, $thumbWid, $adcName, $ruta, $i)
{
	$imgOK = false;
	$img_type = $_FILES[$input]['type'][$i];
	if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
		$extens = str_replace("image/",".",$_FILES[$input]['type'][$i]);
		$imagen = $adcName;
		if(move_uploaded_file($_FILES[$input]['tmp_name'][$i], $ruta.$imagen)){
			if(($extens==".jpeg")||($extens==".jpg")){
				$frame = imagecreatefromjpeg($ruta.$imagen);
			}if($extens==".png"){
				$frame = imagecreatefrompng($ruta.$imagen);
			} if($extens==".gif"){
				$frame = imagecreatefromgif($ruta.$imagen);
			}
			$width = imagesx($frame);
			$height = imagesy($frame);
			$new_width = $thumbWid;
			$new_height = floor($height * ($thumbWid / $width));
			$img=imagecreatetruecolor($new_width, $new_height);
			imagealphablending($img, true);
			$transparent = imagecolorallocatealpha( $img, 0, 0, 0, 127 );
			imagefill( $img, 0, 0, $transparent );
			imagecopyresampled($img,$frame,0,0,0,0,$new_width, $new_height,$width,$height);
			imagealphablending($img, false);
			imagesavealpha($img,true);
			imagepng($img,$ruta."min_".$imagen);
			if(($extens==".jpeg")||($extens==".jpg")){
				if(imagejpeg($img,$ruta."min_".$imagen,90)){ $imgOK = true;}
			} if($extens==".png"){
				if(imagepng($img,$ruta."min_".$imagen)){ $imgOK = true;}
			} if($extens==".gif"){
				if(imagegif($img,$ruta."min_".$imagen)){ $imgOK = true;}
			}
			imagedestroy($img);
		}
	}
	return $imgOK;
}

/**
 * Limpia la entrada de caracteres de riesgo para SQL
 * @param  string $string Caracteres a limpiar
 * @return string
 */
function secure_mysql($string)
{
	$arr_not_allowed = array('/*', '*/', '--', '//');
	return str_replace($arr_not_allowed, " ", $string);
}
?>
