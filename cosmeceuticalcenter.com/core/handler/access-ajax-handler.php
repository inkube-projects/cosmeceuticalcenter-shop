<?php
/**
 * Handler que controla el acceso a los archivos ajax
*/
$db = new Connection();
$current_ajax = $_GET['m'];

$role = "ROLE_ANONYMOUS";

$arr_ajax = array(
   'login' => array('ROLE_ANONYMOUS'),
   'user' => array('ROLE_ANONYMOUS'),
   'account' => array('ROLE_USER'),
   'cart' => array('ROLE_ANONYMOUS', 'ROLE_USER'),
   'favorites' => array('ROLE_USER'),
   'notifications' => array('ROLE_USER'),
   'opinion' => array('ROLE_USER'),

);

/**
 * Se verifica si existe una sesión, sino se le asigna un Rol anónimo
*/
if (isset($_SESSION['USER_SESSION_COSME']['id'])) {
   $role = $_SESSION['USER_SESSION_COSME']['role'];
   $admin_pp_id = $_SESSION['USER_SESSION_COSME']['id'];
   $admin_pp_code = $_SESSION['USER_SESSION_COSME']['code'];
   $admin_pp_email = $_SESSION['USER_SESSION_COSME']['email'];
}

if (isset($_COOKIE['cookie_cart_cosmeceutical'])) {
    $arr_cart_cookie = json_decode($_COOKIE['cookie_cart_cosmeceutical'], true);
    $arr_products_cookies = array();

    foreach ($arr_cart_cookie as $key => $val) {
        if (!isset($val['quantity'])) {
            $val['quantity'] = 1;
        }

        if (!isset($val['product_id'])) {
            unset($arr_cart_cookie[$key]);
        }
    }

    setcookie('cookie_cart_cosmeceutical','',time() - 3600, "/");
    setcookie('cookie_cart_cosmeceutical', json_encode($arr_cart_cookie), time() + (86400 * 30), "/");
    $_SESSION['COSME_CART'] = json_encode($arr_cart_cookie);
}

try {
    foreach ($arr_ajax as $key => $val) {
       if ($key == $current_ajax) {
          if (!in_array($role, $val)) {
            throw new \Exception("No tienes acceso a esta acción", 1);
          }
       }
    }
} catch (Exception $e) {
    $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    header('Content-Type: application/json');
    echo json_encode($arr_response);
    exit;
}
?>
