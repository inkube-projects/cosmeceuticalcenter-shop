<?php
/**
 * Clase de conexion
 * @var Connection
 */
$db = new Connection();
$user_view = (isset($_GET['view']) && (!empty($_GET['view']))) ? $_GET['view'] : "index" ;
$user_meth = (isset($_GET['meth']) && (!empty($_GET['meth']))) ? $_GET['meth'] : "home" ;
$redirect = (isset($_GET['r']) && (!empty($_GET['r']))) ? $_GET['r'] : "" ;
$role = "ROLE_ANONYMOUS";

$arr_pages = array(
    'Index' => array('ROLE_ANONYMOUS', 'ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER'),
    'Error' => array('ROLE_ANONYMOUS', 'ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER'),
    'Products' => array('ROLE_ANONYMOUS', 'ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER'),
    'Account' => array('ROLE_ANONYMOUS', 'ROLE_ADMIN', 'ROLE_MODERATOR'),
    'Dashboard' => array('ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER'),
    'Notifications' => array('ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER'),
    'Cart' => array('ROLE_ANONYMOUS', 'ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER'),
);

/**
 * Se verifica si existe una sesión, sino se le asigna un Rol anónimo
 */
if (isset($_SESSION['USER_SESSION_COSME'])) {
    if ($_SESSION['USER_SESSION_COSME'] != "") {
        $role = $_SESSION['USER_SESSION_COSME']['role'];
        $admin_pp_id = $_SESSION['USER_SESSION_COSME']['id'];
        $admin_username = $_SESSION['USER_SESSION_COSME']['username'];
    }
}

foreach ($arr_pages as $key => $val) {
    if ($key == $user_view) {
        if (!in_array($role, $val)) {
            header("location: ".BASE_URL."login/denied"); exit;
        }
    }
}

if (isset($_COOKIE['cookie_cart_cosmeceutical'])) {
    $arr_cart_cookie = json_decode($_COOKIE['cookie_cart_cosmeceutical'], true);
    $arr_products_cookies = array();

    foreach ($arr_cart_cookie as $key => $val) {
        if (!isset($val['quantity'])) {
            $arr_cart_cookie[$key]['quantity'] = 1;
        }

        if (!isset($val['product_id'])) {
            unset($arr_cart_cookie[$key]);
        }
    }

    setcookie('cookie_cart_cosmeceutical','',time() - 3600, "/");
    setcookie('cookie_cart_cosmeceutical', json_encode($arr_cart_cookie), time() + (86400 * 30), "/");
    $_SESSION['COSME_CART'] = json_encode($arr_cart_cookie);
}

?>
