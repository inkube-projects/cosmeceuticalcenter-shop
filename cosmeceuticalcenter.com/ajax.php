<?php

require('core/core.php');

if (isset($_GET['m'])) {
    require('core/handler/access-ajax-handler.php');

    if ($_GET['m'] == "user") {  // Usuarios
        require('core/bin/ajax/ajx-user.php');
    } elseif ($_GET['m'] == "login") { // Inicio de sesión
        require('core/bin/ajax/ajx-login.php');
    } elseif ($_GET['m'] == "account") { // Acciones de cuenta
        require('core/bin/ajax/ajx-account.php');
    } elseif ($_GET['m'] == "cart") { // Carrito
        require('core/bin/ajax/ajx-cart.php');
    } elseif ($_GET['m'] == "product") { // Productos
        require('core/bin/ajax/ajx-product.php');
    } elseif ($_GET['m'] == "favorites") { // Favoritos
        require('core/bin/ajax/ajx-favorites.php');
    } elseif ($_GET['m'] == "notifications") { // Notificaciones
        require('core/bin/ajax/ajx-notifications.php');
    } elseif ($_GET['m'] == "opinion") { // Opiniones
        require('core/bin/ajax/ajx-opinion.php');
    } elseif ($_GET['m'] == "newsletter") { // Newsletter
        require('core/bin/ajax/ajx-newsletter.php');
    } else {
        header('location: index.php');
    }
} else {
    include('location: index.php');
}

?>
