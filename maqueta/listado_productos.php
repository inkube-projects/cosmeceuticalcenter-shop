<?php
   require('includes/core.php');
?>
<!doctype html>
<html class="no-js" lang="es">
   <head>
      <meta charset="utf-8">
      <title> </title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
   </head><!--/head-->

<body>
	<?php include("includes/analytics.php"); ?>
	<?php include("includes/cookies.php"); ?>

   <div class="home_three_body_wrapper">
		
	<?php include("includes/header.php"); ?>
	<?php include("includes/breadcrumbs.php"); ?>
	<?php include("includes/marcasTop.php"); ?>
    
    <!--shop  area start-->
    <div class="shop_area shop_fullwidth shop_reverse">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!--shop wrapper start-->
                    <!--shop toolbar start-->
                    <div class="shop_toolbar">
                        <div class="list_button">
                            <ul class="nav" role="tablist">
                                <li>
                                    <a class="active" data-toggle="tab" href="shop-fullwidth.html#large" role="tab" aria-controls="large" aria-selected="true"><i class="ion-grid"></i></a>
                                </li>
                                <li>
                                    <a  data-toggle="tab" href="shop-fullwidth.html#list" role="tab" aria-controls="list" aria-selected="false"><i class="ion-ios-list-outline"></i> </a>
                                </li>
                            </ul>
                        </div>
                        <div class="orderby_wrapper">
                            <h3>Ordenar por : </h3>
                            <div class=" niceselect_option">

                                <form class="select_option" action="#">

                                    <select name="orderby" id="short">
                                        <option selected value="1">Más populares</option>
                                        <option  value="2">Precio ascendente</option>
                                        <option value="3">Precio descendente</option>
                                        <option value="4">Novedades</option>
                                        <option value="5">De la A a la Z</option>
                                        <option value="6">De la Z a la A</option>
                                    </select>
                                </form>


                            </div>
                            <div class="page_amount">
                                <p>Total 1–9 de 21 resultados</p>
                            </div>
                        </div>
                    </div>
                    <!--shop toolbar end-->

                    <!--shop tab product start-->
                     <div class="tab-content">
                        <div class="tab-pane grid_view fade show active" id="large" role="tabpanel">
                            <div class="row">
								
												<!--ITEM 01-->
												<div class="col-lg-3 col-md-4 col-sm-6">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product12.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product12B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																					<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
													  </div>
											   <!--ITEM 01.END -->
								
												<!--ITEM 01-->
												<div class="col-lg-3 col-md-4 col-sm-6">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product12.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product12B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																					<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
													  </div>
											   <!--ITEM 01.END -->
												
												<!--ITEM 01-->
												<div class="col-lg-3 col-md-4 col-sm-6">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product12.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product12B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																					<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
													  </div>
											   <!--ITEM 01.END -->
								
												<!--ITEM 01-->
												<div class="col-lg-3 col-md-4 col-sm-6">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product12.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product12B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																					<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
													  </div>
											   <!--ITEM 01.END -->
								
                             	  <!--ITEM 01-->
												<div class="col-lg-3 col-md-4 col-sm-6">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product12.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product12B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																					<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
													  </div>
											   <!--ITEM 01.END -->
								
												<!--ITEM 01-->
												<div class="col-lg-3 col-md-4 col-sm-6">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product12.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product12B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																					<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
													  </div>
											   <!--ITEM 01.END -->
												
												<!--ITEM 01-->
												<div class="col-lg-3 col-md-4 col-sm-6">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product12.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product12B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																					<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
													  </div>
											   <!--ITEM 01.END -->
								
												<!--ITEM 01-->
												<div class="col-lg-3 col-md-4 col-sm-6">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product12.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product12B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																					<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
													  </div>
											   <!--ITEM 01.END -->
								
                         
								
								
                            </div>
                        </div>
                        <div class="tab-pane list_view fade " id="list" role="tabpanel">
                           <div class="single_product product_list_item">
                               <div class="row">
                                   <div class="col-lg-4 col-md-5">
                                       <div class="product_thumb">
                                            <a class="primary_img" href="product-details.html"><img src="assets/img/product/product6.jpg" alt=""></a>
                                            <a class="secondary_img" href="product-details.html"><img src="assets/img/product/product5.jpg" alt=""></a>
                                            <div class="quick_button">
                                                <a href="shop-fullwidth.html#" data-toggle="modal" data-target="#modal_box"  data-original-title="quick view"> quick view</a>
                                            </div>
                                        </div>
                                   </div>
                                   <div class="col-lg-8 col-md-7">
                                        <div class="product_content">               
                                          <h3><a href="product-details.html">Aliquam furniture</a></h3>
                                          <div class="product_ratings">
                                                <ul>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product_desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et </p>
                                            </div>
                                           <div class="price_box">
                                                <span class="old_price">$70.00</span>
                                                <span class="current_price">$75.00</span>
                                            </div>

                                            <div class="action_links">
                                                <ul>
                                                    <li class="add_to_cart"><a href="shop-fullwidth.html#" title="add to cart">add to cart</a></li>
                                                    <li><a href="shop-fullwidth.html#" title="Wishlist"><span class="icon icon-Heart"></span></a></li>

                                                    <li><a href="shop-fullwidth.html#" title="compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                   </div>
                               </div>
                            </div>
                            <div class="single_product product_list_item">
                               <div class="row">
                                   <div class="col-lg-4 col-md-5">
                                       <div class="product_thumb">
                                            <a class="primary_img" href="product-details.html"><img src="assets/img/product/product1.jpg" alt=""></a>
                                            <a class="secondary_img" href="product-details.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                            <div class="quick_button">
                                                <a href="shop-fullwidth.html#" data-toggle="modal" data-target="#modal_box"  data-original-title="quick view"> quick view</a>
                                            </div>
                                        </div>
                                   </div>
                                   <div class="col-lg-8 col-md-7">
                                        <div class="product_content">               
                                          <h3><a href="product-details.html">Donec eu animal</a></h3>
                                          <div class="product_ratings">
                                                <ul>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product_desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et </p>
                                            </div>
                                           <div class="price_box">
                                                <span class="current_price">$65.00</span>
                                            </div>

                                            <div class="action_links">
                                                <ul>
                                                    <li class="add_to_cart"><a href="shop-fullwidth.html#" title="add to cart">add to cart</a></li>
                                                    <li><a href="shop-fullwidth.html#" title="Wishlist"><span class="icon icon-Heart"></span></a></li>

                                                    <li><a href="shop-fullwidth.html#" title="compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                   </div>
                               </div>
                            </div>
                            <div class="single_product product_list_item">
                               <div class="row">
                                   <div class="col-lg-4 col-md-5">
                                       <div class="product_thumb">
                                            <a class="primary_img" href="product-details.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                            <a class="secondary_img" href="product-details.html"><img src="assets/img/product/product4.jpg" alt=""></a>
                                            <div class="quick_button">
                                                <a href="shop-fullwidth.html#" data-toggle="modal" data-target="#modal_box"  data-original-title="quick view"> quick view</a>
                                            </div>
                                        </div>
                                   </div>
                                   <div class="col-lg-8 col-md-7">
                                        <div class="product_content">               
                                          <h3><a href="product-details.html">Donec eu furniture</a></h3>
                                          <div class="product_ratings">
                                                <ul>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product_desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et </p>
                                            </div>
                                           <div class="price_box">
                                                <span class="old_price">$70.00</span>
                                                <span class="current_price">$75.00</span>
                                            </div>

                                            <div class="action_links">
                                                <ul>
                                                    <li class="add_to_cart"><a href="shop-fullwidth.html#" title="add to cart">add to cart</a></li>
                                                    <li><a href="shop-fullwidth.html#" title="Wishlist"><span class="icon icon-Heart"></span></a></li>

                                                    <li><a href="shop-fullwidth.html#" title="compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                   </div>
                               </div>
                            </div>
                            <div class="single_product product_list_item">
                               <div class="row">
                                   <div class="col-lg-4 col-md-5">
                                       <div class="product_thumb">
                                            <a class="primary_img" href="product-details.html"><img src="assets/img/product/product7.jpg" alt=""></a>
                                            <a class="secondary_img" href="product-details.html"><img src="assets/img/product/product8.jpg" alt=""></a>
                                            <div class="quick_button">
                                                <a href="shop-fullwidth.html#" data-toggle="modal" data-target="#modal_box"  data-original-title="quick view"> quick view</a>
                                            </div>
                                        </div>
                                   </div>
                                   <div class="col-lg-8 col-md-7">
                                        <div class="product_content">               
                                          <h3><a href="product-details.html">Duis pulvinar</a></h3>
                                          <div class="product_ratings">
                                                <ul>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product_desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et </p>
                                            </div>
                                           <div class="price_box">
                                                <span class="old_price">$70.00</span>
                                                <span class="current_price">$60.00</span>
                                            </div>

                                            <div class="action_links">
                                                <ul>
                                                    <li class="add_to_cart"><a href="shop-fullwidth.html#" title="add to cart">add to cart</a></li>
                                                    <li><a href="shop-fullwidth.html#" title="Wishlist"><span class="icon icon-Heart"></span></a></li>

                                                    <li><a href="shop-fullwidth.html#" title="compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                   </div>
                               </div>
                            </div>
                            <div class="single_product product_list_item">
                               <div class="row">
                                   <div class="col-lg-4 col-md-5">
                                       <div class="product_thumb">
                                            <a class="primary_img" href="product-details.html"><img src="assets/img/product/product9.jpg" alt=""></a>
                                            <a class="secondary_img" href="product-details.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                            <div class="quick_button">
                                                <a href="shop-fullwidth.html#" data-toggle="modal" data-target="#modal_box"  data-original-title="quick view"> quick view</a>
                                            </div>
                                        </div>
                                   </div>
                                   <div class="col-lg-8 col-md-7">
                                        <div class="product_content">               
                                          <h3><a href="product-details.html">Dummy animal</a></h3>
                                          <div class="product_ratings">
                                                <ul>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product_desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et </p>
                                            </div>
                                           <div class="price_box">
                                                <span class="old_price">$70.00</span>
                                                <span class="current_price">$75.00</span>
                                            </div>

                                            <div class="action_links">
                                                <ul>
                                                    <li class="add_to_cart"><a href="shop-fullwidth.html#" title="add to cart">add to cart</a></li>
                                                    <li><a href="shop-fullwidth.html#" title="Wishlist"><span class="icon icon-Heart"></span></a></li>

                                                    <li><a href="shop-fullwidth.html#" title="compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                   </div>
                               </div>
                            </div>
                            <div class="single_product product_list_item">
                               <div class="row">
                                   <div class="col-lg-4 col-md-5">
                                       <div class="product_thumb">
                                            <a class="primary_img" href="product-details.html"><img src="assets/img/product/product10.jpg" alt=""></a>
                                            <a class="secondary_img" href="product-details.html"><img src="assets/img/product/product11.jpg" alt=""></a>
                                            <div class="quick_button">
                                                <a href="shop-fullwidth.html#" data-toggle="modal" data-target="#modal_box"  data-original-title="quick view"> quick view</a>
                                            </div>
                                        </div>
                                   </div>
                                   <div class="col-lg-8 col-md-7">
                                        <div class="product_content">               
                                          <h3><a href="product-details.html">Furniture</a></h3>
                                          <div class="product_ratings">
                                                <ul>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product_desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et </p>
                                            </div>
                                           <div class="price_box">
                                                <span class="old_price">$70.00</span>
                                                <span class="current_price">$75.00</span>
                                            </div>

                                            <div class="action_links">
                                                <ul>
                                                    <li class="add_to_cart"><a href="shop-fullwidth.html#" title="add to cart">add to cart</a></li>
                                                    <li><a href="shop-fullwidth.html#" title="Wishlist"><span class="icon icon-Heart"></span></a></li>

                                                    <li><a href="shop-fullwidth.html#" title="compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                   </div>
                               </div>
                            </div>
                            <div class="single_product product_list_item">
                               <div class="row">
                                   <div class="col-lg-4 col-md-5">
                                       <div class="product_thumb">
                                            <a class="primary_img" href="product-details.html"><img src="assets/img/product/product12.jpg" alt=""></a>
                                            <a class="secondary_img" href="product-details.html"><img src="assets/img/product/product13.jpg" alt=""></a>
                                            <div class="quick_button">
                                                <a href="shop-fullwidth.html#" data-toggle="modal" data-target="#modal_box"  data-original-title="quick view"> quick view</a>
                                            </div>
                                        </div>
                                   </div>
                                   <div class="col-lg-8 col-md-7">
                                        <div class="product_content">               
                                          <h3><a href="product-details.html">Letraset animal</a></h3>
                                          <div class="product_ratings">
                                                <ul>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product_desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et </p>
                                            </div>
                                           <div class="price_box">
                                                <span class="current_price">$55.00</span>
                                            </div>

                                            <div class="action_links">
                                                <ul>
                                                    <li class="add_to_cart"><a href="shop-fullwidth.html#" title="add to cart">add to cart</a></li>
                                                    <li><a href="shop-fullwidth.html#" title="Wishlist"><span class="icon icon-Heart"></span></a></li>

                                                    <li><a href="shop-fullwidth.html#" title="compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                   </div>
                               </div>
                            </div>
                            <div class="single_product product_list_item">
                               <div class="row">
                                   <div class="col-lg-4 col-md-5">
                                       <div class="product_thumb">
                                            <a class="primary_img" href="product-details.html"><img src="assets/img/product/product14.jpg" alt=""></a>
                                            <a class="secondary_img" href="product-details.html"><img src="assets/img/product/product15.jpg" alt=""></a>
                                            <div class="quick_button">
                                                <a href="shop-fullwidth.html#" data-toggle="modal" data-target="#modal_box"  data-original-title="quick view"> quick view</a>
                                            </div>
                                        </div>
                                   </div>
                                   <div class="col-lg-8 col-md-7">
                                        <div class="product_content">               
                                          <h3><a href="product-details.html">Natural Contrary</a></h3>
                                          <div class="product_ratings">
                                                <ul>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product_desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et </p>
                                            </div>
                                           <div class="price_box">
                                                <span class="old_price">$70.00</span>
                                                <span class="current_price">$75.00</span>
                                            </div>

                                            <div class="action_links">
                                                <ul>
                                                    <li class="add_to_cart"><a href="shop-fullwidth.html#" title="add to cart">add to cart</a></li>
                                                    <li><a href="shop-fullwidth.html#" title="Wishlist"><span class="icon icon-Heart"></span></a></li>

                                                    <li><a href="shop-fullwidth.html#" title="compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                   </div>
                               </div>
                            </div>
                            <div class="single_product product_list_item">
                               <div class="row">
                                   <div class="col-lg-4 col-md-5">
                                       <div class="product_thumb">
                                            <a class="primary_img" href="product-details.html"><img src="assets/img/product/product16.jpg" alt=""></a>
                                            <a class="secondary_img" href="product-details.html"><img src="assets/img/product/product17.jpg" alt=""></a>
                                            <div class="quick_button">
                                                <a href="shop-fullwidth.html#" data-toggle="modal" data-target="#modal_box"  data-original-title="quick view"> quick view</a>
                                            </div>
                                        </div>
                                   </div>
                                   <div class="col-lg-8 col-md-7">
                                        <div class="product_content">               
                                          <h3><a href="product-details.html"> Lorem Ipsum</a></h3>
                                          <div class="product_ratings">
                                                <ul>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product_desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et </p>
                                            </div>
                                           <div class="price_box">
                                                <span class="current_price">$75.00</span>
                                            </div>

                                            <div class="action_links">
                                                <ul>
                                                    <li class="add_to_cart"><a href="shop-fullwidth.html#" title="add to cart">add to cart</a></li>
                                                    <li><a href="shop-fullwidth.html#" title="Wishlist"><span class="icon icon-Heart"></span></a></li>

                                                    <li><a href="shop-fullwidth.html#" title="compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                   </div>
                               </div>
                            </div>
                            <div class="single_product product_list_item">
                               <div class="row">
                                   <div class="col-lg-4 col-md-5">
                                       <div class="product_thumb">
                                            <a class="primary_img" href="product-details.html"><img src="assets/img/product/product18.jpg" alt=""></a>
                                            <a class="secondary_img" href="product-details.html"><img src="assets/img/product/product19.jpg" alt=""></a>
                                            <div class="quick_button">
                                                <a href="shop-fullwidth.html#" data-toggle="modal" data-target="#modal_box"  data-original-title="quick view"> quick view</a>
                                            </div>
                                        </div>
                                   </div>
                                   <div class="col-lg-8 col-md-7">
                                        <div class="product_content">               
                                          <h3><a href="product-details.html">Natural passages</a></h3>
                                          <div class="product_ratings">
                                                <ul>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product_desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et </p>
                                            </div>
                                           <div class="price_box">
                                                <span class="old_price">$70.00</span>
                                                <span class="current_price">$75.00</span>
                                            </div>

                                            <div class="action_links">
                                                <ul>
                                                    <li class="add_to_cart"><a href="shop-fullwidth.html#" title="add to cart">add to cart</a></li>
                                                    <li><a href="shop-fullwidth.html#" title="Wishlist"><span class="icon icon-Heart"></span></a></li>

                                                    <li><a href="shop-fullwidth.html#" title="compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                   </div>
                               </div>
                            </div>
                            <div class="single_product product_list_item">
                               <div class="row">
                                   <div class="col-lg-4 col-md-5">
                                       <div class="product_thumb">
                                            <a class="primary_img" href="product-details.html"><img src="assets/img/product/product20.jpg" alt=""></a>
                                            <a class="secondary_img" href="product-details.html"><img src="assets/img/product/product2.jpg" alt=""></a>
                                            <div class="quick_button">
                                                <a href="shop-fullwidth.html#" data-toggle="modal" data-target="#modal_box"  data-original-title="quick view"> quick view</a>
                                            </div>
                                        </div>
                                   </div>
                                   <div class="col-lg-8 col-md-7">
                                        <div class="product_content">               
                                          <h3><a href="product-details.html">Natural popularised</a></h3>
                                          <div class="product_ratings">
                                                <ul>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product_desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et </p>
                                            </div>
                                           <div class="price_box">
                                                <span class="current_price">$70.00</span>
                                            </div>

                                            <div class="action_links">
                                                <ul>
                                                    <li class="add_to_cart"><a href="shop-fullwidth.html#" title="add to cart">add to cart</a></li>
                                                    <li><a href="shop-fullwidth.html#" title="Wishlist"><span class="icon icon-Heart"></span></a></li>

                                                    <li><a href="shop-fullwidth.html#" title="compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                   </div>
                               </div>
                            </div>
                            <div class="single_product product_list_item">
                               <div class="row">
                                   <div class="col-lg-4 col-md-5">
                                       <div class="product_thumb">
                                            <a class="primary_img" href="product-details.html"><img src="assets/img/product/product19.jpg" alt=""></a>
                                            <a class="secondary_img" href="product-details.html"><img src="assets/img/product/product3.jpg" alt=""></a>
                                            <div class="quick_button">
                                                <a href="shop-fullwidth.html#" data-toggle="modal" data-target="#modal_box"  data-original-title="quick view"> quick view</a>
                                            </div>
                                        </div>
                                   </div>
                                   <div class="col-lg-8 col-md-7">
                                        <div class="product_content">               
                                          <h3><a href="product-details.html">Aliquam furniture</a></h3>
                                          <div class="product_ratings">
                                                <ul>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                    <li><a href="shop-fullwidth.html#"><i class="ion-ios-star-outline"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product_desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et </p>
                                            </div>
                                           <div class="price_box">
                                                <span class="old_price">$70.00</span>
                                                <span class="current_price">$75.00</span>
                                            </div>

                                            <div class="action_links">
                                                <ul>
                                                    <li class="add_to_cart"><a href="shop-fullwidth.html#" title="add to cart">add to cart</a></li>
                                                    <li><a href="shop-fullwidth.html#" title="Wishlist"><span class="icon icon-Heart"></span></a></li>

                                                    <li><a href="shop-fullwidth.html#" title="compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                   </div>
                               </div>
                            </div>
                        </div>

                    </div>
                    <!--shop tab product end-->
                    <!--shop toolbar start-->
                    <div class="shop_toolbar t_bottom">
                        <div class="pagination">
                            <ul>
                                <li class="current">1</li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li class="next"><a href="#">next</a></li>
                                <li><a href="#">>></a></li>
                            </ul>
                        </div>
                    </div>
                    <!--shop toolbar end-->
                    <!--shop wrapper end-->
                </div>
            </div>    
        </div>
    </div>
    <!--shop  area end-->
		
	<?php include("includes/marcasNicho.php"); ?>
	<?php include("includes/newsletterFooter.php"); ?>
	<?php include("includes/footer.php"); ?>

	</div>
	<!--!FINAL home_three_body_wrapper ---->
	<?php include("includes/vistapreviaModal.php"); ?>
	<?php include("includes/newsletterModal.php"); ?>

<!-- JS
============================================ -->
<?php include("includes/js.php"); ?>

</body>

</html>