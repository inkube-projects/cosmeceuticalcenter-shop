<?php
   require('includes/core.php');
?>
<!doctype html>
<html class="no-js" lang="es">
   <head>
      <meta charset="utf-8">
      <title> </title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
	   <link rel="stylesheet" href="<?=URL?>assets/css/cesta.css">
   </head><!--/head-->

<body>
	<?php include("includes/analytics.php"); ?>
	<?php include("includes/cookies.php"); ?>

   <div class="home_three_body_wrapper">
		
	<?php include("includes/header.php"); ?>
	 <!--breadcrumbs-->
    <div class="breadcrumbs_area">
        <div class="container">   
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <h3>Cesta de la compra</h3>
                        <ul>
                            <li><a href="index.php">home</a></li>
                            <li>></li>
                            <li>Cesta Compra</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>         
    </div>
	<!--end.breadcrumbs-->
    
   <!--CONTENIDO-->

 <div class="Checkout_section textura_Gris">
       <div class="container">
                <div class="row">
				
								<!--datos user-->
                     <div class="col-lg-7 col-md-7">
						 		<form id="cesta">
						 		<div class="row">
                        
                        <!--ITEM CART -->
                    	<div class="col-12 itemCart">
                        	<div class="row">
                            
                                <div  class="col-xs-4 col-ms-2 col-sm-2">
												<a href="#"><img src="assets/img/s-product/product2.jpg" alt=""></a>
                                
                                </div>
                                
                                <div class="col-xs-8 col-ms-3 col-sm-4 borderRightN bottomxs bottommd">
                                    <h4>Marca </h4>
                                    <p><span class="verde">Título reducido del producto</span></p>
                                    <div class="imgMarca">
                                   	 Categoría
                                    </div>
                                </div>
                                
                                <div  class="col-xs-3 col-ms-2 col-sm-2 borderRightN cantidadCart">
                                        <div class="form-group ">
                                       		<label for="cantidad">Cantidad</label>
                                           <input min="0" max="100" value="1" type="number">
                        	 			</div>
                                </div>
                                 
                                  <div  class="col-xs-9 col-ms-5 col-sm-4 ">
                                  
                                      <div class="borderRightN" style="width:75%; float:left;">
                                        <h4>60,45 €</h4>
                                        <p>UNIDAD</p>
                                        <!--<p> <span class="negro"><strong>2 UNIDADES 120,90 €</strong></span></p>-->
                                        <p><span class="verde">i.v.a incluido</span></p>         
                                      </div>
                                  
                                      <div style="width:25%; float:left; position:relative; height:100%; text-align:center;">
                                      <button type="button" class="btn btn-link btn-xs">
										  				 <i class="fa fa-trash-o borrarCart"></i>
                                       </button>
                                   	   </div>
                                   </div>
                               
                                
                            </div>
                        </div>
                        <!--END ITEM CART -->
									
									 <!--ITEM CART -->
                    	<div class="col-12 itemCart">
                        	<div class="row">
                            
                                <div  class="col-xs-4 col-ms-2 col-sm-2">
												<a href="#"><img src="assets/img/s-product/product3.jpg" alt=""></a>
                                
                                </div>
                                
                                <div class="col-xs-8 col-ms-3 col-sm-4 borderRightN bottomxs bottommd">
                                    <h4>Marca </h4>
                                    <p><span class="verde">Título reducido del producto</span></p>
                                    <div class="imgMarca">
                                   	 Categoría
                                    </div>
                                </div>
                                
                                <div  class="col-xs-3 col-ms-2 col-sm-2 borderRightN cantidadCart">
                                        <div class="form-group ">
                                       		<label for="cantidad">Cantidad</label>
                                           <input min="0" max="100" value="1" type="number">
                        	 			</div>
                                </div>
                                 
                                  <div  class="col-xs-9 col-ms-5 col-sm-4 ">
                                  
                                      <div class="borderRightN" style="width:75%; float:left;">
                                        <h4>60,45 €</h4>
                                        <p>UNIDAD</p>
                                        <!--<p> <span class="negro"><strong>2 UNIDADES 120,90 €</strong></span></p>-->
                                        <p><span class="verde">i.v.a incluido</span></p>         
                                      </div>
                                  
                                      <div style="width:25%; float:left; position:relative; height:100%; text-align:center;">
                                      <button type="button" class="btn btn-link btn-xs">
										  				 <i class="fa fa-trash-o borrarCart"></i>
                                       </button>
                                   	   </div>
                                   </div>
                               
                                
                            </div>
                        </div>
                        <!--END ITEM CART -->
                        
                      
                       
                        <!--CALCULO PORTES-->
                        <div class="col-12 col-ms-8 col-ms-offset-4  col-sm-10 col-sm-offset-2 col-md-8 col-md-offset-4 calculoPortes">
                        	<i class="fa fa-truck fa-1x css-color-grisIco absoluteIco"></i>
                        	<div class="txtPortes">
                        		<p><br>Elije tu provincia<br>para calcular los portes</p>
                        	</div>
                         	<div class="ProvinciaPortes" style="width:54%; float:right;">
                                <div class="form-group">
                                            <label for="provincia">Provincia</label>
                                            <select id="provincia" class="form-control ">
                                              <option>Elige Provincia</option>
                                              <option>Provincia 2</option>
                                              <option>Provincia 3</option>
                                              <option>Provincia 4</option>
                                              <option>Provincia 5</option>
                                            </select>
                                </div>
                            </div>
                        </div>  
                        <!--END CALCULO PORTES-->
                       
                                   
                    </div>
						 
						 		</form>
							  </div>
					
							  <div class="col-lg-4 offset-lg-1 col-md-4 offset-md-1">
								  
								  <div class="row">
                   
   
                        <!--CALCULO CART-->
                    	<div class="col-12 ResumenCart">
                        	<h3><i class="fa fa-calculator fa-1x css-color-grisIco"></i> Cálculo cesta</h3><br>
                            
                        	<div class="camposCart">
                            	<p>Subtotal:</p>
                                <p>Portes:</p>
                                <p>21% i.v.a:</p>
                            </div>                        
                        	<div class="importesCart">
                            	<p>34,20 €</p>
                            	<p>345,60 €</p>
                                <p>24 €</p>
                            </div>
                           <div  class="hr"></div>
                            <div class="totalCart">
                              <p>Total:</p>
                            </div>
                            <div class="pvpCart">
                              <h2>645,24 €</h2>
                            </div>
                          
                          	<div  class="hr"></div>
										
							<div class="descuentos_bono css-fondoGris" style="width:100%; float:left; padding:10px;">
                   				 <div class="campoLabel" style="width:100%; float:left; font-size:16px">
                            	<label for=" " class="css-fonsize12"> <i class="fa fa-tag css-color-grisIco css-fonsize18" aria-hidden="true"></i> Tienes un Vale descuento</label>
                                </div>
                                <div class="col-8 float-left" style="margin: 0px; padding: 0px;">
    							 					<input type="text" id="valeDto" name="valeDto" class="form-control" style="width: 100%; border-radius: 0px" value="" placeholder="introduce código">
                                 </div>
                                 <div class="col-4 float-right"  style="margin: 0px; padding: 0px;">
                                  <a id="aplicarVale" class="btn btn-warning" style="width: 100%;border-radius: 0px" href="javascript:;" role="button">Aplicar</a>
                                  </div>
                        	</div>
							
										<div  class="hr"></div>

										
										<div class="checkout_form" style="width: 100%; float: left;">
                                    <div class="col-12">
                                    <input id="account" type="checkbox" data-target="createp_account">
                                    <label class="css-fonsize12" for="account">Acepto las <a href="#" target="_blank">condiciones de venta</a></label>
                                </div>
                                </div>
							
										  <div  class="hr"></div>
                            
                            <div class="buttonSeguir">
                            <a class="btn btn-warning" href="#" role="button">seguir comprando</a>
                            </div>
                            <div class="buttonPago">
                            <input type="submit"  class="btn btn-lg customButtonCar"  value="continuar">
                            </div>
                        </div>
                        <!--END.CALCULO CART-->
                        
                    </div>
								  
							  </div>
					
						</div>
		   </div>
	 </div>
	   
	<!--END.CONTENIDO-->

	<?php include("includes/footer.php"); ?>

	</div>


<!-- JS
============================================ -->
<?php include("includes/js.php"); ?>

</body>

</html>