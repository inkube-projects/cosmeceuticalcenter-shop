<?php
   require('includes/core.php');
?>
<!doctype html>
<html class="no-js" lang="es">
   <head>
      <meta charset="utf-8">
      <title> </title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
   </head><!--/head-->

<body>
	<?php include("includes/analytics.php"); ?>
	<?php include("includes/cookies.php"); ?>

   <div class="home_three_body_wrapper">
		
	<?php include("includes/header.php"); ?>

	 <!--breadcrumbs-->
    <div class="breadcrumbs_area">
        <div class="container">   
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <h3>Mi cuenta</h3>
                        <ul>
                            <li><a href="index.php">home</a></li>
                            <li>></li>
                            <li>Mi cuenta</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>         
    </div>
	<!--end.breadcrumbs-->
    
   <!--CONTENIDO-->
	   
	   

	 <?php include("includes/loginA.php"); ?>  
	   
	   
	   
	   
	<!--END.CONTENIDO-->

	<?php include("includes/marcasNicho.php"); ?>
	<?php include("includes/newsletterFooter.php"); ?>
	<?php include("includes/footer.php"); ?>

	</div>


<!-- JS
============================================ -->
<?php include("includes/js.php"); ?>

</body>

</html>