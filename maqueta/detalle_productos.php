<?php
   require('includes/core.php');
?>
<!doctype html>
<html class="no-js" lang="es">
   <head>
      <meta charset="utf-8">
      <title> </title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
   </head><!--/head-->

<body>
	<?php include("includes/analytics.php"); ?>
	<?php include("includes/cookies.php"); ?>

   <div class="home_three_body_wrapper">
		
	<?php include("includes/header.php"); ?>
	<?php include("includes/breadcrumbs.php"); ?>
	   
	<!--CONTENIDO-->
	 

     <!--product details start-->
    <div class="product_details variable_product">
        <div class="container">
            <div class="row">
                 <div class="col-lg-6 col-md-6">
                   <div class="product-details-tab">

                        <div id="img-1" class="zoomWrapper single-zoom">
                            <a href="#">
                                <img id="zoom1" src="assets/img/product/product1.jpg" data-zoom-image="assets/img/product/product1.jpg" alt="big-1">
                            </a>
                        </div>

                        <div class="single-zoom-thumb">
                            <ul class="s-tab-zoom owl-carousel single-product-active" id="gallery_01">
                                <li>
                                    <a href="#" class="elevatezoom-gallery active" data-update="" data-image="assets/img/product/product2.jpg" data-zoom-image="assets/img/product/product2.jpg">
                                        <img src="assets/img/product/product2.jpg" alt="zo-th-1"/>
                                    </a>

                                </li>
                                <li >
                                    <a href="#" class="elevatezoom-gallery active" data-update="" data-image="assets/img/product/product1.jpg" data-zoom-image="assets/img/product/product1.jpg">
                                        <img src="assets/img/product/product13.jpg" alt="zo-th-1"/>
                                    </a>

                                </li>
                                <li >
                                    <a href="#" class="elevatezoom-gallery active" data-update="" data-image="assets/img/product/product3.jpg" data-zoom-image="assets/img/product/product3.jpg">
                                        <img src="assets/img/product/product4.jpg" alt="zo-th-1"/>
                                    </a>

                                </li>
                                <li >
                                    <a href="#" class="elevatezoom-gallery active" data-update="" data-image="assets/img/product/product2.jpg" data-zoom-image="assets/img/product/product2.jpg">
                                        <img src="assets/img/product/product2.jpg" alt="zo-th-1"/>
                                    </a>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="product_d_right">
                       <form action="#">
                             <h2><a href="#">Marca</a></</h2>
                            <h1>Título o nombre del producto</h1>
						   				
                            <div class="product_nav">
                                <ul>
                                    <li class="prev"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                    <li class="next"><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
								 		
								 		<div class=" product_ratting">
                                <ul>
                                    <li><a href="#"><i class="fa fa-star css-color-amarillo"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star css-color-amarillo"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star css-color-amarillo"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-o "></i></a></li>
                                    <li><a href="#"> (comentarios de clientes ) </a></li>
                                </ul>
                            </div>
                            
                            <div class="product_price">
                                <span class="old_price">80.00 €</span>
                                <span class="current_price">70.00 €</span>
                            </div>
                            <div class="product_desc">
												<h4>Presentación</h4>
                                <p>eget velit. Donec ac tempus ante. Fusce ultricies massa massa. Fusce aliquam, purus eget sagittis vulputate, sapien libero hendrerit est, sed commodo augue nisi non neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor, lorem et placerat vestibulum, metus nisi posuere nisl, in </p>
												<a href="#" class=" "><strong> ampliar info</strong> <i class="fa fa-chevron-circle-down css-fonsize18" aria-hidden="true"></i></a>
                            </div>
                            
                            <div class="product_variant quantity">
                                <label>Cantidad</label>
                                <input min="0" max="100" value="1" type="number">
                                <button class="button" type="submit"><i class="fa fa-plus-circle"></i>  añadir a la cesta </button>  
												<a href="CARRITO" class="css-btn css-btn-my-cart"> <i class="ion-android-cart"></i>  cesta</a>
                                
                            </div>
                           <div class=" product_d_action">
                               <ul>
                                   <li><a href="#" title="Mis favoritos"> <i class="fa fa-heart css-fonsize18" aria-hidden="true"></i> + añadir a mis favoritos</a></li>
                               </ul>
                            </div>  
                        </form>
                        
						   <!--control de stock-->
                        <div class="product_d_meta">
                            <span>STOCK: <i class="fa fa-check-circle css-color-verde css-fonsize18" aria-hidden="true"></i><span class="css-color-crema"> DISPONIBLE</span></span>
										 <span>STOCK: <i class="fa fa-ban css-color-rojo css-fonsize18" aria-hidden="true"></i><span class="css-color-crema"> AGOTADO</span></span>
										 <span>AVISO: <i class="fa fa-exclamation-triangle css-color-naranja css-fonsize18" aria-hidden="true"></i><span class="css-color-crema"> Te avisamos cuando haya stock <a href="" class="css-colorB"> AVÍSAME ></a></span></span>
                        </div>
						   <!--cend.ontrol de stock-->
						   <!--categorias etiquetas-->
						   			<div class="product_d_meta">
                            <span>CATEGORÍA: <a href="#" class="css-color-crema">CUERPO</a></span>
                            <span>TAGS: 
                                <a href="#" class="css-color-crema">Crema</a>
                                <a href="#" class="css-color-crema">Antieging</a>
                            </span>
                        </div>
						   	<!--end.categorias etiquetas-->
                        <div class="priduct_social">
                            <ul>
                                <li><a href="#" title="facebook"><i class="fa fa-facebook"></i></a></li>           
                                <li><a href="#" title="twitter"><i class="fa fa-twitter"></i></a></li>           
                                <li><a href="#" title="pinterest"><i class="fa fa-pinterest"></i></a></li>                
                                <li><a href="#" title="linkedin"><i class="fa fa-linkedin"></i></a></li>        
                            </ul>      
                        </div>
						   
                    </div>
                </div>
              
            </div>
        </div>    
    </div> 
    <!--product details end-->
    
     <!--product info start-->
    <div class="product_d_info">
        <div class="container">   
            <div class="row">
                <div class="col-12">
                    <div class="product_d_inner">   
                        <div class="product_info_button">    
                            <ul class="nav" role="tablist">
                                <li >
                                    <a class="active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">Descripción</a>
                                </li>
                                <li>
                                     <a data-toggle="tab" href="#sheet" role="tab" aria-controls="sheet" aria-selected="false">Principios activos</a>
                                </li>
                                <li>
                                   <a data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Prescripción</a>
                                </li>
								          <li>
                                   <a data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Recomendaciones</a>
                                </li>
								           <li>
                                   <a data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Prescripción</a>
                                </li>
												<li>
                                   <a data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Resultados & Beneficios </a>
                                </li>
                            </ul>
                        </div>
						
                        <div class="tab-content">
							
                            <div class="tab-pane fade show active" id="info" role="tabpanel" >
                                <div class="product_info_content">
                                    <p><h4>Título o frase más importante</h4>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sed risus lobortis, ultricies sem eget, pretium quam. Fusce nec aliquam diam. Morbi facilisis tristique turpis non semper. Cras pretium hendrerit nulla at fringilla. Aliquam non gravida libero. Integer lobortis ex in erat feugiat, sed semper libero tempus. Phasellus ultrices metus tincidunt, mollis ligula non, vestibulum magna. Fusce convallis neque ac purus aliquam pulvinar.

Quisque a consequat nibh. In eget imperdiet ante, nec vehicula lacus. Donec hendrerit pulvinar eros, quis varius sapien tempor a. Vestibulum posuere hendrerit purus, a vulputate nulla rutrum id. Nam luctus dui at iaculis tempus.</p>
                                </div>    
                            </div>

                            <div class="tab-pane fade" id="sheet" role="tabpanel" >
                                <div class="product_d_table">
                                   <form action="#">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td class="first_child">Compositions</td>
                                                    <td>Polyester</td>
                                                </tr>
                                                <tr>
                                                    <td class="first_child">Styles</td>
                                                    <td>Girly</td>
                                                </tr>
                                                <tr>
                                                    <td class="first_child">Properties</td>
                                                    <td>Short Dress</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                <div class="product_info_content">
                                    <p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sed risus lobortis, ultricies sem eget, pretium quam. Fusce nec aliquam diam. Morbi facilisis tristique turpis non semper. Cras pretium hendrerit nulla at fringilla. Aliquam non gravida libero. Integer lobortis ex in erat feugiat, sed semper libero tempus. Phasellus ultrices metus tincidunt, mollis ligula non, vestibulum magna. Fusce convallis neque ac purus aliquam pulvinar.

Quisque a consequat nibh. In eget imperdiet ante, nec vehicula lacus. Donec hendrerit pulvinar eros, quis varius sapien tempor a. Vestibulum posuere hendrerit purus, a vulputate nulla rutrum id. Nam luctus dui at iaculis tempus.
													  </p>
                                </div>    
                            </div>
										
										<div class="tab-pane fade show active" id="reviews" role="tabpanel" >
                                <div class="product_info_content">
                                    <p><h4>Título o frase más importante</h4>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sed risus lobortis, ultricies sem eget, pretium quam. Fusce nec aliquam diam. Morbi facilisis tristique turpis non semper. Cras pretium hendrerit nulla at fringilla. Aliquam non gravida libero. Integer lobortis ex in erat feugiat, sed semper libero tempus. Phasellus ultrices metus tincidunt, mollis ligula non, vestibulum magna. Fusce convallis neque ac purus aliquam pulvinar.

Quisque a consequat nibh. In eget imperdiet ante, nec vehicula lacus. Donec hendrerit pulvinar eros, quis varius sapien tempor a. Vestibulum posuere hendrerit purus, a vulputate nulla rutrum id. Nam luctus dui at iaculis tempus.</p>
                                </div>    
                            </div>
						
                            
                        </div>
				
                    </div>     
                </div>
            </div>
        </div>    
    </div>  
    <!--product info end-->
    
    <!--product section area start-->
    <section class="product_section  p_section1 related_product">
        <div class="container">
            <div class="row">
               <div class="col-12">
                    <div class="section_title">
                        <h2>Productos relacionados</h2>
                    </div> 
                </div>  
                <div class="col-12">
                    <div class="product_area ">
                         <div class="product_container bottom">
                            <div class="custom-row product_row1">
                                
								
												<?php include("includes/productosPrueba.php"); ?>
								
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </section>
    <!--product section area end-->	   
	   


	   
	<!--END.CONTENIDO-->
	
	<?php include("includes/eleccionProductos.php"); ?>

	<?php include("includes/newsletterFooter.php"); ?>
	<?php include("includes/footer.php"); ?>

	</div>
	<!--!FINAL home_three_body_wrapper ---->
	<?php include("includes/vistapreviaModal.php"); ?>
	<?php include("includes/newsletterModal.php"); ?>

<!-- JS
============================================ -->
<?php include("includes/js.php"); ?>

</body>

</html>