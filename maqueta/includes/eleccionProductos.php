	     <!--banner fullwidth start-->
        <section class="banner_fullwidth banner_bg03">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="slider_content text-center mx-auto" style="width: 540px; height: 380px;">
										<div class="slider_content_info">
                            <p>Información especializada y rigurosa.</p>
                            <h1> Para cuidarse es necesario saber qué producto elegir</h1>
                            <p class="slider_price" style="font-size: 13px; line-height: 18px;">Te damos información detallada basándonos en los parámetros que nos comentes de tu perfil cutáneo para proporcionar un asesoramiento directo y de calidad. Para cuidarse es necesario saber qué producto elegir.</p>
                            <a class="button" href="http://www.cosmeceuticalcenter.com/ASESORIA-COSMECEUTICA" target="_blank">+ asesoria antiaging</a>
										 </div>
                        </div>
                    </div>
                </div>  
            </div>
        </section>
        <!--banner area end-->