<header class="header_area header_three">
            <!--header top start-->
            <div class="header_top">
                <div class="container">   
                    <div class="row align-items-center">
						
                    <div class="col-lg-5 col-md-5">
                        <div class="social_icone text-left">
                            <ul>
                                <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                                <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                                <li><a href="#"><i class="ion-social-pinterest"></i></a></li>
                                <li><a href="#"><i class="ion-social-instagram"></i></a></li>
												<li><i class="fa fa-phone css-fonsize16" aria-hidden="true"></i> <span class="css-fonsize12">Pedidos - 954 378 643 - 673 375 357</span> </li>
                            </ul>
                        </div>
                    </div>
						
                        <div class="col-lg-7 col-md-7">
                            <div class="middel_right">
								
												<div class="box_setting">
                                <div class="search_btn">
                                    <form action="#">
                                        <input placeholder="¿Qué buscas...?" type="text">
                                        <button type="submit"><i class="ion-ios-search-strong"></i></button> 
                                    </form>
                                </div>
												</div>
                                
                                 
                                <div class="cart_link">
                                    <a href="#"><i class="ion-android-cart"></i>138.00 €<i class="fa fa-angle-down"></i></a>
                                    <span class="cart_quantity">2</span>
                                    <!--mini cart-->
                                     <div class="mini_cart">
                                        <div class="mini_cart_inner"> 
                                            <div class="cart_item">
                                               <div class="cart_img">
                                                   <a href="#"><img src="assets/img/s-product/product.jpg" alt=""></a>
                                               </div>
                                                <div class="cart_info">
                                                    <a href="#">Producto 1</a>

                                                    <span class="quantity">Cant: 1</span>
                                                    <span class="price_cart">60.00 €</span>

                                                </div>
                                                <div class="cart_remove">
                                                    <a href="#"><i class="ion-android-close"></i></a>
                                                </div>
                                            </div>
                                            <div class="cart_item">
                                               <div class="cart_img">
                                                   <a href="#"><img src="assets/img/s-product/product2.jpg" alt=""></a>
                                               </div>
                                                <div class="cart_info">
                                                    <a href="#">Producto 2</a>
                                                    <span class="quantity">Cant: 1</span>
                                                    <span class="price_cart">69.00 €</span>
                                                </div>
                                                <div class="cart_remove">
                                                    <a href="#"><i class="ion-android-close"></i></a>
                                                </div>
                                            </div>
                                            <div class="cart_total">
                                                <span>Subtotal:</span>
                                                <span>138.00 €</span>
                                            </div>
                                        </div> 
                                        <div class="mini_cart_footer">
                                           <div class="cart_button view_cart">
                                                <a href="#">Ver cesta</a>
                                            </div>
                                            <div class="cart_button checkout">
                                                <a href="#">Datos</a>
                                            </div>

                                        </div>

                                    </div>
                                    <!--mini cart end-->
									
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--header top start-->

            <!--header middel start-->
            <div class="header_middel">
                <div class="container">
                    <div class="row align-items-center">
						
									<div class="col-lg-4 col-md-4"></div>
						
                        <div class="col-lg-4 col-md-4">
                            <div class="logo text-center">
                                <a href="#"><img src="assets/img/logo/logo.png" alt=""></a>
                            </div>
                        </div>
						
									<div class="col-lg-4 col-md-4"></div>

                    </div>
                </div>
            </div>
            <!--header middel end-->

            <!--header bottom satrt-->
            <div class="header_bottom sticky-header">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-12">
                            <div class="main_menu_inner">
                                <div class="logo_sticky">
                                   <a href="index.html"><img src="assets/img/logo/logo.jpg" alt=""></a>
                                </div>
                                <div class="main_menu d-none d-lg-block"> 
                                <nav>  
                                    <ul>
                                        
                                        <li><a href="#"><i class="fa fa-home css-fonsize16  css-padding-left20"></i></a></li>
										
                                        <li><a href="#">CATEGORÍAS <i class="fa fa-angle-down"></i></a>
                                            <ul class="mega_menu">
																		<li>
                                                    <div style="width: 300px;">
																			  <img src="assets/img/bg/imgMenuCategoria.jpg" width="300" height="170" alt=""/>
																			  </div>
                                                </li>
																		<li class="boder-right1" style="margin-right: 40px;">
                                                    <ul>
                                                        <li><a href="#" class="css-bold-lowercase">Novedades <i class="fa fa-angle-right"></i></a></li>
                                                        <li><a href="#" class="css-bold-lowercase">Con descuento <i class="fa fa-angle-right"></i></a></li>
																					<li><a href="#" class="css-bold-lowercase">Más vendidas <i class="fa fa-angle-right"></i></a></li>
																					<li><a href="#" class="css-bold-lowercase">Próximamente <i class="fa fa-angle-right"></i></a></li>
                                                    </ul>
                                                </li>
                                                <li class="boder-right1">
                                                    <ul>
                                                        <li><a href="#">CATEGORÍA  1</a></li>
                                                        <li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
                                                    </ul>
                                                </li>
                                               <li class="boder-right1">
                                                    <ul>
                                                 			<li><a href="#">CATEGORÍA  1</a></li>
                                                        <li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
                                                    </ul>
                                                </li>
                                               <li class="boder-right1">
                                                    <ul>
																					<li><a href="#">CATEGORÍA  1</a></li>
                                                        <li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
                                                    </ul>
                                                </li>
																		<li class="boder-right1">
                                                    <ul>
																					<li><a href="#">CATEGORÍA  1</a></li>
                                                        <li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
                                                    </ul>
                                                </li>
																		<li class="boder-right1">
                                                    <ul>
																					<li><a href="#">CATEGORÍA  1</a></li>
                                                        <li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
																					<li><a href="#">CATEGORÍA  1</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
										
														   <li><a href="#">MARCAS <i class="fa fa-angle-down"></i></a>
                                            <ul class="mega_menu">
																		<li>
                                                    <div style="width: 300px;">
																			  <img src="assets/img/bg/imgMenuMarca.jpg" width="300" height="170" alt=""/>
																			  </div>
                                                </li>
																		<li class="boder-right1" style="margin-right: 40px;">
                                                    <ul>
                                                        <li><a href="#" class="css-bold-lowercase">Novedades <i class="fa fa-angle-right"></i></a></li>
                                                        <li><a href="#" class="css-bold-lowercase">Con descuento <i class="fa fa-angle-right"></i></a></li>
																					<li><a href="#" class="css-bold-lowercase">Más vendidas <i class="fa fa-angle-right"></i></a></li>
																					<li><a href="#" class="css-bold-lowercase">Próximamente <i class="fa fa-angle-right"></i></a></li>
                                                    </ul>
                                                </li>
                                                <li class="boder-right1">
                                                    <ul>
                                                        <li><a href="#">Marcas 1</a></li>
                                                        <li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
                                                    </ul>
                                                </li>
                                               <li class="boder-right1">
                                                    <ul>
                                                 			<li><a href="#">Marcas 1</a></li>
                                                        <li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
                                                    </ul>
                                                </li>
                                               <li class="boder-right1">
                                                    <ul>
																					<li><a href="#">Marcas 1</a></li>
                                                        <li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
                                                    </ul>
                                                </li>
																		<li class="boder-right1">
                                                    <ul>
																					<li><a href="#">Marcas 1</a></li>
                                                        <li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
                                                    </ul>
                                                </li>
																		<li class="boder-right1">
                                                    <ul>
																					<li><a href="#">Marcas 1</a></li>
                                                        <li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
																					<li><a href="#">Marcas 1</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
									
                                        <li><a href="portfolio.html">Contacto</a></li>
										
															<li class="position-relative"><a href="#">Servicios <i class="fa fa-angle-down"></i></a>
																<ul class="sub_menu css-left-78">
                                                <li><a href="#">Tratamientos en clínica</a></li>
                                                <li><a href="#">Asesoria Cosmecéutica</a></li>
                                            </ul>
															</li>
										
															<li class="position-relative"><a href="#"><i class="fa fa-user css-fonsize16"> <i class="fa fa-angle-down"></i></i></a>
																<ul class="sub_menu css-left-78">
                                                <li><a href="#">Mi cuenta</a></li>
                                                <li><a href="#">Registro</a></li>
                                                <li><a href="#">Favoritos</a></li>
                                            </ul>
															</li>
										
															
										
                                        
                                    </ul>  
                                </nav> 
                            </div>
								
                            <div class="mobile-menu d-lg-none">
                                 <nav>  
                                    <ul>
                                        
                                        <li><a href="index.html">Home</a>
                                            <ul class="sub_menu">
                                                <li><a href="index.html">Home 1</a></li>
                                                <li><a href="index-2.html">Home 2</a></li>
                                                <li><a href="index-3.html">Home 3</a></li>
                                                <li><a href="index-4.html">Home 4</a></li>
                                                <li><a href="index-5.html">Home 5</a></li>
                                                <li><a href="index-6.html">Home 6</a></li>
                                                <li><a href="index-7.html">Home 7</a></li>
                                                <li><a href="index-8.html">Home 8</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="shop.html">shop</a>
                                            <ul>
                                                <li><a href="#">Shop Layouts</a>
                                                    <ul>
                                                        <li><a href="shop-fullwidth.html">Full Width</a></li>
                                                        <li><a href="shop-fullwidth-list.html">Full Width list</a></li>
                                                        <li><a href="shop-right-sidebar.html">Right Sidebar </a></li>
                                                        <li><a href="shop-right-sidebar-list.html"> Right Sidebar list</a></li>
                                                        <li><a href="shop-list.html">List View</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">other Pages</a>
                                                    <ul>
                                                        <li><a href="portfolio.html">portfolio</a></li>
                                                        <li><a href="portfolio-details.html">portfolio details</a></li>
                                                        <li><a href="cart.html">cart</a></li>
                                                        <li><a href="checkout.html">Checkout</a></li>
                                                        <li><a href="my-account.html">my account</a></li>


                                                    </ul>
                                                </li>
                                                <li><a href="#">Product Types</a>
                                                    <ul>
                                                        <li><a href="product-details.html">product details</a></li>
                                                        <li><a href="product-sidebar.html">product sidebar</a></li>
                                                        <li><a href="product-grouped.html">product grouped</a></li>
                                                        <li><a href="variable-product.html">product variable</a></li>

                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="blog.html">blog</a>
                                            <ul>
                                                <li><a href="blog-details.html">blog details</a></li>
                                                <li><a href="blog-fullwidth.html">blog fullwidth</a></li>
                                                <li><a href="blog-left.html">blog left</a></li>
                                                <li><a href="blog-none-sidebar.html">no sidebar</a></li>
                                                <li><a href="blog-right.html">blog right</a></li>
                                                <li><a href="blog-sticky.html">blog sticky</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="portfolio.html">portfolio</a></li>
                                        <li><a href="#">pages</a>
                                            <ul>
                                                <li><a href="about.html">About Us</a></li>
                                                <li><a href="services.html">services</a></li>
                                                <li><a href="faq.html">Frequently Questions</a></li>
                                                <li><a href="contact.html">contact</a></li>
                                                <li><a href="login.html">login</a></li>
                                                <li><a href="wishlist.html">Wishlist</a></li>
                                                <li><a href="404.html">Error 404</a></li>
                                            </ul>
                                        </li>
                                    </ul>  
                                </nav>     
                            </div>
                            </div> 
                        </div>

                    </div>
                </div>
            </div>
            <!--header bottom end-->
        </header>