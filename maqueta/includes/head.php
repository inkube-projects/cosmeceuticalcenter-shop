<meta name="author" CONTENT="inkube.es" />
<meta name="subject" CONTENT="COSMECEUTICAL CENTER S.L." />
<meta name="Classification" CONTENT="Cosmética, Belleza, " />
<meta name="Geography" CONTENT="Cosmeceutical Center S.L., c/ Dr. Francisco Vázquez Limón nº 20, 5ºA, 21002 de HUELVA, España, Spain." />
<meta name="Language" CONTENT="castellano" />
<meta HTTP-EQUIV="Expires" CONTENT="\"never\"">
<meta name="Revisit-After" CONTENT="10 days" />
<meta name="distribution" CONTENT="Global" />
<meta name="Robots" CONTENT="INDEX,FOLLOW" />
<meta name="country" CONTENT="España" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="shortcut icon" type="image/x-icon" href="<?=URL?>assets/img/favicon.ico">
<link href="https://www.cosmeceuticalcenter.com/assets/css/cookies.css" rel="stylesheet">

<!-- Plugins CSS -->
<link rel="stylesheet" href="<?=URL?>assets/css/plugins.css">
    
 <!-- Main Style CSS -->
 <link rel="stylesheet" href="<?=URL?>assets/css/style.css">


