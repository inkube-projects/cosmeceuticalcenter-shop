	 <!-- modal shipping_area start-->
    <div class="modal fade" id="modal_top2" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
				
                <div class="modal_body">
                    <div class="container">
                        <div class="row">
							
							 		<div class="col-sm-12 shipping_two">

                         <div class="single-shipping">
                            <div class="shipping_icone icone_1"></div>
                            <div class="shipping_content">
                                <h3>PEDIDOS TELEFÓNICOS</h3>
                                <p>Atención personalizada, te asesoramos en tus compras <br>954 378 643 - 673 375 357</p>
												<h4><i class="fa fa-clock-o"></i> HORARIO DE ATENCIÓN</h4>
												<p>De lunes a viernes 10:00 a 21:00 h </p>
                            </div>
                        </div>
                                    
                                    <div class="modal_description mb-15 text-center">
                                        <p>En Cosmeceutical Center cada detalle es importante, pero lo más importante eres tú, por ese motivo te ofrecemos nuestro trato personalizado en cada pedido. </p>    
                                    </div> 
                                    
                           			   <img src="assets/img/bg/modal02.jpg" alt=""/> </div>  
                        </div>     
                    </div>
                </div> 
				
            </div>
        </div>
    </div> 
    <!-- modal shipping_area end->