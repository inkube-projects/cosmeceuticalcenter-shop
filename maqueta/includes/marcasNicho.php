       <!--banner fullwidth start-->
        <section class="banner_fullwidth banner_bg04">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="slider_content text-center mx-auto" style="width: 540px; height: 320px;">
										<div class="slider_content_info">
                            <p>Cosmética, Belleza y antiaging</p>
                            <h1>MARCAS NICHO</h1>
                            <p class="slider_price" style="font-size: 13px; line-height: 18px;">Antes de incluir un producto en nuestro catálogo, siempre comprobamos la eficacia del principio activo, la penetración, su forma de liberación y el análisis de excipientes, vehículos y coadyuvantes.</p>
                            <a class="button" href="http://www.cosmeceuticalcenter.com/NEW-MARCAS-NICHO-BELLEZA-COSMETICA" target="_blank">+ ver todas las marcas</a>
										 </div>
                        </div>
                    </div>
                </div>  
            </div>
        </section>
        <!--banner area end-->