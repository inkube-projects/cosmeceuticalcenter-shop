    <!--footer area start-->
        <footer class="footer_widgets css-paddingT50">
            <div class="container">  
                <div class="footer_top">
                    <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <div class="widgets_container contact_us">
                                    <h3><img src="assets/img/logo/logo.png" alt=""/></h3>
                                    <div class="footer_contact">
                                      <p>C/ Jesús de la Vera Cruz, 27 <br>41002 Sevilla</p>
                                        <p>Tel.: +34 954 378 643 - 673 375 357</p>
                                        <p>Email: info@cosmeceuticalcenter.com </p>
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="ion-social-rss"></i></a></li>
                                            <li><a href="#"><i class="ion-social-googleplus"></i></a></li>

                                            <li><a href="#"><i class="ion-social-youtube"></i></a></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="widgets_container widget_menu">
                                    <h3>Productos por Marcas</h3>
                                    <div class="footer_menu">
                                        <ul>
 																 <li><a href="#">Marca 1</a></li>
                                            <li><a href="#">Marca 2</a></li>
                                            <li><a href="#">Marca 3</a></li>
                                            <li><a href="#">Marca 4</a></li>
                                            <li><a href="#">Marca 5</a></li>	
																  <li><a href="#">Marca 6</a></li>
                                            <li><a href="#">Marca 7</a></li>
                                            <li><a href="#">Marca 8</a></li>
                                            <li><a href="#">Marca 9</a></li>
                                            <li><a href="#">Marca 10</a></li>
																  <li><a href="#">Marca 11</a></li>
                                            <li><a href="#">Marca 12</a></li>
                                            <li><a href="#">Marca 13</a></li>
                                            <li><a href="#">Marca 14</a></li>
                                            <li><a href="#">Marca 15</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="widgets_container widget_menu">
                                    <h3>Productos por Categorias</h3>
                                    <div class="footer_menu">
                                        <ul>
 																  <li><a href="#">Categoria 1</a></li>
                                            <li><a href="#">Categoria 2</a></li>
                                            <li><a href="#">Categoria 3</a></li>
                                            <li><a href="#">Categoria 4</a></li>
                                            <li><a href="#">Categoria 5</a></li>	
																  <li><a href="#">Categoria 6</a></li>
                                            <li><a href="#">Categoria 7</a></li>
                                            <li><a href="#">Categoria 8</a></li>
                                            <li><a href="#">Categoria 9</a></li>
                                            <li><a href="#">Categoria 10</a></li>
																  <li><a href="#">Categoria 11</a></li>
                                            <li><a href="#">Categoria 12</a></li>
                                            <li><a href="#">Categoria 13</a></li>
                                            <li><a href="#">Categoria 14</a></li>
                                            <li><a href="#">Categoria 15</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6">
                                <div class="widgets_container widget_menu">
                                    <h3>Secciones</h3>
                                    <div class="footer_menu">
                                        <ul>
                                            <li><a href="#">Tratamientos en clínica</a></li>
																 <li><a href="#">Asesoría Antiaging</a></li>
                                            <li><a href="#">Contacto</a></li>
                                            <li><a href="#">Mi cuenta</a></li>
                                            <li><a href="#">Registro</a></li>
                                            <li><a href="#">Faqs/Ayuda</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="footer_middel">
                    <div class="row">
                        <div class="col-12">
                            <div class="footer_middel_menu">
                                <ul>
                                    <li><a href="#">Shop Online</a></li>
                                    <li><a href="#">Web Corporativa</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Contacto</a></li>
                                    <li><a href="#">Aviso Legal-RGPD</a></li>
                                    <li><a href="#">Cookies</a></li>
                                    <li><a href="#">Condiciones de Venta</a></li>
                                    <li><a href="#">Cambios y devoluciones</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer_bottom">
                   <div class="row">
                        <div class="col-12">
                            <div class="copyright_area">
                                <p>Copyright &copy; 2019 <a href="#">Coemceutial Center S.L.</a>  Todo los derechos reservados.</p>
                                <img src="assets/img/icon/papyel2.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>     
        </footer>
        <!--footer area end-->

