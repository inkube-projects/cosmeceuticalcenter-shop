<?php
   require('includes/core.php');
?>
<!doctype html>
<html class="no-js" lang="es">
   <head>
      <meta charset="utf-8">
      <title> </title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
	   <link rel="stylesheet" href="<?=URL?>assets/css/cesta.css">
   </head><!--/head-->

<body>
	<?php include("includes/analytics.php"); ?>
	<?php include("includes/cookies.php"); ?>

   <div class="home_three_body_wrapper">
		
	<?php include("includes/header.php"); ?>
	 <!--breadcrumbs-->
    <div class="breadcrumbs_area">
        <div class="container">   
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <h3>Cesta de la compra</h3>
                        <ul>
                            <li><a href="index.php">home</a></li>
                            <li>></li>
                            <li>Cesta Compra</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>         
    </div>
	<!--end.breadcrumbs-->
	   
	     <!--PASOS CESTA ================================================== -->
        <div class="contenidos">
           <div class="container">
            	<div class="row">
                	<div class=" col-12 col-sm-8 offset-sm-2">
                		<div class="process">
                            <div class="process-row">
                                <div class="process-step">
                                    <button id="iconBasicos" type="button" class="btn   btn-success btn-circle" disabled="disabled"><i class="fa fa-user fa-3x "></i></button>
                                    <h2>PASO 01</h2><p>ACCESO / REGISTRO</p>
                                </div>
                                <div class="process-step">
                                    <button id="iconUbicacion" type="button" class="btn  btn-default  btn-circle" disabled="disabled"><i class="fa fa-shopping-cart fa-3x "></i></button>
                                    <h2>PASO 02</h2><p>REALIZAR PAGO</p>
                                </div>
                                <div class="process-step">
                                    <button type="button" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-thumbs-up fa-3x "></i></button>
                                    <h2>PASO 03</h2><p>VALIDACIÓN PAGO</p>
                                </div> 
                            </div>
                         </div>
                	</div>
            	</div>
        	</div>
       </div>
	<!-- /.END PASO CESTA -->
    
   <!--CONTENIDO-->

 <div class="Checkout_section textura_Gris">
		<?php include("includes/loginA.php"); ?>  
 </div>
	   
	<!--END.CONTENIDO-->

	<?php include("includes/footer.php"); ?>

	</div>


<!-- JS
============================================ -->
<?php include("includes/js.php"); ?>

</body>

</html>