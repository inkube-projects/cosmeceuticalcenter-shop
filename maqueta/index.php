<?php
   require('includes/core.php');
?>
<!doctype html>
<html class="no-js" lang="es">
   <head>
      <meta charset="utf-8">
      <title> </title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
   </head><!--/head-->

<body>
    <div class="home_three_body_wrapper">
         <!--header area start-->
        <?php include("includes/header.php"); ?>
        <!--header area end-->


      <!--slider area start-->
    <div class="slider_area owl-carousel">
         <div class="single_slider" data-bgimg="assets/img/slider/slider1.jpg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="slider_content ">
										<div class="slider_content_info">
                            <p>Novedad <strong>-10% </strong>en Cosmeceutical Center</p>
                            <h1>r-Retinoate</h1>
										  <p><strong>Medik8</strong></p>
                            <p class="slider_price">compralo por<span>249 €</span></p>
                            <a class="button" href="#">+ info</a>
										 </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="single_slider" data-bgimg="assets/img/slider/slider2.jpg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="slider_content">
										<div class="slider_content_info">
                            <p>Solo hoy <strong>-5% </strong>de descuento</p>
                            <h1>Colección Urban Senses</h1>
                            <p class="slider_price">Cosmopolita, Majestuosa, Iridiscente</p>
                            <a class="button" href="#">+ info</a>
										 </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
			<div class="single_slider" data-bgimg="assets/img/slider/slider3.jpg">
				<div class="container">
					<div class="row align-items-center ">
						<div class="col-12">
							<div class="slider_content css_border_Black">
								<div class="slider_content_info css_background_Black css-fontcolorFFF">
								<p>Lyric Women</p>
								<h1 class="css-fontcolorFFF">Eau de Parfum Spray </h1>
								<p class="slider_price css-fontcolorFFFB">Amouage</p>
								<a class="button" href="#">+ info</a>
											 </div>
							</div>
						</div>
					</div>
				</div>
			</div>
    </div>
    <!--slider area end-->
        
         <!--shipping area start-->
        <div class="shipping_area shipping_two">
            <div class="container">
                <div class="row">
							
                    <div class="col-lg-3 col-md-6">
                        <div class="single-shipping">
                            <a href="" data-toggle="modal" data-target="#modal_top1" data-placement="top" data-original-title="quick view">
												<div class="shipping_icone icone_1"> 
                                </div>
										  </a>
                            <div class="shipping_content">
                                <h3>Pedidos Telefónicos</h3>
                                <p>Atención personalizada, te asesoramos en tus compras <br>954 378 643 - 673 375 357</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="single-shipping">
									<a href="" data-toggle="modal" data-target="#modal_top2" data-placement="top" data-original-title="quick view">
                            <div class="shipping_icone icone_2">
                                
                            </div>
									</a>
                            <div class="shipping_content">
                                <h3>Muestras Gratuitas</h3>
                                <p>Muestras adaptadas a tus necesidades en cada pedido. no experiementes con tu piel!!</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="single-shipping">
										<a href="" data-toggle="modal" data-target="#modal_top3" data-placement="top" data-original-title="quick view">
                            <div class="shipping_icone icone_3">
                                
                            </div>
										</a>
                            <div class="shipping_content">
                                <h3>Envíos Gratuitos</h3>
                                <p>En tus compras online o teléfonicas portes gratuitos en pedidos superiores a 80 euros</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="single-shipping">
										<a href="" data-toggle="modal" data-target="#modal_top4" data-placement="top" data-original-title="quick view">
                            <div class="shipping_icone icone_4">
                                
                            </div>
										 </a>
                            <div class="shipping_content">
                                <h3>Tu cuenta</h3>
                                <p>Siempre informado de todo, descuento, novedas, pedidos, mis favoritos y mucho más...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--shipping area end-->
		
		
		<!--banner area start-->
    <section class="banner_section home_banner_two">
        <div class="container">
            <div class="row ">
                <div class="col-lg-6 col-md-6">
                   <div class="single_banner">
                       <div class="banner_thumb">
                            <a href="http://www.cosmeceuticalcenter.com/NEW-TRATAMIENTOS" target="_blank"><img src="assets/img/bg/banner5.jpg" alt="tratamientos de belleza y salud"></a>
                            <div class="banner_content">
                                <p><i class="fa fa-map-marker css-fonsize20 css-colorB"></i> Solo en clínica</p>
                                <h2>TRATAMIENTOS</h2>
                                <span>Servicio integral de salud y belleza</span><br>
												<a class="button btn-sm css-marginT20" href="http://www.cosmeceuticalcenter.com/NEW-TRATAMIENTOS" target="_blank">+ nuestros tratamientos</a>
                            </div>
                        </div>
                   </div>
                    
                </div>
                <div class="col-lg-6 col-md-6">
                   <div class="single_banner">
                       <div class="banner_thumb">
                            <a href="http://www.cosmeceuticalcenter.com/ASESORIA-COSMECEUTICA" target="_blank"><img src="assets/img/bg/banner6.jpg" alt="asesoria antiaging"></a>
                            <div class="banner_content ">
                                <p><i class="fa fa-map-marker css-fonsize20 css-fontcolorFFF"></i> <span class="css-fontcolorFFFB">En clinica y </span> <i class="fa fa-television css-fonsize20 css-fontcolorFFF"></i> <span class="css-fontcolorFFFB">Online</span></p>
                                <h2 class="css-fontcolorFFF">ANTIAGING</h2>
                                <span class="css-fontcolorFFF">Consultoría y Asesoramiento</span><br>
												<a class="button btn-sm css-marginT20" href="http://www.cosmeceuticalcenter.com/ASESORIA-COSMECEUTICA" target="_blank">+ asesoria antiaging</a>
                            </div>
                        </div>
                   </div>
                    
                </div>
            </div>
        </div>
    </section>
    <!--banner area end-->
		
		
	 <!--product section area start-->
    <section class="product_section  p_section1">
        <div class="container">
            <div class="row">
               <div class="col-12">
				   			<p class="css-marginB0">Productos antiaging y suplementos nutricionales recomendados de la semana</p>
                    <div class="section_title">
                        <h2>Recomendación semanal</h2>
                    </div> 
				   			
                </div>  
                <div class="col-12">
                    <div class="product_area ">
                         <div class="product_container bottom">
                            <div class="custom-row product_row1">
								
												<?php include("includes/productosPrueba.php"); ?>
											
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </section>
    <!--product section area end-->
		
	<!--shipping area start-->
    <div class="shipping_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="single-shipping">
                        <h3>Marcas de primera calidad</h3>
                        <p>Estudiamos las formulaciones y calidad de cada<br> producto antes de recomendarlo</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="single-shipping">
                        <h3>Productos testados</h3>
                        <p>Muchos de los productos que recomendamos han <br>sido probados en nuestra clínica</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--shipping area end-->
		
		

		

        <!--product section area start-->
        <section class="product_section p_section1 product_s_three">
            <div class="container-fluid">
                <div class="row">   
                    <div class="col-12">
                        <div class="product_area"> 
                            <div class="product_tab_button">
                                <ul class="nav" role="tablist">
                                    <li>
                                        <a class="active" data-toggle="tab" href="#featured" role="tab" aria-controls="featured" aria-selected="true">Novedades</a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#arrivals" role="tab" aria-controls="arrivals" aria-selected="false">Con descuento</a>
                                    </li>

                                </ul>
                            </div>
							
                             <div class="tab-content">
                                  <div class="tab-pane fade show active" id="featured" role="tabpanel">
                                         <div class="product_container">
                                            <div class="custom-row product_column3">
											
								
																			<div class="custom-col-5">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product12.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product12B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																				<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

																			<div class="custom-col-5">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product9.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product9B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      
																					<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>							

																			<div class="custom-col-5">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product1.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product1B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																												<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

																			<div class="custom-col-5">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product8.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product8B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																												<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>											

																			<div class="custom-col-5">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product12.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product12B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																												<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

																				
                                            </div>
                                        </div>
                                  </div>
								 
                                    <div class="tab-pane fade" id="arrivals" role="tabpanel">
                                        <div class="product_container">
                                            <div class="custom-row product_column3">
												
												
														<div class="custom-col-5">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product12.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product12B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																												<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

																			<div class="custom-col-5">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product9.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product9B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																												<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>							

																			<div class="custom-col-5">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product1.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product1B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																												<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

																			<div class="custom-col-5">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product8.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product8B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      

																												<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>											

																<div class="custom-col-5">
																<div class="single_product">
																	<div class="product_thumb">
																		<a class="primary_img" href="#"><img src="assets/img/product/product12.jpg" alt=""></a>
																		<a class="secondary_img" href="#"><img src="assets/img/product/product12B.jpg" alt=""></a>
																		<div class="quick_button">
																			<a href="#" data-toggle="modal" data-target="#modal_box" data-placement="top" data-original-title="60.00 €"> vista rápida</a>
																		</div>
																	</div>
																	<div class="product_content">
																		<div class="tag_cate">
																			<a href="#">Marca</a>
																		</div>
																		<h3><a href="#">Nombre o título</a></h3>
																		<div class="price_box">
																							<span class="old_price">86.00 €</span>
																							<span class="current_price">60.00 €</span>
																						</div>
																		<div class="product_hover">
																			<div class="product_desc">
																				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce posuere metus vitae </p>
																			</div>
																			<div class="action_links">
																				<ul>                                                      
																					<li class="add_to_cart"><a href="#"> <i class="fa fa-cart-plus css-fonsize18"></i> añadir carro</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
												

                                            </div>
                                        </div>
                                   </div> 
                                 
                            </div>
                        </div>

                    </div>
                </div>    
            </div>
        </section>
        <!--product section area end-->
		
				
		
		

	<?php include("includes/marcasNicho.php"); ?>
	<?php include("includes/newsletterFooter.php"); ?>
	<?php include("includes/footer.php"); ?>

	</div>
	<!--!FINAL home_three_body_wrapper ---->
	<?php include("includes/vistapreviaModal.php"); ?>
	<?php include("includes/newsletterModal.php"); ?>
	<?php include("includes/modalTop1.php"); ?>
	<?php include("includes/modalTop2.php"); ?>
	<?php include("includes/modalTop3.php"); ?>
	<?php include("includes/modalTop4.php"); ?>

<!-- JS
============================================ -->
<?php include("includes/js.php"); ?>

</body>

</html>