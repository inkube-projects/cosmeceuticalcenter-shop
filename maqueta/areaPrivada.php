<?php
   require('includes/core.php');
?>
<!doctype html>
<html class="no-js" lang="es">
   <head>
      <meta charset="utf-8">
      <title> </title>
	   <meta name="Description" CONTENT=" " />
		<meta name="Keywords" CONTENT="" />
      <?php include("includes/head.php"); ?>
   </head><!--/head-->
	<style>
		.tab-content {
    padding-top: 20px;
    padding-bottom: 30px;
    padding-right: 30px;
    padding-left: 30px;
}
		
		.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #495057;
    background-color: #f8f9f9;
    border-color: #dee2e6 #dee2e6 #f8f9f9;
}
		.tab-content {
    padding-top: 20px;
    padding-bottom: 30px;
    padding-right: 30px;
    padding-left: 30px;
    border-bottom: solid 1px #dee2e6;
    border-left: solid 1px #dee2e6;
    border-right: solid 1px #dee2e6;
}
		
		
		.table {
    background-color: #fff;
}
		.table-responsive table tbody tr td {
    text-align: left;
}
		
		.css-color-verde{ color: #8dc716;}
		.css-color-naranja{ color: #ff9c01;}
		.css-color-azul{ color:#0096ff;}
		.css-color-rojo{ color:#AB0002;}
	
		
		.table-responsive table thead {
    background-color: #1B1B1B;
}
		
		.table-responsive table thead tr th {
    text-align: left;
    color: #FFF;
    font-family: "Rubik", sans-serif;
}
                                        
	</style>

<body>
	<?php include("includes/analytics.php"); ?>
	<?php include("includes/cookies.php"); ?>

   <div class="home_three_body_wrapper">
		
	<?php include("includes/header.php"); ?>

	 <!--breadcrumbs-->
    <div class="breadcrumbs_area">
        <div class="container">   
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <h3>Área Privada</h3>
                        <ul>
                            <li><a href="index.php">home</a></li>
                            <li>></li>
                            <li>Área privada</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>         
    </div>
	<!--end.breadcrumbs-->
    
   <!--CONTENIDO-->
	   
	   
	   
	 <!--CREAR CUENTA-->

       <div class="container">
		   
		   
 <ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="perfil-tab" data-toggle="tab" href="#perfil" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-user css-fonsize16"></i> Mi perfil <i class="fa fa-angle-down"></i></a>
  </li>
<li class="nav-item">
    <a class="nav-link" id="factura-tab" data-toggle="tab" href="#factura" role="tab" aria-controls="profile" aria-selected="false"><i class="fa fa-folder-open css-fonsize16"></i> Datos facturación <i class="fa fa-angle-down"></i></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pedidos-tab" data-toggle="tab" href="#pedidos" role="tab" aria-controls="profile" aria-selected="false"><i class="fa fa-folder-open css-fonsize16"></i> Mis pedidos <i class="fa fa-angle-down"></i></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="direcciones-tab" data-toggle="tab" href="#direcciones" role="tab" aria-controls="contact" aria-selected="false"><i class="fa fa-truck css-fonsize16"></i> Datos de envío <i class="fa fa-angle-down"></i></a>
  </li>
	 <li class="nav-item">
    <a class="nav-link" id="cupones-tab" data-toggle="tab" href="#cupones" role="tab" aria-controls="contact" aria-selected="false"><i class="fa fa-tag css-fonsize16"></i> Vales descuento <i class="fa fa-angle-down"></i></a>
  </li>
</ul>
<div class="tab-content bgGray" id="myTabContent">
	
	
	<!--TAB DE PERFIL-->
  <div class="tab-pane fade show active" id="perfil" role="tabpanel" aria-labelledby="perfil-tab">
	  <div class="col-xs-12 col-md-8 offset-md-2 checkout_form">
		<form action="#">
           <div class="row">
												<div class="col-12 mb-20">
                                    <label>Nombre</label>
                                    <input type="text">     
                                </div>
								
												<div class="col-lg-6 mb-20">
                                    <label>Móvil<span>*</span></label>
                                    <input type="text"> 

                                </div> 
								
                                 <div class="col-lg-6 mb-20">
                                    <label>Email <span>*</span></label>
                                      <input type="text">
												 </div>
												
										
			   
			   <!--fech anacimienot-->
							
													<div class="col-12">
                                    <p><hr></p>
                                	</div>
									
													<div class="col-lg-12 mb-20">
														 <label>Fecha de nacimiento:<span>*</span></label>
													</div>
							
												<div class="col-lg-2 mb-20">
                                    <label for="country">Dia <span>*</span></label>
                                    <select class="niceselect_option" name="cuntry" id="country"> 
														   <option value="2">1</option>     
                                        <option value="2">2</option>      
                                        <option value="3">3</option> 
                                        <option value="4">3</option>    
                                        <option value="5">5a</option>    
                                    </select>
                                </div>
							
												<div class="col-lg-5 mb-20">
                                    <label for="country">Mes <span>*</span></label>
                                    <select class="niceselect_option" name="cuntry" id="country"> 
														   <option value="2">Enero</option>     
                                        <option value="2">Febrero</option>      
                                        <option value="3">Marzo</option> 
                                        <option value="4">Abril</option>    
    
                                    </select>
                                </div>
							
												<div class="col-lg-5 mb-20">
                                    <label for="country">Año <span>*</span></label>
                                    <select class="niceselect_option" name="cuntry" id="country"> 
														   <option value="2">1976</option>     
                                        <option value="2">1974</option>        
                                    </select>
                                </div>
										
												<!--end fecha nacimiento-->
			
			
			
												<div class="col-lg-6 mb-20">
                                    <label>Contraseña<span>*</span></label>
                                    <input type="password"> 
                                </div> 
								
                                 <div class="col-lg-6 mb-20">
                                    <label>Confirmar contraseña<span>*</span></label>
                                      <input type="password"> 
                                </div>
			
												<div class="col-12">
                                    <div class="order-notes">
                                         <label for="order_note">Información adicional </label>
                                        <textarea id="order_note" placeholder=" " rows="4" ></textarea>
                                    </div>    
                                </div>  
			
			
												<div class="col-8 offset-2 css-marginT20">
		  										 <div class="col-12">
                                    <input id="account" type="checkbox" data-target="createp_account" />
                                    <label for="account">Suscripción al newsletter</label>
                                </div>
                            
								
												<div class="col-12">
													<div class="cuenta_button">
                                    <button type="submit">guardar datos ></button> 
                                	</div>
												</div>
											</div>
			   
			
								</div>
		  				</form>
				</div>
	  
  </div>
	<!--END.TAB DE PERFIL-->
	
	<!--TAB DE DATOS FACTURA-->
	<div class="tab-pane fade" id="factura" role="tabpanel" aria-labelledby="factura-tab">
		
		<div class="col-xs-12 col-md-8 offset-md-2 checkout_form">
		<form action="#">

                            <div class="row">
								
												<div class="col-6">
                                    <input id="account" type="checkbox" data-target="createp_account">
                                    <label for="account">Particular</label>
                                </div>
												
												<div class="col-6">
                                    <input id="account" type="checkbox" data-target="createp_account">
                                    <label for="account">Empresa</label>
                                </div>
								
												<div class="col-12">
                                    <p></p><hr><p></p>
                                </div>

                                <div class="col-lg-6 mb-20">
                                    <label>NIF/CIF/NIE <span>*</span></label>
                                    <input type="text">    
                                </div>
								
                                <div class="col-lg-6 mb-20">
                                    <label>Nombre  <span>*</span></label>
                                    <input type="text"> 
                                </div>
								
								          <div class="col-lg-6 mb-20">
                                    <label>Apellido 1 <span>*</span></label>
                                    <input type="text">    
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <label>Apellido 2 <span>*</span></label>
                                    <input type="text"> 
                                </div>
								
                                <div class="col-12 mb-20">
                                    <label>Empresa</label>
                                    <input type="text">     
                                </div>
								
										
							
												
												<div class="col-12">
                                    <p></p><hr><p></p>
                                </div>
								
                                <div class="col-6 mb-20">
                                    <label for="country">Pais <span>*</span></label>
                                    <select class="niceselect_option" name="cuntry" id="country" style="display: none;"> 
														   <option value="2">España</option>     
                                        <option value="2">bangladesh</option>      
                                        <option value="3">Algeria</option> 
                                        <option value="4">Afghanistan</option>    
                                        <option value="5">Ghana</option>    
                                        <option value="6">Albania</option>    
                                        <option value="7">Bahrain</option>    
                                        <option value="8">Colombia</option>    
                                        <option value="9">Dominican Republic</option>   

                                    </select><div class="nice-select niceselect_option" tabindex="0"><span class="current">España</span><ul class="list"><li data-value="2" class="option selected">España</li><li data-value="2" class="option">bangladesh</li><li data-value="3" class="option">Algeria</li><li data-value="4" class="option">Afghanistan</li><li data-value="5" class="option">Ghana</li><li data-value="6" class="option">Albania</li><li data-value="7" class="option">Bahrain</li><li data-value="8" class="option">Colombia</li><li data-value="9" class="option">Dominican Republic</li></ul></div>
                                </div>
								
											 <div class="col-6 mb-20">
                                    <label for="country">Provincia <span>*</span></label>
                                    <select class="niceselect_option" name="cuntry" id="country" style="display: none;"> 
														   <option value="2">Madrid</option>     
                                        <option value="2">Barcelona</option>      
                                        <option value="3">Vakencia</option> 
                                        <option value="4">Huelva</option>    
                                        <option value="5">Sevilla</option>    
                                    </select><div class="nice-select niceselect_option" tabindex="0"><span class="current">Madrid</span><ul class="list"><li data-value="2" class="option selected">Madrid</li><li data-value="2" class="option">Barcelona</li><li data-value="3" class="option">Vakencia</li><li data-value="4" class="option">Huelva</li><li data-value="5" class="option">Sevilla</li></ul></div>
                                </div>

                                <div class="col-12 mb-20">
                                    <label>Dirección  <span>*</span></label>
                                    <input placeholder="Calle, código postal, nombre de empresa, c / o" type="text">     
                                </div>
								
                                <div class="col-12 mb-20">
                                    <input placeholder="Apartamento, suite, unidad, edificio, piso, etc" type="text">     
                                </div>
								
                                <div class="col-6 mb-20">
                                    <label>Ciudad <span>*</span></label>
                                    <input type="text">    
                                </div> 
                                 <div class="col-6 mb-20">
                                    <label>Cóigo postal <span>*</span></label>
                                    <input type="text">    
                                </div> 
                               
								
                            </div>
                        </form>    
	  
		</div>
	  
	  	
  </div>
	<!--END.TAB DE DATOS FACTURA-->
	
	<!--TAB DE PEDIDOS-->
  <div class="tab-pane fade" id="pedidos" role="tabpanel" aria-labelledby="pedidos-tab">
	  
	  	  							<div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="tableUser">Nº Pedido</th>
                                                <th class="tableUser">Fecha</th>
                                                <th class="tableUser">Estado</th>
                                                <th class="tableUser">Importe total</th>
                                                <th class="tableUser">Factura</th>	 	 	 	
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>0123</td>
                                                <td>28-03-2019</td>
                                                <td><span class="success"><i class="fa fa-check-square css-fonsize16 css-color-verde"></i> Entregado</span></td>
                                                <td>148,50 € </td>
                                                <td><a href="#" class="view"><i class="fa fa-cloud-download css-fonsize16 css-color-azul"></i> Factura</a></td>
                                            </tr>
                                            <tr>
                                                <td>0123</td>
                                                <td>28-03-201</td>
                                                <td><i class="fa fa-hourglass-start css-fonsize16 css-color-naranja"></i> Preparando pedido</td>
                                                <td>234,60 € </td>
                                                <td><a href="#" class="view"><i class="fa fa-cloud-download css-fonsize16 css-color-azul"></i> Factura</a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                             </div>
	  
	  	
  </div>
	<!--END. TAB DE PEDIDOS-->
	
	
	<!--TAB DE DIRECCIONES -->
	<div class="tab-pane fade" id="direcciones" role="tabpanel" aria-labelledby="direcciones-tab">
		
		<div class="col-xs-12 col-md-8 offset-md-2 checkout_form">
			<form action="#">
           <div class="row">
			   
			   <div class="row">
											
												<div class="col-12 mb-20">
                                    <label>Nombre dirección</label>
                                    <input type="text" placeholder="indica un nombre para recorda esta dirección." >     
                                </div>
								
                                <div class="col-6 mb-20">
                                    <label for="country">Pais <span>*</span></label>
                                    <select class="niceselect_option" name="cuntry" id="country"> 
														   <option value="2">España</option>     
                                        <option value="2">bangladesh</option>      
                                        <option value="3">Algeria</option> 
                                        <option value="4">Afghanistan</option>    
                                        <option value="5">Ghana</option>    
                                        <option value="6">Albania</option>    
                                        <option value="7">Bahrain</option>    
                                        <option value="8">Colombia</option>    
                                        <option value="9">Dominican Republic</option>   

                                    </select>
                                </div>
								
											 <div class="col-6 mb-20">
                                    <label for="country">Provincia <span>*</span></label>
                                    <select class="niceselect_option" name="cuntry" id="country"> 
														   <option value="2">Madrid</option>     
                                        <option value="2">Barcelona</option>      
                                        <option value="3">Vakencia</option> 
                                        <option value="4">Huelva</option>    
                                        <option value="5">Sevilla</option>    
                                    </select>
                                </div>

                                <div class="col-12 mb-20">
                                    <label>Dirección  <span>*</span></label>
                                    <input placeholder="Calle, código postal, nombre de empresa, c / o" type="text">     
                                </div>
                                <div class="col-12 mb-20">
                                    <input placeholder="Apartamento, suite, unidad, edificio, piso, etc" type="text">     
                                </div>
                                <div class="col-6 mb-20">
                                    <label>Ciudad <span>*</span></label>
                                    <input  type="text">    
                                </div> 
                                 <div class="col-6 mb-20">
                                    <label>Cóigo postal <span>*</span></label>
                                    <input type="text">    
                                </div> 
                                
                               
                                <div class="col-12">
                                    <div class="order-notes">
                                         <label for="order_note">Información adicional </label>
                                        <textarea id="order_note" placeholder=" " rows="4" ></textarea>
                                    </div>    
                                </div>  
								
								
                            </div>
			   
				</div>
			</form>
		</div>
  </div>
	<!--END.TAB DE DIRECCIONES-->
	
	<!--TAB DE DATOS CUPONES-->
  <div class="tab-pane fade" id="cupones" role="tabpanel" aria-labelledby="cupones-tab">
	  <div class="table-responsive">
	  					<table class="table">
                                        <thead>
                                            <tr>
                                                <th class="tableUser">Nº Vale</th>
                                                <th class="tableUser">Fecha incio</th>
                                                <th class="tableUser">Fecha de fin</th>
                                                <th class="tableUser">Descuento</th>
                                                <th class="tableUser">Estado</th>	 	 	 	
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>BLAKCFRIDAY2019</td>
                                                <td>28-03-2019</td>
																		<td>15-04-2019</td>
                                                <td><span class="success"><strong>10%</strong></span></td>
                                                <td><i class="fa fa-thumbs-up css-fonsize16 css-color-verde"></i> sin utlizar</td>
                                            </tr>
																
																<tr>
                                                <td>DIADELPADRE2019</td>
                                                <td>12-03-2019</td>
																		<td>120-03-2019</td>
                                                <td><span class="success"><strong>10%</strong></span></td>
                                                <td><i class="fa fa-times-circle css-fonsize16 css-color-naranja"></i> caducado</td>
                                            </tr>
											
																<tr>
                                                <td>REYES2019</td>
                                                <td>201-12-2018</td>
																		<td>04-01-2019</td>
                                                <td><span class="success"><strong>10%</strong></span></td>
                                                <td><i class="fa fa-thumbs-down css-fonsize16 css-color-rojo"></i> ya ha sido utlizado</td>
                                            </tr>
                                            
                                        </tbody>
      					</table>
	  </div>
	  <!--END.TABLE RESPONSIVE-->
	  
 	</div>
	<!--END.TAB DE CUPONES-->
	  
  </div>
	
</div>
           
            
 </div>       
	<!--END.CONTENIDO-->

	
	<?php include("includes/footer.php"); ?>

	</div>
	<!--!FINAL home_three_body_wrapper ---->
	<?php include("includes/vistapreviaModal.php"); ?>
	<?php include("includes/newsletterModal.php"); ?>

<!-- JS
============================================ -->
<?php include("includes/js.php"); ?>

</body>

</html>